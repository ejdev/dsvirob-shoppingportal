<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
	
	'sendgrid'=>[
        'url'=>'https://api.sendgrid.com/api/mail.send.json',
        'api_user'=>'onlinesensor',
        'api_key'=>'ejugiter@123'
    ],	
	'google'=>[
        'api_key'=>'AIzaSyCIMNDfl2tZYg-9Y3GMhN4coNEqYtw_QTs',
        'fcm_url'=>'https://fcm.googleapis.com/fcm/send'
    ],	
	'sms'=>[
        'user'=>'5dollar',
        'key'=>'fde6f26353XX',
        'senderid'=>'OSENSR',
        'url'=>'sms.onlinesensor.com/submitsms.jsp?',
    ],	
	'vpi'=> [
		'url' =>'http://localhost/dropshipping/vpi/v1/',
	],
	'api'=> [
		'url' =>'http://localhost/virob_shopping/api/v1/',
	],
	'localApi'=> [
		'url' =>'http://localhost/virob_shopping/api/v1/',
	],
];
