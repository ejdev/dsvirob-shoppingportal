@extends('layouts.product_layout')
@section('pagetitle')
@stop
@section('content')
<input type="hidden" id="path" value="{{$path or ''}}">
<div class="row">
    <!-- Left colunm -->
    <div class="column col-xs-12 col-sm-3" id="left_column">
        <!-- block category -->
        <div class="block left-module">
            <p class="title_block">Product types</p>
            <div class="block_content">
                <!-- layered -->
                <div class="layered layered-category">
                    <div class="layered-content">
                        <ul class="tree-menu categories-list">
                        </ul>
                    </div>
                </div>
                <!-- ./layered -->
            </div>
        </div>
        <!-- ./block category  -->
        <!-- block filter -->
        <div class="block left-module">
            <p class="title_block">Filter selection</p>
            <form id="filter-form">
                <div class="block_content">
                    <div class="layered layered-filter-price" id="filters">

                    </div>
                </div>
            </form>
        </div>
        <!-- ./block filter  -->

        <!-- left silide -->
        <div class="col-left-slide left-module">
            <ul class="owl-carousel owl-style2 owl-theme owl-loaded" data-loop="true" data-nav="false" data-margin="30" data-autoplaytimeout="1000" data-autoplayhoverpause="true" data-items="1" data-autoplay="true">
                <div class="owl-stage-outer">
					<div class="owl-stage" style="transform: translate3d(-1200px, 0px, 0px); transition: 0s; width: 2100px;">
						<div class="owl-item cloned" style="width: 270px; margin-right: 30px;">
							<li><a href="#"><img src="{{Config('ThemeAsset')}}/data/slide-left2.jpg" alt="slide-left"></a></li>
						</div>
						<div class="owl-item cloned" style="width: 270px; margin-right: 30px;">
							<li><a href="#"><img src="{{Config('ThemeAsset')}}/data/slide-left3.png" alt="slide-left"></a></li>
						</div>
						<div class="owl-item" style="width: 270px; margin-right: 30px;">
							<li><a href="#"><img src="{{Config('ThemeAsset')}}/data/slide-left.jpg" alt="slide-left"></a></li>
						</div>
						<div class="owl-item animated owl-animated-out fadeOut" style="width: 270px; margin-right: 30px; left: 300px;">
							<li><a href="#"><img src="{{Config('ThemeAsset')}}/data/slide-left2.jpg" alt="slide-left"></a></li>
						</div>
						<div class="owl-item animated owl-animated-in fadeIn active" style="width: 270px; margin-right: 30px;">
							<li><a href="#"><img src="{{Config('ThemeAsset')}}/data/slide-left3.png" alt="slide-left"></a></li>
						</div>
						<div class="owl-item cloned" style="width: 270px; margin-right: 30px;">
							<li><a href="#"><img src="{{Config('ThemeAsset')}}/data/slide-left.jpg" alt="slide-left"></a></li>
						</div>
						<div class="owl-item cloned" style="width: 270px; margin-right: 30px;">
							<li><a href="#"><img src="{{Config('ThemeAsset')}}/data/slide-left2.jpg" alt="slide-left"></a></li>
						</div>
					</div>
				</div>
				<div class="owl-controls">
					<div class="owl-nav">
						<div class="owl-prev" style="display: none;">
							<i class="fa fa-angle-left"></i>
						</div>
						<div class="owl-next" style="display: none;">
							<i class="fa fa-angle-right"></i>
						</div>
					</div>
					<div class="owl-dots" style="">
						<div class="owl-dot"><span></span></div>
						<div class="owl-dot"><span></span></div>
						<div class="owl-dot active"><span></span>
					</div>
                    </div>
                </div>
            </ul>
        </div>
        <!--./left silde-->
        <!-- SPECIAL -->
        <div class="block left-module">
            <p class="title_block">SPECIAL PRODUCTS</p>
            <div class="block_content">
                <ul class="products-block">
                    <li>
                        <div class="products-block-left">
                            <a href="#">
                                <img src="{{Config('ThemeAsset')}}/data/product-100x122.jpg" alt="SPECIAL PRODUCTS">
                            </a>
                        </div>
                        <div class="products-block-right">
                            <p class="product-name">
                                <a href="#">Woman Within Plus Size Flared</a>
                            </p>
                            <p class="product-price">$38,95</p>
                            <p class="product-star">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </p>
                        </div>
                    </li>
                </ul>
                <div class="products-block">
                    <div class="products-block-bottom">
                        <a class="link-all" href="#">All Products</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- ./SPECIAL -->
        <!-- TAGS -->
        <div class="block left-module">
            <p class="title_block">TAGS</p>
            <div class="block_content">
                <div class="tags" id="browse-tags">
                </div>
            </div>
        </div>
        <!-- ./TAGS -->
        <!-- Testimonials -->
        <div class="block left-module">
            <p class="title_block">Testimonials</p>
            <div class="block_content">
                <ul class="testimonials owl-carousel owl-theme owl-loaded" data-loop="true" data-nav="false" data-margin="30" data-autoplaytimeout="1000" data-autoplay="true" data-autoplayhoverpause="true" data-items="1">
                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(-1032px, 0px, 0px); transition: 0.3s; width: 1806px;">
                            <div class="owl-item cloned" style="width: 228px; margin-right: 30px;"><li>
                                    <div class="client-mane">Roverto &amp; Maria</div>
                                    <div class="client-avarta">
                                        <img src="public/frontend/theme/kuteshop/data/testimonial.jpg" alt="client-avarta">
                                    </div>
                                    <div class="testimonial">
                                        "Your product needs to improve more. To suit the needs and update your image up"
                                    </div>
                                </li>

                            </div>
                            <div class="owl-item cloned" style="width: 228px; margin-right: 30px;"><li>
                                    <div class="client-mane">Roverto &amp; Maria</div>
                                    <div class="client-avarta">
                                        <img src="public/frontend/theme/kuteshop/data/testimonial.jpg" alt="client-avarta">
                                    </div>
                                    <div class="testimonial">
                                        "Your product needs to improve more. To suit the needs and update your image up"
                                    </div>
                                </li></div><div class="owl-item" style="width: 228px; margin-right: 30px;"><li>
                                    <div class="client-mane">Roverto &amp; Maria</div>
                                    <div class="client-avarta">
                                        <img src="public/frontend/theme/kuteshop/data/testimonial.jpg" alt="client-avarta">
                                    </div>
                                    <div class="testimonial">
                                        "Your product needs to improve more. To suit the needs and update your image up"
                                    </div>
                                </li></div><div class="owl-item" style="width: 228px; margin-right: 30px;"><li>
                                    <div class="client-mane">Roverto &amp; Maria</div>
                                    <div class="client-avarta">
                                        <img src="public/frontend/theme/kuteshop/data/testimonial.jpg" alt="client-avarta">
                                    </div>
                                    <div class="testimonial">
                                        "Your product needs to improve more. To suit the needs and update your image up"
                                    </div>
                                </li></div><div class="owl-item active" style="width: 228px; margin-right: 30px;"><li>
                                    <div class="client-mane">Roverto &amp; Maria</div>
                                    <div class="client-avarta">
                                        <img src="public/frontend/theme/kuteshop/data/testimonial.jpg" alt="client-avarta">
                                    </div>
                                    <div class="testimonial">
                                        "Your product needs to improve more. To suit the needs and update your image up"
                                    </div>
                                </li></div><div class="owl-item cloned" style="width: 228px; margin-right: 30px;"><li>
                                    <div class="client-mane">Roverto &amp; Maria</div>
                                    <div class="client-avarta">
                                        <img src="public/frontend/theme/kuteshop/data/testimonial.jpg" alt="client-avarta">
                                    </div>
                                    <div class="testimonial">
                                        "Your product needs to improve more. To suit the needs and update your image up"
                                    </div>
                                </li></div><div class="owl-item cloned" style="width: 228px; margin-right: 30px;"><li>
                                    <div class="client-mane">Roverto &amp; Maria</div>
                                    <div class="client-avarta">
                                        <img src="public/frontend/theme/kuteshop/data/testimonial.jpg" alt="client-avarta">
                                    </div>
                                    <div class="testimonial">
                                        "Your product needs to improve more. To suit the needs and update your image up"
                                    </div>
                                </li></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style="display: none;"><i class="fa fa-angle-left"></i></div><div class="owl-next" style="display: none;"><i class="fa fa-angle-right"></i></div></div><div class="owl-dots" style=""><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot active"><span></span></div></div></div></ul>
            </div>
        </div>
        <!-- ./Testimonials -->
    </div>
    <!-- ./left colunm -->
    <!-- Center colunm-->
    <div class="center_column col-xs-12 col-sm-9" id="center_column">
        
        <div id="view-product-list" class="view-product-list">
            <h2 class="page-heading">
                <span class="page-heading-title"></span>
            </h2>
            <ul class="display-product-option">
                <li class="view-as-grid selected">
                    <span>grid</span>
                </li>
                <li class="view-as-list">
                    <span>list</span>
                </li>
            </ul>
            <!-- PRODUCT LIST -->
            <ul class="row product-list grid">
            </ul>
            <!-- ./PRODUCT LIST -->
        </div>
        <!-- ./view-product-list-->
        <div class="sortPagiBar">
            <div class="bottom-pagination">
                <nav>
                    <ul class="pagination">
                    </ul>
                </nav>
            </div>
            <div class="sort-product">
                <select id="sort_by">
                    <option value="POPULARITY">POPULARITY</option>
                    <option value="PRICE_LOW_TO_HIGH">PRICE LOW TO HIGH</option>
                    <option value="PRICE_HIGH_TO_LOW">PRICE HIGH TO LOW</option>
                    <option value="NEWEST">NEWEST</option>
                </select>
                <div class="sort-product-icon">
                    <i class="fa fa-sort-alpha-asc"></i>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')

<script src="{{ URL::to(Config('ThemeProviders').'/browse_products.js') }}"></script>
<script src="{{ URL::to(Config('ThemeProviders').'/cart.js') }}"></script>
@stop
