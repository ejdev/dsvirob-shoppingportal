<!doctype html>
<html lang="en">
    <base href="{{URL::to('/')}}/">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>@yield('pagetitle') | {{Config('settings.domain')}}</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{$sitesettings->fav_icon}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/lib/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/lib/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/lib/select2/css/select2.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/lib/jquery.bxslider/jquery.bxslider.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/lib/owl.carousel/owl.carousel.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/lib/jquery-ui/jquery-ui.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/css/animate.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/css/reset.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/css/responsive.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/plugins/notifyit/notifIt.css') }}">
        <!--style>
            /* Preloader */
            #preloader {
                position: fixed;
                top:0;
                left:0;
                right:0;
                bottom:0;
                background-color:#fff; /* change if the mask should have another color then white */
                z-index:1099; /* makes sure it stays on top */
                opacity:0.5;
            }

            #preimg {
                width:200px;
                height:200px;
                position:absolute;
                left:50%; /* centers the loading animation horizontally one the screen */
                top:50%; /* centers the loading animation vertically one the screen */
                background-image:url(./themes/kuteshop/assets/loader.gif); /* path to your loading animation */
                background-repeat:no-repeat;
                background-position:center;
                margin:-100px 0 0 -100px; /* is width and height divided by two */
            }
        </style-->
        @yield('stylesheets')
    </head>
    <body class="@yield('page-type')">
        <div id="preloader">
            <div id="preimg">&nbsp;</div>
        </div>
        <div id="header" class="header">
            @yield('page-header')
        </div>
        @yield('page-content')
        <footer id="footer">
            <div class="container">
                @yield('page-footer')
                <div id="footer-menu-box">
                    <p class="text-center">{{$sitesettings->footer_content}}. All Rights Reserved.</p>
                </div>
            </div>
        </footer>
        <a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>

        <script src="{{ URL::to(Config('ThemeAsset').'/lib/jquery/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/lib/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/lib/select2/js/select2.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/lib/validation/jquery.validate.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/lib/validation/additional-methods.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/lib/validation-init.js') }}"></script>

        <script src="{{ URL::to(Config('ThemeAsset').'/lib/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/lib/owl.carousel/owl.carousel.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/lib/jquery.countdown/jquery.countdown.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/lib/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/js/jquery.actual.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/js/theme-script.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeAsset').'/plugins/notifyit/notifIt.min.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeProviders').'/app.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeProviders').'/subscribe.js') }}"></script>
        <script src="{{ URL::to(Config('ThemeProviders').'/Jquery-loadSelect.js') }}"></script>
        @yield('scripts')
        <script type="text/javascript">
$(window).on('load', function () { // makes sure the whole site is loaded
    $('#preimg').fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(350).css({'overflow': 'visible'});
})
        </script>

    </body>
</html>
