@extends('layouts.baselayout')
@section('page-type')
category-page
@stop
@section('page-header')
@include('common.header_without_menu')
@stop
@section('page-content')
<div class="columns-container">
    <div class="container" id="columns">
        <div class="breadcrumb clearfix">
            <a class="home" href="{{URL::to('/')}}" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            @yield('breadcrumb')
            <span class="navigation_page"> @yield('pagetitle')</span>
        </div>
        <h2 class="page-heading">
            <span class="page-heading-title2">@yield('pagetitle')</span>
            <span class="pull-right">@yield('title-options')</span>
        </h2>
        @yield('content')
    </div>
</div>
@stop
