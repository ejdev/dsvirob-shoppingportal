@extends('layouts.baselayout')
@section('page-header')
	@include('common.header')
@stop
@section('page-content')
@yield('page')
@stop
@section('page-footer')
@include('common.footer')
@stop
