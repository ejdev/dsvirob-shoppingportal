@extends('layouts.layout')
@section('page-type')
category-page
@stop
@section('page')
<!-- featured category fashion -->
@include('common.home_slider')
<div class="container">
    <div class="service ">
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" src="{{Config('ThemeAsset')}}/data/s1.png">
            </div>
            <div class="info">
                <a href="#"><h3>Free Shipping</h3></a>
                <span>On order over $200</span>
                
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" src="{{Config('ThemeAsset')}}/data/s2.png">
            </div>
            <div class="info">
                <a href="#"><h3>30-day return</h3></a>
                <span>Moneyback guarantee</span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" src="{{Config('ThemeAsset')}}/data/s3.png">
            </div>

            <div class="info">
                <a href="#"><h3>24/7 support</h3></a>
                <span>Online consultations</span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 service-item">
            <div class="icon">
                <img alt="services" src="{{Config('ThemeAsset')}}/data/s4.png">
            </div>
            <div class="info">
                <a href="#"><h3>SAFE SHOPPING</h3></a>
                <span>Safe Shopping Guarantee</span>
            </div>
        </div>
    </div>
</div>
<div class="content-page">
    <div class="container" id="slider-container">
    </div>
</div>
<div class="container">
    <div class="">
    </div>
</div>
@stop
@section('scripts')
<script src="{{ URL::to(Config('ThemeProviders').'/sliders.js') }}"></script>
<script src="{{ URL::to(Config('ThemeProviders').'/cart.js') }}"></script>
<!--script src="{{ URL::to(Config('ThemeProviders').'/browse_products.js') }}"></script-->
@stop
