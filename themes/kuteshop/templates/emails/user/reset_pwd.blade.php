@extends('emails.maillayout')
@section('content')

      
                <tr>
                  <td align="center" style="font-size:15px;font-weight:bold;color:#000">Dear  <?php echo $name;?>,<br /><br />
                      </td>
                 </tr>
                 <tr>
                  <td align="center" style="font-size:15px;font-weight:bold;color:#000">User Name - <?php echo $name;?><br /><br />
                     </td>
                 </tr>
                  <tr>
                    <td width="40">&nbsp;</td>
                  </tr>
                   <tr>
                     <td bgcolor="#d7d7d7" height="1"></td>
                    </tr>
                     <tr>
                      <td width="40">&nbsp;</td>
                     </tr>
                     <tr>
                        <td align="center" style="font-size:30px;color:#000">Password Reset!</td>
                      </tr>
                      <tr>
                        <td width="40">&nbsp;</td>
                      </tr>
                       <tr>
                          <td bgcolor="#d7d7d7" height="1"></td>
                       </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                       </tr>
           <tr> <td style="font-size:14px;color:#929292"><p>Hey, <?php echo $name;?> ! </p>
                <p>
                   Did you forget your password? That's okay, you can reset your password here.
                </p>
                 <p>
                   In order to reset your password please click on the link below:
                 </p>
                 <p>
                   <a href="<?php echo $resetlink;?>" target="_blank"><?php echo $resetlink;?> </a>
                 </p>
                 <p>
                   If you did not request a password reset, please contact Customer Support.
                 </p>
                  <p>
                    If you have any questions or need assistance, please e-mail us at Dropnwash
                  </p>
                </td>
              </tr>
            
         @stop