@extends('emails.maillayout')
@section('content')
 <tr>
                            <td align="center" style="font-size:15px;font-weight:bold;color:#000">Dear  <?php echo $full_name;?>,</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:15px;font-weight:bold;color:#000">User Name - <?php echo $uname;?></td>
                        </tr>
                           <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td bgcolor="#d7d7d7" height="1"></td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:30px;color:#000">Welcome to <?php echo $pagesettings->site_name;?><br></td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:16px;color:#666666">Congratulations on  becoming a part of the <?php echo $pagesettings->site_name;?> Community
							</td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td bgcolor="#d7d7d7" height="1"></td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:30px;color:#000">Password has been updated successfully.</td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                       
                        <tr>
                            <td bgcolor="#d7d7d7" height="1"></td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="font-size:14px;color:#929292">
                            <p>Hey, <strong style="color:#666"><?php echo $full_name;?>!</strong> </p>
                            <p>As requested, your Password has been changed.</p>
                            <p style="color:#666">New Password: <strong><?php echo $pwd;?></strong>.  </p>
                          
                             <p style="color:#666">Your username is - <strong> <?php echo $uname;?></strong> </p>
                            <p><strong>Important Security Tips:</strong></p>
                            <ul style="margin: 0px; padding: -0px 0px 0px 16px; font-size: 14px; line-height:25px">
                            <li> Always keep your account details safe.</li>
                            <li>Never disclose your login details to anyone.</li>
                            <li>Change your password regularly</li>
                            <li>Should you suspect someone is using your account illegally, please notify us immediately.</li>
                            </ul>
                           
                            
                           </td>
                        </tr>



@stop



