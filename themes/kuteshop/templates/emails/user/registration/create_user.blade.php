@extends('emails.maillayout')
@section('content')
 <tr>
                        <td align="center" style="font-size:15px;font-weight:bold;color:#000">Dear <?php echo $username;?>,</td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center" style="font-size:15px;font-weight:bold;color:#000">User Name - <?php echo $username;?>,</td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td bgcolor="#d7d7d7" height="1"></td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:30px;color:#000">Welcome to <?php echo $pagesettings->site_name;?><br></td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:16px;color:#666666">Congratulations on  becoming a part of the <?php echo $pagesettings->site_name;?> 
							</td>
                        </tr>
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                            <td bgcolor="#d7d7d7" height="1"></td>
                        </tr>
                        
                        
                         
                        
                        <tr>
                            <td width="40">&nbsp;</td>
                        </tr>
                        <tr>
                         <p>
                   In order to verify your account please click on the link below:
                 </p>
                 <p>
                   <a href="<?php echo $act_link;?>" target="_blank"><?php echo $act_link;?> </a>
                 </p>
                            <td align="center"><b style="color:#066fc5">Your Login credentials are :</b>
                            <br />
                            @if(isset($act_link) && !empty($act_link))
                            <p><b>Account Login/Email:</b> <?php echo $email;?></p>
                            <p><b>Account Id:</b><?php echo $employee_id;?></p>
							<p><b>Password:</b><?php echo $pwd;?></p>
                            </td>
                        </tr>

@stop