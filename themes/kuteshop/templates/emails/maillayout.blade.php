<style>
    .panel {
        background: #F0F0F0;
        border: 1px solid #d9d9d9;
        padding: 10px !important;
        width: 700px;
    }
</style>
<div style="background-color:#F0F0F0;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;width: 80%;margin: 0 auto;" width="80%">
        <thead>
            <tr><td style="padding: 1em 2em;"><a href="http://localhost/virob_shopping" style="color: #3291db;text-decoration: none;outline: none;"><img  src="http://localhost/virob_shopping/themes/kuteshop/assets/imgs/logo.png" style="border: 0px; display: block; width: 200px; height: 60px;"></a></td></tr>
        </thead>
        <tbody>
            <tr>
                <td align="center" class="module-td1" style="padding: 30px 0 0;">
                    @yield('content')
                </td>
            </tr>
            <tr>
                <td height="30"></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td align="center" class="colorbg-dark" style="background-image: none; background-color: #F0F0F0;"></td>
            </tr>
            <tr>
                <td class="small-img line2" height="1" style="font-size: 0px;line-height: 0px;border-collapse: collapse;background-color: #F0F0F0;"><img height="1" src="<?php echo URL::asset('http://localhost/virob_shopping/themes/kuteshop/assets/imgs/mail_img/spacer.gif');?>" style="border: 0;display: block;-ms-interpolation-mode: bicubic;" width="1" /></td>
            </tr>
            <tr>
                <td>
                    <table border="0" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td width="50%" align="center" style="background-image: none; background-color: #F0F0F0;">
                                <img alt="icon" height="15" src="<?php echo URL::asset('http://localhost/virob_shopping/themes/kuteshop/assets/imgs/mail_img/icon-15-3.png');?>" style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 15px;height: auto;max-width: 15px;max-height: 15px;" width="15" />
                            <td width="50%">
                                <img alt="icon" height="15" src="<?php echo URL::asset('http://localhost/virob_shopping/themes/kuteshop/assets/imgs/mail_img/icon-15-4.png');?>" style="border: 0;display: block;-ms-interpolation-mode: bicubic;width: 15px;height: auto;max-width: 15px;max-height: 15px;" width="15" />
                                <span class="h5" style="font-family: Montserrat,Tahoma;font-weight: 400;color: #262424;font-size: 15px;line-height: 17px;"><a class="white" href="" style="color: #fff;text-decoration: none;outline: none;"></a></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr align="center" class="colorbg-gray" style="background-image: none; background-color: #2c2c2c;">
                <td colspan="2" align="center" style="font-family: Raleway;font-size: 11px;line-height: 11px;color: #f2f2f2; font-weight: 400;padding: 22px 0 20px;">&nbsp;All rights reserved</td>
            </tr>
        </tfoot>
    </table>
</div>
