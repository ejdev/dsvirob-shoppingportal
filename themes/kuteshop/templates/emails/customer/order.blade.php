@extends('emails.maillayout')
@section('content')
<style>
    table{
        border-collapse: collapse;
        border: none;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        border-radius:3px;
        margin-left: 30px;

    }
    table tr{
        padding:2px 5px;
        border-radius:3px;
    }
    table tr th,table tr td{
        padding:5px;
    }
    table tr td img{
        border-radius:3px;
    }
    table.bordered tr{
        border:1px #cccccc solid;padding:5px;
    }


</style>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;" width="600"><!--start title-->
    <tbody>
        <tr>
            <td align="" class="h3 b title-td" mc:edit="title" style="font-family: 'Playfair Display'; font-weight: 400; color: #262424; font-size: 24px; line-height: 35px; font-style: italic;"><singleline label="title" style="margin-right:400px">   <strong>Hi {{$full_name}} </strong>,</singleline></td>
</tr>
<tr>
    <td align="left" class="content b" mc:edit="content1" style="font-family: 'Playfair Display', Arial; font-weight: 400; font-size: 15px; line-height: 21px; color: #252525; -webkit-font-smoothing: antialiased; font-style: italic;">
<multiline label="content1">
    <font style="font-family: 'Playfair Display', Arial; font-weight: 400; font-size: 15px; line-height: 21px; color: #252525; -webkit-font-smoothing: antialiased; margin: 0px !important;">
    <p> Greetings from VirobShopping.com!</p>
    <p> Order Code : {{$ecom_order_code}}</p>	
    </font>
</multiline>
</td>
</tr>
<tr>
    <td>
        <table class="bordered" width="570">
            <thead>
                <tr>
                    <th bgcolor="#E4E7EA">Image</th>
                    <th bgcolor="#E4E7EA">Name</th>
                    <th align="center" bgcolor="#E4E7EA">Item Price</th>
                    <th align="center" bgcolor="#E4E7EA">Qty</th>
                    <th align="right" bgcolor="#E4E7EA">Sub Total</th>
                </tr>
            </thead>
           
           
            @foreach($supplier_ords as $sub_order)
				
				<?php $tax= 0; ?>
            <tbody>
                @foreach($sub_order['products'] as $detail)
                <tr>
                    <td><img width="60" height="60" src="<?php echo $detail['options']['image_path']; ?>" /></td>
                    <td>{{$detail['name']}}</td>
                    <td align="center">{{number_format($detail['price'], 2, '.', ',')}}</td>
                    <td align="center">{{$detail['qty']}}</td>
                    <td align="right">{{number_format($detail['subtotal'], 2, '.', ',')}}</td>
                </tr>
					<?php $tax = $tax + $detail['options']['tax_info']['tax_value']; ?>
                @endforeach               
            </tbody>
			
				
			<tfoot>
                <tr>
                    <td bgcolor="#E4E7EA" colspan="3" align="right">Sub Total</td>
                    <td bgcolor="#E4E7EA" align="center"></td>
                    <td bgcolor="#E4E7EA" align="right">{{number_format($sub_order['sub_total'], 2, '.', ',')}}</td>
                </tr>
				<tr>
                    <td bgcolor="#E4E7EA" colspan="3" align="right">Tax Inclusive</td>
                    <td bgcolor="#E4E7EA" align="center"></td>
                    <td bgcolor="#E4E7EA" align="right">{{number_format($tax, 2, '.', ',')}}</td>
                </tr>
				<tr>
                    <td bgcolor="#E4E7EA" colspan="3" align="right">Shipping Charge</td>
                    <td bgcolor="#E4E7EA" align="center"></td>
                    <td bgcolor="#E4E7EA" align="right">{{number_format($sub_order['shipping_charge'], 2, '.', ',')}}</td>
                </tr>
				<tr>
                    <td bgcolor="#E4E7EA" colspan="3" align="right">Net Pay</td>
                    <td bgcolor="#E4E7EA" align="center"></td>
                    <td bgcolor="#E4E7EA" align="right">{{number_format($sub_order['net_pay'], 2, '.', ',')}}</td>
                </tr>
            </tfoot>
            @endforeach
            
            
            
        </table>
    </td>
</tr>
</tbody>
</table>
@stop



