@extends('layouts.content_page')
@section('pagetitle')
	Update Email/Mobile
@stop
@section('contents')
<style>
ul.notes-text {
	margin: 30px 0 10px;
	padding: 0;
}
ul.notes-text li {
	margin-bottom: 15px;
}
ul.notes-text li h4 {
	font-size: 14px;
	font-weight: bold;
}
.error {
	color: red;
}
</style>
<div class="page-content">
	<div class="row">
		@include('account.personaInfo_leftmenu')
		<div class="col-sm-9">
			<div class="contentpanel">
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>Update Email/Mobile</h4>
						</br>
						<form class="form-horizontal" id="update_email" name="update_email" action="{{URL::to('account/updateemail')}}" method="post">
							<div class='row' id="changeemail_box" style="display:none">
								<div class="col-lg-12">
									<div id="emailEnteryfrm">
										<div class="form-group">
											<div class="col-lg-4">
												<label class="control-label">Enter New Email</label>
											</div>
											<div class="col-lg-8">
												<input type="text" class="form-control" id="email_show" name="email_show" placeholder="Enter email address" value={{$email}}>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-8 col-lg-offset-4">
											<button type="button" id="email_add" data-url="{{URL::to('account/verify_email')}}" class="btn btn-primary email_mob_add"><i class="fa fa-mail-forward"></i>&nbsp;Send Verification</button> 
											<button type="button"class="btn btn-default email_mob_cancel"><i class="fa fa-close"></i>&nbsp;Cancel</button>
											</div>
										</div>
									</div>
									<div id="emailVerifyfrm" style="display:none">
										<div class="form-group">
											<div class="col-lg-4">
												<label class="control-label">Enter Verification Code</label>
											</div>
											<div class="col-lg-8">
												<input type="text" class="form-control verification_code" id="email_code" name="email_code" placeholder="Enter verification code">
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-8 col-lg-offset-4">
											<button type="button" id="email_ver_code" class="btn btn-primary verify_verification_code"><i class="fa fa-mail-forward"></i>Continue</button> 
											<button type="button" class="btn btn-default email_mob_cancel"><i class="fa fa-close"></i>&nbsp;Cancel</button>&nbsp;&nbsp;
											<a href="" id="email_add_a" class="email_mob_add text-info" data-url="{{URL::to('account/verify_email')}}"><i class="fa fa-mail-forward"></i>&nbsp;Resend Verification</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" id="email_mobile_info">
								<div class="col-lg-12">
									<table class="table table-striped" style="margin-bottom: 0px">
										<tr id="update_email_tr">
											<td class="col-sm-4" style="vertical-align: middle;">
												<label class="control-label" style="padding-top:0px;">{{Lang::get('account_email_update.email_addr')}}</label>
											</td>
											<td class="col-sm-8">
												<div id="email_info">
													<span id="email_info_span">
														@if(!empty($email))
															<span id="email_chng"> {{$email}}</span>&nbsp;&nbsp;
															<a id="edit_email" class="edit_info text-info" data="{{$email}}" href="">Edit</a>
														@else
															<input type="text" class="form-control" id="email_addr" name="email_addr" placeholder="Enter email address">
														@endif
													</span>
												</div>
											</td>
										</tr>
										<tr id="update_mobile_tr">	
											<td class="col-sm-4" style="vertical-align: middle;">
												<label control-label">{{Lang::get('account_email_update.mob_num')}}</label>
											</td>
											<td class="col-sm-8">
												<div id="mob_info">
													<span id="mob_info_span">
														@if(!empty($mobile))
															<span id="mob_chng">+91 {{$mobile}}</span>&nbsp;&nbsp;
															<a  id="edit_mob" class="edit_info text-info" data="{{$mobile}}" href="">Edit</a>
														@else
															<input type="text" class="form-control" id="mobile_no" name="mobile_no" placeholder="Enter mobile number" onkeypress="return isNumberKey(event);">';
														@endif
													</span>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</div>
							<div class='row' id="changemob_box" style="display:none">
								<div class="col-lg-12">
									<div id="mobEnteryfrm">
										<div class="form-group">
											<div class="col-lg-4">
												<label class="control-label">Enter New Mobile Number</label>
											</div>
											<div class="col-lg-8">
												<input type="text" class="form-control" id="mob_show" name="mob_show" placeholder="Enter mobile number" value={{$mobile}}>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-8 col-lg-offset-4">
											<button type="button" id="mob_add" data-url="{{URL::to('account/verify_mobile')}}" class="btn btn-primary email_mob_add"><i class="fa fa-mail-forward"></i>&nbsp;Send Verification</button> 
											<button type="button"class="btn btn-default email_mob_cancel"><i class="fa fa-close"></i>&nbsp;Cancel</button>
											</div>
										</div>
									</div>
									<div id="mobVerifyfrm" style="display:none">
										<div class="form-group">
											<div class="col-lg-4">
												<label class="control-label">Enter Verification Code</label>
											</div>
											<div class="col-lg-8">
												<input type="text" class="form-control verification_code" id="mob_code" name="mob_code" placeholder="Enter verification code">
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-8 col-lg-offset-4">
											<button type="button" id="mob_ver_code" class="btn btn-primary verify_verification_code"><i class="fa fa-mail-forward"></i>Continue</button> 
											<button type="button" class="btn btn-default email_mob_cancel"><i class="fa fa-close"></i>&nbsp;Cancel</button>
											<a href="" id="mob_add_a" class="email_mob_add text-info" data-url="{{URL::to('account/verify_mobile')}}"><i class="fa fa-mail-forward"></i>&nbsp;Resend Verification</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="form-group">
								<label class="col-sm-4"> </label>
								<div class="col-sm-8 fieldgroup">
									<button type="submit" class="btn btn-primary" id="submit" name="submit" style="margin-left: 5px; line-height: 1.5em; display: none;"><i class="fa fa-save"></i>&nbsp;&nbsp;{{trans('account_email_update.svn_chng')}}</button>
								</div>
							</div> -->
						</form>
						<ul id="update_email_mobile_notes" class="notes-text">
							{!!trans('account_email_update.information_email_mob')!!}
						</ul>
					</div>
				</div>
				<div class="row" id="page_content">
					<div class="col-sm-12">
						<div id="msg"></div>
						<div class="row filemanager" id="prod">
							<table id="my_wish_list" class="table">
							</table>				
						</div>
					</div>
				</div>
				<div id="s_cart"></div>
			</div>
		</div>
	</div>
</div>
@stop
@section('scripts')
{{ Html::script('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js')}}
<script src="{{URL::to(Config('ThemeProviders').'/account/account_email_update_new.js') }}"></script>
@stop
