@extends('layouts.content_page')
@section('pagetitle')
My Wishlist
@stop
@section('contents')
<div class="page-content">
    <div class="row">
	    @include('account.personaInfo_leftmenu')
        <div class="col-sm-9">
              <div class="contentpanel">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-4">
									<input type="text" class="form-control" id="search_term" placeholder="Search">
								</div>
								<div class="col-sm-2">
									<input type="button" class="btn btn-primary btn-sm" id="search_btn" value="Search">
								</div>
							</div>
						</div>
					</div>
					<div class="row" id="page_content">
						<div class="col-sm-12">
							<div id="msg"></div>
							<div class="row filemanager" id="prod">
								<table  id="my_wish_list" class="table">
								
								</table>				
							</div>
						</div>
					   </div>
					<div id="s_cart"></div>
				</div>
				<div class="modal fade" id="products" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" style="width: 650px;">
						<div class="modal-content">
							<div class="modal-body">
							</div>
						</div>
					</div>
				</div>
        </div>
    </div>
</div>
@stop
@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{URL::to(Config('ThemeAsset').'/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{URL::to(Config('ThemeAsset').'/plugins/rating/scripts/rateit.css') }}">
@stop
@section('scripts')
<script src="{{ URL::to(Config('ThemeAsset').'/plugins/rating/scripts/jquery.rateit.js') }}"></script>
<script src="{{ URL::to(Config('ThemeAsset').'/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ URL::to(Config('ThemeProviders').'/my_wish_list.js') }}"></script>
@stop






