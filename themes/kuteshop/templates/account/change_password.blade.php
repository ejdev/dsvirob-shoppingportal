@extends('layouts.content_page')
@section('pagetitle')
	Change Password
@stop
@section('contents')
<div class="page-content">
	<div class="row">
		@include('account.personaInfo_leftmenu')
		<div class="col-sm-9">
			<div class="contentpanel">
				<div id="msg_div"></div>
				<div class="panel panel-default">
					<div class="panel-body">
						<form class="form-horizontal" id="changepassword" name="changepassword" action="{{URL::to('account/savepassword')}}" method="post" >
						 	<h4>Reset Password</h4>
							</br>
							<span>It's a good idea to use a strong password that you're not using elsewhere.</span>
							</br></br>
							<div id="msg"></div>
							<div class="form-group">
								<label class="col-sm-4 control-label">
									<b>{{Lang::get('change_password.your_current_password')}}</b><!-- <span class="danger">*</span> -->
								</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="oldpassword" name="oldpassword" placeholder="Your current password">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">
									<b>{{Lang::get('change_password.new_password')}}</b><!-- <span class="danger">*</span> -->
								</label>
								<div class="col-sm-6 fieldgroup">
									<input type="password" class="form-control" id="new_password" name="new_password" placeholder="New password">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">
									<b>{{Lang::get('change_password.repeat_new_password')}}</b><!-- <span class="danger">*</span> -->
								</label>
								<div class="col-sm-6 fieldgroup">
									<input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Repeat new password">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4"> </label>
								<div class="col-sm-3 fieldgroup">
									<button type="submit" class="btn btn-sm btn-primary" id="submit" name="submit" ><i class="fa fa-save"></i>&nbsp;&nbsp;{{Lang::get('general.update_btn')}}</button>
								</div>
							</div>
							<div class="well">If you change the password, you will be signed out. Please ensure that everything is closed and saved.</div>
						</form>
					</div>
				</div>
				<div class="row" id="page_content">
					<div class="col-sm-12">
						<div id="msg"></div>
						<div class="row filemanager" id="prod">
							<table id="my_wish_list" class="table">
							</table>				
						</div>
					</div>
				</div>
				<div id="s_cart"></div>
			</div>
			<div class="modal fade" id="products" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" style="width: 650px;">
					<div class="modal-content">
						<div class="modal-body"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
 @section('scripts')
 <script src="{{URL::to(Config('ThemeProviders').'/account/change_pwd.js') }}"></script>
 @stop
