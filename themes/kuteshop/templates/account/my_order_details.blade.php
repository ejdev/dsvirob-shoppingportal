@extends('layouts.blank_layout')
@section('pagetitle')
Order Details
@stop
@section('title-options')
<a class="btn btn-danger cancel-order" href="">Cancel Order</a>
<a class="btn btn-warning return-order" href="">Return Order</a>
@stop
@section('content')
<input type="hidden" value="{{$sub_order_code}}" id="sub_order_code">
<div class="page-content page-order step" id="order-details">
    <div class="row">
        <div class="col-sm-6" id="order-info">
            <div class="form-group">
                <label class="control-label col-xs-3">Order Code: </label>
                <div class="col-xs-9">
                    <p class="" id="order_code"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Seller: </label>
                <div class="col-xs-9">
                    <p class="" id="supplier"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Order Date: </label>
                <div class="col-xs-9">
                    <p class="" id="order_date"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Payment: </label>
                <div class="col-xs-9">
                    <p class="" id="payment"></p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Amount: </label>
                <div class="col-xs-9">
                    <p class="" id="amount"></p>
                </div>
            </div>
        </div>
        <div class="col-sm-6" id="address-info">
            <h4 id="full_name"></h4>
            <address id="address">
            </address>
        </div>
    </div>
    <hr/>
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-5"><b></b></div>
                <div class="col-sm-3"><b>Status</b></div>
                <div class="col-sm-2"><b>Delivery Date</b></div>
                <div class="col-sm-2"><b>Sub Total</b></div>
            </div>
        </div>
        <div class="panel-body">
            <ul id="my-order-items" class="order-items">

            </ul>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script src="{{ URL::to(Config('ThemeProviders').'/account/my-order-items.js') }}"></script>
@stop
