@extends('layouts.blank_layout')
@section('pagetitle')
Checkout
@stop
@section('content')
<style>
.danger { color:red; }
#total_amount input{
   text-align:right;
}
</style>
<div class="page-content page-order step">
    <ul class="step">
        @if(!isset($user_details) || empty($user_details))
        <li><a href="#step-login"><span>Sign in</span></a></li>
        @endif
        <li><a href="#step-address"><span>Address</span></a></li>
        <li><a href="#step-summary"><span>Summary</span></a></li>
        <li><a href="#step-payment"><span>Payment</span></a></li>
    </ul>
    <div class="heading-counter warning">Your shopping cart contains:
        <span>1 Product</span>
    </div>
    <div class="step-content">
        @if(!isset($user_details) || empty($user_details))
        <div id="step-login" class="steps">
            <div class="page-content">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="box-authentication">
                            <form method="post" id="signupfrm" action="<?php echo URL::to('login');?>">
                                <h3>Create an account</h3>
                                <p>Please enter your email address to create an account.</p>
                                <label for="emmail_register">Email address</label>
                                <input id="user_login" name="user_login" placeholder="Account Id" type="text" class="form-control">
                                <button class="button"><i class="fa fa-user"></i> Create an account</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-authentication">
                            <form method="post" id="loginfrm" action="<?php echo URL::to('/login');?>">
                                <h3>Already registered?</h3>
                                <label for="emmail_login">Email address</label>
                                <input id="username" name="username" placeholder="Account Id" type="text" class="form-control">
                                <label for="password_login">Password</label>
                                <input id="password" name="password" placeholder="Password" type="password" class="form-control">
                                <p class="forgot-pass"><a href="#" id="forgot_password">Forgot your password?</a></p>
                                <button class="button"><i class="fa fa-lock"></i> Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div id="step-address" class="steps">
            <form action="{{URL::to('save-mycart-details/address')}}" class="form" id="my-cart-address">
                <div class="box-border col-sm-6">
                    <h4>Billing Address</h4><hr/>								
                    <ul class="billing">
                        <li class="row">
                            <div class="col-sm-12">
                                <label for="first_name" class="required">Full Name</label>
                                <input type="text" class="input form-control" name="address[billing][full_name]" id="full_name">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-12">
                                <label for="address" class="required">Address Line 1</label>
                                <input type="text" class="input form-control" name="address[billing][address1]" id="address1">
                            </div>
                            <div class="col-sm-12">
                                <label for="address" class="required">Address Line 2</label>
                                <input type="text" class="input form-control" name="address[billing][address2]" id="address2">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="city" class="required">City</label>
                                <input class="input form-control" type="text" name="address[billing][city]" id="city">
                            </div>
                            <div class="col-sm-6">
                                <label class="required">State/Province</label>
                                <select class="input form-control" name="address[billing][state_id]" id="state_id">
                                    @if ($state)
										<option value="">Select State</option>
										@foreach ($state as $st)
											<option value="{{$st->state}}">{{$st->state}}</option>
										@endforeach
									@endif
                                </select>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="postal_code" class="required">Zip/Postal Code</label>
                                <input class="input form-control" type="text" name="address[billing][postal_code]" id="postal_code">
                            </div>
                            <div  class="col-sm-6">
                                <label class="required">Country</label>
                                <select class="input form-control" name="address[billing][country_id]" id="country_id">
									@if ($country)
										<option value="">Select Country</option>
										@foreach ($country as $cnty)
											<option value="{{$cnty->country}}">{{$cnty->country}}</option>
										@endforeach
									@endif
                                </select>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="company_name">Mobile</label>
                                <input type="text" name="address[billing][mobile_no]" class="input form-control" id="mobile_no">
                            </div>
                            <div class="col-sm-6">
                                <label for="email_address" class="required">Email Address</label>
                                <input type="email" class="input form-control" name="address[billing][email_id]" id="email_id">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="box-border col-sm-6">
                    <h4 class="col-sm-8">Shipping Address</h4>
                    <div class="col-sm-4 checkbox-inline pull-right">
                        <label><input type="checkbox" value="" id="same-as-billing">Same as Billing</label>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <ul class="shipping">
                        <li class="row">
                            <div class="col-sm-12">
                                <label for="first_name" class="required">Full Name</label>
                                <input type="text" class="input form-control" name="address[shipping][full_name]" id="full_name">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-12">
                                <label for="address" class="required">Address Line 1</label>
                                <input type="text" class="input form-control" name="address[shipping][address1]" id="address1">
                            </div>
                            <div class="col-sm-12">
                                <label for="address" class="required">Address Line 2</label>
                                <input type="text" class="input form-control" name="address[shipping][address2]" id="address2">
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="city" class="required">City</label>
                                <input class="input form-control" type="text" name="address[shipping][city]" id="city">
                            </div>
                            <div class="col-sm-6">
                                <label class="required">State/Province</label>
                                <select class="input form-control" name="address[shipping][state_id]" id="state_id">
                                    @if ($state)
										<option value="">Select State</option>
										@foreach ($state as $st)
											<option value="{{$st->state}}">{{$st->state}}</option>
										@endforeach
									@endif
                                </select>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="postal_code" class="required">Zip/Postal Code</label>
                                <input class="input form-control" type="text" name="address[shipping][postal_code]" id="postal_code">
                            </div>
                            <div  class="col-sm-6">
                                <label class="required">Country</label>
                                <select class="input form-control" name="address[shipping][country_id]" id="country_id">
                                    @if ($country)
										<option value="">Select Country</option>
										@foreach ($country as $cnty)
											<option value="{{$cnty->country}}">{{$cnty->country}}</option>
										@endforeach
									@endif
                                </select>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="company_name">Mobile</label>
                                <input type="text" name="address[shipping][mobile_no]" class="input form-control" id="mobile_no">
                            </div>
                            <div class="col-sm-6">
                                <label for="email_address" class="required">Email Address</label>
                                <input type="email" class="input form-control" name="address[shipping][email_id]" id="email_id">
                            </div>
                        </li>
                    </ul>
                </div>
            </form>
            <div class="cart_navigation">
                <a class="prev-btn" href="#">Continue shopping</a>
                <a class="next-btn" href="#">Proceed to checkout</a>
            </div>
        </div>
        <div id="step-summary" class="steps order-detail-content">
		</div>
        <div id="step-payment" class="steps">
            <div class="box-border">				
                <form action="{{URL::to('save-mycart-details/payment')}}">
                    <div class="col-sm-6" id="payment"></div>
                    <div class="col-sm-3" id="wallet"></div>
					<div class="col-sm-3" id="total_amount"></div>					
                </form>
            </div>
            <div class="cart_navigation">
                <a class="prev-btn" href="#">Continue shopping</a>
                <a class="next-btn" href="#">Proceed to checkout</a>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script src="{{ URL::to(Config('ThemeProviders').'/login.js') }}"></script>
<script src="{{ URL::to(Config('ThemeProviders').'/payments.js') }}"></script>
<script src="{{ URL::to(Config('ThemeProviders').'/cart.js') }}"></script>
@stop
