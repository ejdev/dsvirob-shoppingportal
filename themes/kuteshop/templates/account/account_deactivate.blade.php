@extends('layouts.content_page')
@section('pagetitle')
	Account Deactivation
@stop
@section('contents')
<div class="page-content">
    <div class="row">
	    @include('account.personaInfo_leftmenu')
        <div class="col-sm-9">
              <div class="contentpanel">
					<div class="panel panel-default">
						<div class="panel-body">
                            <h4>Deactivate Account</h4>
                            </br>
							<form class="form-horizontal" id="acc_deactivate" name="acc_deactivate" action="{{URL::to('account/savedeactivate')}}" method="post" >
                            <!-- <div class="form-group" id="email_acc">
                           
                            </div> 
                             <div class="form-group" id="mob_acc">
                              
                            </div> -->
                              <div class="form-group">
                                <label class="col-sm-2 control-label" style="padding-right: 0px;">Account Password:</label>
                                <div class="col-sm-3">
                                <input type="password" class="form-control" id="pwd" name="pwd">
                                <span id="mailerr" style="color:red; font-size:11px"></span>
                               </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2"> </label>
                                <div class="col-sm-2 fieldgroup">
                                    <button type="submit" class="btn btn-sm btn-primary" id="submit" name="submit">{{Lang::get('account_deactivate.crfm_deactivate')}}</button>
                                </div>
                            </div>
                        </form>
							{!!trans('account_deactivate.information_deactivate',array('site_name'=>$sitesettings->site_name))!!}
						</div>
					</div>
					
					<div id="s_cart"></div>
				</div>
			
        </div>
    </div>
</div>
@stop
 @section('scripts')
 <script src="{{URL::to(Config('ThemeProviders').'/account/account_deactivation.js') }}"></script>
 @stop
