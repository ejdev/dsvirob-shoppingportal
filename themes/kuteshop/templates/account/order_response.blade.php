@extends('layouts.content_page')
@section('pagetitle')
Order Response
@stop
@section('contents')
<div class="page-content page-order step">
	<div class="row">
		<div class="col-sm-6">
			<h3>Thank you for your order.</h3><br>
			<p>Your order has been placed and is being processed. When the item(s) are shipped. You will receive an email with the details. You can track this order through <a href="myorders">My orders</a> page. </p><br>
			<h3>Rs. {{$order_details['order']->net_pay}}</h3>
		</div>
		<div class="col-sm-6">
			<div><h4>{{$order_details['shipping']->full_name}}</h4><p>{{$order_details['shipping']->mobile_no}}</p> </div>
			<p>{{$order_details['shipping']->address1}}, {{$order_details['shipping']->address2}}, {{$order_details['shipping']->city}}, {{$order_details['shipping']->state_id}}, {{$order_details['shipping']->country_id}}, {{$order_details['shipping']->postal_code}} </p><br>
			
			<p><span>Your Complete order will be delivered by Mar 10, 2017.</span></p>
		</div>	
	</div><hr>
	<div class="row" style="text-align:center;">
		<br><div class="col-sm-12" >
			<h3>You can now TRACK, CANCEL & RETURN ordered items from MY ORDERS</h3><br>			
		</div>	
		
	</div>
	<hr>
	<div class="row" style="text-align:center;">
		<br><div class="col-sm-12" >
			<h3>Your Order Summary : {{$order_details['order']->qty}} Items</h3><br>			
		</div>	
		
	</div><hr><br>
    
</div>
@stop

