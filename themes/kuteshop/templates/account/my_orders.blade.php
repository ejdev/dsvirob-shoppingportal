@extends('layouts.blank_layout')
@section('pagetitle')
My Orders
@stop
@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ URL::to(Config('ThemeAsset').'/plugins/datatables/dataTables.bootstrap.css')}}">
@stop
@section('content')
<div class="page-content page-order step">
    <table id="my-orders-table">

    </table>
</div>
@stop
@section('scripts')
<script src="{{ URL::to(Config('ThemeAsset').'/plugins/datatables/jquery.dataTables.js') }}"></script>    
<script src="{{ URL::to(Config('ThemeProviders').'/account/my-orders.js') }}"></script>
@stop
