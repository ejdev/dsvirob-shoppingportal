 
@extends('frontend.layouts.blank_layout')
@section('pagetitle')
My order <span class="navigation-pipe">&nbsp;</span> {{isset($particular_order_details->sub_order_code)?$particular_order_details->sub_order_code :''}} <span class="navigation-pipe">&nbsp;</span> Return
@stop
<style>
	
#pickup-address,.line,.lastUnit,#change-addr,#pick_up_addr
{   
 margin-top: 10px;
}
.select-address
{
    border: 1px solid #f2c700;
	padding: 0 35px;
}
.border{
	border-bottom: solid 1px #eaeaea;
}
option:disabled {display:none;}

</style>
@section('content')

<div class="page-content page-order step" id="order-details">
  <div class="row">
    <div class="col-sm-3" id="order-info">
      <div class="form-group">
        <div class="col-xs-12">
          <h4>ITEM DETAILS</h4>
          <label class="control-label">YOU ORDERED </label>
          <img class="img img-responsive" src="{{$imgs[0]->img_path}}" id="img_product">
          <p id="ord-produc-details">{{$particular_order_details->product_name}}</p>
          <p >Quantity: {{$particular_order_details->qty}}</p>
        <div class="border"></div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-xs-12">
          <div id="pick_up_addr">
            <label class="control-label">PICK UP ADDRESS </label>
            <p class="" id="full_name"> {{$particular_order_details->full_name}} </p>
            <div class="border"></div>
            <small class="text-info">
            <p> {{$particular_order_details->address1.$particular_order_details->address2}}</p>
            <p>{{$particular_order_details->city.','.$particular_order_details->state}} </p>
            <p>{{$particular_order_details->postal_code}} </p>
            </small>
            <div class="border"></div>
              <p>{{$particular_order_details->mobile_no}} </p>
              <div class="border"></div>
            <button class="btn btn-sm btn-default" id="change-addr">change pickup address</button>
          </div>
          <div id="pickup-address" style="display: none;">
            <div class="address-list">
              <div class="select-address">
                          <p class="itemInfo">{{$particular_order_details->full_name}}</p>
                          <p class="itemInfo small-info"> {{$particular_order_details->address1.$particular_order_details->address2}}<br>
                          {{$particular_order_details->city.','.$particular_order_details->state}}<br>
                           {{$particular_order_details->postal_code}}<br>
                          </p>
                          <p class="itemInfo border">  {{$particular_order_details->mobile_no}}</p>
              </div>
            </div>
            <div class="line">
              <button class="btn btn-sm btn-danger" id="cancel_pickup">Cancel</button>
              <button class="btn btn-sm btn-primary">Add New Address</button>
            </div>
          </div>
        </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <label class="control-label"> YOU ARE RETURNING </label>
            <img class="img img-responsive" src="{{$imgs[0]->img_path}}" id="img_product">
            <p id="ord-produc-details">{{$particular_order_details->product_name}}</p>
            <p>Quantity: {{$particular_order_details->qty}}</p>
          </div>
        </div>
      </div>
      <div class="col-sm-9">
      <form class="return-form">
      <input type="hidden" value="{{isset($particular_order_details->sub_order_code)?$particular_order_details->sub_order_code :''}}" name="product[sub_order_code]" id="sub_order_code">
  <input type="hidden" value="{{isset($particular_order_details->product_id)?$particular_order_details->product_id :''}}" name="product[product_id]" id="product_id">
  <input type="hidden" value="{{isset($particular_order_details->order_item_id)?$particular_order_details->order_item_id :''}}" name="product[order_item_id]" id="sub_order_code">
        <h4>EASY RETURNS</h4>
        <div class="panel panel-info">
          <div class="panel-heading">
            <h4>Reason For Return</h4>
          </div>
          <div class="panel-body">
            <div class="row">
              <label class="control-label col-xs-3"> Reason for return : </label>
              <div class="col-xs-9">
                <select id="my-order-quality-issues" class="order-items form-control">
                 <option value="">Select An Quality  Issues</option>
                @if(!empty($return_category))
             
                  @foreach($return_category as $main_cat)
                 <?php $parent_id[] = $main_cat['parent']->parent_category_id;?>
                  <option value="{{$main_cat['parent']->parent_category_id}}">{{$main_cat['parent']->parent_category}}</option>
              
                 @endforeach
                  @endif
                  
               </select>
              </div>
            </div>
             <div class="row">
              <label class="control-label col-xs-3"> Issue for return : </label>
              <div class="col-xs-9">
                
              <select id="my-order-items" class="order-items form-control" name="product[request_type_id]">
                 <option value="0">Select An Issues</option>
                    @if(!empty($return_category))
             
                  @foreach($return_category as $main_cat)
                  @foreach($main_cat['child'] as $child_cat)
                     <option value="{{$child_cat->child_category_id}}" data-parent_id = "{{$child_cat->parent_category_id}}"> {{$child_cat->child_category}}</option>
                  
                  
  				
                
                
                     
               @endforeach   
                @endforeach   
             
                  @endif
                    
                 
          
               </select>
               
              </div>
            </div>
            
            <div class="row">
              <label class="control-label col-xs-3"> Comments : </label>
              <div class="col-xs-9">
                <textarea rows="5" placeholder="eg. phone hanging problem" name="product[comments]" class="form-control"></textarea>
              </div>
            </div>
         </div>
         
          
        </div>
     
    <div class="panel panel-info" id="reason_container" style="display:none;">
             <div class="panel-body">
            <div class="row">
              <label class="control-label col-xs-3"> Reason for return : </label>
              <div class="col-xs-9">
                <select id="my-order-reason" class="order-items form-control" name="product[reason_type_id]">
                 <option value="">Select An Quality  Issues</option>
                @if(!empty($return_category))
             
                  @foreach($return_category as $main_cat)
                   @foreach($main_cat['child'] as $child_cat)
                 
                  <option value="{{$child_cat->reason_id}}" data-child-id ="{{$child_cat->child_category_id}}" >{{$child_cat->reason}}</option>
              @endforeach   
                 @endforeach
                  @endif
                  
               </select>
              </div>
            </div>
            <div class="returnTypeInfo REFUND-info" style="display: block;"><ul class="line refund_modes">
              <li class="lastUnit">
                              <input type="hidden" name="refundMode" class="singleMode" id="NEFT" data-container="NEFT-info" value="NEFT">
                  <label>Your money will be transferred with Bank Transfer.</label>
                      </li>
      </ul>
      <p class="err-all"></p>
      <div class="NEFT-info ">
          <ul class="line">
              <li class="" id="NEFT-details-col">
                <div class="row">
                      <div class="unit  small-info"><label  class="control-label col-xs-3">IFSC Code </label></div>
                       <div class="col-sm-9">
                          <input type="text" class="fk-input" autocomplete="off" placeholder="Enter IFSC code" id="ifsc_code" name="bank[ifscCode]" value="">
                          <span class="fk-font-small lmargin5">(OR) &nbsp;<a class="js-ifsc-helper">Find IFSC</a></span>
                
                  </div>
                  <div class="col-sm-12">
                      <div class="unit  small-info"><label  class="control-label col-xs-3">Account Number</label></div>
                      <input type="text" class="fk-input" pattern="^(?!\s*$).{9,35}" placeholder="Your account number" id="account_no" name="bank[accountNumber]" value="">
                  </div>
                 
                      <div class="unit  small-info"><label  class="control-label col-xs-3">Confirm Account Number </label></div>
                       <div class="col-sm-9">
                      <input type="text" class="fk-input" pattern="^(?!\s*$).{9,35}" placeholder="Confirm account number" id="confirm_account_no" name="bank[confirmAccountNumber]" value=""></div>
                  
                 
                      <div class="unit  small-info"><label  class="control-label col-xs-3">Account Holder </label></div>
                       <div class="col-sm-9">
                          <input type="text" class="fk-input" id="account_name" name="bank[accountHolder]" value="">
                      </div>
                  
                 
                      <div class="unit  small-info"><label  class="control-label col-xs-3">Phone Number</label></div>
                      
                           <div class="col-sm-9">
                           <input type="text" value="" disabled="" class="unit fk-input phone_code">
                           <input type="text" class="lastUnit phone_number fk-input" id="phone_number" name="bank[phone_number]" value="">
                          </div>
                      </div>
                   
                    <div class="unit  small-info"><label  class="control-label col-xs-3">&nbsp;</label></div>
                    <div>
                       <div class="unit size1of3 rpadding10">&nbsp;</div>
                       <input type="checkbox" name="saveBankDetails" id="save" checked=""><label for="save"> Save for later use. </label></div>
                  
                     <div class="col-sm-9">
                       <div class="unit  rpadding10">&nbsp;</div>
                      <input type="hidden" name="tandcId" value="NEFT_TANDC">
                           <input type="checkbox" name="uatc" id="uatc"><label for="uatc"> I agree to the </label><a class="terms-link" target="_blank" href="">terms and conditions</a>
                                                </div>
                                            
                                                
                   
              </li>
              
          </ul>
          <p class="fk-alert tmargin8">We will pick up the item you wish to return from the mentioned pick up address.</p>
      </div>
          


  </div>
            </div>
          </div>
          <div class="panel-footer">
          <button class="btn btn-sm btn-primary" id="return_id_btn">return</button>
          </div>
          </form>
    <hr/>
     </div>
    </div>
</div>

@stop
@section('scripts')
<script>

 var child_cat = []; 
 var child_reason=[];
 var reason_id = [];
$('#change-addr').on('click',function()
{
$('#pick_up_addr').hide();
$('#pickup-address').show();
});
$('#cancel_pickup').on('click',function()
{
  $('#pick_up_addr').show();
  $('#pickup-address').hide();
});
$(window).bind("load", function() { 

 
  $("#my-order-items option").each(function() { 
	var CurELe=$(this);
  if(CurELe.val() !=0 && CurELe.val !=null)
  {
	  if(child_cat[CurELe.data('parent_id')]==undefined)
	  {
		 child_cat[CurELe.data('parent_id')] = [];		 
	  }
	   child_cat[CurELe.data('parent_id')].push('<option value="'+CurELe.val()+'">'+CurELe.text()+'</option>');
	
   //	child_cat[$(this).data('parent_id')]= Array('<option value="'+$(this).val()+'">'+$(this).text());
	console.log(child_cat)
	
	
  }
  });
   $("#my-order-reason>option").map(function() { 
var CurELe=$(this);
  if(CurELe.val() !=0 && CurELe.val !=null)
  {
	  
   	child_reason[CurELe.data('child-id')] = '<option value="'+CurELe.val()+'">'+CurELe.text()+'</option>';
	if($(this).val() == 1)
	{
	reason_id[$(this).val()] =  $(this).val();
	}
   }
  });
  
  $("#my-order-items").html('');
  $('#my-order-reason').html('');


});
$('#my-order-quality-issues').on('change',function()
{
  var child_category  = ''; 
  var curle = $(this);
  var value  =((curle.val()!= '') ? curle.val() : '');
  $(child_cat).map(function() { 
  var child_vals = child_cat[value]
  console.log(child_vals)
  $("#my-order-items").html(child_vals);
  });
  });

 $('#my-order-items').on('change',function()
{
	var curle = $(this);
	$('#reason_container').show();
	 var value1  =((curle.val()!= '') ? curle.val() : '');
  
  var child_vals1 = child_reason[value1]
  console.log(child_vals1)
  $("#my-order-reason").html(child_vals1);
  var reason_id_match = reason_id[$("#my-order-reason").val()]
  
  if(reason_id_match)
	{
		$('.REFUND-info').show();
	}
	else
	{
	$('.REFUND-info').hide();	
	}
 

});
 $('#my-order-reason').on('change',function()
{
	var curle = $(this);
	
	
});
$('#return_id_btn').on('click',function(e)
{
	e.preventDefault();
	console.log($('.return-form').serialize());
	$.ajax({
				 // async:false,
			   type: 'POST',
                url:ajaxUrl+'return/data',
                dataType: 'JSON',
				data:$('.return-form').serialize(),
				 beforeSend:function(){
				loadPreloader();
				$(this).attr('disabled', true);
			},
	});
});

</script>
@stop 