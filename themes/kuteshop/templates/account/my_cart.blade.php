@extends('layouts.content_page')
@section('pagetitle')
My Cart
@stop
@section('contents')
<div class="page-content page-order step">
    <div class="step-content">
        <div id="step-summary" class="steps order-detail-content" data-chechoutUrl="{{$chechoutUrl}}">
        </div>
    </div>
</div>
@stop
@section('scripts')
<script src="{{ URL::to(Config('ThemeProviders').'/cart.js') }}"></script>
@stop
