<div class="col-sm-3">
    <h2><b>My Account</b></h2>
    <br />
    <h5><b>SETTINGS</b></h5>
    <ul >
        <li><a href="{{route('account.personal_info')}}"><span>Personal Information</span></a></li>
        <li><a href="{{route('account.change_password')}}"><span>Change Password</span></a></li>
        <li><a href="{{route('account.address')}}"><span>Addresses</span></a></li>
        <li><a href="{{route('account.email_update')}}"><span>Update Email/Mobile</span></a></li>
        <li><a href="{{route('account.account_deactivate')}}"><span>Deactivate Account</span></a></li>
    </ul>
    <hr />
    <h5><b>ORDERS</b></h5>
    <ul >
        <li><a href="account/my-orders"><span>My Orders</span></a></li>
    </ul>
    <hr />
        <h5><b>MY STUFF</b></h5>
    <ul >
        <li><a href="account/wishlist"><span>My Wishlist</span></a></li>
    </ul>
    <hr />
</div>