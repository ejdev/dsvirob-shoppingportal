@extends('layouts.content_page')
@section('pagetitle')
	Address
@stop
@section('contents')
<div class="page-content">
    <div class="row">
	    @include('account.personaInfo_leftmenu')
        <div class="col-sm-9">
		<div id="msg_div"></div>
              <div class="contentpanel">
					<div class="panel panel-default">
						<div class="panel-body">
                            <h4>Addresses</h4>
                            </br>
						 <form class="form-horizontal" id="address_form" name="address_form" action="{{URL::to('account/saveaddress')}}" method="post" >
                        <div id="msg"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{Lang::get('update_address.street1')}}
 <span class="danger">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="street1" name="street1" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{Lang::get('update_address.street2')}} <span class="danger">*</span></label>
                                <div class="col-sm-6 fieldgroup">
                                    <input type="text" class="form-control" id="street2" name="street2" >
                                </div>
                            </div>
							<div class="form-group">
                                <label class="col-sm-3 control-label">{{Lang::get('update_address.country')}} <span class="danger">*</span></label>
                                <div class="col-sm-4 fieldgroup">
                                    <select name="country" id="country"  class="form-control select-font">
                                       @foreach($countries as $country)
									      <option value="{{$country->country_id}}" @if($country->country_id  == $default_country){{'selected'}}@endif />{{$country->country}}</option>
									   @endforeach
                                    </select>
                                </div>
                            </div>
							 <div class="form-group">
                               <label class="col-sm-3 control-label">{{Lang::get('update_address.state')}} <span class="danger">*</span></label>
                               <div class="col-sm-4 fieldgroup">
                                    <select name="state" id="state"  class="form-control select-font">
                                      
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{Lang::get('update_address.city')}} <span class="danger">*</span></label>
                                <div class="col-sm-6 fieldgroup">
                                    <input type="text" class="form-control" id="city" name="city" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">{{Lang::get('update_address.pin_code')}} <span class="danger">*</span></label>
                                <div class="col-sm-6 fieldgroup">
                                    <input type="text" class="form-control" id="pin_code" name="pin_code" onkeypress="return isNumberKey(event);" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3"> </label>
                                <div class="col-sm-3 fieldgroup">
                                    <button type="submit" class="btn btn-sm btn-primary" id="submit" name="submit" >{{Lang::get('general.submit')}}</button>
                                </div>
                            </div>
                        </form>
						</div>
					</div>
					<div class="row" id="page_content">
						<div class="col-sm-12">
							<div id="msg"></div>
							<div class="row filemanager" id="prod">
								<table  id="my_wish_list" class="table">
								
								</table>				
							</div>
						</div>
					   </div>
					<div id="s_cart"></div>
				</div>
				<div class="col-sm-12">
					<h5><b>{{Lang::get('update_address.svd_addr')}}</b></h5><br />
					<table class="table table-striped">
						<thead>
							<th>{{Lang::get('update_address.default')}}</th>
							<th>{{Lang::get('update_address.street')}}</th>
							<th>{{Lang::get('update_address.city')}}</th>
							<th>{{Lang::get('update_address.state')}}</th>
							<th>{{Lang::get('update_address.country')}}</th>
							<th>Remove</th>
						</thead>
						<tbody id="addr_list">
						  
						</tbody>
				   
				   </table>
				</div>
				<div class="modal fade" id="products" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" style="width: 650px;">
						<div class="modal-content">
							<div class="modal-body">
							</div>
						</div>
					</div>
				</div>
        </div>
    </div>
</div>
@stop
 @section('scripts')
 {{ Html::script('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js')}}
 <script src="{{URL::to(Config('ThemeProviders').'/account/update_address.js') }}"></script>
 
 @stop
