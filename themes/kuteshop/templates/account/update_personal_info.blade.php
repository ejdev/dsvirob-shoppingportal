@extends('layouts.content_page')
@section('pagetitle')
	Personal Informations
@stop
@section('contents')
<div class="page-content">
	<div class="row">
		@include('account.personaInfo_leftmenu')
		<div class="col-sm-9">
			<div class="contentpanel">
				<div id="msg_div"></div>
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>Personal Information</h4>
						</br>
						<form class="form-horizontal" id="update_pinfo" name="update_pinfo" action="{{URL::to('account/update-personal-info')}}" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label class="col-sm-3 control-label">{{Lang::get('update_personal_info.first_name')}}
									<span class="danger" style="color: red;" > * </span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="firstname" name="firstname" value="{{$info->data->firstname or ''}}" />
									<span id="f_error" style="color: red;"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">{{Lang::get('update_personal_info.last_name')}}
									<span class="danger" style="color: red;" > * </span>
								</label>
								<div class="col-sm-6">
									<input type="text" class="form-control" id="lastname" name="lastname" onkeypress="return alphaNumeric_specialchar(event);" value="{{$info->data->lastname or ''}}" />
									<span id="l_error" style="color: red;"></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">{{Lang::get('update_personal_info.gender')}} 
									<span class="danger" style="color: red;">*</span>
								</label>
								<div class="col-sm-6 fieldgroup">
									<?php //echo '<pre>';print_r($info->data->gender_id); ?>
									<select name="gender" id="gender" class="form-control">									
										@foreach($gender as $gen)
											<option value="{{$gen->gender_id}}" @if($info->data->gender_id == $gen->gender_id){{'selected'}}@endif/>{{$gen->gender}}</option>
										@endforeach	
									</select>
									<span id="g_error" style="color: red;"></span>
								</div>
							</div>
							<div class="form-group">	
								<label class="col-sm-3 control-label"> </label>							
								<div class="col-sm-6 fieldgroup">
									<button name="submit" type="submit" id="save_chng" class="btn btn-sm btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;{{Lang::get('update_personal_info.chng_save')}} </button>
									<span id="errmsg" style="color: red;"></span>			
								</div>
							</div>					
						</form>
					</div>
				</div>
				<div class="row" id="page_content">
					<div class="col-sm-12">
						<div id="msg"></div>
						<div class="row filemanager" id="prod">
							<table id="my_wish_list" class="table">
							</table>				
						</div>
					</div>
				</div>
				<div id="s_cart"></div>
			</div>
			<div class="modal fade" id="products" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" style="width: 650px;">
					<div class="modal-content">
						<div class="modal-body"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('scripts')
	<script src="{{URL::to(Config('ThemeProviders').'/account/update_personal_info.js') }}"></script>
@stop
