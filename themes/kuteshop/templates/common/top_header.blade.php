<div class="top-header">
    <div class="container">
        <div class="nav-top-links">
            <a class="first-item" href="javascript:void(0);"><img alt="phone" src="{{Config('ThemeAsset')}}/images/phone.png" />00-62-658-658</a>
            <a href="{{URL::to('contact-us')}}"><img alt="email" src="{{Config('ThemeAsset')}}/images/email.png" />Contact us today!</a>
        </div>
        
        <div class="support-link">
            <a href="#">Services</a>
            <a href="#">Support</a>			
			
			@if (!Session::has('user_data')) 					
			<a href="{{URL::to('login')}}">Login</a>
			@else 
			<a>Welcome {{$user_details->uname}}</a>
			<a href="{{URL::to('logout')}}">Logout</a>	
			@endif
        </div>
		@if (Session::has('user_data')) 
        <div id="user-info-top" class="user-info pull-right">
            <div class="dropdown">
                <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account</span></a>
                <ul class="dropdown-menu mega_dropdown" role="menu">					
                    <li><a href="#">Compare</a></li>
                    <li><a href="{{URL::to('account/wishlist')}}">Wishlists</a></li>                    
                </ul>
            </div>
        </div>
		@endif
    </div>
</div>
