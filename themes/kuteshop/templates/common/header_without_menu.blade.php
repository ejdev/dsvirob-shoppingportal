@include('common.top_header')
<div class="container main-header">
    <div class="row">
        <div class="col-xs-12 col-sm-3 logo">
            <a href="{{URL::to('/')}}"><img alt="{{$sitesettings->site_name}}" src="{{URL::asset(Config('ThemeAsset').'/imgs/'.$sitesettings->site_logo)}}" /></a>
        </div>
        <div class="col-xs-7 col-sm-7 header-search-box">
            <form class="form-inline">
                <div class="form-group form-category">
                    <select class="select-category">
                         <option value="2">All Categories</option>
                        @if(!empty($category))
                        @foreach($category as $cat)
                        <option value="{{$cat->category_id}}" data-url="{{URL::asset($cat->url)}}">{{$cat->category}}</option>
                        @endforeach
                        @endif
                      
                    </select>
                </div>
                <div class="form-group input-serach">
                    <input type="text"  placeholder="Keyword here...">
                </div>
                <button type="submit" class="pull-right btn-search"></button>
            </form>
        </div>
        <div id="cart-block" class="col-xs-5 col-sm-2 shopping-cart-box">
        </div>
    </div>
</div>
<div id="form-search-opntop">
</div>
<div id="user-info-opntop">
</div>
<div id="shopping-cart-box-ontop">
    <i class="fa fa-shopping-cart"></i>
    <div class="shopping-cart-box-ontop-content"></div>
</div>
