@include('common.top_header')
<div class="container main-header">
    <div class="row">
        <div class="col-xs-12 col-sm-3 logo">
            <a href="{{URL::asset('/')}}"><img alt="{{$sitesettings->site_name}}" src="{{URL::asset(Config('ThemeAsset').'/imgs/'.$sitesettings->site_logo)}}" /></a>
        </div>
        <div class="col-xs-7 col-sm-7 header-search-box">
            <form class="form-inline" id="search-products">
                <div class="form-group form-category">
                    <select class="select-category" id="searchCategory">
                        <option value="">All Categories</option>
                        @if(!empty($category)) 
                            @foreach($category as $cat)
                                @if($cat->parent_category_id <= 1)
                                    <option value="{{$cat->category_id}}" data-url="{{URL::asset($cat->url)}}">{{$cat->category}}</option>
                                @endif
                            @endforeach
                        @endif                        
                    </select>
                </div>
                <div class="form-group input-serach">
                    <input type="text"  placeholder="Keyword here..." id="searchTerm">
                </div>
                <button type="submit" class="pull-right btn-search"></button>
            </form>
        </div>
        <div id="cart-block" class="col-xs-5 col-sm-2 shopping-cart-box">
        </div>
    </div>
</div>
<div id="nav-top-menu" class="nav-top-menu">
    <div class="container">
        <div class="row">
            <div class="col-sm-3" id="box-vertical-megamenus">
                <div class="box-vertical-megamenus">
                    <h4 class="title">
                        <span class="title-menu">Categories</span>
                        <span class="btn-open-mobile pull-right home-page"><i class="fa fa-bars"></i></span>
                    </h4>
                    <div class="vertical-menu-content is-home" style="display: none;">
                        <ul class="vertical-menu-list">
                            <?php
                            if (!empty($menu_data->catlog_menu))
                            {
                                foreach ($menu_data->catlog_menu as $data)
                                {

                                    if (empty($data->group))
                                    {
                                        if($data->is_login_required == 1&& isset($user_details) &&!empty($user_details))
                                        {
                                        ?>
                                            <li><a href="{{URL::asset($data->url)}}"><img class="icon-menu" alt="Funky roots" src="{{URL::asset(Config('ThemeAsset').'/data/1.png')}}">{{ $data->title or ''}}</a></li>
                                        <?php
                                        }
                                        else
                                        { ?>
                                            <li><a href="{{URL::asset($data->url)}}"><img class="icon-menu" alt="Funky roots" src="{{URL::asset(Config('ThemeAsset').'/data/1.png')}}">{{ $data->title or ''}}</a></li>
                                       <?php }
                                    }
                                    else if (!empty($data->group))
                                    {
                                        
                                        ?>
                                        <li>
                                            @if($data->is_login_required == 1&& isset($user_details) &&!empty($user_details))
                                            <a class="parent" href="{{URL::asset($data->url)}}"><img class="icon-menu" alt="Funky roots" src="{{URL::asset(Config('ThemeAsset').'/data/2.png')}}">{{$data->title or ''}}</a>
                                            @else
                                             <a class="parent" href="{{URL::asset($data->url)}}"><img class="icon-menu" alt="Funky roots" src="{{URL::asset(Config('ThemeAsset').'/data/2.png')}}">{{$data->title or ''}}</a>
                                             @endif
                                            <div class="vertical-dropdown-menu">
                                                <div class="vertical-groups col-sm-12">
                                                    <div class="mega-group col-sm-4">
                                                        <?php
                                                        foreach ($data->group as $grp)
                                                        {
                                                            foreach ($grp as $group)
                                                            {
                                                                ?>
                                                                  @if($group->is_login_required == 1&& isset($user_details) &&!empty($user_details))
                                                                <h4 class="mega-group-header"><span>{{$group->title}}</span></h4>
                                                                 @else
                                                                 <h4 class="mega-group-header"><span>{{$group->title}}</span></h4>
                                                                 @endif
                                                                <?php
                                                                if (!empty($group->links))
                                                                {
                                                                    foreach ($group->links as $link)
                                                                    {
                                                                        ?>
                                                                 @if($link->is_login_required == 1&& isset($user_details) &&!empty($user_details))
                                                                        <ul class="group-link-default">
                                                                            <li><a href="{{URL::asset($link->url)}}">{{ $link->title}}</a></li>
                                                                        </ul>
                                                                  @else
                                                                         <ul class="group-link-default">
                                                                            <li><a href="{{URL::asset($link->url)}}">{{ $link->title}}</a></li>
                                                                        </ul>
                                                                  @endif

                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>

                                                    </div>
                                                    <div class="mega-custom-html col-sm-12">
                                                        <a href="#"><img src="public/assets/user/data/banner-megamenu.jpg" alt="Banner"></a>
                                                    </div>
                                                </div>
                                                <div class="mega-custom-html col-sm-12">
                                                    <a href="#"><img src="public/assets/user/data/banner-megamenu.jpg" alt="Banner"></a>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </ul>
                        <div class="all-category"><span class="open-cate">All Categories</span></div>
                    </div>
                </div>
            </div>
            <div id="main-menu" class="col-sm-9 main-menu">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <i class="fa fa-bars"></i>
                            </button>
                            <a class="navbar-brand" href="#">MENU</a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <?php
                                if (!empty($menu_data->menu))
                                {
                                    foreach ($menu_data->menu as $data)
                                    {
                                        if (empty($data->group))
                                        {
                                            
                                            ?>
                                            @if($data->is_login_required == 1 && isset($user_details) && !empty($user_details))
                                            <li class="active"><a href="{{URL::asset($data->url)}}">{{$data->title or ''}}</a></li>
                                            @else
                                             <li class="active"><a href="{{URL::asset($data->url)}}">{{$data->title or ''}}</a></li>
                                            @endif
                                            
                                                <?php
                                        }
                                        else if (!empty($data->group))
                                        {
                                            ?>
                                            <li class="dropdown">
                                                @if($data->is_login_required == 1&& isset($user_details) &&!empty($user_details))
                                                <a href="category.html" class="dropdown-toggle" data-toggle="dropdown">{{$data->title or ''}}</a>
                                                 @else
                                                 <a href="category.html" class="dropdown-toggle" data-toggle="dropdown">{{$data->title or ''}}</a>
                                                 @endif
                                                 
                                                <ul class="dropdown-menu mega_dropdown" role="menu" style="width: 830px;">

                                                    <?php
                                                    foreach ($data->group as $grp)
                                                    {
                                                        ?>
                                                        <li class="block-container col-sm-3">
                                                            <ul class="block">


                                                                <?php
                                                                foreach ($grp as $group)
                                                                {
                                                                    ?>
                                                                     @if($group->is_login_required == 1&& isset($user_details) &&!empty($user_details))
                                                                    <li class="link_container group_header"><a href="{{URL::asset($group->url)}}">{{$group->title or ''}}</a></li>
                                                                    @else
                                                                    <li class="link_container group_header"><a href="{{URL::asset($group->url)}}">{{$group->title or ''}}</a></li>
                                                                    @endif
                                                                    <?php
                                                                    if (!empty($group->links))
                                                                    {
                                                                        foreach ($group->links as $link)
                                                                        {
                                                                            ?>
                                                                            @if($group->is_login_required == 1&& isset($user_details) &&!empty($user_details))
                                                                              <li class="link_container"><a href="{{URL::asset($link->url)}}">{{$link->title or ''}}</a></li>
                                                                            @else
                                                                              <li class="link_container"><a href="{{URL::asset($link->url)}}">{{$link->title or ''}}</a></li>
                                                                            @endif
                                                                             
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>




                                                            </ul>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        </ul>
                                        <?php
										}
										?>



                                    <?php
                                }
                            }
                            ?>
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
<div id="form-search-opntop">
</div>
<div id="user-info-opntop">
    <div id="user-info-top" class="user-info pull-right">
        <!--div class="dropdown">
            <a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account</span></a>
            <ul class="dropdown-menu mega_dropdown" role="menu">
                <li><a href="login.html">Login</a></li>
                <li><a href="#">Compare</a></li>
                <li><a href="#">Wishlists</a></li>
            </ul>
        </div-->
    </div>
</div>
<!--div id="shopping-cart-box-ontop" style="display: block;">
    <i class="fa fa-shopping-cart"></i>
    <div class="shopping-cart-box-ontop-content">
    </div>
</div-->
