@extends('layouts.content_page')
@section('pagetitle')
Help & Faq
@stop
@section('breadcrumb')
<div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
			<li>Home</li>
			<li><a href="user/faq/view">Help Centre</a></li>
			<li class="active">FAQ's</li>
      </ol>
</div>
@stop
@section('contents')
<div class="page-content">
	<div class="row">
		<div class="col-sm-9">
			<!-- leftbar -->
			<form id="faq_search_form" class="well" action="{{URL::to('helper/faq/search-query')}}" method="get">
				<!-- searchform -->
				<b>Assistance at your finger tips</b> <span class="text-muted">(ex : Returns, Exchange, Damaged product)</span>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-group" id="mail-box">
							<input type="text" class="form-control" id="search_term" name="search_term" placeholder="Search">
							<span class="input-group-btn">
							<button type="submit" class="btn btn-primary" id="search_btn">Search</button>
							</span>
						</div>
					</div>
				</div>
				<!-- searchform -->
			</form>
			@if(empty($search_query_based))  
			<div class="row">
				<div class="col-sm-12" id="faq_queries_container">
					<div class="panel-body faq_qry">
						<div class="panel-group">
							@if(!empty($qaCatwise))	
							@foreach($qaCatwise as $catkey=>$qaItemcat)
							<div class="panel panel-default">
								<!-- qpanel -->
								<div class="panel-heading">
									<h4 class="panel-title"><a data-toggle="collapse" href="#{{$qaItemcat->category->category_slug}}">{{$qaItemcat->category->category_name}}</a></h4>
								</div>
								<div id="{{$qaItemcat->category->category_slug}}" class="panel-collapse collapse">
									<div class="panel-body">
										<ol class='qaitem_list'>
										@foreach($qaItemcat->qalist as $qakey=>$qaItem) 
										<li>{{$qakey+1}} .<a href="javascript:void();" data-toggle="collapse" data-target="#{{$catkey}}faq{{$qakey}}"><b>{{$qaItem->title}}</b></a>
										<div id="{{$catkey}}faq{{$qakey}}" class="collapse" style='margin-left:15px;'>								{{$qaItem->description}}</div></li>
										@endforeach											
										@if(!empty($qaItemcat->related_catlist))
										<?php $related_cat = new stdClass(); ?>
										<li><a href="javascript:void();" data-toggle="collapse" data-target="#{{$catkey}}"><b>Related Category</b></a>
										<div id="{{$catkey}}" class="collapse" style='margin-left:15px;'>
										@foreach($qaItemcat->related_catlist as $related_category=>$catval)
										{{$related_cat->id =$related_category}}
										<a href="{{URL::to('help-center/faq/'.$catval->category_slug)}}"><b>{{$catval->title}}</b></a><br/>
										@endforeach
										</div>
										</li> 
										@endif	
										</ol>
									</div>
								</div>
								<!-- qpanel -->
							</div>
							@endforeach	
							@endif						
						</div>
					</div>
				</div>
			</div>
			@endif
			@if(!empty($search_query_based))  
			
				<div id="faq_queries_search_container">
				<div class="panel panel-default">
					<!-- qpanel -->
					<div class="panel-heading">
						<h4 class="panel-title"><a data-toggle="collapse" href=""><div id="searched_query_msg">{!! isset($search_term) ? 'Showing results for <strong>'.$search_term.'</strong>':'' !!}</div></a></h4>
					</div>
					<div class="panel-body">
					<?php $i=1;?>
					<ol class="qaitem_faq_sear" start="number">
					@if(!empty($content))
					  @foreach($content as $faq_ques_ans)
						<li>{{$i.'.'}}<a href="javascript:void();" data-toggle="collapse" data-target="#{{$faq_ques_ans->id}}faq"><strong>{{$faq_ques_ans->title}}</strong></a>
						<div id="{{$faq_ques_ans->id}}faq" class="collapse" style='margin-left:15px;'>
						{!!$faq_ques_ans->description!!}
						</div>
						 <?php $i++;?>
						</li>
						@endforeach
						@endif             
					</ol>
					</div>
				</div>
				</div>
			@endif				
			<!-- leftbar -->
		</div>
		<div class="col-sm-3">
			<!-- rightbar -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h5>Issues still not resolved?</h5>
				</div>
				<div class="panel-body">
					<a href="{{URL::to('support/contact-us')}}" class="btn btn-md btn-primary">Contact Us</a>
				</div>
			</div>	
			@if(!empty($feature_faqs))	
			<div class="panel panel-default">      
				<div class="panel-heading">
					<h5>Frequently asked Question</h5>			   
				</div>
				<div class="panel-body">					
					<ol class='qaitem_list' type="1">
						<?php $i=1;?>
						@foreach($feature_faqs as $qaItem) 
						@if(!empty($qaItem->is_feature))
						<li>{{$i.'.'}}<a href="javascript:void();" data-toggle="collapse" data-target="#{{$qaItem->id}}faq"><b>{{$qaItem->title}}</b></a>
						<div id="{{$qaItem->id}}faq" class="collapse" style='margin-left:15px;'>
						{{$qaItem->description}}
						</div>
						<?php $i++;?>
						</li>
						@endif
						@endforeach						
					</ol>					
				</div>
			</div>
			@endif
			<!-- rightbar -->
		</div>
	</div>
</div>
@stop
@section('scripts')
@stop 