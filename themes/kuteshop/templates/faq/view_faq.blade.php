@extends('member.layouts.dashboard')
@section('pagetitle')
FAQ
@stop
@section('layoutContent')


<div class="pageheader">
    <h2><i class="fa fa-money"></i>FAQ's </h2>
    <div class="breadcrumb-wrapper"> <span class="label">You are here:</span>
            <ol class="breadcrumb">
             <li><a href="user/dashboard">Dashboard</a></li>
             <li><a href="user/faq/view">Help</a></li>
             <li class="active">FAQ's</li>
          </ol>
    </div>
</div>
<div class="contentpanel">
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body" >
          <div class="panel-group" id="accordion">
          @if(!empty($faqs))
          @foreach($faqs as $faq)
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse_{{$faq->id}}">{{$faq->title}} </a> </h4>
              </div>
              <div id="collapse_{{$faq->id}}" class="panel-collapse collapse">
                <div class="panel-body">
                  {{$faq->description}}
                </div>
              </div>
            </div>
            @endforeach
            @endif
          </div>
        </div>
        <!-- panel body -->
      </div>
      <!-- panel default -->
    </div>
    <!-- col-md-12 -->
  </div>
</div>
@stop 