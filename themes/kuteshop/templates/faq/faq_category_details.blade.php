@extends('member.layouts.login')
@section('pagetitle')
FAQ
@stop
@section('contents')


<div class="pageheader">
    <h2><i class="fa fa-money"></i>FAQ's </h2>
    <div class="breadcrumb-wrapper"> <span class="label">You are here:</span>
            <ol class="breadcrumb">
             <li><a href="user/dashboard">Dashboard</a></li>
             <li><a href="user/faq/view">Help</a></li>
             <li class="active">FAQ's</li>
          </ol>
    </div>
</div>
<div class="contentpanel">
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body" >
          <div class="panel-group" id="accordion">
         
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> Browse From Categories </h4>
              </div>
                <div class="panel-body">
                    <div class="row">
                    <div class="col-sm-6">
                      <h4>{{$faq_category->category}}</h4>
                    
                            @if(!empty($faq_details))
                             @foreach($faq_details as $dtls)
                             <div>
                                  <ul><li><a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse_{{$dtls->id}}">{{$dtls->title}}</a></li> 
                                  </ul>                             </div>
                            <div id="collapse_{{$dtls->id}}" class="panel-collapse collapse">
                              {{$dtls->description}}
                            </div>
                          
                            @endforeach
                           @endif
                            <div>
                                  <ul>
                                  <li><a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse_others">Others</a></li>
                                  </ul> 
                            </div>
                            
                             <div id="collapse_others" class="panel-collapse collapse">
                            @if(!empty($related_cat_list))
                             @foreach($related_cat_list as $related_dtls)
                              <p><a href="{{$related_dtls->slug}}" data-id="{{$related_dtls->category_id}}">View More Queries Related To{{$related_dtls->category_name}}</a></p>
                              @endforeach
                           @endif
                            
                            </div>
                            
                     </div>
                
                     <div class="col-sm-6">
                          @if(!empty($cat_list))
                            @foreach($cat_list as $category)
                            <p><a href="{{$category->slug}}" data-id="{{$category->category_id}}">{{$category->category_name}}</a></p>
                           @endforeach
                         @endif 

                     </div>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- panel body -->
      </div>
      <!-- panel default -->
    </div>
    <!-- col-md-12 -->
  </div>
</div>
@stop 