@extends('layouts.content_page')
@section('pagetitle')
Authentication
@stop
@section('contents')
<style>

</style>
<div class="page-content">
    <div class="row">


        <div id="login_register_container">
            <div class="col-sm-6">
                <div class="box-authentication">
                    <div class="signup_cont">
                        <h3> New User? Signup</h3>
                        <p><b>We do not share your personal details with anyone.</b></p>
<!--<div id="msg"></div>-->
                        <label for="email_register">Email/Mobile no</label>
                        <input id="email_mobile" name="email_mobile" placeholder="Email or Mobile no" type="text" class="form-control">
                        <span id="email_phone_error" class="email_phone_error"></span>

                    </div>
                    <div id="otp_validate" style="display:none">
                        <h3> Please Enter Your OTP</h3>
<!--<div id="otp_fld"></div>-->

                        <label for="">OTP</label>
                        <div class="input-group">
                            <input type="text" placeholder="Enter Your OTP" id="otp_code" class="form-control"/>
                            <span class="err_msg help-block"></span>
                            <span class="input-group-btn">
                                <button class="btn btn-primary" id="otp_btn">Continue</button>
                            </span>
                        </div>




                        <a id=  "reset_otp_btn" style=" margin-top: 10px" href="javascript:void(0)"> Resend&nbsp;OTP</a>




                    </div>
                    <div id="otp_valid_reg" style="display:none">
                        <h3 id="new-acc"> New Account</h3>
<!--<div id="msg1"></div>-->
                        <form method="post" id="signupfrm" name="signup" action="<?php echo URL::to('user/register'); ?>">
                            <div class="email_reg" style="display:none">
                                <label for="username">Email</label>
                                <input id="email_id" name="email_id" placeholder="Email" type="text" class="form-control" disabled="disabled">                            </div>
                            <div class="mobile_reg_sec" style="display:none">
                                <label for="username">Mobile no:</label>
                                <input id="mobile_no" name="mobile_no" placeholder="mobile_no" type="text" class="form-control" disabled="disabled">
                            </div>
                            <label for="username">Full name</label>
                            <input id="full_name" name="full_name" placeholder="Full name" type="text" class="form-control">
                            <label for="password">Password</label>
                            <input id="password" name="password" placeholder="xxxxxx" type="password" class="form-control">
                            <button class="button" id="sign_up_btn" type="submit" style="display:none"><i class="fa fa-user"></i> Create  account</button>
                        </form>
                    </div>
                    <button class="button" id="sign_up"><i class="fa fa-user"></i> Create  account</button>

                </div>
            </div>
            <div class="col-sm-6">
                <div class="box-authentication">
                    @if(Session::get('deny-msg'))
                    <div class="alert alert-info"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ Session::get('deny-msg')}}</div>
                    @endif
                    @if(Session::get('success'))

                    <div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ Session::get('success')}}</div>


                    @elseif (Session::get('error'))
                    <div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ Session::get('error')}}</div>
                    @endif
                    @if (Session::has('msg'))
                    <div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{ Session::get('msg') }}</div>
                    @endif
                    <div id="login_err"></div>
                    <form method="post" id="loginfrm" action="<?php echo URL::to('/login'); ?>">
                        <h3>Existing User? Signin</h3>
                        <label for="emmail_login">User Name</label>
                        <input id="username" name="username" placeholder="Email or Mobile no" type="text" class="form-control">
                        <div id="signin_username_error" style="color:red; font-size:12px"></div>
                        <label for="password_login">Password</label>
                        <input id="password" name="password" placeholder="Password" type="password" class="form-control">
                        <div id="signin_password_error" style="color:red; font-size:12px"></div>
                        <p class="forgot-pass"><a href="#" id="forgot_password">Forgot your password?</a></p>
                        <button class="button"><i class="fa fa-lock"></i> Sign in </button>
                    </form>
                </div>
            </div>
        </div>
        <div id="forgot_password_container" style="display:none">
            <div class="box-authentication" id="forgot_pwd_info">
                <div id="login_err"></div>

                <h3 id="head-title">FORGOT YOUR PASSWORD?.</h3>
                <form method="post" id="forgot_form" name="forgot_form" action="<?php echo URL::to('/forgot/pwd'); ?>">
                    <div id="email_mob_forg_container">
                        <div id="forgot_err">Don't panic - we've got you covered. Give us the email address/Mobile No. you use to log in to Virob Shopping and we'll send you instructions for resetting your password.</div>
                        <div id="succ_msg"></div>

                        <label for="emmail_login">Email ID/Mobile no</label>
                        <input id="email_mob_verification" name="email_mob_verification" placeholder="Email ID/Mobile no" type="text" class="form-control">



                        <button type="submit" class="button" id="sub_butt"><i class="fa fa-lock"></i> Continue </button>

                    </div>
                    <div id="otp_forg_container" style="display: none">
                        <h3> Please Enter Your OTP</h3>
                        <div id="otp_succ"></div>
                        <div id="forg_fld"></div>
                        <input type="text" placeholder="Enter Your OTP" id="forg_otp_code" class="form-control"/>
                        <span class="help-block forg_otp_err" style="display: none">Please enter Your OTP code</span>

                        <button class="button" id="forg_otp_btn"><i class="fa fa-user"></i> Continue</button>
                        <p> <a href="javascript:void(0)" id="resend_forg_otp_btn"> Resend OTP</a>

                    </div>
                </form>


                <div class="copy_t">
                    <small style="" id="bck_login">Never mind, <a id="go_to_login" href="javascript:void(0)">send me back to the sign-in screen</a></small></div>
            </div>
            <div class="reset_pwd-forg" style="display: none">
                <div class="col-sm-5">
                    <div class="box-authentication">       
                        <h3>Change New Password</h3>
                        <hr>
                        <div id="new_pwd_succ"></div>
                        <form id="change_pwd_forg_form" name="change_pwd_forg" method="post" action="{{URL::to('user/reset/pwd')}}">
                            <div id="reset_err">A strong password is combination of letters and punctuation marks.its must be 6 charecters long</div>
                            <label for="forg_password">Password</label>
                            <input type="password" name="forg_new_pass"  id="forg_new_pass" class="form-control"/>
                            <label for="forg_password">Confirm Password</label>    
                            <input type="password" name="confirm_new_pass"   class="form-control"/>
                            <input type="hidden" name="reset_code"  id="reset_code" class="form-control"/>

                            <button class="button"  id="forg_new_pass_btn"><i class="fa fa-user"></i> Update</button>
                        </form>

                    </div>



                </div>
            </div>
        </div>
        @stop
        @section('scripts') 
        <script src="{{ URL::to(Config('ThemeProviders').'/login.js') }}"></script> 
        <script src="{{ URL::to(Config('ThemeProviders').'/forgot.js') }}"></script> 
        @stop 