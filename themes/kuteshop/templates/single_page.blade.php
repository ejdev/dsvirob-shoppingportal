@extends('layouts.product_page')
@section('pagetitle','product_name')
@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{URL::to(Config('ThemeAsset').'/lib/fancyBox/jquery.fancybox.css') }}">
@stop
@section('content')
<style>
.prog_hght {
    height: 5px;
	width: 150px;
	margin-bottom: 0px; 
}

#product .pb-right-column .form-option .attributes .attribute-label { width : auto; }
</style>
<div class="center_column col-xs-12 col-sm-12" id="center_column">
    <div id="product" style="display:{{isset($product)?'block':'none'}}">
    </div>
    <div id="product-sellers" style="display:{{isset($productSellers)?'block':'none'}}">
    </div>
    <div id="seller-review" style="display:{{isset($supplierReview)?'block':'none'}}">
    </div>
    <div id="seller-details" style="display:{{isset($supplierDetails)?'block':'none'}}">
    </div>                                   
</div>
@stop
@section('scripts')
<script src="{{ URL::to(Config('ThemeAsset').'/lib/jquery.elevatezoom.js') }}"></script>
<script src="{{ URL::to(Config('ThemeAsset').'/lib/fancyBox/jquery.fancybox.js') }}"></script>
<script src="{{ URL::to(Config('ThemeProviders').'/product_details.js') }}"></script>
<script src="{{ URL::to(Config('ThemeProviders').'/cart.js') }}"></script>
@stop
