$(document).ready(function () {
    $('#subscribe-form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        if ($('#email_id', form).val() != '') {
            $.ajax({
                type: "POST",
                url: APIURL + 'subscribe',
                data: form.serialize(),
                dataType: 'JSON',
                beforeSend: function () {
                    loadPreloader();
                },
                success: function (data) {
                    removePreloader();
                },
                error: function () {
                    removePreloader();
                }
            });
        }
    });
});
