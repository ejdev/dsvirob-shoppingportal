$.fn.extend({
    addSlider: function (data) {
        var _this = $(this);
        _this = $.extend(_this, data);
        _this.print = function () {
            _this.find(_this.featuredSlider).html('');
            _this.find(_this.imgSlider).html('');
            $.each(_this.sliders, function (k, slider) {
                if (slider.slider_type == Constants.SLIDER_TYPE.FEATURED) {
                    _this.find(_this.featuredSlider).append(_this.printFeaturedSlider(slider));
                }
                else if (slider.slider_type == Constants.SLIDER_TYPE.IMG) {
                    _this.find(_this.imgSlider).append(_this.printImgSlider(slider));
                }
            });
            $(".owl-carousel").each(function (index, el) {
                var config = $(this).data();
                config.navText = ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'];
                config.smartSpeed = "300";
                if ($(this).hasClass('owl-style2')) {
                    config.animateOut = "fadeOut";
                    config.animateIn = "fadeIn";
                }
                $(this).owlCarousel(config);
            });
            $('#contenhomeslider').bxSlider(
                    {
                        nextText: '<i class="fa fa-angle-right"></i>',
                        prevText: '<i class="fa fa-angle-left"></i>',
                        auto: true,
                    }
            );
        };
        _this.printFeaturedSlider = function (slider) {
            return $('<div>').attr({class: 'category-featured'}).append([
                $('<nav>').attr({class: 'navbar nav-menu nav-menu-green show-brand'}).append([
                    $('<div>').attr({class: 'container'}).append([
                        $('<div>').attr({class: 'navbar-brand'}).append([
                            $('<a>').attr({}).append([
                                slider.title
                            ])
                        ])
                    ])
                ]),
                $('<div>').attr({class: 'product-featured clearfix'}).append([
//                    $('<div>').attr({class: 'banner-featured text-center'}).append([
//                        //$('<div>').attr({class: 'featured-text'}).append($('<span>').append(slider.title)),
//                        $('<div>').attr({class: 'banner-img'}).append($('<a>').attr({}).append($('<img>').attr({src: slider.img_path}))),
//                        $('<h2>').append(slider.title)
//                    ]),
                    $('<div>').attr({class: 'product-featured-content'}).append([
                        $('<div>').attr({class: 'product-featured-list'}).append([
                            $('<ul>').attr({class: 'product-list owl-carousel', 'data-dots': 'false', 'data-loop': 'true', 'data-nav': 'true', 'data-margin': '0', 'data-autoplayTimeout': '1000', 'data-autoplayHoverPause': 'true', 'data-responsive': '{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'}).append(function () {
                                var blocks = [];
                                $.each(slider.blocks, function (k, block) {
                                    blocks.push(_this.printFeaturedBlock(block));
                                });
                                return blocks;
                            })
                        ])
                    ])
                ])
            ]);
        };
        _this.printFeaturedBlock = function (block) {
            return $('<li>').append([
                $('<a>').attr({href: block.price_info.url}).append([
                    $('<div>').attr({class: 'left-block'}).append([
                        $('<a>').attr({}).append($('<img>').attr({src: block.img_path, alt: block.title}))
                    ]),
                    $('<div>').attr({class: 'right-block'}).append([
                        $('<h4>').attr({class: 'product-name text-center'}).append([
                            $('<a>').attr({class: ''}).append(block.subtitle)
                        ]),
                        $('<h3>').attr({class: 'product-name text-center'}).append([
                            $('<a>').attr({class: ''}).append(block.title)
                        ]),
                        $('<h5>').attr({class: 'product-name text-center'}).append([
                            $('<a>').attr({class: ''}).append(block.price_info.currency_code + ' ' + block.price_info.selling_price)
                        ])
                    ])
                ])
            ]);
        };
        _this.printImgSlider = function (slider) {
            return $('<ul>').attr({id: 'contenhomeslider'}).append(function () {
                var blocks = [];
                $.each(slider.blocks, function (k, block) {
                    blocks.push(_this.printImgBlock(block));
                });
                return blocks;
            });
        };
        _this.printImgBlock = function (block) {
            return $('<li>').append([
                $('<a>').attr({href: block.url}).append([               
                    $('<img>').attr({src: block.img_path, alt: block.title})
                ])
            ]);
        };
        _this.loadSliders = function () {
            $.ajax({                
                url: ajaxUrl + 'get-sliders',				
                type: "POST",
                dataType: "JSON",
                data: {page: _this.page},
                success: function (data) {					
                    _this = $.extend(_this, data);
                    _this.print();
                }
            });
        };
        if (_this.sliders == undefined || !_this.sliders.length) {
            _this.loadSliders();
        }
        else {
            _this.print();
        }
    }
});
$(document).ready(function () {
    $(document.body).addSlider({page: 'home', featuredSlider: 'div#slider-container', imgSlider: 'div#img-slider'});
});
