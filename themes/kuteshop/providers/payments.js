$.fn.extend({
    initPayment: function (data) {
        var _this = this;
        _this.modes = {};	
        _this.getPaymentModes = function () {
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: ajaxUrl + 'get-paymodes',
                success: function (data) {
					_this.modes = data.payment_modes;					
                    _this.UpdatePaymentMode();
                }
            });
        };
        _this.UpdatePaymentMode = function () {
            _this.html('');
            _this.append(function () {
                var modes = [];
                if (_this.modes.length > 0) {
                    for (var i in  _this.modes) {
                        modes.push(
						$('<div>').attr({class: 'radio'})
						.append(
								$('<label>')
								.append([
									$('<input>').attr({type: 'radio', name: 'paymode_id', value: _this.modes[i].id, data: _this.modes[i].balance}),
									_this.modes[i].name
								])
							)
						);
                    }
					
                }
                else {
                    _this.append($('<p>').attr({class: 'text-danger'}).text('No Payment Modes to Proceed'));
                }
                return modes;
            });
        };
		
        if (_this.length) {			
            _this.getPaymentModes();			
            return _this;
        }
        else {
            return false;
        }
    },
	initWallet: function (data) {
        var _this = this;        
        _this.wallet = {};
		_this.getWalletTypes = function () {
            $.ajax({
                type: "POST",
                dataType: 'JSON',
                url: ajaxUrl + 'get-wallet-types',
                success: function (data) {												
					//var	s_total = data[0].cart_total.replace(",", "");						
					var	s_total = data[0].cart_total;
					console.log(s_total);					
					$('#total_amount').html("<strong>Total : </strong> <span class='pull-right'><input type='text' id='sub_total_amt' value='"+s_total+"'></span><strong>Bonus Wallet : </strong><span class='pull-right'><input type='text' id='b_wal' value='0.00'></span><hr><strong>Balance : </strong><span class='pull-right'><input type='text' id='final_amt' value='"+s_total+"'></span><input type='hidden' id='bonus_wallet_balance' value='"+data[0].balance+"'>");
					_this.wallet = data;					
                    _this.WalletTypes();
                }
            });
        };
		_this.WalletTypes = function () {
            _this.html('');
            _this.append(function () {
                var wallet = [];
				//console.log('456');
                if (_this.wallet.length > 0) {						
                    for (var i in  _this.wallet) {						
                        wallet.push(
						$('<div>').attr({class: 'radio'})
						.append(
								$('<label>')
								.append([
									$('<input>').attr({type: 'checkbox', class: 'wallets', name: 'wallets', value: '0',  id: _this.wallet[i].wallet_id}),
									_this.wallet[i].wallet_name
								])
							)
						);													
                    }					
                }
                else {
                    _this.append($('<p>').attr({class: 'text-danger'}).text('No Wallets'));
                }
                return wallet;
            });
        };
        
		
        if (_this.length) {
			_this.getWalletTypes();           			
            return _this;
        }
        else {
            return false;
        }
    }
	
});

$(document).ready(function () {
    var p = $('#payment').initPayment();
    var w = $('#wallet').initWallet();		
	var balance = '';
	var isBonusWallet = false;
	var isMyCashWallet = false;
	 
	$(document.body).on('click', '.wallets', function (e) {
		var s_tot = $('#sub_total_amt').val();	
		var	subtotal = s_tot.replace(",", "");		
		//$('#final_total_amt').html(subtotal);
	    var bonus_wallet_balance = parseFloat($('#bonus_wallet_balance').val());	
		//console.log($.type( bonus_wallet_balance ) );
		var wallet_balance = parseFloat($(this).val());
		//console.log($.type( wallet_balance ) );
		var wallet_type = $(this).attr('id');
		var balance = subtotal;
		
        if ($(this).is(':checked')) {
			
			if (wallet_type == Constants.BONUS_WALLET) {		
				isBonusWallet = true;				
				if (parseFloat(bonus_wallet_balance) > parseFloat(wallet_balance)) {					
					balance = parseFloat(subtotal) - parseFloat(wallet_balance);				
					$('#b_wal').val(wallet_balance.toFixed(2));
					$('#final_amt').val(balance.toFixed(2));
				} else {					
					balance = parseFloat(subtotal) - parseFloat(bonus_wallet_balance);
					$('#b_wal').val(bonus_wallet_balance.toFixed(2));
					$('#final_amt').val(balance.toFixed(2));
				}
			}		
        }
		
		if (!$(this).is(':checked')) {
			if (wallet_type == Constants.BONUS_WALLET) {
				isBonusWallet = false;				
				//$('#final_total_amt').html(subtotal);
				$('#b_wal').val(0.00);
				$('#final_amt').val(subtotal);				
			}			 
        }				
    });
	
	
	$(document.body).on('change', 'input[name="paymode_id"]', function (e) {			
		var subtotal = $('#sub_total_amt').val();	
		//var	subtotal = s_tot.replace(",", "");	
		var final_amt = $('#final_amt').val();
		//alert(subtotal);
		var wallet_balance = $(this).attr('data');			
		
		if ($(this).val() == 7) {			
			if (isBonusWallet == false) {
				if (parseInt(wallet_balance) > parseInt(subtotal)) {		
					//$('input[name="wallets"]').attr("disabled", true);
					var isMyCashWallet = true;									
					$('#final_amt').val(0.00);					
				} else {
					alert('Insufficient Balance');
					this.checked = false;
					//$('input[name="wallets"]').attr("disabled", false);
				} 
			} else {
				//$('input[name="wallets"]').attr("disabled", true);
				if (parseInt(wallet_balance) > parseInt(final_amt)) {				
					var isMyCashWallet = true;									
					$('#final_amt').val(0.00);					
				} else {
					alert('Insufficient Balance');
					this.checked = false;
					//$('input[name="wallets"]').attr("disabled", false);
				}
			}			
		} else {
			//$('input[name="wallets"]').attr("disabled", false);
			isMyCashWallet = false;	
			if (isBonusWallet == false) {				
				subtotal = subtotal;		
			} else {
				isBonusWallet = true;	
				var bonus = $('input[name="wallets"]').val();
				subtotal = parseFloat(subtotal - bonus).toFixed(2);							
			}				
			$('#final_amt').val(subtotal);		
		} 
		
	});
	
});
