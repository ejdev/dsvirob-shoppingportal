$(document).ready(function () {
	
    function load_wishlist() {
        $('#search_btn').click(function () {
            $("#my_wish_list").dataTable({
                "bPagination": true,
                "bLengthChange": false,
                "pageLength": 6,
                "bProcessing": true,
                "bFilter": false,
                "bAutoWidth": false,
                "sDom": "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
                "oLanguage": {
                    "sSearch": "<span>Search:</span> ",
                    "sInfo": "Showing <span>_START_</span> to <span>_END_</span> of <span>_TOTAL_</span> Products",
                    "sEmptyTable": "There is No Products to List"
                },
                "bDestroy": true,
                "bSort": false,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "type": "POST",
                    "url": ajaxUrl + 'account/wishlist',
                    "data": {
                        "search_term": $('#search_term').val(),
                    }
                },
                "columns": [
                    {
                        "data": "title",
                        "name": "title",
                        "orderable": false,
                        "render": function (data, type, row, meta) {
                            var json = $.parseJSON(meta.settings.jqXHR.responseText);
							var price = parseFloat(row.price) + parseFloat(row.partner_price);
                            var content = '';
                            content += '<li class="col-sx-12 col-sm-4" id="rmv_wish_' + row.wishlist_id + '"><div class="product-container" ><div class="left-block"> <a href="#"><img class="img-responsive" src="'+ row.imgs +'"></a><div class="quick-view"><a href="' + ajaxUrl + 'account/remove-wishlist" title="Remove my wishlist" class="heart quick" id="remove"  data-id="' + row.wishlist_id + '" ></a></div><div class="add-to-cart"><span id="add_to_cart" title="Add to Cart" class="add-to-cart" data-currency_id="' + row.currency_id + '" data-supplier_product_id="' + row.supplier_product_id + '">Add to Cart</span></div></div><div class="right-block"><h5 class="product-name" style="min-height:50px;"><a href="#">' + row.product_name + '</a></h5><div class="product-star">';

                            if (row.avg_rating != 0 && row.avg_rating != null) {
                                for (var i = 1; i <= parseInt(row.avg_rating); i++) {
                                    content += '<i class="fa fa-star"></i>';
                                }
                                for (var i = 1; i <= 5 - parseInt(row.avg_rating); i++) {
                                    content += '<i class="fa fa-star-o"></i>';
                                }
                            }
                            content += '</div><div class="content_price"><span class="price product-price">' + row.currency_symbol + price + '</span>';
                            if (row.mrp_price != row.price) {
                                content += '<span class="price old-price">' + row.currency_symbol + row.mrp_price + '</span>&nbsp;&nbsp;';
                            }
                            if (row.off_perc != 0 && row.off_perc != null) {
                                content += '<span class="offer-perc">' + row.off_perc +'% off</span>';
                            }
                            content += '</div></div></div></li>';
                            return content;
                        }
                    }
                ],
                initComplete: function (settings, json) {
                    $('thead', $("#my_wish_list")).remove();
                },
                drawCallback: function (e, settings) {
                    var content = '';
                    content += '<div id="view-product-list" class="view-product-list"><ul class="row product-list grid">  ';
                    $.each($('tr td', $("#my_wish_list")), function () {
                        content += $(this).html();
                        $(this).parent('tr').remove();
                    });
                    $('tbody', $("#my_wish_list")).html('<tr><td>' + content + '</td></tr></ul>');
                }
            });
        });
    }
    load_wishlist();

});

$(document).ready(function () {
    $('#search_btn').trigger('click');
});


$(document).on("click", "#add_to_cart", function (e) {
    e.preventDefault();    
    var sp_id = $(this).attr('data-supplier_product_id');	
    var currency_id = $(this).attr('data-currency_id');	
    $.ajax({
        type: "POST",
        url: ajaxUrl + 'product/add-to-cart',
        data: {spid: sp_id, currency_id: currency_id },
        dataType: "json",
        beforeSend: function () {
            $('#products').modal('hide');
            $(document.body).prepend('<div class="ajax_loader"><div class="loader_mask"></div></div>');
        },
        success: function (op) {
            if (op.status == 'ok') {
                $('.ajax_loader').remove();
                $("#item_count").html("<span class='badge'>" + op.product_count + "</span>");
                SuccessMsg(op.msg);
                $('#search_btn').trigger('click');

            }
        }
    });
});


function SuccessMsg(data) {
    jQuery.gritter.add({
        title: data,
        //text: data,
        class_name: 'growl-info',
        sticky: false,
        time: '1000'
    });
}

$(document).on("click", "#remove", function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var product_id = $(this).attr('data-product_id');
    var id = $(this).attr('data-id');
    if (confirm('Are you sure you wants to remove the item from Wish List?')) {
        $.ajax({
            type: "POST",
            url: url,
            data: {id: id, product_id: product_id},
            dataType: "json",
            beforeSend: function () {
                $(document.body).prepend('<div class="ajax_loader"><div class="loader_mask"></div></div>');
            },
            success: function (op) {
                if (op.status == 'ok') {
                    $('#rmv_wish_' + id).remove();
                }
            }

        });
    }
});

