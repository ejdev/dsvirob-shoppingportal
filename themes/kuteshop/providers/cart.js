$.fn.extend({
    updateCart: function (data) {
        var _this = this,bonus_amount=0;
        _this.html('');
        _this.cartBlock = $('<div>').attr({class: 'cart-block'}).append(
                [
                    $('<div>').attr({class: 'cart-block-content'}).append(
                            [
                                $('<h5>').attr({class: 'cart-title'}).append(data.cart_count + ' Items in my cart'),
                                $('<div>').attr({class: 'cart-block-list'}).append(function () {
                                    var ul = $('<ul>');
                                    for (var i in data.cart_content) {
									
									b_amount = data.cart_content[i].options.bonus_wallet * data.cart_content[i].qty;
									bonus_amount += b_amount;									
									//console.log(bonus_amount);
                                        ul.append(
                                                $('<li>').attr({class: 'product-info'}).append(
                                                [
                                                    $('<div>').attr({class: 'p-left'}).append([
                                                        $('<a>').attr({class: 'remove_link'}),
                                                        $('<a>').attr({'href': 'remove_link'}).append($('<img>').attr({class: 'img-responsive', 'src': data.cart_content[i].options.image_path})),
                                                    ]),
                                                    $('<div>').attr({class: 'p-right'}).append([
                                                        $('<p>').attr({class: 'p-name'}).append(data.cart_content[i].name),
                                                        $('<p>').attr({class: 'p-rice'}).append('Price: ' + data.cart_content[i].price),
                                                        $('<p>').append('Qty: ' + data.cart_content[i].qty),
                                                        $('<p>').attr({class: 'p-rice'}).append('Sub Total: ' + data.cart_content[i].subtotal)
                                                    ])
                                                ])
                                                );
                                    }
                                    return ul;
                                }),
                                $('<div>').attr({class: 'toal-cart'}).append(
                                        [                                            
											$('<div>', {class: 'row'}).append([
                                                $('<span>', {class: 'col-xs-6'}).append('Sub Total'),
                                                $('<span>', {class: 'col-xs-6 toal-price text-right'}).append(data.cart_sub_total),
                                            ]),
                                            $('<div>', {class: 'row'}).append([
                                                $('<span>', {class: 'col-xs-6'}).append('Tax'),
                                                $('<span>', {class: 'col-xs-6 toal-price text-right'}).append(data.cart_tax),
                                            ]),
                                            $('<div>', {class: 'row'}).append([
                                                $('<span>', {class: 'col-xs-6'}).append('Shipping Charge'),
                                                $('<span>', {class: 'col-xs-6 toal-price text-right'}).append(data.cart_shipping_charge),
                                            ]),
                                            $('<div>', {class: 'row'}).append([
                                                $('<span>', {class: 'col-xs-6'}).append('Total'),
                                                $('<span>', {class: 'col-xs-6 toal-price text-right'}).append(data.cart_total),
                                            ]),
                                        ]),
                                $('<div>').attr({class: 'cart-buttons'}).append($('<a>').attr({class: 'btn-check-out', href: ajaxUrl + 'checkout'}).append('Checkout'))
                            ])
                ]);
				
		console.log(parseFloat(bonus_amount).toFixed(2));		
		$('.wallets').val(parseFloat(bonus_amount).toFixed(2));
        _this.append(
                [
                    $('<a>').attr({class: 'cart-link', href: ajaxUrl + 'my-cart'}).append(
                            [
                                $('<span>').attr({class: 'title'}).append('Shopping cart'),
                                $('<span>').attr({class: 'total'}).append(data.cart_count + ' items - ' + data.cart_total),
                                $('<span>').attr({class: 'notify notify-left'}).append(data.cart_count)
                            ]),
                    _this.cartBlock
                ]
                );
				
        $('.shopping-cart-box-ontop-content').append(_this.cartBlock);
        if ($('#step-summary').length) {
            $('#step-summary').updateCartSummary(data);
        }
				
    },
    updateCartSummary: function (data) {
        var _this = this;
        _this.html('');
        _this.append(
			[
				$('<table>').attr({class: 'table table-bordered table-responsive cart_summary'}).append([
					$('<thead>').append($('<tr>').append([
						$('<th>').attr({class: 'cart_product'}).append('Product'),
						$('<th>').append('Description'),
						$('<th>').append('Avail.'),
						$('<th>').append('Unit price'),
						$('<th>').append('Qty'),
						$('<th>').append('Total')
					])),
					$('<tbody>').append(function () {
						var trs = [];
						for (var i in data.cart_content) {
							trs.push($('<tr>').append(
									[
										$('<td>').attr({class: 'cart_product'}).append([
											$('<a>').attr({'href': 'remove_link'}).append($('<img>').attr({class: 'img-responsive', alt: 'Product', 'src': data.cart_content[i].options.image_path})),
										]),
										$('<td>').attr({class: 'cart_description'}).append([
											$('<p>').attr({class: 'product-name'}).append(data.cart_content[i].name),
											$('<small>').attr({class: 'cart_ref'}).append(),
											$('<p>').append('Qty: ' + data.cart_content[i].qty),
											$('<a>').attr({href: '#', class: ' btn btn-danger btn-xs remove-from-cart', 'data-rowid': i}).append('Delete item')
										]),
										$('<td>').attr({class: 'cart_avail'}).append([
											$('<span>').attr({class: 'label label-success'}).append(data.cart_content[i].options.stock_status),
										]),
										$('<td>').attr({class: 'price'}).append(data.cart_content[i].price),
										$('<td>').attr({class: 'qty'}).append([
											$('<input>').attr({class: 'form-control input-sm change-cart-item-qty', readonly: 'readonly', type: 'number', value: data.cart_content[i].qty, 'data-rowid': i}).append(),
											$('<a>').attr({href: '#', class: 'increase-qty'}).append($('<i>').attr({class: 'fa fa-caret-up'})),
											$('<a>').attr({href: '#', class: 'decrease-qty'}).append($('<i>').attr({class: 'fa fa-caret-down'})),
										]),
										$('<td>').attr({class: 'price'}).append(data.cart_content[i].subtotal),
									]));
						}
						return trs;
					}),
					$('<tfoot>').append([                           
						$('<tr>').append([
							$('<td>').attr({colspan: 3, rowspan: 4}),
							$('<td>').append('Total'),
							$('<td>', {class: 'text-right'}).append(data.cart_count),
							$('<td>').append(data.cart_sub_total_with_tax),
						]),
						$('<tr>').append([
							$('<td>').attr({colspan: 2}).append('Tax Inclusive'),
							$('<td>').append(data.cart_tax),
						]),
						$('<tr>').append([
							$('<td>').attr({colspan: 2}).append('Shipping Charges'),
							$('<td>').append(data.cart_shipping_charge),
						]),
						$('<tr>').append([
							$('<td>').attr({colspan: 2}).append($('<strong>').text('Total')),
							$('<td>').append($('<strong>').text(data.cart_total)),
						]),
					]),
				]),
				$('<div>').attr({class: 'cart_navigation'}).append([						
					$('<a>').attr({class: 'prev-btn'}).html('Continue shopping'),						
					$('<a>').attr({class: 'next-btn', href: ajaxUrl + 'checkout'!=document.location.href?ajaxUrl + 'checkout':'#', disabled: (data.cart_count > 0) ? false : true}).html('Proceed to Checkout')
					//$('<a>').attr({class: 'next-btn', href: '#'}).html('Proceed to Checkout')
				])
			]
        );
    }
});
$(document).ready(function () {
    $(document.body).on('click', '.increase-qty', function (e) {
        e.preventDefault();
        var input = $(this).parent('td').find('input.change-cart-item-qty');
        input.val(parseInt(input.val()) + 1).trigger('change');
    });
    $(document.body).on('click', '.decrease-qty', function (e) {
        e.preventDefault();
        var input = $(this).parent('td').find('input.change-cart-item-qty');
        input.val(parseInt(input.val()) - 1).trigger('change');
    });
	
    $.ajax({
        type: "POST",       
        url: ajaxUrl + 'product/my-cart',
        dataType: 'JSON',
        beforeSend: function () {
            loadPreloader();
        },
        success: function (data) {
            removePreloader();
			console.log(data);
			
            $('#cart-block').updateCart(data);
            if (data.UserDetails != undefined) {
                $('#full_name', $('#my-cart-address .billing')).val(data.UserDetails.full_name);
                $('#address1', $('#my-cart-address .billing')).val(data.UserDetails.address1);
                $('#address2', $('#my-cart-address .billing')).val(data.UserDetails.address2);
                $('#city', $('#my-cart-address .billing')).val(data.UserDetails.city);
                $('#state_id', $('#my-cart-address .billing')).val(data.UserDetails.state_id);
                $('#country_id', $('#my-cart-address .billing')).val(data.UserDetails.country_id);
                $('#postal_code', $('#my-cart-address .billing')).val(data.UserDetails.postal_code);
                $('#mobile_no', $('#my-cart-address .billing')).val(data.UserDetails.mobile_no);
                $('#email_id', $('#my-cart-address .billing')).val(data.UserDetails.email_id);
            }
        },
        error: function () {
            removePreloader();
        }
    });
    $(document.body).on('click', '.add-to-cart-btn', function (e) {		
        e.preventDefault();
        var CurELe = $(this);		
        $.ajax({
            type: "POST",
            url: ajaxUrl + 'product/add-to-cart',
            data: {spid: CurELe.data('spid') },
            dataType: 'JSON',
            beforeSend: function () {				
                loadPreloader();
            },
            success: function (data) {										
                removePreloader();
				console.log(data);
                $('#cart-block').updateCart(data);
            }, error: function (jqXHR, exception) {
                removePreloader();
            }
        });
    });
    $(document.body).on('click', '.add-to-wish-list', function (e) {
        e.preventDefault();
        var CurELe = $(this);		
        $.ajax({
            type: "POST",
            url: ajaxUrl + 'add-to-wish-list',            
            data: {supplier_product_id: CurELe.data('supplier_product_id'), spc_id: ((CurELe.data('spc_id') != undefined) ? CurELe.data('spc_id') : null)},
            dataType: 'JSON',
            beforeSend: function () {
                loadPreloader();
            },
            success: function (data) {
				console.log(data);
                removePreloader();
                $('.my-cart-count').text(data.cart_count);
            }, error: function (jqXHR, exception) {
                removePreloader();
            }
        });
    });
    $(document.body).on('click', '.remove-from-cart', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        $.ajax({
            type: "POST",
            url: ajaxUrl + 'product/remove-from-cart',
            data: {rowid: CurEle.data('rowid')},
            dataType: 'JSON',
            beforeSend: function () {
                loadPreloader();
            },
            success: function (data) {
                removePreloader();
                $('#cart-block').updateCart(data);
            }, error: function () {
                removePreloader();
            }
        });
    });
    $(document.body).on('change', '.change-cart-item-qty', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        $.ajax({
            type: "POST",
            url: ajaxUrl + 'product/change-cart-item-qty',
            data: {rowid: CurEle.data('rowid'), qty: parseInt(CurEle.val())},
            dataType: 'JSON',
            beforeSend: function () {
                loadPreloader();
                CurEle.attr('disabled', true);
                CurEle.parent('td').find('.increase-qty').attr('disabled', true);
                CurEle.parent('td').find('.decrease-qty').attr('disabled', true);
            },
            success: function (data) {
                removePreloader();
                CurEle.attr('disabled', false);
                CurEle.parent('td').find('.increase-qty').attr('disabled', false);
                CurEle.parent('td').find('.decrease-qty').attr('disabled', false);
                $('#cart-block').updateCart(data);
            }, error: function () {
                removePreloader();
            }
        });
    });
    $(document.body).on('click', '.direct-buyNow', function (e) {
        e.preventDefault();
        var CurELe = $(this);
        $.ajax({
            type: "POST",
            url: ajaxUrl + 'products/direct-buy-now',
            data: {supplier_product_id: CurELe.data('supplier_product_id'), spc_id: ((CurELe.data('spc_id') != undefined) ? CurELe.data('spc_id') : null)},
            dataType: 'JSON',
            beforeSend: function () {
                loadPreloader();
            },
            success: function (data) {
                removePreloader();
                $('.my-cart-count').text(data.cart_count);
                CurELe.remove();
            }, error: function () {
                removePreloader();
            }
        });
    });
    $(document.body).on('click', '.buyNow', function (e) {
        e.preventDefault();
        var CurELe = $(this);
        $.ajax({
            type: "POST",
            url: ajaxUrl + 'products/buy-now',
            dataType: 'JSON',
            beforeSend: function () {
                loadPreloader();
            },
            success: function (data) {
                removePreloader();
                $('.my-cart-count').text(data.cart_count);
                CurELe.remove();
            }, error: function () {
                removePreloader();
            }
        });
    });
    $(document.body).on('click', '.changeOrderDispachAddress', function (e) {
        e.preventDefault();
        var CurELe = $(this);
        var data = $(this).closest('form').serialize();
        $.ajax({
            type: "POST",
            url: ajaxUrl + 'products/change-order-dispach-address',
            dataType: 'JSON',
            data: data,
            beforeSend: function () {
                loadPreloader();
            },
            success: function (data) {
                removePreloader();
                $('.my-cart-count').text(data.cart_count);
                CurELe.remove();
            }, error: function () {
                removePreloader();
            }
        });
    });
    $('ul.step li').on('click', function (e) {
        e.preventDefault();
        var CurEle = $(this), CurDiv = $($('ul.step li.current-step').find('a').attr('href'));
        if (CurEle.hasClass('clickable')) {
            $('.step-content .steps').hide();
            $('ul.step li.current-step').removeClass('current-step');
            CurEle.addClass('current-step');
            CurDiv = $($('ul.step li.current-step').find('a').attr('href'));
            CurDiv.show();
        }
    });
    $('ul.step li:first').addClass('clickable');
    $('ul.step li:first').trigger('click');
    $('.step-content').on('click', '.prev-btn,.next-btn', function (e) {
        e.preventDefault();
        var CurEle = $(this);		
			
        if (CurEle.attr('href') == '' || CurEle.attr('href') == '#') {
			var CurDiv = $(this).parents('div.steps');
			var index = $(CurDiv, $('.step-content')).index();	                 
            if (index >= 0 && $('ul.step li:eq(' + index + ')').length) {
                if ($('form', CurDiv).length) {
				
                    $.ajax({
                        type: "POST",
                        url: $('form', CurDiv).attr('action'),
                        dataType: 'JSON',
                        data: $('form', CurDiv).serialize(),
                        beforeSend: function () {
                            loadPreloader();
                        },
                        success: function (data) {
							//console.log(data);
                            removePreloader();
							if (data.url) {
								console.log(data.url);
								window.location.href = data.url;
							}
                            index = index + ($(this).hasClass('prev-btn') ? -1 : 1);							                       
                            $('ul.step li:eq(' + index + ')').addClass('clickable').trigger('click');
                        },
                        error: function (xhr) {
                            removePreloader();
                            var data = xhr.responseJSON;
                            append_laravel_error(data.error, $('form', CurDiv));
                        }
                    });
                }
                else
                {
                    index = index + ($(this).hasClass('prev-btn') ? -1 : 1);
                    $('ul.step li:eq(' + index + ')').addClass('clickable').trigger('click');
                }
            }
        }
        else {
            window.document.location.href = CurEle.attr('href');
        }
    });
    $('#same-as-billing').on('change', function () {
        if ($(this).is(':checked')) {
            $('input,select', $('#my-cart-address .billing')).each(function () {
                $('#my-cart-address .shipping #' + $(this).attr('id')).val($(this).val()).trigger('change');
            });
        }
    });
});
