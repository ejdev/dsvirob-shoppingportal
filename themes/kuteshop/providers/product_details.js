var SetRatingStar = function () {
    return $('.star-rating .fa').each(function () {
        if (parseInt($('.star-rating .fa').siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
};
var productDetails = {};
$(document).ready(function () {
    $('#review_form_wrapper').hide();
});
$.fn.extend({
    productDetails: function (data) {
        var _this = $(this);
        _this.productDetails = null;
        _this.print = function () {
            if (_this.breadcrums != undefined) {
                $('.breadcrumb').html('');
                $.each(_this.breadcrums, function (k, e) {
                    $('.breadcrumb').append($('<a>').attr({href: e.url, class: 'change-category'}).text(e.categories));
                    if (k < _this.breadcrums.length - 1)
                        $('.breadcrumb').append($('<span>').attr('class', 'navigation-pipe'));
                });
            }
            if (_this.productDetails != null) {
                $('.page-heading-title').text(_this.productDetails.info.product_name);
                document.title = _this.productDetails.info.product_name;
                _this.html('');
                _this.append([					
                    $('<div>').attr({class: 'primary-box row'}).append([
                        $('<div>').attr({class: 'pb-left-column col-xs-12 col-sm-5'}).append([
                            $('<div>').attr({class: 'product-image'}).append([
                                $('<div>').attr({class: 'product-full'}).append($('<img>').attr({'id': 'product-zoom', 'data-zoom-image': _this.productDetails.imgs[0].img_path['product-details-zoom'], src: (_this.productDetails.imgs[0].img_path['product-details']) ? _this.productDetails.imgs[0].img_path['product-details'] : _this.productDetails.imgs[0].img_path})),
                                $('<div>').attr({class: 'product-img-thumb', id: 'gallery_01'}).append([
                                    $('<ul>').attr({class: 'owl-carousel', 'data-items': _this.productDetails.imgs.length, 'data-nav': 'true', 'data-dots': 'false', 'data-margin': '20', 'data-loop': 'true'}).append(function () {
                                        var imgs = [];
                                        for (var i in _this.productDetails.imgs) {
                                            imgs.push($('<li>').append($("<a>").attr({href: '#', 'data-image': _this.productDetails.imgs[i].img_path['product-details'], 'data-zoom-image': _this.productDetails.imgs[i].img_path['product-details-zoom']}).append($('<img>').attr({'id': 'product-zoom', src: _this.productDetails.imgs[i].img_path['product-details-additional']}))));
                                        }
                                        return imgs;
                                    })
                                ])
                            ]),
                        ]),
                        $('<div>').attr({class: 'pb-right-column col-xs-12 col-sm-7'}).append([
                            $('<h1>').attr({class: 'product-name'}).append(_this.productDetails.info.product_name),
                            $('<div>').attr({class: 'product-comments'}).append([
                                $('<div>').attr({class: 'product-star'}).append(function () {
                                    var stars = [];
                                    _this.productDetails.avg_rating = (_this.productDetails.ratings[0].avg_rating != null && _this.productDetails.ratings[0].avg_rating != undefined) ? _this.productDetails.ratings[0].avg_rating : 0;
                                    for (var i = 1; i <= parseInt(_this.productDetails.avg_rating); i++) {
                                        stars.push($('<i>').attr('class', 'fa fa-star'));
                                    }
                                    for (var i = 1; i <= 5 - parseInt(_this.productDetails.avg_rating); i++) {
                                        stars.push($('<i>').attr('class', 'fa fa-star-o'));
                                    }
                                    return stars;
                                }),
                                $('<div>').attr({class: 'comments-advices'}).append([
                                    $('<a>').attr({href: 'javascript:void(0)'}).append('Based on ' + _this.productDetails.ratings[0].rating_count + ' ratings'),
                                  /*   $('<a>').attr({href: 'javascript:void(0)', id: 'write_a_review'}).append([
                                        $('<i>').attr({class: 'fa fa-pencil'}),
                                        ' Write a review'
										]) */ 
								]),
								$('<input>').attr({type: 'hidden', class: 'product_title', id: 'product_title', name: 'product_title'}).val(_this.productDetails.info.product_name),														
								$('<input>').attr({type: 'hidden', class: 'currency_id', id: 'currency_id', name: 'currency_id'}).val(_this.productDetails.info.currency_id),					
								$('<input>').attr({type: 'hidden', class: 'supplier_product_id', id: 'sp_id', name: 'supplier_product_id'}).val(_this.productDetails.info.supplier_product_id),
                            ]),
                            $('<div>').attr({class: 'product-price-group'}).append(function () {
                                var elements = [];								
                                elements.push($('<span>').attr({class: 'price'}).append(_this.productDetails.info.currency_code + ' ' + _this.productDetails.info.selling_price));								
								if (_this.productDetails.info.selling_price < _this.productDetails.info.mrp_price) {
										elements.push($('<span>').attr({class: 'old-price'}).append(_this.productDetails.info.currency_code + ' ' +_this.productDetails.info.mrp_price))										 
										elements.push($('<span>').attr({class: 'discount'}).append(Math.floor(_this.productDetails.info.off_perc) + '% off'));
								}                               
                                return elements;
                            }),
                            $('<div>').attr({class: 'info-orther'}).append(function () {
                                var elements = [];
                                if (_this.productDetails.info.sku != undefined && _this.productDetails.info.sku != '') {
                                    elements.push($('<p>').append(['Item Code: ', _this.productDetails.info.sku]));
                                }
                                elements.push($('<p>').append([
                                    'Seller: ', $('<span>').attr({class: ''}).append(
                                            function () {
                                                var supplier = [];
                                                supplier.push($('<a>').attr({class: 'seller-details', href: _this.productDetails.url, 'data-supplier_code': _this.productDetails.info.supplier_code}).append(_this.productDetails.info.supplier));
                                                supplier.push(' ');
                                                if (_this.productDetails.info != undefined) {
                                                    supplier.push($('<a>').attr({href: _this.productDetails.sellers_url, class: 'fancybox product-sellers'}).append([1 + ' Other Sellers']));
                                                }
                                                return supplier;
                                            }
                                    )
                                ]));
                                //elements.push($('<p>').append(['Availability: ', $('<span>').attr({class: 'in-stock'}).html(_this.productDetails.supplier.stock_status)]));
                                //elements.push($('<p>').append(['Condition: ', _this.productDetails.supplier.condition_desc]));                                
                                return elements;
                            }),
							/* $('<div>').attr('id', 'delivery_form_wrapper').append([
								$('<form>').attr({action: 'api/v1/check_delivery_charge', method: 'post', id: 'delivery', name: 'delivery', style: 'display:none'}).append([								
									$('<p>').attr({class: 'comment-form-title'}).append([
										$('<label>').append(['Delivery', $('<span>').attr({class: 'required'}).append([])]),
										$('<input>').attr({type: 'text', class: 'form-control', id: 'title', name: 'title'})
									]),									
									$('<p>').attr({class: 'form-submit'}).append([
										$('<input>').attr({name: 'submit', type: 'submit', id: 'submit', class: 'btn btn-md btn-primary submit', value: 'Check'})
									]),
								]),
							]), */
                            $('<div>').attr({class: 'form-option'}).append(function () {
                                var attributes = [];
								//console.log($.isEmptyObject(_this.productDetails.properties));								
								if ($.isEmptyObject(_this.productDetails.properties) == false) {
									attributes.push($('<p>').attr({class: 'form-option-title'}).append(['Available Options:']));
									for (var i in _this.productDetails.properties) {
										attributes.push(_this.addProperty(_this.productDetails.properties[i]));
									}
								}
                                return attributes;
                            }),
                            $('<div>').attr({class: 'form-action'}).append([
                                $('<div>').attr({class: 'button-group'}).append([
                                    $('<a>').attr({class: 'add-to-cart-btn btn-add-cart', href: '', 'data-spid': _this.productDetails.info.supplier_product_id}).html('Add to cart')
                                ]),
                                $('<div>').attr({class: 'button-group'}).append([
                                    $('<a>').attr({class: 'wishlist add-to-wish-list', href: '', 'data-supplier_product_id': _this.productDetails.info.supplier_product_id }).append([$('<i>').attr({class: 'fa fa-heart-o' }), ' Wishlist']),
                                    $('<a>').attr({class: 'compare', href: ''}).append([$('<i>').attr({class: 'fa fa-signal'}), ' Compare'])
                                ])
                            ]),
                            $('<div>').attr({class: 'form-share'}).append([
                                $('<div>').attr({class: 'sendtofriend-print'}).append([
                                    $('<a>').attr({href: 'javascript:print();'}).append([$('<i>').attr('class', 'fa fa-print'), ' Print']),
                                    $('<a>').attr({href: 'javascript:void(0);'}).append([$('<i>').attr('class', 'fa fa-envelope-o fa-fw'), ' Send to a friend']),
                                ]),
                                $('<div>').attr({class: 'network-share'})
                            ]),
                           

                            $('<div>').attr({class: 'form-rating'}).append([
                                $('<div>').attr({class: 'col-xs-4'}).append([
                                    $('<span>').append([
                                        _this.productDetails.ratings[0].avg_ratiing,
                                        $('<i>').attr({class: 'fa fa-star'})
                                    ]),
                                    $('<span>').append([
                                        _this.productDetails.ratings[0].avg_ratiing,
                                        'Ratings'
                                    ]),
                                    $('<span>').append([
                                        _this.productDetails.ratings[0].avg_ratiing,
                                        'Reviews'
                                    ]),
                                ]),
                                $('<div>').attr({class: 'col-xs-8'}).append([
                                    $('<ul>').attr({class: 'col-xs-8'}).append(
                                            function () {
                                                var r = [], color = ['progress-bar-danger', 'progress-bar-warning', 'progress-bar-success', 'progress-bar-success', 'progress-bar-success'];
                                                for (var i = 1; i <= 5; i++) {
                                                    r.push($('<li>').attr({class: 'row'}).append([
                                                        $('<span>').attr({class: 'col-sm-2'}).append([i, $('<i>').attr({class: 'fa fa-star'}).append([])]),
                                                        $('<span>').attr({class: 'col-sm-2'}).append(['(', _this.productDetails.ratings[0]['rating_' + i], ')']),
                                                        $('<div class="pull-right">').attr({class: 'col-sm-8 progress progress-small prog_hght'}).append([
                                                            $('<div>').attr({class: 'progress-bar ' + color[i - 1], style: 'width:' + ((_this.productDetails.ratings[0]['rating_' + i] / _this.productDetails.ratings[0].rating_count) * 100) + '%'})
                                                        ])

                                                    ]));
                                                }
                                                return r;
                                            })
                                ])

                            ]),
                        ])
                    ]),
                    $('<div>').attr({class: 'product-tab'}).append([
                        $('<ul>').attr({class: 'nav-tab'}).append([
                            $('<li>').attr({class: 'active'}).append($('<a>').attr({href: '#product-detail', 'data-toggle': 'tab'}).html('Product Details')),
                            $('<li>').append($('<a>').attr({href: '#information', 'data-toggle': 'tab'}).html('Information')),
                            $('<li>').append($('<a>').attr({href: '#product-reviews', 'data-toggle': 'tab'}).html('Reviews')),
                        ]),
                        $('<div>').attr({class: 'tab-container'}).append([
                            $('<div>').attr({id: 'product-detail', class: 'tab-panel active'}).append(_this.productDetails.info.description),
                            $('<div>').attr({id: 'information', class: 'tab-panel'}).append([
                                $('<table>').attr({class: 'table'}).append(function () {
                                    var specs = [];
                                    for (var i in _this.productDetails.properties) {
                                        specs.push($('<tr>').append([
                                            $('<td>').append(_this.productDetails.properties[i].property),
                                            $('<td>').append(_this.productDetails.properties[i].value)
                                        ]))
                                    }
                                    return specs;
                                })
                            ]),
                            $('<div>').attr({id: 'product-reviews', class: 'tab-panel'}).append([
                                $('<ul>').loadReviews({post_type_id: Constants.POST_TYPE.PRODUCT, relative_post_id: _this.productDetails.info.product_id}),
                                $('<div>').attr({class: 'pagination'}),
                                $('<div>').attr({class: 'write-review'})
                            ]),
                        ])
                    ]),
                    $('<div>').pageProductBox(),
                    $('<div>').pageProductBox(),
                ]);
                if (_this.productDetails.sellers != undefined) {
                    var data = $.getURLParams();					
                    $('#product-sellers').productSellers(data, _this.productDetails, _this.productDetails.sellers);
                }
            }
            _this.updatePlugin();
        };
        _this.addProperty = function (property) {
            return $('<div>').attr({class: 'attributes'}).append([
                $('<div>').attr({class: 'attribute-label'}).append(property.property),
                $('<div>').attr({class: 'attribute-list'}).append(function () {
                    var values = [];
                    if (property.values_options_type == Constants.VALUE_TYPE.COLOR) {
                        var ul = $('<ul>').attr({class: 'list-color'});
                        $.each(property.values, function (k, value) {
                            ul.append($('<li>').attr({style: 'background:' + value.value + ';', class: ((value.selected) ? 'active' : '')}).append($('<a>').attr({href: value.url, class: 'click-update-product'}).append(value.value)));
                        });
                        values.push(ul);
                    }
                    else {
                        var ul = $('<select>').attr({class: 'change-update-product'});
                        $.each(property.values, function (k, value) {
                            ul.append($('<option>').attr({selected: value.selected, value: value.url}).append(value.value));
                        });
                        values.push(ul);
                    }
                    return values;
                }),
            ]);
        };
        _this.getProductDetails = function () {		
		    // For Partner Discount Percentage
			$.get("discount-percentage", function(pop){			
					if ((pop != 0)) {
						data.pop  = pop[0];
					}					
					$.ajax({
						type: "POST",
						url: ajaxUrl + 'product/details',
						data: data,
						dataType: 'JSON',
						beforeSend: function () {							
							loadPreloader();						
						},
						success: function (data) {						
							if (data != '' && data != null) {
								_this = $.extend(_this, data);										
								productDetails = _this.productDetails;									
								_this.print();
							}
							removePreloader();
						},
						error: function () {
							removePreloader();
						}
					});
			});			
            
        };
        _this.updatePlugin = function () {
            if ($('#product-zoom').length > 0) {
                $('#product-zoom').elevateZoom({
                    zoomType: "inner",
                    cursor: "crosshair",
                    zoomWindowFadeIn: 500,
                    zoomWindowFadeOut: 750,
                    gallery: 'gallery_01'
                });
            }
            if ($('#size_chart').length > 0) {
                $('#size_chart').fancybox();
            }
            $(".owl-carousel").each(function (index, el) {
                var config = $(this).data();
                config.navText = ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'];
                config.smartSpeed = "300";
                if ($(this).hasClass('owl-style2')) {
                    config.animateOut = "fadeOut";
                    config.animateIn = "fadeIn";
                }
                $(this).owlCarousel(config);
            });
            $(".owl-carousel-vertical").each(function (index, el) {
                var config = $(this).data();
                config.navText = ['<span class="icon-up"></span>', '<span class="icon-down"></span>'];
                config.smartSpeed = "900";
                config.animateOut = "";
                config.animateIn = "fadeInUp";
                $(this).owlCarousel(config);
            });
        };		
        _this.getProductDetails();		
    },
    productSellers: function (data, product, sellers) {
        var _this = $(this);
        _this.product = (product != undefined) ? product : null;
        _this.sellers = (sellers != undefined) ? sellers : null;
        _this.print = function () {
            _this.append([
                $('<h2>').attr({class: 'page-heading'}).append([
                    _this.product.product_name
                ]),
                $('<table>').attr({class: 'table table-stripped'}).append([
                    $('<thead>').append([
                        $('<tr>').append([
                            $('<th>').attr({class: 'text-center'}).append('Seller'),
                            $('<th>').attr({class: 'text-center'}).append('Price'),
                            $('<th>').attr({class: 'text-center'}).append('Deliery'),
                            $('<th>').attr({class: 'text-center'}).append('')
                        ])
                    ]),
                    $('<tbody>').append(function () {
                        var sellers = [];
                        $.each(_this.sellers, function (k, seller) {
                            sellers.push($('<tr>').append([
                                $('<td>').append([
                                    $('<a>').attr({'href': seller.url, class: 'seller-details', 'data-supplier_code': seller.supplier_code}).append(seller.supplier),
                                    $('<span>').attr({class: 'badge'}).append(seller.supplier_rating)]),
                                $('<td>').attr({class: 'text-right'}).append(function () {
                                    var elements = [];
                                    elements.push($('<span>').attr({class: 'price'}).append(seller.selling_price));
                                    if (seller != undefined) {
                                        elements.push($('<span>').attr({class: 'old-price'}).append(seller.price))
                                        elements.push($('<span>').attr({class: 'discount'}).append(seller.off_per));
                                    }
                                    return elements;
                                }),
                                $('<td>').attr({class: ''}).append([$('<p>').append([seller.delivery_charge, ', ', seller.mode, ' Delivery On ', seller.expected_delivery_date, ])]),
                                $('<td>').attr({class: 'text-center'}).append([
                                    $('<a>').attr({class: 'add-to-cart-btn btn-add-cart', href: '', 'data-supplier_product_id': seller.supplier_product_id, 'data-spc_id': seller.spc_id}).html('Add to cart')
                                ])
                            ]));
                        });
                        return sellers;
                    })
                ]),
            ]);
        };
        _this.getProductSellers = function () {
            $.ajax({
                type: "POST",
                url: ajaxUrl + 'products/sellers',
                data: data,
                dataType: 'JSON',
                beforeSend: function () {
                    //loadPreloader();
                },
                success: function (data) {
					//console.log(data);
                    //removePreloader();
                    if (data != '' && data != null) {
                        _this = $.extend(_this, data);
                        _this.print();
                    }
                },
                error: function () {
                    removePreloader();
                }
            });
        };
        if (_this.sellers == null) {
            _this.getProductSellers();
        }
        else {
            _this.print();
        }
    },
    writeReview: function (data) {
        var _this = $(this);
        _this.print = function () {
            _this.append([
                $('<h2>').attr({class: 'page-heading'}).append([
                    _this.seller.supplier
                ]),
                $('<div>').attr({class: 'row'}).append([
                    $('<div>').attr({class: 'col-xs-6'}).append([
                    ]),
                    $('<div>').attr({class: 'col-xs-6'}).append([
                    ])
                ])
            ]);
        }
        _this.getSupplierReview = function () {
            $.ajax({
                type: "POST",
                url: ajaxUrl + 'seller/details',
                data: data,
                dataType: 'JSON',
                beforeSend: function () {
                    //loadPreloader();
                },
                success: function (data) {
                    //removePreloader();
                    if (data != '' && data != null) {
                        _this = $.extend(_this, data);
                        _this.print();
                    }
                },
                error: function () {
                    removePreloader();
                }
            });
        };
        _this.getSupplierReview();
    },
    supplierDetails: function (data) {
        var _this = $(this);
        _this.print = function () {
            window.document.title = _this.seller.supplier;
            _this.append([
                $('<h2>').attr({class: 'page-heading'}).append([
                    _this.seller.supplier
                ]),
                $('<div>').attr({class: 'row'}).append([
                    $('<div>').attr({class: 'col-xs-6'}).append([
                        _this.seller.avg_ratiing,
                        $('<i>').attr({class: 'fa fa-star'})
                    ]),
                    $('<div>').attr({class: 'col-xs-6'}).append(function () {
                        var r = [];
                        for (var i = 1; i <= 5; i++) {
                            r.push($('<p>').attr({class: ''}).append([
                                i,
                                $('<i>').attr({class: 'fa fa-star'}),
                                '(',
                                _this.seller[i],
                                ')',
                            ]));
                        }
                        return r;
                    })
                ]),
                $('<div>').attr({class: 'row'}).append([
                    $('<ul>').loadReviews({post_type_id: Constants.POST_TYPE.SUPPLIER, relative_post_id: _this.seller.supplier_id}),
                    $('<div>').attr({class: 'pagination'})
                ])
            ]);
        }
        _this.getSupplierReview = function () {
            $.ajax({
                type: "POST",
                url: ajaxUrl + 'seller/details',
                data: data,
                dataType: 'JSON',
                beforeSend: function () {
                    //loadPreloader();
                },
                success: function (data) {
                    //removePreloader();
                    if (data != '' && data != null) {
                        _this = $.extend(_this, data);
                        _this.print();
                    }
                },
                error: function () {
                    removePreloader();
                }
            });
        };
        _this.getSupplierReview();
    },
    loadReviews: function (options) {
        options = (options !== undefined) ? options : {};
        var _this = this;
        _this.xhr = false;
        _this.currentPage = 1;
        _this.totalPages = 0;
        _this.brand_id = null;
        _this.caregory_id = null;
        _this.totalReviews = 0;
        _this.reviews = [];
        _this.options = {
            reviewsPerPage: Constants.BROWSE_REVIEWS_PAGE_LENGTH,
            url: '',
            data: {},
            success: null
        };
        $.extend(_this.options.data, options);
        _this.loadReviews = function () {
            _this.options.data.page = _this.currentPage;
            if (_this.xhr && _this.xhr.readyState !== 4) {
                _this.xhr.abort();
            }			
            _this.xhr = $.ajax({
                type: "POST",
                url: ajaxUrl + 'product/reviews',
                data: _this.options.data,
                dataType: 'JSON',
                beforeSend: function () {
                    //loadPreloader();
                },
                success: function (data) {
                    //removePreloader();
					//console.log(data);
                    _this.data = data;
                    _this.totalReviews = data.totalReviews;
                    _this.totalPages = (_this.totalReviews / _this.options.reviewsPerPage) + (((_this.totalReviews % _this.options.reviewsPerPage) > 0) ? 1 : 0);
                    _this.reviews = data.reviews;
                    _this.print();
                },
                error: function () {
                    removePreloader();
                }
            });
        };
        _this.print = function () {
            _this.printReviews();
            _this.printPage();
            _this.reviewForm();
            SetRatingStar();

        };
        _this.printReviews = function () {
            _this.html('');
            for (var i in _this.reviews) {
                _this.append(_this.layout(_this.reviews[i]));
            }
        };
        _this.reviewForm = function () {
            if (UserDetails != undefined) {
                $('.write-review').append([
                    $('<p>').attr({id: 'rvw_btn'}).append([$('<a>').attr({class: 'btn btn-primary', href: ''}).append(['Write your review !']).on('click', function (e) {
                            e.preventDefault();
                            $(this).hide();
                            $('#create_review').show();							
                        })
                    ]),
                    $('<div>').attr('id', 'review_form_wrapper').append([
                        $('<form>').attr({action: 'rating/product', method: 'post', id: 'create_review', name: 'create_review', style: 'display:none;'}).append([
                            $('<input>').attr({type: 'hidden', class: 'relative_post_id', id: 'relative_post_id', name: 'relative_post_id'}).val(productDetails.info.product_id),							
                            $('<h3>').attr({id: 'reply-title', class: 'comment-reply-title'}).append(['Add a review']),
                            $('<p>').attr({class: 'comment-form-rating'}).append([
                                $('<label>').append(['Your Rating:', $('<span>').attr('class', 'danger').append(['*'])]),
                                $('<p>').attr('class', '').append([
                                    $('<div>').attr('class', 'star-rating').append([
                                        $('<span>').attr({class: 'fa fa-star-o', 'data-rating': '1'}),
                                        $('<span>').attr({class: 'fa fa-star-o', 'data-rating': '2'}),
                                        $('<span>').attr({class: 'fa fa-star-o', 'data-rating': '3'}),
                                        $('<span>').attr({class: 'fa fa-star-o', 'data-rating': '4'}),
                                        $('<span>').attr({class: 'fa fa-star-o', 'data-rating': '5'}),
                                        $('<input>').attr({type: 'hidden', id: 'rating', name: 'rating', class: 'rating-value', 'data-errmsg_to': 'rating_errmsg'})
                                    ]),
                                ]),
                            ]),
                            $('<p>').attr({class: 'comment-form-title'}).append([
                                $('<label>').append(['Title', $('<span>').attr({class: 'required'}).append(['*'])]),
                                $('<input>').attr({type: 'text', class: 'form-control', id: 'title', name: 'title'})
                            ]),
                            $('<p>').attr({class: 'comment-form-comment'}).append([
                                $('<label>').append(['Your Review', $('<span>').attr({class: 'required'}).append(['*'])]),
                                $('<p>').append([$('<textarea>').attr({id: 'description', class: 'form-control', name: 'description', cols: '45', rows: '8', 'aria-required': 'true'})])
                            ]),
                            $('<p>').attr({class: 'form-submit'}).append([
                                $('<input>').attr({name: 'submit', type: 'submit', id: 'submit', class: 'btn btn-md btn-primary submit', value: 'Submit'})
                            ]),
                                    /* $('<div>').attr('class','form-group').append([
                                     $('<label>').attr('class','col-sm-3 control-label').append(['Your Rating:',$('<span>').attr('class','danger').append(['*'])]),
                                     $('<div>').attr('class','col-sm-8').append([
                                     $('<div>').attr('class','star-rating').append([
                                     $('<span>').attr({class:'fa fa-star-o','data-rating':'1'}),
                                     $('<span>').attr({class:'fa fa-star-o','data-rating':'2'}),
                                     $('<span>').attr({class:'fa fa-star-o','data-rating':'3'}),
                                     $('<span>').attr({class:'fa fa-star-o','data-rating':'4'}),
                                     $('<span>').attr({class:'fa fa-star-o','data-rating':'5'}),
                                     $('<input>').attr({type:'hidden',id:'rating',name:'rating',class:'rating-value','data-errmsg_to':'rating_errmsg'})
                                     ]),
                                     ]),


                                     ]),*/



                        ]),
                    ]),
                ]);
            }
        }
        _this.layout = function (review) {
            return $('<li>').attr({class: 'row'}).append(
                    $('<div>').attr('class', 'col-xs-12').append(
                    [
                        $('<h2>').append([review.title], ' ', $('<small>').append([$('<i>').attr({class: 'fa fa-star'}), review.rating])),
                        $('<span>').attr({class: 'text-justify'}).append(review.description),
                        $('<p>').attr({class: 'text-right'}).append([
                            $('<span>').append(review.name, ', ', review.created_on),
                            ' ',
                            $('<span>').attr({class: 'label label-success'}).append([
                                review.likes_count,
                                ' ',
                                $('<a>').attr({class: '', href: ''}).append($('<i>').attr({class: 'fa fa-2x fa-thumbs-up'})).click(function (e) {
                                    e.preventDefault();
                                    _this.like(review.review_id);
                                })
                            ]),
                            ' ',
                            $('<span>').attr({class: 'label label-danger'}).append([
                                review.unlikes_count,
                                ' ',
                                $('<a>').attr({class: '', href: '', 'data-review_id': review.review_id}).append($('<i>').attr({class: 'fa fa-2x fa-thumbs-down'})).click(function (e) {
                                    e.preventDefault();
                                    _this.unlike($(this).data('review_id'));
                                })
                            ]),
                            ' ',
                            $('<a>').attr({class: 'abuse_rmv btn btn-danger btn-xs', href: '#', 'data-review_id': review.review_id}).append([
                                'Report Abuse'
                            ]).click(function (e) {
                                e.preventDefault();
                                _this.abuse($(this).data('review_id'));
                            }),
                        ]),
                    ])
                    );
        };
        _this.like = function (review_id) {
            $.ajax({
                type: "POST",
                url: ajaxUrl + 'review/like',
                data: {review_id: review_id},
                dataType: 'JSON',
                beforeSend: function () {
                    //loadPreloader();
                },
                success: function (data) {
                    //removePreloader();
                    _this.loadReviews();
                },
                error: function () {
                    removePreloader();
                }
            });
        };
        _this.unlike = function (review_id) {
            $.ajax({
                type: "POST",
                url: ajaxUrl + 'review/unlike',
                data: {review_id: review_id},
                dataType: 'JSON',
                beforeSend: function () {
                    //loadPreloader();
                },
                success: function (data) {
                    //removePreloader();
                    _this.loadReviews();
                },
                error: function () {
                    removePreloader();
                }
            });
        };
        _this.abuse = function (review_id) {
            if (confirm('Are you sure? You want to report this review as abusive?')) {
                $.ajax({
                    type: "POST",
                    url: ajaxUrl + 'review/abuse',
                    data: {review_id: review_id},
                    dataType: 'JSON',
                    beforeSend: function () {
                        //loadPreloader();
                    },
                    success: function (data) {
                        //removePreloader();
                        $('.abuse_rmv').remove();
                    },
                    error: function () {
                        removePreloader();
                    }
                });
            }
        };
        _this.printPage = function () {
            $('.pagination').html('');
            $('.pagination').append([
                $('<p>').append([((_this.currentPage - 1) * _this.options.reviewsPerPage) + 1, ' to ', (_this.options.reviewsPerPage * _this.currentPage), ' of ', _this.totalReviews, ' reviews'])
            ]);
            $('.pagination').append([
                $('<li>').append([
                    $('<a>').attr({'href': '#'}).append([
                        $('<i>').attr('class', 'fa fa-angle-double-left')
                    ]).click(function (e) {
                        e.preventDefault();
                        _this.previousPage();
                    })
                ]),
                $('<li>').append([
                    $('<select>').attr({'class': ''}).append(function () {
                        var pages = [];
                        for (var i = 1; i <= _this.totalPages; i++) {
                            pages.push((_this.currentPage == i) ? $('<option>').attr('value', i).text(i) : $('<option>').attr({'value': i, 'selected': 'selected'}).text(i));
                        }
                        return pages;
                    }).change(function (e) {
                        e.preventDefault();
                        _this.goToPage($(this).val());
                    })
                ]),
                $('<li>').append([
                    $('<a>').attr({'href': '#'}).append([
                        $('<i>').attr('class', 'fa fa-angle-double-right')
                    ]).click(function (e) {
                        e.preventDefault();
                        _this.nextPage();
                    })
                ])
            ]);
        };
        _this.goToPage = function (p) {
            if (p >= 1 && _this.totalPages <= p) {
                _this.currentPage = p;
                _this.loadReviews();
            }
        };
        _this.nextPage = function () {
            if (_this.totalPages >= _this.currentPage + 1) {
                _this.currentPage += 1;
                _this.loadReviews();
            }
        };
        _this.previousPage = function () {
            _this.currentPage - 1;
            if ((_this.currentPage - 1) >= 1) {
                _this.currentPage -= 1;
                _this.loadReviews();
            }
        };
        _this.loadReviews();
        return _this;
    }
});
$(document).ready(function () {

    $(document.body).on('click', '.star-rating .fa', function () {
        $('.star-rating .fa').siblings('input.rating-value').val($(this).data('rating'));
        return SetRatingStar();
    });
    $(document.body).on('click', '#submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: $('#create_review').attr('action'),
            type: "POST",
            data: $('#create_review').serialize(),
            dataType: "json",
            beforeSend: function () {
                $('button[type="submit"]', $('#create_review')).val('Processing...');
            },
            success: function (op) {
                $('#review_form_wrapper').hide();
                $('#rvw_btn').show();
            },
            error: function (jqXhr) {
                $('button[type="submit"]').attr('disabled', false);
                if (jqXhr.status === 422) {
                    var data = jqXhr.responseJSON;					
                    append_laravel_error(data.error, $('#create_review'));
                }
                else {
                    alert('Something went wrong');
                }
            }

        });
    });

    var data = $.getURLParams();	
    var cur = $('#center_column').children('div:visible');	
    switch (cur.attr('id'))
    {
        case 'product':
            $('#product').productDetails(data);
            break;
        case 'product-sellers':
            $('#product-sellers').productSellers(data);
            break;
        case 'seller-review':
            $('#seller-review').writeReview(data);
            break;
        case 'seller-details':
            $('#seller-details').supplierDetails(data);
            break;
    }
    var updateProduct = function (e) {
        e.preventDefault();
        var CurEle = $(this), url = (CurEle.attr('href') != undefined) ? CurEle.attr('href') : CurEle.val();
        if (url != null && url != '#' && url != '' && url != window.document.location.href) {
            var data = $.getURLParams(url);
            ChangeUrl(CurEle.text(), url);
            $('#product').productDetails(data);
            if ($('#product:hidden').length) {
                $('#center_column').children('div').fadeOut(2000, function () {
                    $('#product').fadeIn(100);
                });
            }
        }
    };
    $(document.body).on('click', '.seller-details', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        ChangeUrl(CurEle.text(), CurEle.attr('href'));
        $('#seller-details').supplierDetails({id: CurEle.data('supplier_code')});
        if ($('#seller-details:hidden').length) {
            $('#center_column').children('div').fadeOut(2000, function () {
                $('#seller-details').fadeIn(100);
            });
        }
    });
    $(document.body).on('click', 'write-review', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        ChangeUrl(CurEle.text(), CurEle.attr('href'));
        $('#seller-review').writeReview({post_type_id: CurEle.data('post_type_id'), relative_post_id: CurEle.data('relative_post_id')});
        if ($('#seller-review:hidden').length) {
            $('#center_column').children('div').fadeOut(2000, function () {
                $('#seller-review').fadeIn(100);
            });
        }
    });
    $(document.body).on('change', '.change-update-product', updateProduct);
    $(document.body).on('click', '.click-update-product', updateProduct);
    $(document.body).on('click', '.product-sellers', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        ChangeUrl(CurEle.text(), CurEle.attr('href'));
        if ($('#product-sellers:hidden').length) {
            $('#center_column').children('div').fadeOut(2000, function () {
                $('#product-sellers').fadeIn(100);
            });
        }
    });	

});
