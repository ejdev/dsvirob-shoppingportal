$.fn.extend({
    loadOrderItems: function (sub_order_code) {
        var _this = this;
        _this.html('');
        $.ajax({
            url: ajaxUrl + 'account/my-orders-details',
            type: "POST",
            dataType: "JSON",
            data: {soc: sub_order_code},
			beforeSend: function () {
                    loadPreloader();						
                },
            success: function (data) {
				console.log(data);
                $.each(data.order_details, function (k, item) {
                    $('#' + k).html(item);
                });
                if (data.order_details.cancel_order_url != undefined && data.order_details.cancel_order_url) {
                    $('.cancel-order').attr('href', 'account/my-orders/cancel/'+ data.order_details.order_code);
                }
                else {
                    $('.cancel-order').remove();
                }
                $.each(data.product_details, function (k, item) {
                    _this.append(
                            $('<li>').attr({class: "row"}).append(
                            [
                                $('<div>').attr({class: 'col-sm-1 text-center'}).append([
                                    $('<img>').attr({class: 'img img-responsive', src: item.imgs})
                                ]),
                                $('<div>').attr({class: 'col-sm-4'}).append([
                                    $('<h2>').append(item.product_name),
                                    $('<p>').append('Brand: ' + item.brand_name),
                                    $('<p>').append('Price: ' + item.currency_symbol + ' ' + item.price),
									$('<p>').append('Qty: ' + item.qty)
                                ]),
                                $('<div>').attr({class: 'col-sm-3'}).append([
                                    item.statusInfo,
                                    $('<strong>').append(item.status),
                                    $('<p>').append(item.status_msg),
                                    (item.cancel_url != undefined && item.cancel_url) ? $('<a>').attr({class: ' btn btn-xs btn-danger cancel-order-item', href: 'account/my-orders/item-cancel/'+ item.order_item_id, title: 'Cancel'}).append(['Cancel']) : ''
                                ]),
                                $('<div>').attr({class: 'col-sm-2 text-center'}).append([
                                    $('<p>').append(item.delivery_date),
                                ]),
                                $('<div>').attr({class: 'col-sm-2 text-right'}).append([
                                    $('<p>').append(item.currency_symbol + ' ' + item.amount)
                                ])
                            ])
                            );
                });
				removePreloader();
            }
        });
    }
});
$(document).ready(function () {
    $('#my-order-items').loadOrderItems($('#sub_order_code').val());
    $(document.body).on('click', '.cancel-order', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        $.ajax({
            url: ajaxUrl + CurEle.attr('href'),
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                $('#my-order-items').loadOrderItems($('#sub_order_code').val());
            }
        });
    });
    $(document.body).on('click', '.cancel-order-item', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        $.ajax({
            url: ajaxUrl + CurEle.attr('href'),
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                $('#my-order-items').loadOrderItems($('#sub_order_code').val());
            }
        });
    });
});

