$(document).ready(function () {

    $("#submit").on('click', function (e) {
        e.preventDefault();
        if ($('#pwd').val() == '') {
            $("#mailerr").html("Enter Your password").addClass("error");
        }
        else {
            curLink = $(this);
            $.ajax({
                url: $('#acc_deactivate').attr('action'),
                type: "POST",
                data: {password: $('#pwd').val()},
                dataType: "json",
                async: true,
                success: function (data) {
                    if (data.status == 'OK') {
                        window.location.href = data.url;
                    } else if(data.notify == false) {
                        $("#mailerr").html("&nbsp;&nbsp;&nbsp;&nbsp;" + data.msg)/*.addClass("danger")*/;
                    } else {
                        $("#mailerr").html("");
                    }
                },
                error: function () {
                    alert('Something went wrong');
                }
            });
        }
    });
});


