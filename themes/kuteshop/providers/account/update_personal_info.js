
$(document).ready(function () {
    $('#save_chng').click(function (event) {
        event.preventDefault();
        $.ajax({
                url: $('#update_pinfo').attr('action'),
                type: "POST",
                data: $('#update_pinfo').serialize(),
                dataType: "json",
                beforeSend: function () {
                                $('button[type="submit"]', $('#update_pinfo')).val('Processing...');
                                $('button[type="submit"]').attr('disabled', 'disabled');
                },
                success: function (op) {
                		        $('button[type="submit"]').removeAttr("disabled");
                                if (op.notify == false) {
                                    append_laravel_error(op.error, $('#update_pinfo'));
                                }
//$('#msg_div').html(op.msg);
    	        },
                error: function (jqXhr) {
                		        $('button[type="submit"]').attr('disabled', false);
                				console.log(jqXhr);
                                if (jqXhr.status === 422) {
                                    var data = jqXhr.responseJSON;
                                    append_laravel_error(data.error, $('#update_pinfo'));
                                } else {
                                    alert('Something went wrong');
                                }
                }
        });
    });
});
