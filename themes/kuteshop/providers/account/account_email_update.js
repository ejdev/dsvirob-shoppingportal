var update_mail = '';
$(document).ready(function () {
	$('#email_show').hide();
    $("#email_add").hide();
    $('#email_cancel').hide();

    $('#mob_show').hide();
    $("#mob_add").hide();
    $('#mob_cancel').hide();
    $('#submit').hide()/*attr('disabled', 'disabled')*/;

    /* $.ajax({
        type: "POST",
        url: APIURL + 'account/personalinformation',
        dataType: "json",
        beforeSend: function () {
        },
        success: function (op) {
            var content = '';
            if ((op.account_details.email != undefined) && (op.account_details.email != null)) {
                content += '<span id="email_chng">' + op.account_details.email + '</span>&nbsp;&nbsp; <a class="edit_info" data="' + op.account_details.email + '" href="">Edit</a>';
            } else {
                content += '<input type="text" class="form-control" id="email_addr" name="email_addr" placeholder="Enter email address">';
            }
            content += '<input type="text" class="form-control" id="email_show" name="email_show" placeholder="Enter email address"> <a id="email_add" href="' + APIURL + 'account/verify_email">Add</a> <a  id="email_cancel" href="">Cancel</a><input type="text" class="form-control" id="v_code" name="v_code" placeholder="Enter verification code"><span id="sucverify"></span>';
            $('#email_info').append(content);
            var data = '';
            if ((op.account_details.mobile != undefined) && (op.account_details.mobile != null)) {
                data += ' <span id="mob_chng">' + op.account_details.mobile + '</span>&nbsp;&nbsp; <a class="edit_mob" data="' + op.account_details.mobile + '" href="">Edit</a>';
            } else {
                data += '<input type="text" class="form-control" id="mobile_no" name="mobile_no" placeholder="Enter mobile number" onkeypress="return isNumberKey(event);">';
            }
            data += '<input type="text" class="form-control" id="mob_show" name="mob_show" placeholder="Enter mobile number" onkeypress="return isNumberKey(event);"> <a id="mob_add" href="' + APIURL + 'account/verify_mobile">Add</a> <a  id="mob_cancel" href="">Cancel</a><input type="text" class="form-control" id="ver_code" name="ver_code" placeholder="Enter verification code" onkeypress="return isNumberKey(event);">';
            $('#mob_info').append(data);

            $('#email_show').hide();
            $("#email_add").hide();
            $('#v_code').hide();
            $('#email_cancel').hide();

            $('#mob_show').hide();
            $("#mob_add").hide();
            $('#ver_code').hide();
            $('#mob_cancel').hide();
            $('#submit').attr('disabled', 'disabled');
        }
    }); */

    $(document).on('click', '.edit_info', function (e) {
        e.preventDefault();
        $('#email_chng').hide();
        var mail_data = $(this).attr('data');
        $('#email_show').show();
        $('#email_show').val(mail_data);
        $('.edit_info').hide();
        $('#email_add').show();
        $('#email_cancel').show();

        $('#update_mobile_tr').hide();
        $('#update_email_mobile_notes').hide();
        /*$("#mobverify").removeClass("glyphicon glyphicon-ok text-success");*/
    });

    $(document).on('click', '#email_cancel', function (e) {
        e.preventDefault();
        $("tr#update_email_tr td:first").html("Email Address");
        $("#mailerr").html('');
        $('#email_chng').show();
        $('#email_show').hide();
        $('.edit_info').show();
        $('#email_add').hide();
        $('#email_cancel').hide();
        $('#email_show').val('');
        /* $("#sucverify").removeClass("glyphicon glyphicon-ok text-success");*/
        $('#submit').hide();

        $('#update_mobile_tr').show();
        $('#update_email_mobile_notes').show();
        $('#email_verify_span').hide();
        $('#email_info_span').show();
    });

    $(document).on('click', '#email_add', function (e) {
        e.preventDefault();
		update_mail = '';
        var mail_data = $('.edit_info').attr('data');
        update_mail = $('#email_show').val();
		$("#mailerr").html('');
        if ($('#email_show').val() == '') {
            $("#mailerr").html("Enter email address").addClass("error");
        }
        else {
            if ($('#email_show').val() != mail_data) {
                if (isValidEmailAddress($('#email_show').val())) {
                    curLink = $(this);
                    $("form#update_email").addClass('ajax-loader');
                    $.ajax({
                            url: curLink.attr('url'),
                            type: "POST",
                            data: {email_id: $('#email_show').val()},
                            dataType: "json",
                            async: true,
                            success: function (data) {
                                        $("form#update_email").removeClass('ajax-loader');
                                        $("tr#update_email_tr td:first").html("Enter Verification code");
                                        $("#email_verify_span").show();
                                        if (!data.notify) {
                                            $("#v_code_notif").html(data.msg);
                                        }
                                        if (data.status == 'ok') {
                                            $('#email_info_span').hide();
                                        }
                            },
                            error: function () {
                                        $("form#update_email").removeClass('ajax-loader');
                                        $("tr#update_email_tr td:first").html("Enter Verification code");
                                        $("#email_verify_span").show();
                                        alert('Something went wrong');
                            }
                    });
                }
                else {
                    $("#mailerr").html("We were unable to process the email address. Please try again with a valid email.").addClass("error");
                }
            } else {
                $("#mailerr").html("New email is same as existing email").addClass("error");
            }
        }
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        return pattern.test(emailAddress);
    }

    $(document).on('click', '#verify_v_code', function (e) {
        e.preventDefault();
		if ($('#v_code').val().length >= 4) {
            $("form#update_email").addClass('ajax-loader');
			$.ajax({
    				url: APIURL + 'account/checkverificationcode',
    				type: "POST",
    				data: {mail: update_mail, v_code: $('#v_code').val()},
    				dataType: "json",
    				async: true,
    				success: function (data) {
                                $("form#update_email").removeClass('ajax-loader');
            					if (data.status == 'ok') {
            						$("#v_code_err").html('');
            						$("#submit").show();
            					} else {
                                    $("#v_code_err").show();
            						$("#v_code_err").html(data.msg).addClass("error");
            						$('#submit').hide();
            					}
    				},
    				error: function () {
                                $("form#update_email").removeClass('ajax-loader');
            					alert('Something went wrong');
    				}
			});
		} else {
            $("#v_code_err").show();
            $("#v_code_err").html("Please enter a valid OTP").addClass("error");
        }
    });

    $(document).on('click', '.edit_mob', function (e) {
        e.preventDefault();
        $('#mob_chng').hide();
        var mob_data = $(this).attr('data');
        $('#mob_show').show();
        $('#mob_show').val(mob_data);
        $('.edit_mob').hide();
        $('#mob_add').show();
        $('#mob_cancel').show();

        $('#update_email_tr').hide();
        $('#update_email_mobile_notes').hide();
    });

    $(document).on('click', '#mob_cancel', function (e) {
        e.preventDefault();
        $("tr#update_mobile_tr td:first").html("Mobile Number");
        $("#mailerr").html('');
        $('#mob_chng').show();
        $('#mob_show').hide();
        $('.edit_mob').show();
        $('#mob_add').hide();
        $('#mob_cancel').hide();
        $('#mob_show').val('');
        /*$("#mobverify").removeClass("glyphicon glyphicon-ok text-success");*/
        $('#submit').hide();

        $('#update_email_tr').show();
        $('#update_email_mobile_notes').show();
        $('#mob_verify_span').hide();
        $('#mob_info_span').show();
    });

    $(document).on('click', '#mob_add', function (e) {
        e.preventDefault();
        var mob_data = $('.edit_mob').attr('data');
        if ($('#mob_show').val() == '') {
            $("#moberr").html("Enter Mobile Number").addClass("text-danger");
        } else {
            $("#moberr").html("");
            if ($('#mob_show').val() != mob_data) {
                if (isValidMobileNo($('#mob_show').val())) {
                    curLink = $(this);
                    $("form#update_email").addClass('ajax-loader');
                    $.ajax({
                            url: curLink.attr('url'),
                            type: "POST",
                            data: {mobile: $('#mob_show').val()},
                            dataType: "json",
                            async: true,
                            success: function (data) {
                                        $("form#update_email").removeClass('ajax-loader');
                                        $("tr#update_mobile_tr td:first").html("Enter Verification code");
                                        $("#mob_verify_span").show();
                                        if (!data.notify) {
                                            $("#ver_code_notif").html(data.msg);
                                        }
                                        if (data.status == 'ok') {
                                            $('#mob_info_span').hide();
                                        }
                            },
                            error: function () {
                                        $("form#update_email").removeClass('ajax-loader');
                                        $("tr#update_mobile_tr td:first").html("Enter Verification code");
                                        $("#mob_verify_span").show();
                                        alert('Something went wrong');
                            }
                    });
                } else {
                    $("#moberr").html("Please enter a valid moble number.").addClass("error");
                }
            } else {
                $("#moberr").html("New Mobile is same as existing Mobile").addClass("error");
            }
        }
    });

    function isValidMobileNo(mobileNo) {
        var pattern = new RegExp(/^[7-9]{1}[0-9]{9}$/i);
        return pattern.test(mobileNo);
    }

    $(document).on('click', '#verify_ver_code', function (e) {
        e.preventDefault();		
        if ($('#ver_code').val().length >= 4) {
            $("form#update_email").addClass('ajax-loader');
    		$.ajax({
        			url: APIURL + 'account/checkverificationcode',
        			type: "POST",
        			data: {v_code: $('#ver_code').val()},
        			dataType: "json",
        			async: true,
        			success: function (data) {
                				console.log(data);
                                $("form#update_email").removeClass('ajax-loader');
                                if (data.status == 'ok') {
                                    $("#moberr").html('');
                					$("#submit").show();
                                    $("#ver_code_err").html("");
                				} else {
                                    $("#ver_code_err").show();
                                    $("#ver_code_err").html(data.msg).addClass("error");
                                    $('#submit').hide();
                				}
        			},
        			error: function () {
                                $("form#update_email").removeClass('ajax-loader');
                				alert('Something went wrong');
        			}
    		});	
        } else {
            $("#ver_code_err").show();
            $("#ver_code_err").html("Please enter a valid OTP").addClass("error");
        }	
    });

    $(document).on('click', '#submit', function (e) {
        e.preventDefault();
        if ($('#email_addr').val() == '' || $('#mobile_no').val() == '') {
            if ($('#email_addr').val() == '') {
                $("#mailerr").html("Enter email address").addClass("error");
            }
            if ($('#mobile_no').val() == '') {
                $("#moberr").html("Enter Mobile Number").addClass("error");
            }
        } else {
            $.ajax({
                    url: $('#update_email').attr('action'),
                    type: "POST",
                    data: $('#update_email').serialize(),
                    dataType: "json",
                    async: true,
                    success: function (data) {
                                if (data.status == 'OK') {
                                    $("#submit").show();
                                    //window.location.reload();
                                    window.location.href = data.url;
                                }
                    },
                    error: function () {
                                alert('Something went wrong');
                    }
            });
        }
    });

});