$.fn.extend({
    loadOrderItems: function (order_code) {
        var _this = this;
        if (_this.is(':empty')) {
            $.ajax({
                url: APIURL + 'account/my-orders-items',
                type: "POST",
                dataType: "JSON",
                data: {'soc': order_code},
				beforeSend: function () {
                    loadPreloader();						
                },
                success: function (data) {
                    var ul = $('<ul>').attr({class: 'order-items'});
                    $.each(data.product_details, function (k, item) {
                        ul.append(
                                $('<li>').attr({class: 'row'})
                                .append([
                                    $('<div>').attr({class: 'col-sm-1'}).append([
                                        $('<img>').attr({class: 'img img-responsive', src: item.imgs})
                                    ]),
                                    $('<div>').attr({class: 'col-sm-5'}).append([
                                        $('<h2>').append(item.product_name),
                                        $('<p>').append('Brand: ' + item.brand_name),
                                        $('<p>').append('Price: ' + item.price),
                                        $('<p>').append('Qty: ' + item.qty)
                                    ]),
                                    $('<div>').attr({class: 'col-sm-4'}).append([
                                        $('<strong>').append(item.status),
                                        $('<p>').append(item.status_msg)
                                    ]),
                                    $('<div>').attr({class: 'col-sm-2'}).append([
										 $('<p>').append('Shipping Charge: ' + item.currency_symbol + ' ' +item.shipping_charge),
                                        $('<p>').append('Price: ' + item.currency_symbol + ' ' +item.product_total)
                                    ])
                                ])
                                );
                    });
                    _this.append(ul);
					removePreloader();
                }
            });
        }
    }
});
$(document).ready(function () {
    $("#my-orders-table").dataTable({
        "bPaginate": true,
        "bProcessing": true,		
        "bFilter": false,
        "bAutoWidth": false,
        "bDestroy": true,
        "bInfo": true,
        "bSort": true,
        "processing": true,
        "serverSide": true,
        "sDom": "t+<''p>",
        "oLanguage": {
            "sLengthMenu": "_MENU_",			
        },		
        "ajax": {
            'url': ajaxUrl + 'account/my-orders',
            "type": "POST",
            "data": {
                "from_date": $('#from_date').val(),
                "to_date": $('#to_date').val(),
                "search_status": $('#search_status').val()
            }
        },
        "columns": [
            {
                "orderable": false,
                "render": function (data, type, row, meta) {
                    var json = $.parseJSON(meta.settings.jqXHR.responseText);
                    return $('<div>').attr({class: "col-sm-12"}).append(
                            [
                                $('<div>').attr({class: 'panel panel-info'}).append([
                                    $('<div>').attr({class: 'panel-heading'}).append([
                                        $('<h4>').attr({class: 'panel-title'}).append(function(){
										    var datas=[];
                                            datas.push($('<a>').attr({class: '', href: ajaxUrl + 'account/my-orders/' + row.order_code}).append($('<b>').text(row.order_code)));		
											//datas.push((row.sub_order_status_id == 7)?'Cancelled':$('<span>').attr({'class': 'btn btn-default btn-xs btn-danger pull-right', 'data-order_code': row.order_code, 'id': 'cancel_order'}).append('Cancel'));
										
                                            datas.push($('<div>').attr({class: 'pull-right'}).append([												
                                                $('<span>').attr({}).append('<b>Order Date: </b>' + row.order_date),
                                                $('<a>').attr({class: 'btn btn-xs get-order-items collapsed', 'data-toggle': 'collapse', 'href': '#' + row.order_code, 'data-order_code': row.order_code})
                                                        .append($('<i>').attr({class: 'fa fa-chevron-down'}))
                                            ]));
											return datas;

                                        }),
                                    ]),
                                    $('<div>').attr({class: 'panel-body collapse order-items', id: row.order_code}).append([
                                    ]),
                                    $('<div>').attr({class: 'panel-footer'}).append([
                                        $('<strong>').attr({class: 'text-success'}).append(
                                                [
                                                    $('<strong>').attr({class: 'text-success pull-right'}).append('Total: ' + row.amount),
                                                    $('<span>').append('Supplier: ' + row.supplier),
                                                ])
                                    ])
                                ])
                            ]
                            )[0].outerHTML;
                    
                }
            }
        ],
        initComplete: function (settings, json) {
            $('thead', $("#my-orders-table")).remove();
        },
        drawCallback: function (e, settings) {
            var content = '';
            $.each($('tr td', $("#my-orders-table")), function () {
                content += $(this).html();
                $(this).parent('tr').remove();
            });
            $('tbody', $("#my-orders-table")).html('<tr><td width="100%">' + content + '</td></tr>');
            removePreloader();
        }
    });
    $(document.body).on('click', '.get-order-items', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        $(CurEle.attr('href')).loadOrderItems(CurEle.data('order_code'));
    });
	
	$(document.body).on('click', '#cancel_order', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        var soc = CurEle.data('order_code');
		console.log(soc);
		 $.ajax({
			url: ajaxUrl + 'account/my-orders/cancel/'+soc,
			type: "POST",
			dataType: "JSON",
			data: {'soc': soc},
			success: function (data) {
				console.log(data);				
			}
		});
    });
});

