var updateInfo = '';
var emailOrMob = '';
$(document).ready(function () {
    var curEmail = $('#edit_email').attr('data');
    var curMob = $('#edit_mob').attr('data');
	$(document).on('click','.edit_info',function(e){
        e.preventDefault();
        $('#email_mobile_info').hide();
        if (this.id == "edit_email") {
            emailOrMob = "email";
        } else {
            emailOrMob = "mob";
        }
        $('#change' + emailOrMob + '_box').show();
        $('#' + emailOrMob + 'Enteryfrm').show();
    });

    $(document).on('click','.email_mob_add',function(e){ // Send Verification button
        e.preventDefault();
        var curElement = this;
        emailOrMob = ( (curElement.id == "email_add") || (curElement.id == "email_add_a") ) ? "email" : "mob";
        var curInfo = $('#edit_' + emailOrMob).attr('data');
        updateInfo = $('#' + emailOrMob + '_show').val();
        var msg = ( emailOrMob == "email" ) ? "email address" : "mobile number";
        if (updateInfo == '') {
            $("form#update_email").prepend('<div class="alert alert-danger">Please enter ' + msg + '</div>');
        } else {
            if (updateInfo != curInfo) {
                validInfo = ( emailOrMob == "email" ) ? isValidEmailAddress(updateInfo) : isValidMobileNo(updateInfo);
                if (validInfo) {
                    var ajaxData = {};
                    ajaxData[(emailOrMob == "email") ? "email_id" : "mobile"] = updateInfo;
                    $.ajax({
                            url: $(curElement).data('url'),
                            type: "POST",
                            data: ajaxData,
                            dataType: "json",
                            async: true,
                            beforeSend:function(){
                                $("#change" + emailOrMob + "_box").addClass('ajax-loader');
                                $('.alert').remove();
                            },
                            success: function (data) {
                                $("#change" + emailOrMob + "_box").removeClass('ajax-loader');                                
                                if (!data.notify) {
                                    $("form#update_email").prepend('<div class="alert alert-success">'+data.msg+'</div>');
                                }
                                if (data.status == 'ok') {
                                    $('#' + emailOrMob + 'Enteryfrm').hide();
                                    $('#' + emailOrMob + 'Verifyfrm').show();
                                    $('#' + emailOrMob + 'Verifyfrm #' + emailOrMob + '_ver_code').attr('disabled',true).hide();
                                }
                             },
                            error: function () {
                                $("#change" + emailOrMob + "_box").removeClass('ajax-loader');                                                                
                                alert('Something went wrong');
                            }
                    });
                }
                else {
                    $("form#update_email").prepend('<div class="alert alert-danger">We were unable to process the ' + msg + '. Please try again with a valid ' + msg + '.</div>');
                }
            } else {
                $("form#update_email").prepend('<div class="alert alert-danger">New ' + msg + ' is same as existing ' + msg + '</div>');
            }
        }
    });

    $(document).on('keyup','.verification_code',function(){
        emailOrMob = ( this.id == "email_code" ) ? "email" : "mob";
        if ($('#' + emailOrMob + '_code').val().length >= 4) {
            $('#change' + emailOrMob + '_box #' + emailOrMob + '_ver_code').attr('disabled',false).show();
        } else {
            $('#change' + emailOrMob + '_box #' + emailOrMob + '_ver_code').hide();
        }
    });

    $(document).on('click','.verify_verification_code',function(){
        emailOrMob = ( this.id == "email_ver_code" ) ? "email" : "mob";
        var verCode = $('#' + emailOrMob + '_code').val();
        if (verCode.length >= 4) {
            $.ajax({
                    url: APIURL + 'account/checkverificationcode',
                    type: "POST",
                    data: {v_code: verCode},
                    dataType: "json",
                    beforeSend:function(){
                        $("#change" + emailOrMob + "_box").addClass('ajax-loader');
                        $('.alert').remove();
                    },
                    async: true,
                    success: function (data) {
                        if (data.status == 'ok') {
                            updateEmailMobile();
                        } else {
                            $("form#update_email").prepend('<div class="alert alert-danger">'+data.msg+'</div>');
                        }
                    },
                    error: function () {
                        $("#change" + emailOrMob + "_box").removeClass('ajax-loader');
                        alert('Something went wrong');
                    }
            });
        } else {
            $("form#update_email").prepend('<div class="alert alert-danger">Please enter a valid OTP</div>');
        }
    });

    $(document).on('click','.email_mob_cancel',function(){
        $('.alert').remove();
        $('#email_mobile_info').show();
        $('#email_show').val( $('#edit_email').attr('data') );
        $('#mob_show').val( $('#edit_mob').attr('data') );
        $('#emailVerifyfrm').hide();
        $('#mobVerifyfrm').hide();
        $('#changeemail_box').hide();
        $('#changemob_box').hide();
    });

    function updateEmailMobile() {
        $.ajax({
                url: $('#update_email').attr('action'),
                type: "POST",
                data: $('#update_email').serialize(),
                dataType: "json",
                async: true,
                beforeSend:function(){
                    $('.alert').remove();
                },
                success: function (data) {
                    if (data.status == 'OK') {
                        //window.location.reload();
                        window.location.href = data.url;
                    }
                },
                error: function () {
                    alert('Something went wrong');
                }
        });
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        return pattern.test(emailAddress);
    }

    function isValidMobileNo(mobileNo) {
        var pattern = new RegExp(/^[7-9]{1}[0-9]{9}$/i);
        return pattern.test(mobileNo);
    }

});