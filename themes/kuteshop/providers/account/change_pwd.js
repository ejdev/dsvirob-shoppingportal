$(document).ready(function () {
	$('#submit').click(function (event) {
        event.preventDefault();
        $.ajax({
                url: $('#changepassword').attr('action'),
                type: "POST",
                data:  $('#changepassword').serialize(),                  
                dataType: "json",
                beforeSend: function () {
	                        	$('button[type="submit"]', $('#changepassword')).val('Processing...');
	                        	$('button[type="submit"]').attr('disabled', 'disabled');
                },
                success: function (op) {   
								if(op.status =='param_err'){
									$('button[type="submit"]').attr('disabled', false);
									append_laravel_error(op.error, $('#changepassword'));
								} else if (op.status == 'ok') {
		                        	window.location.href = op.url;
								} else {
									$('button[type="submit"]').removeAttr("disabled");
									$('#new_password').val('');
									$('#confirm_password').val('');
									$('#oldpassword').val(''); 
									//$('#msg_div').html(op.msg);						
								}
				},
				error: function (jqXhr) {
					alert("something went wrong");
				}
        });
    });
});   
	