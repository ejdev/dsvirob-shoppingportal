$(document).ready(function () {
	
	$("#country").change(function () {
        var country_id = $("#country").val();
        $("#user_city").change();
        if (country_id != '') {
            $("#state").html("<option value=''> loading... </option>");
            var stateOpt = "<option value=''>--Select State--</option>";
            var phoneOpt = '';
            $.post('account/get_state_phonecode', {country_id: country_id}, function (data) {
                if(data.state_list != '' && data.state_list != null) {
                    var states 	 = 	data.state_list;
                    $.each(states, function (key, elements) {
                        stateOpt += "<option value='" + elements.state_id + "'>" + elements.state + "</option>";
                    });
                }
                $("#state").html(stateOpt);
            }, 'json');
        } else {
            $("#state").html("<option value=''>--Select State--</option>");
        }
    });
	
    /* city */
  /*   $("#state").change(function () {
        var state_id = $("#state").val();
        if (state_id != '') {
			 $("#city").html("<option value=''> loading... </option>");
            var cityOpt = "<option value=''>--Select City--</option>";
            $.post('account/get_city', {state_id: state_id}, function (data) {
                if (data.city_list != '' && data.city_list != null) {
                    var cities = data.city_list;
                    $.each(cities, function (key, elements) {
                        cityOpt += "<option value='" + elements.city_id + "'>" + elements.city_name + "</option>";
                    });
                }
                $("#city").html(cityOpt);
            }, 'json');
        } else {
            $("#city").html("<option value=''>--Select City--</option>");
        }
        $("#city").change();
    }); */

	$.ajax({
            type: "POST",
            url: APIURL + 'account/get_address_list',
            dataType: "json",
            beforeSend: function () {
    			            loadPreloader();	
            },
            success: function (op) {			
                			if (op.address_list.data != null) {			
                				$.each(op.address_list.data, function (key, val) {
                					var content = '<tr id="rmv_addr_' + val.address_id + '">';
                					content += '<td> <input type="radio" class="default_addr" value="' + val.address_id + '" name="default_addr"' + ((val.address_type_id == 1) ? 'checked="checked"' : '') + '  required></td>';
                					content += ' <td>' + val.street1 + '<br />' + val.street2 + '</td>';
                					content += '<td>' + val.city + '-' + val.postal_code;
                					content += '<td>' + val.state + '</td>';
                					content += '<td>' + val.country + '</td>';
                					content += '<td><a class="delete_addr" data="' + val.address_id + '" href="' + APIURL + 'account/address/delete/' + val.address_id + '"><i class="fa fa-close"></i></a></td>';
                					content += '</tr>';
                					$('#addr_list').append(content);
                				});
                			} else {
                				$('#addr_list').html('<br><b>Address Not Found.</b>');
                			}
                			removePreloader();
            }
    });

    $('#submit').click(function (event) {
        event.preventDefault();
        $.ajax({
                url: $('#address_form').attr('action'),
                type: "POST",
                data: $('#address_form').serialize(),
                dataType: "json",
                beforeSend: function () {
                                $('button[type="submit"]', $('#address_form')).val('Processing...');
                                $('button[type="submit"]').attr('disabled', 'disabled');
                },
                success: function (data) {
                				if(data.status == 'ok'){
                			//$('#msg_div').html(data.msg);
                				    $('button[type="submit"]').removeAttr("disabled");
                					window.location.reload();
                				} else if(data.status =='param_err') {
                				    $('button[type="submit"]').attr('disabled', false);
                				    append_laravel_error(data.error, $('#address_form'));
                				} else if(data.status =='err') {
                			//$('#msg_div').html(data.msg);
                				}
    		    },
                error: function (jqXhr) {
                                $('button[type="submit"]').attr('disabled', false);                
                }
        });
    });

    $(document).on('click', '.delete_addr', function (e) {
        e.preventDefault();
        $('.alert').remove();
        var curLink = $(this);
        curaddress = curLink.attr('data');
        if (confirm('Are you sure you wants to delete this contact?')) {
            $.ajax({
                    url: curLink.attr('href'),
                    type: "POST",
                    dataType: "json",
                    async: true,
        			beforeSend: function () {
        				            loadPreloader();	
        			},
                    success: function (data) {
                    				//console.log(data);
                                    if (data.status == 'ok') {
                                        $("#rmv_addr_" + curaddress).remove();
                                    }
                    				removePreloader();
                    },
                    error: function () {
                                    alert('Something went wrong');
                    }
            });
        }
    });

    $(document).on('change', '.default_addr', function (e) {
        e.preventDefault();
        var default_addr = $('.default_addr:checked').val();
        $.ajax({
                url: APIURL + 'account/address/default',
                type: "POST",
                data: {'default_addr': default_addr},
                dataType: "json",
                async: true,
    			beforeSend: function () {
				                loadPreloader();	
    			},
                success: function (data) {
                				//console.log(data);
                                //if (data.status == 'OK') {}
                				removePreloader();
                },
                error: function () {				
                                alert('Something went wrong');
                }
        });
    });

	$('#country').trigger('change');
	
});




