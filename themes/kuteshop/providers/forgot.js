var otp_code_val = '';
var reset_code_val = '';
$(document).ready(function () {
    $('#sub_butt').attr('disabled', false);
    $('#er_msg').html('');
$('#resend_forg_otp_btn').on('click',function(e){
    e.preventDefault();
    $('#forgot_form').trigger('submit');
});
     $("#forgot_form").validate({
            errorElement: 'div',
            errorClass: 'text-danger',
            focusInvalid: false,
            // Specify the validation rules
            rules: {
                email_mob_verification: "required",
            },
            // Specify the validation error messages
            messages: {
                email_mob_verification: "Please Enter Your Email ID/Mobile no",
            },
            submitHandler: function (form, event) {
                event.preventDefault();
                if ($(form).valid()) {

                    var datastring = $(form).serialize();
                    $.ajax({
                        type: 'POST',
                        url: $('#forgot_form').attr('action'),
                        data: datastring,
                        dataType: 'json',
                        error: function () {
                            removePreloader();
                            $('#sub_butt').val('Continue').attr('disabled', false);
                            $('#succ_msg').text('Please try later. Something went wrong');
                            setTimeout(function () {
                                $("#succ_msg").fadeOut('slow', function () {
                                    $(this).remove();
                                });
                            }, 2000);
                            $('#sub_butt').html('Continue').attr('disabled', false);
                        },
                        beforeSend: function () {
                            $('#er_msg').html('');
                            loadPreloader();
                            $('#sub_butt').val('Processing..').attr('disabled', true);
                            $('#succ_msg').text('');
                            $('#succ_msg').removeClass("alert alert-success");
                            $('#succ_msg').removeClass("alert alert-danger");
                        },
                        success: function (op) {
                            removePreloader();
                            $('#sub_butt').val('Check').attr('disabled', false);
                            if (op.status == 'ok') {
                                if (op.option == 'mobile')
                                {
                                    $('#otp_forg_container').show();
                                    $('#email_mob_forg_container').hide();
                                    otp_code_val = op.code_id;
                                    $('#otp_succ').html('<div class="alert alert-success">' + op.msg + '</div>');
                                    $('.forg_otp_err').hide();
                                    $('#head-title').hide();
                                }
                               
                                else
                                {
                                    $('#succ_msg').addClass("alert alert-success");
                                    $('#succ_msg').html(op.msg);
                                    $('#forgot_err').text('');
                                }

                            }
                             else if (op.status == 'fail') {
                                   
                                    $('#succ_msg').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + op.msg + '</div>');
                                }
                        }
                    });
                }
            }
        });
    $("#forg_otp_btn").on('click', function (e) {

        $('#otp_succ').html('');
        $('.forg_otp_err').hide();
        e.preventDefault();
        if ($('#forg_otp_code').val() != '')
        {
            // Specify the validation error messages
            $.ajax({
                url: ajaxUrl + 'check/user/otp', // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: {'otp': $('#forg_otp_code').val(), 'id': otp_code_val}, // Data sent to server, a set of key/value pairs representing form fields and values
                datatype: "json",
                beforeSend: function () {
                    loadPreloader();
                    $(this).attr('disabled', true);
                },
                success: function (data)
                {
                    removePreloader();
                    if (data.status == 'ok')
                    {
                        $('#reset_code').val(data.reset_code);
                        $('#otp_forg_container').hide();
                        $('#email_mob_forg_container').hide();
                        $('.reset_pwd-forg').show();
                        $('#forgot_pwd_info').hide();
                        $('#new_pwd_succ').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                    }
                    else
                    {

                    }
                },
                error: function () {
                    $('#sign_up_btn').hide();
                    $("#email_avail_status").text('');
                    $(this).attr('disabled', false);
                    removePreloader();
                    alert('Something went wrong');
                    return false;
                }
            });
        }
        else
        {
            $('.forg_otp_err').show();
        }
    });



    $("#change_pwd_forg_form").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        // Specify the validation rules
        rules: {
            forg_new_pass: {
                required: true,
                minlength: 4,
                maxlength: 8,
            },
            confirm_new_pass: {
                equalTo: "#forg_new_pass"
            }
        },
        // Specify the validation error messages
        messages: {
            forg_new_pass: {
                required: "Enter your Password",
                minlength: "Password should be 4 Charecters long",
                maxlength: "Password should not be greater than 8 Charecters long",
            },
        },
        submitHandler: function (form, event) {
            event.preventDefault();
            if ($(form).valid()) {

                var datastring = $(form).serialize() + '&id=' + JSON.stringify(reset_code_val);
                $.ajax({
                    type: 'POST',
                    url: $('#change_pwd_forg_form').attr('action'),
                    data: datastring,
                    dataType: 'json',
                    error: function () {
                        removePreloader();
                        $('#forg_new_pass_btn').attr('disabled', false);
                        $('#succ_msg').text('Please try later. Something went wrong');
                        setTimeout(function () {
                            $("#succ_msg").fadeOut('slow', function () {
                                $(this).remove();
                            });
                        }, 2000);
                        $('#sub_butt').html('Continue').attr('disabled', false);
                    },
                    beforeSend: function () {
                        $('#er_msg').html('');
                        loadPreloader();
                        $('#forg_new_pass_btn').attr('disabled', false);
                        $('#succ_msg').text('');
                        $('#succ_msg').removeClass("alert alert-success");
                        $('#succ_msg').removeClass("alert alert-danger");
                    },
                    success: function (op) {
                        removePreloader();

                        if (op.status == 'ok') {
                            setTimeout(function () {
                                $('#new_pwd_succ').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + op.msg + '</div>');
                                $("#new_pwd_succ").fadeOut('slow', function () {
                                    $(this).remove();
                                    $('#email_mob_forg_container').show();
                                    $('#forgot_pwd_info').show();
                                    $('#login_register_container').show();
                                    $('#forgot_password_container').hide();
                                    $("#change_pwd_forg_form")[0].reset();
                                    $('.reset_pwd-forg').hide();
                                      $('#head-title').show();

                                });
                            }, 5000);


                        }
                    }
                });
            }
        }
    });



    $(document.body).on('click', '#go_to_login', function ()
    {
        $('.close').remove();
        $('#loginfrm')[0].reset();
        $('#login_err').html('');
        $('#login_register_container').show();
        $('#forgot_password_container').hide();
    });
});
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    if (pattern.test(emailAddress) == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}
