var notif;
//var ajaxUrl = $('base').attr('href'), APIURL = ajaxUrl + 'api/v1/';
var ajaxUrl = $('base').attr('href'), APIURL = ajaxUrl;
var CurPage = {url: '', title: ''};

$(document).ajaxComplete(function (event, xhr, settings) {
    var data = xhr.responseJSON;
    var pushNotify = true;
    if (xhr.status == 401) {
        UserDetails = undefined;
        window.location.href = data.url;
        $('#loginfrm #user_login').val('');
        $('#loginfrm #user_password').val('');
        $('#login_modal').modal();
    }
    else if (xhr.status == 308) {
        if (data != undefined && data.url != undefined) {
            window.location.href = data.url;
        }
    }
    else if (xhr.status == 200) {
        if (data.UserDetails != undefined) {
            UserDetails = data.UserDetails;
        }
        if (data.notify != undefined) {
            pushNotify = data.notify;
        }     

        if ( pushNotify==true && data != undefined && data.msg != undefined && data.msg != '' && data.msg != null) {
           if (notif != undefined) {
                notif({
                    msg: data.msg,
                    type: "success",
                    position: "left"
                });
            }
        }
    }
    else {
        if (data != undefined && data.msg != undefined && data.msg != '' && data.msg != null) {
            if (notif != undefined) {
                notif({
                    msg: data.msg,
                    type: "error",
                    position: "right"
                });
            }
        }
    }
});

$.fn.extend({
    resetForm: function () {
        var form = $(this);
        $.each($('input', form), function () {
            if (!$(this).hasClass('ignore-reset'))
            {
                switch ($(this).attr('type').toLowerCase())
                {
                    case "text":
                    case "password":
                    case "textarea":
                    case "hidden":
                    case "number":
                        $(this).val('');
                        break;
                    case "radio":
                    case "checkbox":
                        $(this).prop('checked', false);
                        break;
                }
            }
        });
        $.each($('textarea', form), function () {
            $(this).val('');
            if (!$(this).hasClass('ignore-reset'))
            {
                if (CKEDITOR != undefined) {
                    CKEDITOR.instances[$(this).attr('id')].setData('');
                    //CKEDITOR.instances[$(this).attr('id')].updateElement();
                }
            }
        });
        $.each($('select', form), function () {
            if (!$(this).hasClass('ignore-reset'))
            {
                $(this).val('');
            }
        });
    }
});
jQuery.extend({
    getURLParams: function (url) {
        url = (url != undefined) ? url : window.location.search;
        var params = {};
        url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (str, key, value) {
            params[decodeURI(key)] = decodeURI(value);
        });
        return params;
    }
});
function form_reset(frm_elements)
{
    for (i = 0; i < frm_elements.length; i++)
    {
        field_type = frm_elements[i].type.toLowerCase();

    }
}
jQuery.extend({
    stringify: function stringify(obj) {
        var t = typeof (obj);
        if (t != "object" || obj === null) {
            if (t == "string")
                obj = '"' + obj + '"';
            return String(obj);
        } else {
            var n, v, json = [], arr = (obj && obj.constructor == Array);
            for (n in obj) {
                v = obj[n];
                t = typeof (v);
                if (obj.hasOwnProperty(n)) {
                    if (t == "string")
                        v = '"' + v + '"';
                    else if (t == "object" && v !== null)
                        v = jQuery.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
            }
            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    }
});
jQuery.fn.serializeObject = function ()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
var Constants = {};
$.holdReady(true);
$.ajax({
    url: ajaxUrl + 'init',
    type: "POST",
    dataType: "JSON",
    success: function (data) {
        Constants = data.Constants;
        UserDetails = data.UserDetails;
        $.holdReady(false);
    }
});
$(document).ready(function () {
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }
    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })
    $(document).ajaxSuccess(function (event, xhr, settings) {
        var json = $.parseJSON(xhr.responseText);
        if (json.login_status != undefined && json.login_status == false) {
            window.location.href = json.url;
            $('#loginfrm #user_login').val('');
            $('#loginfrm #user_password').val('');
            $('#login_modal').modal();
        }
    });

    if ($.validator) {
        $.validator.addMethod("alphaSpace", function (value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, 'please enter alphabets');
        $.validator.addMethod("phonecode", function (value, element) {
            return this.optional(element) || value == value.match(/^[+]{1}[0-9]+$/);
        }, 'please enter Phonecode');
        $.validator.addMethod("checkHost", function (value, element) {
            var pattern = new RegExp(/^[a-z0-9]+([\-\.]{1}[a-z0-9]+)+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/);
            return pattern.test(host);
        }, 'Please enter a valid Host Address.');
    }
});
function AddToUrl(title, url) {
    if (typeof (window.history.pushState) != undefined) {
        var href = window.location.href, c_url = (href.indexOf('?') > 1) ? href.substring(0, href.indexOf('?')) : href;
        var obj = {Page: title, Url: c_url + ((url != '') ? '?' + url : '')};
        CurPage.url = window.location.href;
        CurPage.title = document.title;
        window.history.pushState(obj, obj.Page, obj.Url);
    }
}
function ChangeUrl(title, url) {
    if (typeof (window.history.pushState) != undefined) {
        var obj = {Page: title, Url: url};
        CurPage.url = window.location.href;
        CurPage.title = document.title;
        window.history.pushState(obj, obj.Page, obj.Url);
    }
}
function GoToPrevious() {
    if (CurPage.url != null) {
        window.history.pushState(CurPage, CurPage.title, CurPage.url);
    }
}
function validateRegularExpression(event, expression) {
    var regex = new RegExp(expression);
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (regex.test(str)) {
        return true;
    }
    event.preventDefault();
    return false;
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 46)) {
        return false;
    }
    return true;
}
function isAlfa(evt) {
    // evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        return false;
    }
    return true;
}
function alphaNumeric_withspace(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || code == 32 || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 9 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function alphaNumeric_specialchar(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || code == 32 || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 9 || code == 45 || code == 95 || code == 43 || code == 38 || code == 40 || code == 41 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function alphaNumeric_withoutspace_schar(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || code == 45 || code == 92)) {
        return true;
    }
    return false;
}
function alphaBets(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || code == 116 || code == 9 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function alphaBets_withspace(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 116 || code == 32 || code == 9 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function validateRegno(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 9 || code == 8 || code == 45 || code == 92 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function alphaNumeric_withoutspace(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 9 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function isNumberKeydot(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode > 57 || charCode < 48 || charCode == 46)) {
        return false;
    }
    return true;
}
function RestrictSpace(evt) {
    if (event.keyCode == 32) {
        return false;
    }
}
function selectallchk(evt) {
    alert('fg');
    if (evt.checked) { // check select status
        $('.checkbox').each(function () { //loop through each checkbox
            this.checked = true; //select all checkboxes with class "checkbox1"
        });
    } else {
        $('.checkbox').each(function () { //loop through each checkbox
            this.checked = false; //deselect all checkboxes with class "checkbox1"
        });
    }
}
function selectall() {
    /*	alert(evt);
     if(evt.checked) { // check select status
     $('.checkbox').each(function() { //loop through each checkbox
     this.checked = true;  //select all checkboxes with class "checkbox1"
     });
     }else{
     $('.checkbox').each(function() { //loop through each checkbox
     this.checked = false; //deselect all checkboxes with class "checkbox1"
     });
     }*/
}
$("#id").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Enter Digits Only").show().fadeOut("slow");
        return false;
    }
});
$(document).ready(function () {
    $("#amount").blur(function () {
        var amt = parseFloat(this.value);
        $(this).val(amt.toFixed(2));
    });
    // $('input[id=selectall]').on('ifCreated ifClicked ifChanged ifChecked ifUnchecked ifDisabled ifEnabled ifDestroyed', function (event) {
    //		alert('<li><span>#' + this.id + '</span> is ' + event.type.replace('if', '').toLowerCase() + '</li>');
    //});
    $('#selectall').on('ifChecked', function (event) {
        $('.check').iCheck('check');
    });
    $('#selectall').on('ifUnchecked', function (event) {
        $('.check').iCheck('uncheck');
    });
    // Removed the checked state from "All" if any checkbox is unchecked
    $('#selectall').on('ifChanged', function (event) {
        if (!this.changed) {
            this.changed = true;
            $('#selectall').iCheck('check');
        } else {
            this.changed = false;
            $('#selectall').iCheck('uncheck');
        }
        $('#selectall').iCheck('update');
    });
});
$.fn.serializeObject = function ()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
function loadPreloader() {
		$("#preimg").fadeIn();
		$("#preloader").fadeIn();
}
function removePreloader() {
     $("#preimg").fadeOut();
     $("#preloader").fadeOut();
}
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function stripHtmlTags(string) {
    return string.replace(/(<([^>]+)>)/ig, "");
}
function addSlashes(string) {
    return string.replace(/\\/g, '\\\\').
            replace(/\u0008/g, '\\b').
            replace(/\t/g, '\\t').
            replace(/\n/g, '\\n').
            replace(/\f/g, '\\f').
            replace(/\r/g, '\\r').
            replace(/'/g, '\\\'').
            replace(/"/g, '\\"');
}
function stripSlashes(string) {
    return string.replace(/\\/g, '');
}
$(document.body).on('click', '.business_card_info', function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var relation_id = $(this).data('relation_id');
    $.ajax({
        type: "post",
        data: {id: relation_id},
        url: url,
        beforeSend: function () {
            $("#user_data .modal-body").html('Loading..');
            $("#user_data").modal();
        },
        success: function (data) {
            $("#user_data .modal-body").html("");
            $("#user_data .modal-body").html(data);
            $("#user_data").modal();
        }
    });
});
function checkformat(ele, doctypes, str) {
    txtext = ele;
    txtext = txtext.toString().toLowerCase();
    fformats = doctypes.split('|');
    if (fformats.indexOf(txtext) == -1) {
        return false;
    } else {
        return true;
    }
}
function append_laravel_error(error, form) {
    if (error != undefined) {
        $.each(error, function (k, e) {
            k = dot_to_array_name(k);
            $('[for="' + k + '"]', form).remove();
            if ($('[name="' + k + '"]', form).hasClass('noValidate') == false)
            {
                if ($('[name="' + k + '"]', form).length == 1) {

                    if ($('[name="' + k + '"]', form).data('errmsg_to') != undefined) {
                        /* display errmsg on single container */
                        $('#' + $('[name="' + k + '"]', form).data('errmsg_to'), form).attr({for : k, class: 'text-danger'}).append(e);
                        $('[name="' + k + '"]', form).on('change', function () {
                            $('[for="' + $(this).attr('name') + '"]', form).remove();
                        });
                    }
                    else {
                        $('[name="' + k + '"]', form).after($('<span>').attr({for : k, class: 'text-danger'}).html(e)).on('change', function () {
                            $('[for="' + $(this).attr('name') + '"]', form).remove();
                        });
                    }
                }
                else if ($('[name="' + k + '"]', form).length > 1) { /* display errmsg for radio control */
                    $('#' + k + '_errmsg', form).attr({for : k, class: 'text-danger'}).html(e);
                    $('[name="' + k + '"]', form).on('change', function () {
                        $('[for="' + $(this).attr('name') + '"]', form).remove();
                    });
                }
            }
            /*if ($('[name="' + k + '"]', form).attr("checked",false))
             {
             $('#rating_error').html('This field cannot be empty')
             if ($('[name="' + k + '"]', form).is(':checked')) {
             $('#rating_error').text('');
             }
             }*/
        });
    }
}

function upload(url, id)
{
    $("#" + id).uploadFile({
        url: url,
        method: "POST",
        fileName: "myfile",
        dragDrop: false,
        multiple: false,
        returnType: "json",
        allowedTypes: "wmv,mp3,jpg,png,gif,doc,docx,pdf",
        sequential: false,
        sequentialCount: 1,
        maxFileCount: 5,
        showFileCounter: false,
        showDelete: true,
        showDownload: false,
        showPreview: false,
        abortStr: "x",
        deletelStr: "x",
        uploadStr: "Change",
        onLoad: function (obj) {
            file_id = obj.formGroup;
        },
        onSuccess: function (files, data, xhr, pd) {
            $('#add_images').append($('<input>').attr('type', 'hidden').attr('name', 'files[]').val(data.img_path));
            $('#image_product_id').val(product_id);
        }
    });
}

function dot_to_array_name(str) {
    var arr, s = '';
    arr = str.split('.');
    s = arr[0];
    for (var i in arr) {
        if (i != 0) {
            s += '[' + arr[i] + ']';
        }
    }
    return s;
}
$.fn.extend({
    pageProductBox: function (pageProductBox) {
        if (pageProductBox != undefined && pageProductBox != '' && pageProductBox != null && pageProductBox.products.length > 0) {
            var _this = this;
            _this.html('');
            _this.append([
                $('<h3>').attr({class: 'heading'}).text(pageProductBox.title),
                function () {
                    var products = [];
                    for (var i in pageProductBox.products) {
                        var product = pageProductBox.products[i];
                        products.push($('<li>').attr({class: 'col-sx-12 col-sm-4'}).append([
                            $('<div>').attr('class', 'product-container').append([
                                $('<div>').attr('class', 'left-block').append([
                                    $('<a>').attr({href: product.url}).append(
                                            $('<img>').attr({class: 'img-responsive', 'src': product.imgs[0].img_path})
                                            ),
                                    $('<div>').attr({class: 'quick-view'}).append([
                                        (UserDetails != undefined) ? $('<a>').attr({href: '#', title: 'Add to my wishlist', class: 'heart add-to-wish-list', 'data-supplier_product_id': product.supplier_product_id, 'data-spc_id': product.spc_id}) : '',
                                        $('<a>').attr({href: '#', title: 'Add to compare', class: 'compare'}),
                                        $('<a>').attr({href: '#', title: 'Quick view', class: 'search'})
                                    ]),
                                    $('<div>').attr({class: 'add-to-cart'}).append([
                                        $('<a>').attr({href: '#add', title: 'Add to Cart', class: 'add-to-cart', 'data-supplier_product_id': product.supplier_product_id, 'data-spc_id': product.spc_id}).html('Add to Cart')
                                    ])
                                ]),
                                $('<div>').attr('class', 'right-block').append([
                                    $('<h5>').attr({'class': 'product-name'}).append($('<a>').attr({'href': '#'}).text(product.product_name)),
                                    $('<div>').attr({'class': 'product-star'}).append(function () {
                                        var stars = [];
                                        if (product.rating_count == null) {
                                            product.rating_count = 0
                                        }
                                        for (var i = 1; i <= parseInt(product.rating_count); i++) {
                                            stars.push($('<i>').attr('class', 'fa fa-star'));
                                        }
                                        for (var i = 1; i <= 5 - parseInt(product.rating_count); i++) {
                                            stars.push($('<i>').attr('class', 'fa fa-star-o'));
                                        }

                                        return stars;
                                    }),
                                    $('<div>').attr({'class': 'content_price'}).append([
                                        $('<span>').attr('class', 'price product-price').text(product.selling_price),
                                        $('<span>').attr('class', 'price old-price').text(product.price),
                                    ]),
                                    $('<div>').attr({'class': 'info-orther'}).append([
                                        $('<p>').html('Item Code: #453217907'),
                                        $('<p>').attr('class', 'availability').html('<span>In stock</span>')
                                    ])
                                ])
                            ])
                        ]));
                    }
                    return products;
                }
            ])
        }
    }
});
$(window).on("popstate", function (e) {
    console.log(e);
    if (e.originalEvent.state !== null) {
        console.log(e.originalEvent.state);
    }
});
