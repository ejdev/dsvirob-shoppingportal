var otp_code_val = '';
var mobile = 0;
$(document).ready(function () {

    $('#forgot_password').on('click', function () {
        $('#email_mob_verification').html('');
        $('#forgot_password_container').show();
        $("#forgot_form")[0].reset();
        $('#login_register_container').hide();
    });

    $('#reset_otp_btn, #sign_up').on('click', function () {
        $('.email_phone_error').html('');
        if ($('#email_mobile').val() != '') {
            $.ajax({
                    url: ajaxUrl + 'check_email_phone', // Url to which the request is send
                    type: "POST", // Type of request to be send, called as method
                    data: {'email_mobile': $('#email_mobile').val()}, // Data sent to server, a set of key/value pairs representing form fields and values
                    datatype: "json",
                    beforeSend: function () {
                                    loadPreloader();
                                    $(this).attr('disabled', true);
                    },
                    success: function (data) {
                                    removePreloader();
                                    if (data.status == 'ok') {
                                        if (data.option == 'mobile') {
                                            $("#otp_fld").html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                                            $('.email_reg').show();
                                            $('#email_id').prop('disabled', false);
                                            $('.signup_cont').hide();
                                            $('#otp_validate').show();
                                            $('#sign_up').hide();
                                            $('#sign_up_btn').hide();
                                            $('#new-acc').show();
                                            $('#otp_code').val('');
                                            mobile = 1;
                                        } else {
//$("#msg").html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                                            $('#new-acc').hide();
                                            $('#mob_ver_tit').hide();
                                            $('#otp_validate').hide();
                                            $('.mobile_reg_sec').show();
                                            $('#mobile_no').prop('disabled', false);
                                            $('#otp_valid_reg').show();
                                            $('#sign_up').hide();
                                            $('#sign_up_btn').show();
                                            mobile = 0;
                                        }
                                        //$("#otp_fld").html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                                    } else {
                                        $("#email_phone_error").html('<span style="color:red; font-size:11px">' + data.msg + '</span>');
//$("#msg").html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                                    }
                    },
                    error: function () {
                                    //$('#sign_up_btn').show();					 
                                    $("#email_avail_status").text('');
                                    $('#new-acc').hide();
                                    $('#mob_ver_tit').hide();
                                    $('#otp_validate').hide();
                                    $('.mobile_reg_sec').hide();
                                    $('#mobile_no').prop('disabled', false);
                                    $('#otp_valid_reg').hide();
                                    $('#sign_up').show();
                                    $('#sign_up_btn').hide();
                                    $('.signup_cont').show();
                                    $(this).attr('disabled', false);
                                    removePreloader();
                                    alert('Something went wrong');
                                    return false;
                    }
            });
        } else {
            $('.email_phone_error').addClass('text-danger').html('Please enter your  Email id/Mobile no');
        }
    });

    $(document.body).on('change', '#email_mobile', function () {
        $("#email_avail_status").text('');
        $('#new-acc').hide();
        $('#mob_ver_tit').hide();
        $('#otp_validate').hide();
        $('.mobile_reg_sec').hide();
        $('#mobile_no').prop('disabled', false);
        $('#otp_valid_reg').hide();
        $('#sign_up').show();
        $('#sign_up_btn').hide();
        $('.signup_cont').show();
        $("#signupfrm")[0].reset();
    });

    // Setup form validation on the #create_user element
    $("#signupfrm").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        // Specify the validation rules
        rules: {
            full_name: "required",
            password:
                    {required: true,
                        minlength: 4,
                        maxlength: 8,
                    },
            mobile_no: {
                digits: true,
                minlength: 10,
                maxlength: 10,
                required: function (element) {
                    return $("#mobile_no").is(':empty');
                }
            },
            email_id: {
                email: true,
                required: function (element) {

                    return $("#email_id").is(':empty');
                }
            },
        },
        // Specify the validation error messages
        messages: {
            full_name: "Please enter your Full name",
            password: {
                required: "Please enter password",
                maxlength: "Password should not be greather than 8 charecters",
                minlength: "Password should not be less  than 4 charecters"
            },
            email_id: {
                required: "Please provide a valid email",
            },
            mobile_no: {
                required: "Please provide a valid Mobile Number",
            },
        },
        submitHandler: function (form, event) {
            event.preventDefault();
            if ($(form).valid()) {
                var datastring = $(form).serialize();
                $.ajax({
                    url: $(form).attr('action'), // Url to which the request is send
                    type: "POST", // Type of request to be send, called as method
                    data: datastring, // Data sent to server, a set of key/value pairs representing form fields and values
                    datatype: "json",
                    success: function (data) {
                                if (data.status == 'ok') {
                                    $('#email_mobile').val('');
                                    $('#mob_ver_tit').show();
//$("#msg").html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                                    $('#otp_validate').hide();
                                    $('#otp_valid_reg').hide();
                                    $("#signupfrm")[0].reset();
                                    $('#sign_up').show();
                                    $('.signup_cont').show();
                                    removePreloader();
                                } else {
                                    if (mobile == 1) {
//$("#msg1").html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                                    } else {
//$("#msg").html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                                    }
                                }
                    },
                    error: function () {
                                    $('#mob_ver_tit').show();
                                    $('#otp_validate').hide();
                                    $('#otp_valid_reg').show();

                                    removePreloader();
                                    alert('Something went wrong');

                                    return false;
                    }
                });
            }
        }
    });

    $("#otp_btn").on('click', function (e) {
//$("#msg").html('');
        $('#sign_up_btn').hide();
        e.preventDefault();
        if ($('#otp_code').val() != '') {
            // Specify the validation error messages
            $.ajax({
                    url: ajaxUrl + 'check/register/otp', // Url to which the request is send
                    type: "POST", // Type of request to be send, called as method
                    data: {'otp': $('#otp_code').val()}, // Data sent to server, a set of key/value pairs representing form fields and values
                    datatype: "json",
                    beforeSend: function () {
                                    loadPreloader();
                                    $(this).attr('disabled', true);
                    },
                    success: function (data) {
                                removePreloader();
                                if (data.status == 'ok') {
                                    $("#otp_fld").html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data.msg + '</div>');
                                    $('#otp_validate').hide();
                                    $('#otp_valid_reg').show();
                                    $('#sign_up').hide();
                                    $('#sign_up_btn').show();
                                }
                    },
                    error: function () {
                                $('#sign_up_btn').hide();
                                $("#email_avail_status").text('');
                                $(this).attr('disabled', false);
                                removePreloader();
                                alert('Something went wrong');
                                return false;
                    }
            });
        } else {
            $('#otp_code').after('Please Enter your OTP').addClass('text-danger');
        }
    });

    $("#loginfrm").validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        // Specify the validation rules
        rules: {
            username: "required",
            password:
                    {required: true,
                    },
        },
        // Specify the validation error messages
        messages: {
            username: "Please enter username",
            password: {
                required: "Please enter password .",
            },
        },
        submitHandler: function (form, event) {
            event.preventDefault();
            $("#signin_username_error").html("");
            $("#signin_password_error").html("");
            if ($(form).valid()) {
                var datastring = $(form).serialize();
                $.ajax({
                        url: $(form).attr('action'), // Url to which the request is send
                        type: "POST", // Type of request to be send, called as method
                        data: datastring, // Data sent to server, a set of key/value pairs representing form fields and values
                        datatype: "json",
                        success: function (data) {
                                    var resp = data.response;
                                    if (resp.status == 'error') {
                                        if (data.status_id == 3) {
                                            $("#signin_password_error").html("");
                                            $("#signin_username_error").html("&nbsp;&nbsp;&nbsp;&nbsp;" + resp.msg);
                                        }
                                        else if (data.status_id == 6) {
                                            $("#signin_username_error").html("");
                                            $("#signin_password_error").html("&nbsp;&nbsp;&nbsp;&nbsp;" + resp.msg);
                                        }
//$("#login_err").html('<div class="alert alert-danger"><strong>' + resp.msg + '</strong></div>');
                                    }
                                    if (resp.status == 'ok') {
//$("#login_err").html('<div class="alert alert-success"><strong>' + resp.msg + '</strong></div>');
                                        window.location.href = resp.url;
                                    }
                        },
                        error: function () {
                                    $('#login_button').html('LOG IN').attr('disabled', false);
                                    return false;
                        }
                });
            }
        }
    });

    $(document).on("click", "#forgot_password", function (e) {
        e.preventDefault();
        $('.alert').remove();
        $('#signin').hide();
        $('#forgot_pwd').show();
    });

});
