jQuery.fn.extend({	
    loadProducts: function (options) {
        options = (options !== undefined) ? options : {};
        var _this = this;
        _this.xhr = false;
        _this.searchTerm = '';
        _this.currentPage = 1;
        _this.totalPages = 0;
        _this.sort_by = 'POPULARITY';
        _this.brand_id = null;
        _this.caregory_id = null;
        _this.totalProducts = 0;
        _this.products = [];
        _this.brands = [];
        _this.categories = [];
        _this.properties = [];
        _this.options = {
            productsPerPage: Constants.BROWSE_PRODUCTS_PAGE_LENGTH,
            url: '',
            data: {},
            success: null
        };
        $.extend(_this.options, options);
        _this.printCategory = function (e, c) {
            var li = $('<li>').append('<span>').append($('<a>').attr({href: c.url, class: 'change-category'}).text(c.category));
            e.append(li);
            if (c.children != undefined) {
                var sub = $('<ul>').attr({style: 'display:block'});
                $.each(c.children, function (k, se) {
                    _this.printCategory(sub, se);
                });
                li.append(sub);
            }
        };
        _this.updateContent = function () {
            removePreloader();
            $('.categories-list').removeClass('ajax-loader');
            var listedProducts = (_this.currentPage * _this.options.productsPerPage);
            listedProducts = (_this.totalProducts > listedProducts) ? listedProducts : _this.totalProducts;
            $('.page-heading-title').html([_this.data.title, $('<small>').attr({}).append(['1 to ', listedProducts, ' of ', _this.totalProducts])]);
            document.title = _this.data.title;
            if (_this.data.breadcrums != undefined) {
                $('.breadcrumb').html('');
                $.each(_this.data.breadcrums, function (k, e) {
                    $('.breadcrumb').append($('<a>').attr({href: e.url, class: 'change-category'}).text(e.categories));
                    if (k < _this.data.breadcrums.length - 1)
                        $('.breadcrumb').append($('<span>').attr('class', 'navigation-pipe'));
                });
            }
            if (_this.data.categories != undefined) {
                $('.categories-list').html('');
                $.each(_this.data.categories, function (k, e) {
                    _this.printCategory($('.categories-list'), e);
                });
            }
            $('#filters').loadFilters(_this.data.filters);
			$('#browse-tags').loadTags(_this.data.tags);
        };
        _this.loadProducts = function (empty, withFilters) {
            empty = (empty != undefined) ? empty : false;
            withFilters = (withFilters != undefined) ? withFilters : false;
            _this.options.data.page = _this.currentPage;
            _this.options.data.searchTerm = _this.searchTerm;
            _this.options.data.brand_id = _this.brand_id;
			_this.options.data.tag = $('#browse-tags a.active').data('tag');
            _this.options.data.sort_by = $('#sort_by').val();
            var temp = {};
            if (withFilters) {
                $.extend(temp, _this.options.data, $.getURLParams());
            }
            else {
                var url_data = $.getURLParams();
                $.extend(temp, _this.options.data, {spath: url_data.spath});
            }
            if (_this.xhr && _this.xhr.readyState !== 4) {
                _this.xhr.abort();
            }
            _this.xhr = $.ajax({
                type: "POST",                
                url: ajaxUrl + 'product/list',
                data: temp,
                dataType: 'JSON',
                beforeSend: function () {
                    loadPreloader();
                    _this.addClass('ajax-loader');
                },
                success: function (data) {
                    removePreloader();
                    _this.data = data;
                    _this.totalProducts = data.totalProducts;
                    _this.totalPages = (_this.totalProducts / _this.options.productsPerPage) + (((_this.totalProducts % _this.options.productsPerPage) > 0) ? 1 : 0);
                    _this.updateContent();
                    _this.products = (empty) ? data.products : _this.products.concat(data.products);
                    _this.print();
                    _this.removeClass('ajax-loader');
                },
                error: function () {
                    removePreloader();
                }
            });
        };
        _this.print = function () {
            _this.printProducts();
            _this.printPage();
        };
        _this.printProducts = function () {
            _this.html('');
            for (var i in _this.products) {
                _this.append(_this.layout(_this.products[i]));
            }
        };
        _this.layout = function (product) {
            return $('<li>').attr({class: 'col-sx-12 col-sm-4'}).append(
                    $('<div>').attr('class', 'product-container').append(
                    [
                        $('<div>').attr('class', 'left-block').append(
                                [
                                    $('<a>').attr({href: product.url}).append(
                                            $('<img>').attr({class: 'img-responsive', 'src': product.imgs[0].img_path})
                                            ),
                                    $('<div>').attr({class: 'quick-view'}).append([
										  $('<a>').attr({href: '#', title: 'Add to my wishlist', class: 'heart add-to-wish-list', 'data-spid': product.supplier_product_id}),
										   $('<a>').attr({href: '#', title: 'Add to compare', class: 'compare'}),
										  $('<a>').attr({href: '#', title: 'Quick view', class: 'search'})
                                    ]),
                                    $('<div>').attr({class: 'add-to-cart'}).append(
                                            $('<a>').attr({href: '#add', title: 'Add to Cart', class: 'add-to-cart-btn', 'data-spid': product.supplier_product_id}).html('Add to Cart')
                                            )
                                ]),
                        $('<div>').attr('class', 'right-block').append(
                                [
                                    $('<h5>').attr({'class': 'product-name'}).append($('<a>').attr({'href': '#'}).text(product.name)),
                                    $('<div>').attr({'class': 'product-star'}).append(function () {
                                        var stars = [];
                                        if (product.rating_count == null) {
                                            product.rating_count = 0
                                        }
										if (product.avg_rating > 0) {
											for (var i = 1; i <= parseInt(product.avg_rating); i++) {
												stars.push($('<i>').attr('class', 'fa fa-star'));
											}
											for (var i = 1; i <= 5 - parseInt(product.avg_rating); i++) {
												stars.push($('<i>').attr('class', 'fa fa-star-o'));
											}
										} else {
											for (var i = 1; i <= 5 - 0; i++) {
												stars.push($('<i>').attr('class', 'fa fa-star-o'));
											}
										}
                                        return stars;
                                    }),
									$('<br>'),
                                    $('<div>').attr({'class': 'content_price'}).append(function () {
                                        var price = [];
										price.push($('<span>').attr('class', 'price product-price').text(product.currency_code +' '+product.price));
							if (product.price < product.mrp_price) {
										price.push($('<span>').attr('class', 'price old-price').text(product.currency_code +' '+product.mrp_price));
									if (product.off_per != undefined) {							
										price.push($('<span>').attr('class', 'price off-per').text(Math.floor(product.off_per) + '% OFF'));
									}
							}
							
							
                                        return price;
                                    }),
                                    $('<div>').attr({'class': 'info-orther'}).append(
                                            [
                                                $('<p>').html('Item Code: ' + product.code),
                                                $('<p>').attr('class', 'availability').html('<span>In stock</span>')
                                            ])
                                ])
                    ])
                    );
        };
        _this.printPage = function () {
            $('.pagination').html('');
            $('.pagination').append([
                $('<li>').append([
                    $('<a>').attr({'href': '#'}).append([
                        'More...'
                    ]).click(function (e) {
                        e.preventDefault();
                        _this.nextPage();
                    })
                ])
            ]);
        };
        _this.goToPage = function (p) {
            if (p >= 1 && _this.totalPages <= p) {
                _this.currentPage = p;
                _this.loadProducts(false, true);
            }
        };
        _this.nextPage = function () {
            if (_this.totalPages >= _this.currentPage + 1) {
                _this.currentPage += 1;
                _this.loadProducts(false, true);
            }
        };
        _this.previousPage = function () {
            _this.currentPage - 1;
            if ((_this.currentPage - 1) >= 1) {
                _this.currentPage -= 1;
                _this.loadProducts(false, true);
            }
        };
        _this.loadProducts(true, true);
        return _this;
    },
	loadTags: function (tags) {
        var _this = $(this);
        _this.html('');
        if (tags != null && tags != undefined && tags.length > 0) {
            $.each(tags, function (k, tag) {
                _this.append($('<a>', {class: 'browse-tag', 'data-tag': tag, href: '#'}).append($('<span>', {class: 'level' + (Math.floor(Math.random() * 5) + 1)}).append(tag)))
            });
        }
    },
    loadFilters: function (filters) {
        var _this = $(this);
        _this.html('');
        _this.addFilter = function (e) {
            var filter = '';
            switch (e.type)
            {
                case Constants.FILTER_TYPE.CHECKBOX:
                    filter = $('<div>').attr({class: 'layered-content'}).append(
                            $('<ul>').attr({class: 'check-box-list'}).append(function () {
                        var options = [];
                        $.each(e.options, function (k, option) {
                            options.push(
                                    $('<li>')
                                    .append(
                                            [
                                                $('<input>').attr({type: 'checkbox', name: 'f[' + e.name + '][' + option.id + ']', checked: option.checked, disabled: option.disabled, id: 'filter_' + e.name + '_' + option.id, value: option.id}),
                                                $('<label>').attr({for : 'filter_' + e.name + '_' + option.id}).append([
                                                    $('<span>').attr({class: 'button'}),
                                                    option.name
                                                ])
                                            ])
                                    );
                        });
                        return options;
                    }));
                    break;
                case Constants.FILTER_TYPE.RANGE:
                    filter = $('<div>').attr({class: 'layered-content'}).append([
                        $('<div>').attr({'data-label-reasult': 'Range:', 'data-min': 0, 'data-max': 500, 'data-unit': '', 'class': 'slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all'}),
                        $('<ul>').attr({class: 'check-box-list slider-range'}).append(function () {
                            var options = [];
                            $.each(e.options, function (k, option) {
                                options.push(
                                        $('<li>')
                                        .append(
                                                [
                                                    $('<input>').attr({type: 'checkbox', name: 'f[' + e.name + '][' + option.id + ']', checked: option.checked, disabled: option.disabled, id: 'filter_' + e.name + '_' + option.id, value: option.id}),
                                                    $('<label>').attr({for : 'filter_' + e.name + '_' + option.id, 'style': 'background:' + option.name + ';'}).append([
                                                        $('<span>').attr({class: 'button'}),
                                                        option.name
                                                    ])
                                                ])
                                        );
                            });
                            return options;
                        })
                    ]);
                    break;
                case Constants.FILTER_TYPE.COLOR:
                    filter = $('<div>').attr({class: 'layered-content'})
                            .append(
                                $('<ul>').attr({class: 'check-box-list filter-color'}).append(function () {
                                var options = [];
                                $.each(e.options, function (k, option) {
                                    options.push(
                                            $('<li>')
                                            .append(
                                                    [
                                                        $('<input>').attr({type: 'checkbox', name: 'f[' + e.name + '][' + option.id + ']', checked: option.checked, disabled: option.disabled, id: 'filter_' + e.name + '_' + option.id, value: option.id}),
                                                        $('<label>').attr({for : 'filter_' + e.name + '_' + option.id, 'style': 'background:' + option.name + ';'}).append([
                                                            $('<span>').attr({class: 'button'})
                                                        ])
                                                    ])
                                            );
                                });
                                return options;
                            }));
                    break;
            }
            return filter;
        };
        if (filters != undefined || filters != null) {
            $.each(filters, function (k, e) {
                _this.append(
                        [
                            $('<div>').attr({class: 'layered_subtitle'}).text(e.title),
                            _this.addFilter(e)
                        ]);
            });
        }
    },
});
$(document).ready(function () {
	if ($('.product-list').length) {
		var p = $('.product-list').loadProducts(false, true);
	}
	
    $(document.body).on('change', '#sort_by,#filters input[type="checkbox"]', function () {
        var data = $('#filter-form').serializeArray();
        var url_data = $.getURLParams();
        data.push({name: 'spath', value: url_data.spath});
        AddToUrl(document.title, $.param(data));
        p.loadProducts(true, true);
    });
	$(document.body).on('click', '.browse-tag', function (e) {
        e.preventDefault();
        $(this).addClass('active');
        p.loadProducts(true, true);
    })
    $(document.body).on('click', '.change-category .change-category,.change-category', function (e) {
        e.preventDefault();
        var CurEle = $(this);
        ChangeUrl(CurEle.text(), CurEle.attr('href'));
        p.loadProducts(true);
    });  
    $('#search-products').on('submit', function (e) {		
        e.preventDefault();
		ChangeUrl($('#searchCategory option:selected').text(), $('#searchCategory option:selected').data('url'));
		p.searchTerm = $('#searchTerm', $('#search-products')).val();
        p.loadProducts(true);        
    });
});
