<?php

return[
    'signup'=>[
        'validation'=>[
            'login_mst.email.required'=>'Please enter your Email ID',
            'login_mst.email.email'=>'Please enter your valid Email ID',
            'login_mst.email.unique'=>'Email ID Alredy exist',
            'login_mst.mobile.required'=>'Please Enter your Mobile No.',
            'login_mst.mobile.unique'=>'Mobile No aleready exist',
        ]
    ],
    'accountdetails'=>[
        'validation'=>[
            'login_mst.email.required'=>'Please enter your Email ID',
            'login_mst.email.email'=>'Please enter your valid Email ID',
            'login_mst.email.unique'=>'Email ID Alredy exist',
            'login_mst.mobile.required'=>'Please enter your Mobile No.',
            'login_mst.mobile.unique'=>'Mobile No aleready exist',
            'login_mst.pass_key.required'=>'Please enter your Password',
            'login_mst.pass_key.confirmed'=>'Confirm Password doesn\'t match',
            'login_mst.pass_key_confirm.required'=>'Please enter your Confirm Password',
            'login_mst.pass_key_confirm.confirmed'=>'Confirm Password doesn\'t match',
            'account_mst.firstname.required'=>'Please enter your First Name',
            'account_mst.firstname.min'=>'First Name must be min 3 characters length',
            'account_mst.lastname.required'=>'Please enter your Last Name',
            'account_mst.lastname.min'=>'Last Name must be min 3 characters length',
            'account_supplier.company_name.required'=>'Please enter your Company name',
            'account_supplier.company_name.min'=>'Company name must be min 3 characters length',
            'address.state_id.required'=>'Please select your State',
            'address.postal_code.required'=>'Please enter your Postal Code',
            'address.city.required'=>'Please enter your City',
            'address.street1.required'=>'Please enter your Address 1',
            'address.street2.required'=>'Please enter your Address 2',
        ]
    ],
    'updateaccount'=>[
        'validation'=>[
            'login_mst.email.required'=>'Please enter your Email ID',
            'login_mst.email.email'=>'Please enter your valid Email ID',
            'login_mst.email.unique'=>'Email ID Alredy exist',
            'login_mst.mobile.required'=>'Please enter your Mobile No.',
            'login_mst.mobile.unique'=>'Mobile No aleready exist',
            'account_mst.firstname.required'=>'Please enter your First Name',
            'account_mst.firstname.min'=>'First Name must be min 3 characters length',
            'account_mst.lastname.required'=>'Please enter your Last Name',
            'account_mst.lastname.min'=>'Last Name must be min 3 characters length',
            'store_extras.state_id.required'=>'Please select your State',
            'store_extras.postal_code.required'=>'Please enter your Postal Code',
            'store_extras.city.required'=>'Please enter your City',
            'store_extras.address1.required'=>'Please enter your Address 1',
            'store_extras.address2.required'=>'Please enter your Address 2',
            'account_supplier.website.required'=>'Please enter your website',
            'account_supplier.type_of_bussiness.required'=>'Please select your business type',
            'store_extras.working_hours_from.required'=>'Please enter your working from time',
            'store_extras.working_hours_to.required'=>'Please enter your working to time'
        ]
    ],
    'kyc_verification'=>[
        'validation'=>[
            'kyc_verifiacation.pan_card_no.required'=>'Please enter your PANcard No',
            'kyc_verifiacation.pan_card_no.regex'=>'Please enter the valid PAN card No',
            'kyc_verifiacation.pan_card_no.unique'=>'PAN Card No already exist',
            'kyc_verifiacation.pan_card_name.required'=>'Please enter your PAN Card Name',
            'kyc_verifiacation.pan_card_name.min'=>'PAN Card Name must be min 3 characters length',
            'kyc_verifiacation.dob.required'=>'Please enter your DOB',
            'kyc_verifiacation.dob.date_format'=>'Please enter your valid DOB',
            'kyc_verifiacation.dob.datedetween'=>'Please enter your DOB between :min & :max',
            'kyc_verifiacation.pan_card_image.required'=>'Please select your PAN Card Image',
            'kyc_verifiacation.vat_no.required'=>'Please enter your VAT No',
            'kyc_verifiacation.cst_no.required'=>'Please enter your CST No',
            'kyc_verifiacation.auth_person_name.required'=>'Please enter your Authorize Person Name',
            'kyc_verifiacation.auth_person_id_proof.required'=>'Please enter your Authorize Person ID Proof'
        ]
    ],
    'storebanking'=>[
        'validation'=>[
            'payment_setings.bank_name.required'=>'Please enter your Bank Name',
            'payment_setings.bank_name.min'=>'Bank Name must be min 3 characters length',
            'payment_setings.account_holder_name.required'=>'Please enter your Account Holder Name',
            'payment_setings.account_holder_name.min'=>'Account Holder Name must be min 3 characters length',
            'payment_setings.account_no.required'=>'Please enter your Account Number',
            'payment_setings.account_no.min'=>'Please enter the valid Account Number',
            'payment_setings.account_no.max'=>'Please enter the valid Account Number',
            'payment_setings.account_type.required'=>'Please select your Account Type',
            'payment_setings.ifsc_code.required'=>'Please enter your IFSC Code',
            'payment_setings.ifsc_code.regex'=>'Please enter the valid IFSC Code',
            'payment_setings.state_id.required'=>'Please select your State',
            'payment_setings.postal_code.required'=>'Please enter your Postal Code',
            'payment_setings.city.required'=>'Please enter your City',
            'payment_setings.address1.required'=>'Please enter your Address 1',
            'payment_setings.address2.required'=>'Please enter your Address 2',
        ]
    ]
];
