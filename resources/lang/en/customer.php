<?php

return [
    'email'=>[
        'order_placed'=>'Your Order has been placed',
        'account_created'=>'Account Created',
        'verify_email'=>'Email ID verification Code',
        'verify_mobile'=>'Mobile verification Code'
    ],
    'sms'=>[
        'order_placed'=>'Hi :name(:uname), Your Order has been placed',
        'account_created'=>'Hi :name(:uname), Your account has been created successfully',
        'sign_up_mobile_verification'=>'Hi, Your Verification code is :code',
        'reset_pwd_verification'=>'Hi :name(:uname), Your Verification code to reset the password is :code',
        'verify_email'=>'Hi :name(:uname), Your Verification code to verify your email is :code',
        'verify_mobile'=>'Hi :name(:uname), Your Verification code to verify your mobile is :code'
    ],
    'notification'=>[
        'order_placed'=>['title'=>'Your Order has been placed', 'body'=>'Your Order has been placed'],
        'account_created'=>['title'=>'Your Account created', 'body'=>'Your account has been created successfully'],
        'verify_email'=>['title'=>'Verification Code has been sent', 'body'=>'Your Email ID verification code has been sent to your Email ID'],
        'verify_mobile'=>['title'=>'Verification Code has been sent', 'body'=>'Your Mobile No. verification code has been sent to your Mobile No']
    ],
    'signup_validation'=>[
        'mobile.required'=>'Please enter your Mobile No',
        'mobile.numeric'=>'Please enter your valid Mobile No',
        'mobile.unique'=>'Mobile No already exist',
        'verification_code.required'=>'Please enter the verification code',
        'verification_code.numeric'=>'Please enter valid verification code',
        'verification_code.min'=>'Please enter valid verification code',
        'verification_code.max'=>'Please enter valid verification code',
        'password.required'=>'Please enter your password',
        'password.confirmed'=>'Please enter your confirmation password',
        'password_confirmation.required'=>'Please enter your confirm password'
    ],
    'signup_mobile_validation'=>[
        'mobile.required'=>'Please enter your Mobile No',
        'mobile.numeric'=>'Please enter your valid Mobile No',
        'mobile.unique'=>'Mobile No already exist',
    ],
    'check_account_validation'=>[
        'username.required'=>'Please enter your Mobile No / Email ID'
    ],
    'reset_password_validation'=>[
        'username.required'=>'Please enter your Mobile No / Email ID',
        'verification_code.required'=>'Please enter your verification code',
        'verification_code.numeric'=>'Please enter valid verification code',
        'verification_code.min'=>'Please enter valid verification code',
        'verification_code.max'=>'Please enter valid verification code',
        'password.required'=>'Please enter your Password',
        'password.confirmed'=>'Please enter your confirmation password',
        'password_confirmation.required'=>'Please enter your confirm password',
    ],
    'verify_mail_id_validation'=>[
        'verification_code.required'=>'Please enter your verification code',
        'verification_code.numeric'=>'Please enter valid verification code',
        'verification_code.min'=>'Please enter valid verification code',
        'verification_code.max'=>'Please enter valid verification code',
    ],
    'change_password_validation'=>[
        'current_password.required'=>'Please enter your Password',
        'new_password.required'=>'Please enter your new password',
        'new_password.confirmed'=>'Please enter your new confirmation password',
        'new_password_confirmation.required'=>'Please enter new your confirmation password'
    ]
];
