<?php 
return[
 'acc_deactivate'=>'Deactivate Account',
 'email_addr'=>'Email Address',
 'mob_num'=>'Mobile Number',
 'pwd'=>'Password',
 'crfm_deactivate'=>'Confirm Deactivation',
 'information_deactivate'=>'  <p ><b>When you deactivate your account</b></p>
                        <ul style="list-style-type:circle; padding:15px;">
                            <li>You are logged out of your :site_name Account</li>
                            <li>Your public profile on :site_name is no longer visible</li>
                            <li>Your reviews/ratings are still visible, while your profile information is shown as ‘unavailable’ as a result of deactivation.</li>
                            <li>Your wishlist items are no longer accessible through the associated public hyperlink. Wishlist is shown as ‘unavailable’ as a result of deactivation</li>
                            <li>You will be unsubscribed from receiving promotional emails from :site_name</li>
                            <li>Your account data is retained and is restored in case you choose to reactivate your account</li>
                        </ul>
                        <p ><b>How do I reactivate my :site_name account?</b></p><br />
                        
                        <p>Reactivation is easy.</p><br />
                        <p>Simply login with your registered email id or mobile number and password combination used prior to deactivation. Your account data is fully restored. Default settings are applied and you will be subscribed to receive promotional emails from :site_name.</p><br />
                        <p>:site_name retains your account data for you to conveniently start off from where you left, if you decide to reactivate your account.</p>',
 
];
