<?php 
return[
 'updt_email_mob'=>'Update Email/Mobile',
 'updating_new_email_mob'=>'Enter the new Email ID / Mobile number you wish to associate with your  account.',
 'email_addr'=>'Email Address',
 'entr_email_addr'=>'Enter email address',
 'entr_vcode'=>'Enter Verification Code',
 'mob_num'=>'Mobile Number',
 'entr_mob_num'=>'Enter Mobile Number',
 'svn_chng'=>'Save Changes',
 'information_email_mob'=>"<li><h4>What happens when I update my email address (or mobile number)?</h4>
Your login email id (or mobile number) changes, likewise. You'll receive all your account related communication on your updated email address (or mobile number).</li>
<li><h4>When will my account be updated with the new email address (or mobile number)?</h4>
It happens as soon as you confirm the verification code sent to your email (or mobile) and save the changes.</li>
<li><h4>What happens to my existing account when I update my email address (or mobile number)?</h4>
Updating your email address (or mobile number) doesn't invalidate your account. Your account remains fully functional. You'll continue seeing your Order history, saved information and personal details.</li>
<li><h4>Does my Seller account get affected when I update my email address?</h4>
has a 'single sign-on' policy. Any changes will reflect in your Seller account also.</li>",

];
