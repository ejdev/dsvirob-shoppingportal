<?php

return[
'page_title'=>'Product Brand',
 'page_head'=>'Product Brand',
 'create_brand'=>'Create Brand',
 'created_on'=>'Created On',
 'brand_name'=>'Brand Name',
 'status'=>'Status',
 'modal_title'=>'Create Brand',
 'brand_name_label'=>'Brand Name:',
 'add_brand_btn'=>'Add Brand',
 'add_modal_title'=>'Add Brand',
];
