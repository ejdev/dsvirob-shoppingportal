<?php return  ['change_password' => 'Change Password',
				'old_password' => 'Old Password',
				'new_password' => 'New password',
				'confirm_password' => 'Confirm Password',
				'enter_your_old_password' => 'Enter Your Old Password',
				'enter_your_new_password' => 'Enter Your New Password',
				'please_retype_your_password'=>'Please Retype Your Password',

				'validation'=>['oldpassword' => 'Please enter your old password',
								'new_password' => 'Please provide a new password',
								'confirm_password' => 'Please confirm a new password',
								],

				'your_current_password' => 'Your current password',
				'repeat_new_password' => 'Repeat new password'
				];