<?php

return ['validation'=>[
            'create.store_name.required'=>'Please enter the Store name',
            'create.store_logo.required'=>'Please select an logo image',
            'create.status.required'=>'Please Select an status',
            'add.email.required'=>'Please enter the email',
            'add.email.email'=>'Please enter an valid email',
            'add.website.required'=>'Please enter the website URL',
            'add.website.url'=>'Please enter the valid URL',
            'add.mobile_no.required'=>'Please enter the mobile no',
            'add.landline_no.required'=>'Please enter the Landline no',
            'add.address1.required'=>'Please enter address',
            'add.country_id.required'=>'Please select the country',
            'add.state_id.required'=>'Please select the state',
            'add.city.required'=>'Please enter the city name',
            'add.postal_code.required'=>'Please enter the postal code',
           // 'create.supplier_id.required'=>'Please Select the Supplier',
]];