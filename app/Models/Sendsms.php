<?php
	namespace App\Models;
	
	use Illuminate\Database\Eloquent\Model;
	
	
	class Sendsms extends Model
    {
		public $APIUsername = '';
		public $APIkey = '';
		public $Senderid = '';
		public $APISendName = '';
		public $url			= '';
		public $api_status = 1;
		
		public function __construct(){
			parent::__construct();		
			
			$this->APIUsername  = "drpnwash";			
			$this->APIkey 		= "9b4dc830e9XX";			
			$this->APISendName 	= "DRPWSH";
			$this->url		    = "sms.drop2wash.com/submitsms.jsp";
		}

		
		public function send_sms($sendto, $message){
				
				$data = http_build_query(array(							
					'user'		    => $this->APIUsername,
					'key' 		    => $this->APIkey,
					'senderid' 		=> $this->APISendName,					
					'mobile'		=>  $sendto,
					'message'		=> $message,					
					'accusage'		=> 1
				));
			
			$request = curl_init($this->url.'?'.$data);
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);			
			curl_setopt($request, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($request, CURLOPT_HEADER, 0);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($request);			
			$status   = curl_getinfo($request, CURLINFO_HTTP_CODE);
			curl_close($request);			
		}
		
			/*public function send_sms($sendto,$message){
				
				$data = http_build_query(array(							
					'user'		=> $this->APIUsername,
					'key' 		=> $this->APIkey,
					'senderid' 		=> $this->APISendName,					
					'mobile'		=>  $sendto,
					'message'		=> $message,					
					'accusage'		=> 1
				));
			
			$request = curl_init($this->url.'?'.$data);
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
			
			curl_setopt($request, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($request, CURLOPT_HEADER, 0);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($request);
			
			$status   = curl_getinfo($request, CURLINFO_HTTP_CODE);
			curl_close($request);			
		}*/
		
		private function get_sms_service_settings(){
			$res = DB::table(Config::get('constants.SETTINGS'))
								->where('setting_value',Config::get('constants.ACTIVE'))
								->where('settings_type',3)
								->remember(720)
								->lists('setting_key');
			if(!empty($res)){
				return $res;
			}
			return NULL;
		}
		public function reg_send_sms($arr=array())
		{    
   
			$message = "Welcome to ".$arr['sitename']." Your Login credential are Email - ".$arr['email']." Your Password - ".$arr['pwd'];			
			$data = http_build_query(array(       
			'user'  => $this->APIUsername,
			'key'   => $this->APIkey,
			'senderid'   => $this->APISendName,     
			'mobile'  => $arr['mobile'],
			'message'  => $message,     
			'accusage'  => 1
			));
			
			$request = curl_init($this->url.'?'.$data);
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);			
			curl_setopt($request, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($request, CURLOPT_HEADER, 0);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($request);			
			$status   = curl_getinfo($request, CURLINFO_HTTP_CODE);
			curl_close($request);        
	  }
		
		
		public function sendMessage($arr=array(),$method=''){
			$this->services_settings = $this->get_sms_service_settings();
		
			if($this->api_status && method_exists($this,$method))
				if(in_array($method,$this->services_settings)){
					if(!empty($arr) && isset($arr['mobile'])){
						//$arr['sendto'] = $arr['phonecode'].$arr['mobile'];
						$arr['sendto'] = $arr['mobile'];
						$this->$method($arr);
					}
				}
		}
		
		
		/* 
		Function desc: sending registration details sms 
		param_keys: $message, $sendto;
		*/
		public function order_notification($arr=array()){			
				$message = "Your order ".$arr['code']." is ready to shipping on ". $arr['sitename'];
			
			$data = http_build_query(array(							
					'user'		=> $this->APIUsername,
					'key' 		=> $this->APIkey,
					'senderid' 		=> $this->APISendName,					
					'mobile'		=> $arr['mobile'],
					'message'		=> $message,					
					'accusage'		=> 1
		
		));
			
			$request = curl_init($this->url.'?'.$data);
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);			
			curl_setopt($request, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($request, CURLOPT_HEADER, 0);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($request);			
			$status   = curl_getinfo($request, CURLINFO_HTTP_CODE);
			curl_close($request);		
		}		
	

		public function order_payment_notification($arr=array()){		
			
			
			$message = "";
			extract($arr);			
			if(!isset($arr['message'])){
			$message = 'Your payment'." ".$amount."  ".$currency_code.' has been paid for (#'.$order_code.')';
			}				
			$this->send_sms($sendto,$message);
			return $message;
		}
		public function add_promoters_notification($arr=array())
		{
			$message = "";
			
			extract($arr);			
		  if(!isset($arr['message']))
		  {
			$message = "Welcome to ".$sitename." Your Login credential are Email - ".$email." Your Password - ".$pwd;
		  }
		 
		  $this->send_sms($sendto,$message);
			return $message;
		}
		
		public function pickup_confirm_notification($arr=array())
		{		
			$message = "";
			extract($arr);			
			if(!isset($arr['message']) && $delivery_type == Config::get('constants.PICKUP_HOME_DELIVERY')){
			$message = 'Your pickup request(#'.$pickup_code.') has been confirmed. We will Reach you soon.';
			}
			else if(!isset($arr['message'])&& $delivery_type == Config::get('constants.PICKUP_SELF_DROPS'))
			{
			$message = 'Your pickup request(#'.$pickup_code.') has been confirmed.';	
			}				
			$this->send_sms($sendto,$message);
			return $message;
		}

		
		public function pickup_otp_sms($arr=array()) {   
   
		    $message = "Welcome to ". $arr['sitename'] . "Your Pickup OTP is " . $arr['pickup_otp'] ;		   
		   	$data = http_build_query(array(							
					'user'		=> $this->APIUsername,
					'key' 		=> $this->APIkey,
					'senderid' 	=> $this->APISendName,					
					'mobile'	=> $arr['mobile'],
					'message'	=> $message,					
					'accusage'	=> 1
				));
			
			$request = curl_init($this->url.'?'.$data);			
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
			
			curl_setopt($request, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($request, CURLOPT_HEADER, 0);
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($request);			
			$status   = curl_getinfo($request, CURLINFO_HTTP_CODE);			
			curl_close($request);	
			return $status;
	    }

		
		public function otp_send_sms($arr=array()) {   		   
		   $message = "Welcome to ".$arr['sitename'].". Your (OTP) One Time Password is ".$arr['otp_pwd'];		   
		   $data = http_build_query(array(       
			 'user'  => $this->APIUsername,
			 'key'   => $this->APIkey,
			 'senderid'   => $this->APISendName,     
			 'mobile'  => $arr['mobile'],
			 'message'  => $message,     
			 'accusage'  => 1
			));		   
		   $request = curl_init($this->url.'?'.$data);
		   curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);		   
		   curl_setopt($request, CURLOPT_FOLLOWLOCATION, 1);
		   curl_setopt($request, CURLOPT_HEADER, 0);
		   curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
		   $response = curl_exec($request);		   
		   $status   = curl_getinfo($request, CURLINFO_HTTP_CODE);
		   curl_close($request);     
		   return true;		
		}

	}

