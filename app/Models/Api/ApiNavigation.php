<?php

namespace App\Models\Api;
use Illuminate\Database\Eloquent\Model;
use Config;
use URL;
use DB;

class ApiNavigation extends Model
{
    public function get_menu_ids() {
        return  DB::table(Config::get('tables.TBL_SITE_MENU'))
                        ->where('selected', Config::get('constants.ON'))
                        ->lists('menu_id', 'menu_postion_id');                   
				
    }
	
	
	public function get_menu_struct($menu_id) {
        $res = DB::table(Config::get('tables.TBL_SITE_MENU_NAVIGATION') . ' as smn')
                ->leftJoin(Config::get('tables.TBL_SITE_MENU') . ' as sm', 'smn.menu_id', '=', 'sm.menu_id')
                ->where('sm.is_deleted', Config::get('constants.OFF'))
                ->where('smn.menu_id', $menu_id)
                ->select(DB::raw('smn.*,sm.menu_name'))
                ->orderBy('smn.parent_navigation_id', 'asc')
                ->orderBy('smn.column_no', 'asc')
                ->orderBy('smn.sort_order', 'asc')
                // ->remember(30)
                ->get();

        $data = array();
        array_walk($res, function($v) use(&$data) {
            if ($v->type == 1 && $v->parent_navigation_id == 0) {
                $data[$v->navigation_id] = $v;
            }
        });
        array_walk($res, function($v) use(&$data) {
            if ($v->type == 2 && isset($data[$v->parent_navigation_id])) {

                if (isset($data[$v->parent_navigation_id]->group)) {
                    $data[$v->parent_navigation_id]->group[$v->navigation_id] = $v;
                } else {
                    $data[$v->parent_navigation_id]->group = [];
                    $data[$v->parent_navigation_id]->group[$v->navigation_id] = $v;
                }
            }
        });


        array_walk($res, function($v) use(&$data) {
            if ($v->type == 3 && isset($data[$v->parent_navigation_id]->group[$v->group_link])) {
                if (isset($data[$v->parent_navigation_id]->group[$v->group_link]->normal)) {
                    $data[$v->parent_navigation_id]->group[$v->group_link]->normal[] = $v;
                } else {
                    $data[$v->parent_navigation_id]->group[$v->group_link]->normal = [];
                    $data[$v->parent_navigation_id]->group[$v->group_link]->normal[] = $v;
                }
            } else {
                if ($v->type == 3 && isset($data[$v->group_link]) && !empty($data[$v->group_link])) {

                    if (isset($data[$v->group_link]->group)) {
                        $data[$v->group_link]->normal[$v->navigation_id] = $v;
                    } else {
                        // $data[$v->group_link]->normal=[];
                        $data[$v->group_link]->normal[$v->navigation_id] = $v;
                    }
                } else {
                    $data[$v->navigation_id] = $v;
                }
            }
        });

        return $data;
    }
}
