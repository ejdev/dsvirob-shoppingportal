<?php
namespace App\Models\Api;
use DB;
use Illuminate\Database\Eloquent\Model;
use Config;
use URL;
class APISupport extends Model
{
   public function faqs_list ($arr='')
    {
		$result = DB::table(config('tables.FAQS') . ' as f')
				->leftJoin(config('tables.FAQ_CATEGORIES') . ' as fc', 'fc.faq_category_id', '=', 'f.category_id');
				 if(isset($arr['slug']) && !empty($arr['slug']))
				{
					$result->where('fc.category','LIKE',$arr['slug']);
			    }
		        $result = $result->select(DB::Raw("f.id,fc.category as category_name,fc.faq_category_id,fc.slug as category_slug,fc.related_faq_category_id,f.title,f.description"))
				->where('f.is_deleted',Config::get('constants.OFF'))
				->orderby('f.created_on','asc')
				->get();
		if($result){
			return  $result;
		}
		else {
			return 0;
		}
	}
	
	public function faq_categories_list($arr = array())
	{
		$ids = '';
		$id = 0;
		$catres  = '';
		if(!empty($arr)){
			extract($arr);
			$qry = DB::table(config('tables.FAQ_CATEGORIES') . ' as fc')
				->select(DB::Raw("fc.category as category_name,fc.faq_category_id,fc.slug as category_slug"))
				->where('fc.is_deleted',Config::get('constants.OFF'));
			if(!empty($ids)){
				$qry->whereIn('fc.faq_category_id',explode(',',$ids));
				$catres = $qry->get();
			}
			else if(!empty($id)){
				$qry->where('fc.faq_category_id','=',$id);
				$catres = $qry->get();
			}			
			if(!empty($catres))	{
				 return $catres;
			}		
		}
		return NULL;
	}
	
	 public function getfeatures_faqs ()
    {
		$result = DB::table(config('tables.FAQS') . ' as f')
                 ->select(DB::Raw("f.id,f.title,f.description,f.is_feature"))
				->where('f.is_feature',config('tables.ON'))
				->where('f.is_deleted',config('tables.OFF'))
			    ->orderby('f.created_on','asc')
				->get();
		if($result){
			return  $result;
		}
		else {
			return NULL;
		}
	}
}
