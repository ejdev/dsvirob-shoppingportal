<?php
namespace App\Models\User;
use Session;
use App\Models\Sendsms;
use DB;
use Illuminate\Database\Eloquent\Model;
use Config;
use URL;
use Request;

class Cart extends Model
{
	public $sendsmsObj  = '';
	public function __construct ()
    {
		$this->sendsmsObj  = new Sendsms();
	}
	
	public function add_wish_list ($data = array())
    {
		extract($data);
        if (!empty($data))
        {
			$query = DB::table(Config::get('tables.TBL_USER_WISH_LIST').' as wl')                            
                 ->where('wl.product_id', $product_id)
                 ->where('wl.account_id', $account_id)
				 ->where('wl.is_deleted', 0)
				 ->count();
			if ($query > 0) {
				return array('status'=>'error', 'msg'=>'Already Added to Wishlist');
			} else {	 
				$data['created_on'] = date('Y-m-d H:i:s');			
				DB::table(Config::get('tables.TBL_USER_WISH_LIST'))->insertGetId($data);
				return array('status'=>'ok', 'msg'=>'Product Successfully Added to Wish List');				
			}
        }
        return false;
    }
	
	public function my_wish_list ($arr = array())
    {		
        extract($arr);
        $query = DB::table(Config::get('tables.TBL_USER_WISH_LIST').' as wl')           
                 //->select(DB::Raw("wl.product_id, wl.id")) 
                 ->where('wl.is_deleted', 0)
                 ->where('wl.account_id', $account_id);

		if (isset($count) && !empty($count))
		{
			return $query->count();
		}
		else
		{
			$rest['pid'] = $query->pluck('wl.product_id');
			$rest['wid'] = $query->pluck('wl.id');
			$rest['count'] = $query->count();
			return $rest;
		}				
    }
	
	public function remove_wish_list ($wdata = array())
    {
        $data = array();
        $data['is_deleted'] = Config::get('constants.ON');
        $res = DB::table(Config::get('tables.TBL_USER_WISH_LIST'))
                ->where('id', $wdata['id'])
                ->update($data);
        if ($res)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }
	
	public function get_billing_address ($arr = array())
    {  			
		extract($arr);
		return DB::table(Config::get('tables.TBL_PRODUCT_SHIPPING_DETAILS'))
				->where('account_id', $account_id)
				->orderBy('shipping_id', 'DESC')->first();
	}
	
	public function placeOrder ($arr = array())
    {         
		//return $arr;
		extract($arr);
        $status = false;
        $supplier_ords = $emaildata = array();			
		$shipping_charges = $tax = 0;
        foreach ($products as $product)
        {
			$shipping_charges += ($product['options']['shipping_info']['charge'] * $product['qty']);
			$tax += ($product['options']['tax_info']['tax_value'] * $product['qty']);
			
            $supplier_ords[$product['options']['supplier_id']]['products'][] = $product;
            $supplier_ords[$product['options']['supplier_id']]['qty'] = isset($supplier_ords[$product['options']['supplier_id']]['qty']) ? $supplier_ords[$product['options']['supplier_id']]['qty'] + $product['qty'] : $product['qty'];           		   		
        }	
				
		$payment_status_id = isset($payment_status_id) ? $payment_status_id : Config::get('constants.PAYMENT_STATUS.PENDING');
		$sub_order_code = implode(',', $ecom_sub_order_code);
		
		if (isset($bonus_wallet_amt)) {
			$is_bwa = 1;
			$bwa = $bonus_wallet_amt;
		} else {
			$is_bwa = 0;
			$bwa = '';
		}
		
		if ($payment_type_id == Config::get('constants.PAYMENT_TYPES.MYCASH_WALLET')) {
			$is_mycash = 1;
			$mycash = $mycash_amount;
		} else {
			$is_mycash = 0;
			$mycash = '';
		}
		
			DB::beginTransaction();
			if ($order_id = DB::table(Config::get('tables.TBL_PRODUCT_ORDER'))
							->insertGetId(array(
								'account_id'=>$account_id, 														        
								'ecom_invoice_id'=>$ecom_order_code,
								'qty'=>$item_qty,
								'currency_id'=>$currency_id,
								'sub_total'=>$amount,
								'shipping_charges'=>$shipping_charges,
								'net_pay'=>$amount + $shipping_charges,								
								'tax'=>$tax,								
								'is_bonus_wallet'=>$is_bwa,
								'bonus_amount'=>$bwa,
								'payment_type_id'=>$payment_type_id,
								'payment_status_id'=>$payment_status_id, 
								'created_on'=>date('Y-m-d H:i:s')))) 
			{		
				
				if (!empty($shipping))
				{
					$shipping['order_id'] = $order_id;
					$shipping['account_id'] = $account_id;
					$shipping['created_date'] = date('Y-m-d H:i:s');											
					if (DB::table(Config::get('tables.TBL_PRODUCT_SHIPPING_DETAILS'))->insertGetId($shipping))
					{				
						
						if (!empty($supplier_ords))
						{							
							foreach ($supplier_ords as $supplier_id => $supplier_ord)
							{								
								
								$shipping_charge = $taxx = 0;
								foreach ($supplier_ord['products'] as $so)
								{
									$taxx += $so['options']['tax_info']['tax_value'] * $so['qty'];
									$shipping_charge += $so['options']['shipping_info']['charge'] * $so['qty'];
								}								 
								if ($supplier_id != 0)
								{
									$suborder['order_id'] = $order_id;
								 	$suborder['supplier_id'] = $supplier_id;
									$suborder['account_id'] = $account_id;
									$suborder['currency_id'] = $currency_id;
								 	$suborder['qty'] = $supplier_ord['qty'];
									$suborder['sub_total'] = $amount;
									$suborder['tax'] = $taxx;
									$suborder['shipping_charge'] = $shipping_charge;
									$suborder['sub_order_code'] = $ecom_sub_order_code[$supplier_id];
									$suborder['net_pay'] = $amount + $shipping_charge;  
									$suborder['payment_type_id'] = $payment_type_id;  
									$suborder['created_on'] = date('Y-m-d H:i:s');									
									$sub_order_id = DB::table(Config::get('tables.TBL_PRODUCT_SUB_ORDER'))->insertGetId($suborder);									
									if ($sub_order_id)
									{
										foreach ($supplier_ord['products'] as $product)
										{
											$particulars['order_id'] = $order_id;		
									 		$particulars['sub_order_id'] = $sub_order_id;
											$particulars['supplier_product_id'] = $product['id'];											
											$particulars['qty'] = $product['qty'];
											$particulars['currency_id'] = $currency_id;
											$particulars['price'] = $product['price'];
											$particulars['sub_total'] = $product['subtotal'];	
											$particulars['margin_price_info'] = json_encode($product['options']['partner_info']);	
											$particulars['tax_info'] = json_encode($product['options']['tax_info']);	
											$particulars['shippment_info'] = json_encode($product['options']['shipping_info']);	
											$particulars['shipping_charge'] = $product['options']['shipping_info']['charge'] * $product['qty'];
											$particulars['tax'] = $product['options']['tax_info']['tax_value'] * $product['qty'];
											$particulars['net_pay'] = $product['subtotal'] + $particulars['shipping_charge'];
											$particulars['created_on'] = date('Y-m-d H:i:s');	
											$status = $order_item_id = DB::table(Config::get('tables.TBL_PRODUCT_ORDER_PARTICULARS'))
																		->insertGetId($particulars);
										}										
									} 							
								}
							}
						}
					}
				}
				
			    /* Bonus Wallet Transactions  */
				if (!empty($is_bwa)) {
					$transaction['account_id'] = $account_id;
					$transaction['account_type'] = Config::get('constants.CUSTOMER');
					$transaction['transaction_type'] = Config::get('constants.TRANSACTION_TYPE.DEBIT');
					$transaction['relation_id'] = $order_id;
					$transaction['statement_line'] = Config::get('constants.DEBIT_FROM_BONUS_WALLET');
					$transaction['amt'] = $bonus_wallet_amt;
					$transaction['paid_amt'] = $bonus_wallet_amt;
					$transaction['handle_amt'] = $bonus_wallet_amt;
					$transaction['payment_type_id'] = $payment_type_id;
					$transaction['currency_id'] = $currency_id;
					$transaction['wallet_id'] = Config::get('constants.BONUS_WALLET');
					$transaction['status'] = Config::get('constants.STATUS_CONFIRMED');
					$transaction['ip_address'] = Request::ip();
					$transaction['created_on'] = date('Y-m-d H:i:s');								
					$tid = DB::table(Config::get('tables.ACCOUNT_TRANSACTIONS'))->insertGetId($transaction);
					if ($tid) {
						$wal_bal = DB::table(Config::get('tables.TBL_WALLET_BALANCE').' as twb')  
							 ->select(DB::Raw("twb.current_balance, twb.tot_credit, twb.tot_debit, twb.user_balance_id")) 
							 ->where('twb.currency_id', $currency_id)
							 ->where('twb.wallet_id', Config::get('constants.BONUS_WALLET'))				 
							 ->where('twb.account_id', $account_id)
							 ->first(); 	
							 
						$current_balance = $wal_bal->current_balance - $bonus_wallet_amt;
						$tot_debit = $wal_bal->tot_debit + $bonus_wallet_amt;
						
						DB::table(Config::get('tables.TBL_WALLET_BALANCE'))
							->where('user_balance_id', $wal_bal->user_balance_id)
							->update(['current_balance' => $current_balance, 'tot_debit' => $tot_debit]);
					}
				}
				
				/* MyCash Wallet Transactions  */
				if (!empty($is_mycash)) {
					$transaction['account_id'] = $account_id;
					$transaction['account_type'] = Config::get('constants.CUSTOMER');
					$transaction['transaction_type'] = Config::get('constants.TRANSACTION_TYPE.DEBIT');
					$transaction['relation_id'] = $order_id;
					$transaction['statement_line'] =  Config::get('constants.DEBIT_FROM_MYCASH_WALLET');
					$transaction['amt'] = $mycash_amount;
					$transaction['paid_amt'] = $mycash_amount;
					$transaction['handle_amt'] = $mycash_amount;
					$transaction['payment_type_id'] = $payment_type_id;
					$transaction['currency_id'] = $currency_id;
					$transaction['wallet_id'] = Config::get('constants.MYCASH_WALLET');
					$transaction['status'] = Config::get('constants.STATUS_CONFIRMED');
					$transaction['ip_address'] = Request::ip();
					$transaction['created_on'] = date('Y-m-d H:i:s');								
					$tid = DB::table(Config::get('tables.ACCOUNT_TRANSACTIONS'))->insertGetId($transaction);
					if ($tid) {
						$wal_bal = DB::table(Config::get('tables.TBL_WALLET_BALANCE').' as twb')  
							 ->select(DB::Raw("twb.current_balance, twb.tot_credit, twb.tot_debit, twb.user_balance_id")) 
							 ->where('twb.currency_id', $currency_id)
							 ->where('twb.wallet_id', Config::get('constants.MYCASH_WALLET'))				 
							 ->where('twb.account_id', $account_id)
							 ->first(); 	
							 
						$current_balance = $wal_bal->current_balance - $mycash_amount;
						$tot_debit = $wal_bal->tot_debit + $mycash_amount;
						
						DB::table(Config::get('tables.TBL_WALLET_BALANCE'))
							->where('user_balance_id', $wal_bal->user_balance_id)
							->update(['current_balance' => $current_balance, 'tot_debit' => $tot_debit]);
					}
				}
			}			
				
			if ($status)
			{
				DB::commit();		
				    /* $mstatus = TWMailer::send(array(
                        'to'=>'jayaprakash.ejugiter@gmail.com',
                        'subject'=>'Order Confirmation - Your Order with '.'-'.' ['.$order_id.'] has been successfully placed!',
                        'view'=>'emails.product_purchase_to_member',
                        'data'=>$arr
					)); */
				return $order_id;
			}
			else
			{
				DB::rollback();
			}
			return false;		
    }
	
	public function updateOrdercode ($arr = array())
    {  
		extract($arr);
		return DB::table(Config::get('tables.TBL_PRODUCT_ORDER'))->where('order_code', $order_code)->update(['ecom_invoice_id'=>$ecom_order_code]);
	}
}
