<?php

namespace App\Models\User;

use Session;
use App\Models\Sendsms;
use DB;
use Illuminate\Database\Eloquent\Model;
use Config;
use URL;

class User extends Model {

    public $sendsmsObj = '';

    public function __construct() {
        $this->sendsmsObj = new Sendsms();
    }

    public function get_order_details($arr = array()) {
        $data['order'] = DB::table(Config::get('tables.TBL_PRODUCT_ORDER') . ' as tpo')
                ->select(DB::Raw('tpo.order_id, tpo.account_id, tpo.qty, tpo.net_pay, tpo.created_on, tpo.ecom_invoice_id'))
                ->Where('tpo.ecom_invoice_id', $arr['order_code'])
                ->first();
        $data['shipping'] = DB::table(Config::get('tables.TBL_PRODUCT_SHIPPING_DETAILS') . ' as tpsd')
                ->select(DB::Raw('tpsd.full_name, tpsd.address1, tpsd.address2, tpsd.city, tpsd.state_id, tpsd.country_id, tpsd.mobile_no, tpsd.email_id, tpsd.postal_code'))
                ->Where('tpsd.order_id', $data['order']->order_id)
                ->first();
        $data['sub_order'] = DB::table(Config::get('tables.TBL_PRODUCT_SUB_ORDER') . ' as tpso')
                ->select(DB::Raw('tpso.supplier_id, tpso.sub_order_code, tpso.currency_id, tpso.net_pay, tpso.sub_order_id'))
                ->Where('tpso.order_id', $data['order']->order_id)
                ->get();

        return $data;
    }

    public function check_email_phone_exists($arr = array()) {
        if (!empty($arr)) {
            $result = '';
            extract($arr);
            $result = DB::table(Config('tables.ACCOUNT_LOGIN_MST'))
                    ->where('account_type_id', Config::get('constants.ACCOUNT_TYPE_USER'));

            if (isset($email_id) && !empty($email_id)) {
                $result = $result->where('email', $email_id);
            }
            if (isset($mobile) && !empty($mobile)) {
                $result = $result->where('mobile', $mobile);
            }
            $result = $result->first();
            if ($result) {
                return $result;
            }
        }
        return false;
    }

    public function user_register($arr = array()) {

        $currentdate = date('Y-m-d H:i:s');
        if (isset($arr) && !empty($arr)) {
            $account_mst['created_on'] = $currentdate;
            $account_mst['firstname'] = $arr['full_name'];
            $account_id = DB::table(Config::get('tables.ACCOUNT_MST'))->insertGetId($account_mst);
            $login['account_id'] = $account_id;
            $login['account_type_id'] = Config::get('constants.ACCOUNT_TYPE_USER');
// $login['uname'] = $arr['username'];
            $login['email'] = $arr['email_id'];
            $login['mobile'] = $arr['mobile_no'];
            $login['pass_key'] = md5($arr['password']);
            $login['user_code'] = rand(1111, 9999) . time();
            $login['account_type_id'] = Config::get('constants.ACCOUNT_TYPE_USER');
            $account_id = DB::table(Config::get('tables.ACCOUNT_LOGIN_MST'))->insertGetId($login);
        }
        if ($account_id) {
            //used to identify verified mobile or email.
            $sdata['mobile_verified'] = !empty($arr['mobile_verified']) ? $arr['mobile_verified'] : '';
            $sdata['email_verified'] = !empty($arr['email_verified']) ? $arr['email_verified'] : '';
            $sdata['account_id'] = $account_id;
            $res = DB::table(Config::get('tables.USER_SETTINGS'))
                    ->insertGetId($sdata);

            $data['name'] = $arr['full_name'];
            $data['pwd'] = $arr['password'];
            $data['user_code'] = $login['user_code'];
            $email = $login['email'];
            if (!empty($arr['email_verified'])) {
                $code = [];
                $code = rand(1111, 9999) . time();
                $code = mb_substr($code, 0, 5);
                $result['email_mobile'] = $arr['email_id'];
                $result['code'] = $code;
                $data['act_link'] = URL::to("/user/register/activation/" . $code);
                $postdata['code'] = $code;
                $mstatus = 1;
                $op['msg'] = ' OTP has been Send to Your Email Id';
                $op['status'] = 'ok';
                Session::set('register_code', $result);

//                
//           
            }
            $email_data = array('email' => $email);
//            $mstatus = TWMailer::send(array(
//              'to' => $email_data['email'],
//              'subject' => 'Creating your ' . Config::get('constants.DOMAIN_NAME') . ' login',
//              'view' => 'emails.user.registration.create_user',
//              'data' => $data
//              )); 

            return $account_id;
        }
        return false;
    }

    public function ins_user_settings($arr = array()) {
        if (!empty($arr)) {

            $user_details = $this->check_email_phone_exists($arr);

            if ($user_details) {
                $wdata['email_verified'] = Config::get('constants.EMAIL_VERIFY');
                $wdata['account_id'] = $user_details->account_id;
            }

            $user_setting_id = DB::table(Config::get('tables.USER_SETTINGS'))
                    ->insertGetId($wdata);
            if ($user_setting_id) {
                return $user_setting_id;
            }
        }
    }

//    public function check_email_phone($arr = array()) {
//        $wdata = '';
//        if (!empty($arr)) {
//            if (strpos($arr['email_mobile'], '@')) {
//                if (filter_var($arr['email_mobile'], FILTER_VALIDATE_EMAIL) !== false) {
//                    $wdata['email'] = $arr['email_mobile'];
//                }
//            } else if (strlen($arr['email_mobile']) == 10) {
//                $wdata['mobile'] = $arr['email_mobile'];
//            } else {
//                $op['status'] = 'fail';
//                $op['msg'] = 'Please enter Valid Format';
//                return $op;
//            }
//
//            $result = '';
//            $result = DB::table(Config('tables.ACCOUNT_LOGIN_MST'));
//            if (isset($wdata['mobile']) && !empty($wdata['mobile'])) {
//                $result->where('mobile', $wdata['mobile']);
//            }
//            if (isset($wdata['email']) && !empty($wdata['email'])) {
//                $result->where('email', $wdata['email']);
//            }
//            $result = $result->first();
//
//            if ($result) {
//                $op['status'] = 'fail';
//                if (!empty($wdata['mobile'])) {
//                    $op['msg'] = 'Your Mobile no Already Exists';
//                } else {
//                    $op['msg'] = 'Your Email  Already Exists';
//                }
//                return $op;
//            } else if (empty($result) && !empty($wdata)) {
//
//                if (isset($wdata['email']) && !empty($wdata['email'])) {
//                    $op['status'] = "ok";
//                    $op['msg'] = "We have emailed  to your account.It should appear in your inbox within the next five minutes. If you still don’t see it, please check your spam folder before getting in touch!";
//                    $result = rand(1111, 9999) . time();
//                    $result = mb_substr($result, 0, 5);
//                    $arr['otp_code'] = $result;
//                    $mdata['otp_code'] = $arr['otp_code'];
//                    $op['code'] = $result;
//                    Session::set('user_postdata', $arr);
//                    $op['option'] = 'email';
//
//
//                    $email_data['email'] = $wdata['email'];
//                    $mdata['pagesettings'] = $arr['pagesettings'];
//                    $mstatus = TWMailer::send(array(
//                                'to' => $email_data['email'],
//                                'subject' => 'OTP',
//                                'view' => 'emails.accounts.verify_user',
//                                'data' => $mdata
//                    ));
//                    $op['status_val'] = Config('constants.ON');
//                    /* End */
//                } else if (isset($wdata['mobile']) && !empty($wdata['mobile'])) {
//                    /* Send SMS */
//                    $result = rand(1111, 9999) . time();
//                    $result = mb_substr($result, 0, 5);
//                    $arr['otp_code'] = $result;
//                    Session::set('mobile_code', $result);
//                    Session::set('user_postdata', $arr);
//                    $sdata = Session::get('user_postdata');
//
//                    $this->sendsmsObj->otp_send_sms(array(
//                        'otp_pwd' => $result,
//                        'mobile' => $wdata['mobile'],
//                        'sitename' => $arr['pagesettings']->site_name
//                    ));
//
//                    $op['status'] = 'ok';
//                    $op['msg'] = 'OTP has been send to your mobile';
//                    $op['option'] = 'mobile';
//                    $op['code'] = $result;
//                    $op['status_val'] = Config('constants.ON');
//                } else {
//                    $op['status'] = 'error';
//                    $op['msg'] = 'Registration FailedPlease Contact Our Support Team.!';
//                }
//                return $op;
//            }
//        } else {
//            $op['status'] = 'fail';
//            $op['msg'] = 'Something went Wrong';
//            return $op;
//        }
//    }

    public function ins_reset_log($arr = array()) {

        if (!empty($arr)) {
            $arr['reset_code_timeout'] = date('H:i:s', strtotime("+5 min"));



            return DB::table(Config::get('tables.ACCOUNT_RESETCODE_LOG'))
                            ->insertGetId($arr);
        }
    }

    public function check_user_otp($arr = array()) {
        $op['msg'] = "OTP Code not found";
        $op['status'] = 'fail';
        if (!empty($arr)) {
            $res = DB::table(Config::get('tables.ACCOUNT_RESETCODE_LOG'))
                    ->where('id', $arr['id'])
                    ->where('reset_code_timeout', '>=', date('H:i:s'))
                    ->first();

            if (!empty($res)) {
                if ($res->otp_code == $arr['otp']) {
                    $code = rand(1111, 9999) . time();
                    $update['reset_code'] = mb_substr($code, 0, 5) . $res->id . $res->account_id;
                    $res1 = DB::table(Config::get('tables.ACCOUNT_RESETCODE_LOG'))
                            ->where('id', $res->id)
                            ->update($update);
                    if ($res1) {
                        $op['reset_code'] = $update['reset_code'];
                    }
                    $op['msg'] = "OTP Code Verified Successfully";
                    $op['status'] = 'ok';
                }
            }
////             else {
//                    $op['msg'] = "OTP Code not found";
//                    $op['status'] = 'fail';
//                }else {
////                $op['msg'] = "OTP Time Expired";
////                $op['status'] = 'fail';
////            }
        }
        return $op;
    }

    public function reset_pwd($arr = array()) {
        $op['status'] = 'fail';
        $op['msg'] = 'Password Reset Failed';
        if (!empty($arr)) {
            $uptData = '';

            $res = DB::table(Config::get('tables.ACCOUNT_RESETCODE_LOG'))
                    ->where('reset_code', $arr['reset_code'])
                    ->first();

            if ($res) {
                if (isset($postdata['upd_reset_code']) && !empty($postdata['upd_reset_code'])) {
                    $update['reset_code'] =  $postdata['upd_reset_code'];
                    $res = DB::table(Config::get('tables.ACCOUNT_RESETCODE_LOG'))
                    ->where('id', $res->id)
                    ->update($update);
                    if($res)
                    {
                        return $res;
                    }
                }
                $wdata['account_id'] = $res->account_id;
                $upd['pass_key'] = md5($arr['forg_new_pass']);
                $uptData = DB::table(Config::get('tables.ACCOUNT_LOGIN_MST'))
                        ->where('account_id', $wdata['account_id'])
                        ->update($upd);
                if ($uptData) {
                    /* $mstatus = TWMailer::send(array(
                      'to'=>$userDetails->email,
                      'subject'=>'Password has been reseted successfully! Please check your e-mail',
                      'view'=>'emails.forgot_pwd',
                      'data'=>$postdata
                      )); */
                    $op['status'] = 'ok';
                    $op['msg'] = 'Your password has been reset successfully! Your new password has been sent to your email address.';
                }
            }
        }
        return $op;
    }

    public function validateUser($arr = array()) {
        extract($arr);
        $op['response']['status'] = 'error';
        $op['response']['msg'] = 'Parameter Missing';
        if (isset($username) && !empty($username) && isset($password) && !empty($password)) {
            $userData = DB::table(Config::get('tables.ACCOUNT_LOGIN_MST') . ' as lm')
                    ->select(DB::Raw('lm.user_id, lm.uname, lm.email, lm.mobile, lm.user_code, lm.pass_key, lm.login_block, lm.account_id'))
                    ->Where('lm.email', $username)
                    ->first();

            if (!empty($userData)) {

                if ($userData->login_block == Config::get('constants.OFF')) {

                    if ($userData->pass_key == md5($password)) {
                        if ($userData->login_block == Config::get('constants.UNBLOCKED')) {
						
							$login_id = DB::table(Config::get('tables.ACCOUNT_LOG'))->insertGetId(array('account_id' => $userData->account_id, 'user_login_ip' => \Request::ip()));
							
							$token = md5($login_id);											
							$op['msg'] = 'Success';
							$op['status_id'] = 1;						
								
                            unset($userData->is_deleted);
                            unset($userData->pass_key);
                            unset($userData->login_block);
							
							if (DB::table(Config::get('tables.ACCOUNT_LOGIN_MST'))->where('user_id', $userData->user_id)
												->update(array('last_active'=>date('Y-m-d H:i:s'), 'token'=>$token)))
								{
									$op['response']['status'] = 'ok';
									$op['response']['msg'] = 'Your are successfully logged in..';							
									$op['status_id'] = 1;
								}
								$op['token'] = $token;		
								$userData->token = $token;		
								$op['data'] = $userData;
							
							if (isset($arr['request']) && $arr['request'] == 'api')
							{								
								Session::put($op['token'], $userData);													
								Session::put('apiUserData', $userData);
							} else {
								Session::put('user_data', $userData);								
								$op['response']['url'] = URL::to('home');
								$op['response']['user_details'] = $userData;								
								$op['response']['sesson'] = Session::all();
							}									
                        } else {
							$op['status_id'] = 5;
                            $op['response']['msg'] = 'Your Account Has Been Blocked.';
                        }
                    } else {
						$op['status_id'] = 6;
                        $op['response']['msg'] = 'Incorrect Password';
                    }
                } else {
					$op['status_id'] = 2;
                    $op['response']['msg'] = 'Your Account Not Available or deleted';
                }
            } else {
				$op['status_id'] = 3;
                $op['response']['msg'] = 'Invalid Username';
            }
        }
        return (object) $op;
    }

    public function getIpCountry() {
        try {
            if ($_SERVER['REMOTE_ADDR'] == '::1') {
                $ipInfo = json_decode(file_get_contents('http://freegeoip.net/json/45.123.3.250'));
            } else {
//$ipInfo = json_decode(file_get_contents('http://ipinfo.io/'.$_SERVER['REMOTE_ADDR'].'/json'));
                $ipInfo = json_decode(file_get_contents('http://freegeoip.net/json/' . $_SERVER['REMOTE_ADDR']));
            }
//return $ipInfo->country;
            return $ipInfo;
///$counrtyCode = Ip2Country::get();
        } catch (Exception $e) {
            return NULL;
        }
    }

    public function get_phone_code_by_cname($country_name) {
        return DB::table(Config::get('tables.LOCATION_COUNTRY'))
                        ->where('name', $country_name)
                        ->pluck('phonecode');
    }

//
}
