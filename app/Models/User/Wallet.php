<?php
namespace App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Config;
use URL;
use Session;
use DB;
use Cart;

class Wallet extends Model
{
    public function getWallet ($arr = array())
    {			
        extract($arr);	   
        $wallets = DB::table(Config::get('tables.TBL_WALLET').' as tw')           
                 ->select(DB::Raw("tw.wallet_name, tw.wallet_id")) 
                 ->where('tw.status', 1)
				 ->get();			
		
		array_walk($wallets, function(&$wallet) use($account_id, $cart_subtotal, $shipping_charge)
            {				  
				$wallet->cart_subtotal = (float) str_replace(',', '', $cart_subtotal);
                $wallet->cart_total = (float) $shipping_charge + $wallet->cart_subtotal;                                  
                $wallet->shipping_charge = $shipping_charge;               
				$wallet->balance = $this->getWalletBalance($wallet, $account_id);  
            });		 
        return $wallets;			
    }
	
	public function getWalletBalance ($arr, $account_id)
    {	
			
			$bal = DB::table(Config::get('tables.TBL_WALLET_BALANCE').' as twb')           
					 //->select(DB::Raw("twb.current_balance")) 
					 ->where('twb.currency_id', 1)
					 ->where('twb.wallet_id', $arr->wallet_id)
					 ->where('twb.account_id', $account_id)
					 ->pluck('twb.current_balance'); 					 
			if (!empty($bal)) {
				return $bal[0];
			} else {
				return 0;
			}
		
    }
	
	
}
