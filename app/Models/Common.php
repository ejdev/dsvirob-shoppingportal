<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Config;
use URL;
use Session;

class Common extends Model
{

    public function get_site_settings ()
    {        
        return DB::table(Config::get('tables.SITE_SETTINGS'))->where('sid', 1)->first();
    }
	
	public function get_discount_percentage ()
    {   $cdate = date('Y-m-d');
        $disc =  DB::table(Config::get('tables.DISCOUNTS'))->where('start_date', '<=', $cdate)
				->where('end_date', '>=', $cdate)
				->pluck('discount_value');
		if (!empty($disc)) {		
			return $disc[0];
		} else {
			return 0;
		}
	}

    public function get_payment_settings ()
    {
        $payments = DB::table(Config::get('tables.PAYMENT_TYPES'))
                ->where('status', Config::get('constants.ACTIVE'))
                ->select('payment_type_id as id', 'payment_type as name', 'image_name as img')
                ->get();
        array_walk($payments, function(&$p)
        {
            //        $p->img = URL::asset($p->img);
        });
        return $payments;
    }

    public function getAvaliablePaymentModes ($arr = array())
    {
        extract($arr);
        $modes = DB::table(Config::get('tables.PAYMENT_MODE_LOOKUPS'))
                ->where('status', Config::get('constants.ACTIVE'))
                ->where('is_deleted', Config::get('constants.OFF'))
                ->select('paymode_id as id', 'mode_name as name')
                ->get();

        array_walk($modes, function(&$mode) use($account_id)
        {
            if (!empty($account_id))
            {
                if ($mode->id == 7)
                {
                    $mode->balance = $this->getMycashWalletBalance($account_id);
                }
            }
        });
        return $modes;
    }

    public function getMycashWalletBalance ($account_id)
    {
        $bal = DB::table(Config::get('tables.TBL_WALLET_BALANCE').' as twb')
                ->where('twb.currency_id', 1)
                ->where('twb.wallet_id', Config::get('constants.MYCASH_WALLET'))
                ->where('twb.account_id', $account_id)
                ->pluck('twb.current_balance');
        return $bal[0];
    }

    public function getBonusWalletBalance ($account_id)
    {
        $bal = DB::table(Config::get('tables.TBL_WALLET_BALANCE').' as twb')
                ->where('twb.currency_id', 1)
                ->where('twb.wallet_id', Config::get('constants.BONUS_WALLET'))
                ->where('twb.account_id', $account_id)
                ->pluck('twb.current_balance');
        return $bal[0];
    }

    public function getWalletName ($wallet_id)
    {
        return DB::table(Config::get('tables.TBL_WALLET'))
                        ->where('wallet_id', $wallet_id)
                        ->pluck('wallet_name');
    }

    public function get_currency ($currency_id)
    {
        return DB::table(Config::get('tables.CURRENCIES'))
                        ->where('currency_id', $currency_id)
                        ->select('currency', 'currency_symbol')
                        ->first();
    }

    public function getWalletInfo ()
    {
        return DB::table(Config::get('tables.TBL_WALLET').' as tw')
                        ->where('tw.wallet_id', Config::get('constants.BONUS_WALLET'))
                        ->select('tw.wallet_id', 'tw.wallet_name', 'tw.status', 'tw.prod_purchase_status', 'tw.prod_pruchase_perc')
                        ->first();
    }

    public static function generateBrowseURL ($arr = array(), $full = true)
    {
        extract($arr);
        $url_strings = [];
        $query_strings = [];
        if (isset($category->category_id) || isset($category_id))
        {
            if (isset($category->parent_url_str))
            {
                $url_strings[] = $category->parent_url_str;
            }
            if (isset($category->url_str))
            {
                $url_strings[] = $category->url_str;
                $query_strings[] = 'spath='.$category->category_code;
            }
        }
        if (isset($brand->brand_id) || isset($brand_id))
        {
            if (isset($brand->url_str))
            {
                $url_strings[] = $brand->url_str.'~brand';
            }
        }
        $url_strings[] = 'br';
        if (isset($filters))
        {
            foreach ($filters as $filter)
            {
                foreach ($filter as $option)
                {
                    $query_strings[] = 'f['.$filter.']['.$option.']='.$option;
                }
            }
        }
        $url = implode('/', $url_strings).'?'.implode('&', $query_strings);
        return ($full) ? URL::to($url) : $url;
    }

    public static function getLangContent ($files = [])
    {
        $lang = [];
        if (is_array($files))
        {
            foreach ($files as $file)
            {
                $lang = array_merge($lang, is_array(Lang::get($file)) ? Lang::get($file) : [Lang::get($file)]);
            }
        }
        else
        {
            $lang = is_array(Lang::get($files)) ? Lang::get($files) : [Lang::get($files)];
        }
        echo '<script>';
        echo 'var Lang =$.parseJSON(\''.addslashes(json_encode($lang)).'\');';
        echo 'Lang.get = function (key, values) {values = values || [];var temp=Lang[key];if(values!=[]){for (var i in values){temp = temp.replace(":" + i, values[i]);}}return temp;};';
        echo '</script>';
    }

}
