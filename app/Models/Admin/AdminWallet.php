<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Session;
use App\Models\Sendsms;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common;
use Config;
use URL;

class AdminWallet extends Model
{

    public function getTransactionLog ($arr = array(), $count = false)
    {
        extract($arr);
        $transaction_log = DB::table(Config::get('tables.ACCOUNT_TRANSACTIONS').' as a')
                ->where('a.is_deleted', 0)
                ->where('a.status', '>', 0);
        if ((isset($term) && !empty($term)) || !$count)
        {
            $transaction_log->leftJoin(Config::get('tables.TBL_WALLET').' as b', 'b.wallet_id', '=', 'a.wallet_id')
                    ->leftJoin(Config::get('tables.STATEMENT_LINE').' as e', 'e.statementline_id', '=', 'a.statementline')
                    ->leftjoin(Config::get('tables.CURRENCIES').' as cur', 'cur.currency_id', '=', 'a.currency_id')
                    ->leftjoin(Config::get('tables.PAYMENT_TYPES').' as p', 'p.payment_type_id', '=', 'a.payment_type_id')
                    ->leftJoin(Config::get('tables.ACCOUNT_LOGIN_MST').' as d', 'd.account_id', '=', 'a.account_id')
                    ->leftjoin(Config::get('tables.ACCOUNT_MST').' as ud', 'ud.account_id', '=', 'a.account_id')
                    ->leftJoin(Config::get('tables.ACCOUNT_LOGIN_MST').' as fd', 'fd.account_id', '=', 'a.from_or_to_account_id')
                    ->leftjoin(Config::get('tables.ACCOUNT_MST').' as fud', 'fud.account_id', '=', 'a.from_or_to_account_id');
        }
        if (isset($from) && !empty($from))
        {
            $transaction_log->whereRaw('DATE(a.updated_on) >="'.date('Y-m-d', strtotime($from)).'"');
        }
        if (isset($to) && !empty($to))
        {
            $transaction_log->whereRaw('DATE(a.updated_on) <="'.date('Y-m-d', strtotime($to)).'"');
        }
        if (isset($term) && !empty($term))
        {
            $transaction_log->where(function($subquery) use($term)
            {
                $res->whereRaw('(mst.email like \'%'.$arr['search_term'].'%\'  OR  concat(a.firstname,a.lastname) like \'%'.$arr['search_term'].'%\' OR mst.code like  \'%'.$arr['search_term'].'%\')');
                $subquery->orWhere('concat(ud.firstname,ud.lastname)', 'like', '%'.$term.'%')
                        ->orWhere('a.remark', 'like', '%'.$term.'%')
                        ->orWhere('d.uname', 'like', '%'.$term.'%');
            });
        }
        if (isset($ewallet_id) && !empty($ewallet_id))
        {
            $transaction_log->where('a.wallet_id', '=', $ewallet_id);
        }
        if (isset($start) && isset($length))
        {
            $transaction_log->skip($start)->take($length);
        }
        if (isset($orderby) && isset($order))
        {
            $transaction_log->orderBy($orderby, $order);
        }
        else
        {
            $transaction_log->orderBy('a.id', 'desc');
        }
        if ($count)
        {
            return $transaction_log->count();
        }
        else
        {
            $transaction_log->select(DB::raw('a.updated_on,concat(ud.firstname," ",ud.lastname,"<br/>(",d.uname,")") as full_name,concat(fud.firstname," ",fud.lastname,"<br/>(",fd.uname,")") as ftfull_name,concat(e.statementline," ",a.remark) as description,a.transaction_type,b.wallet_name,cur.currency as currency,cur.currency_symbol,a.transaction_type,a.amt,a.paid_amt,a.handle_amt,a.current_balance,if(a.statementline=2,p.payment_type,"") as payment_type'));
            $logs = $transaction_log->get();
            array_walk($logs, function(&$log)
            {
                $log->Fupdated_on = date('d-M-Y H:i:s', strtotime($log->updated_on));
                $log->Famt = $log->currency.' '.number_format((($log->transaction_type == Config::get('constants.TRANSACTION_TYPE.DEBIT')) ? (-1 * $log->amt) : $log->amt), 2, '.', ',').' '.$log->currency_symbol;
                $log->Fpaid_amt = $log->currency.' '.number_format($log->paid_amt, 2, '.', ',').' '.$log->currency_symbol;
                $log->Fhandle_amt = $log->currency.' '.number_format($log->handle_amt, 2, '.', ',').' '.$log->currency_symbol;
                $log->Fcurrent_balance = $log->currency.' '.number_format($log->current_balance, 2, '.', ',').' '.$log->currency_symbol;
                $log->transaction_type = (($log->transaction_type == Config::get('constants.TRANSACTION_TYPE.DEBIT'))) ? 'text-danger' : 'text-success';
            });
            return $logs;
        }
    }

}
