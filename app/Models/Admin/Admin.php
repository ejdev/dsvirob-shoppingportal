<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Session;
use App\Models\Sendsms;
use DB;
use Illuminate\Database\Eloquent\Model;
use Config;
use URL;

class Admin extends Model
{

    public $sendsmsObj = '';

    public function __construct ()
    {
        $this->sendsmsObj = new Sendsms();
    }
	
	public function get_user_code ($userid)
    {		
		return DB::table(Config::get('tables.ACCOUNT_LOGIN_MST'))
                    ->where('account_id', $userid)
                    ->pluck('user_code');
	}

    public function reset_pwd ($arr = array())
    {
        $op['status'] = 'fail';
        $op['msg'] = 'Password Reset Failed';
        if (!empty($arr))
        {
            $uptData = '';

            $res = DB::table(Config::get('tables.ACCOUNT_RESETCODE_LOG'))
                    ->where('reset_code', $arr['reset_code'])
                    ->first();

            if ($res)
            {
                if (isset($postdata['upd_reset_code']) && !empty($postdata['upd_reset_code']))
                {
                    $update['reset_code'] = $postdata['upd_reset_code'];
                    $res = DB::table(Config::get('tables.ACCOUNT_RESETCODE_LOG'))
                            ->where('id', $res->id)
                            ->update($update);
                    if ($res)
                    {
                        return $res;
                    }
                }
                $wdata['account_id'] = $res->account_id;
                $upd['pass_key'] = md5($arr['forg_new_pass']);
                $uptData = DB::table(Config::get('tables.ACCOUNT_LOGIN_MST'))
                        ->where('account_id', $wdata['account_id'])
                        ->update($upd);
                if ($uptData)
                {
                    /* $mstatus = TWMailer::send(array(
                      'to'=>$userDetails->email,
                      'subject'=>'Password has been reseted successfully! Please check your e-mail',
                      'view'=>'emails.forgot_pwd',
                      'data'=>$postdata
                      )); */
                    $op['status'] = 'ok';
                    $op['msg'] = 'Your password has been reset successfully! Your new password has been sent to your email address.';
                }
            }
        }
        return $op;
    }

    public function validateAdmin ($arr = array())
    {
        extract($arr);
        $op = [
            'response'=>[
                'msg'=>'Invalid Username and Password'
            ],
            'statusCode'=>422];
        if (isset($uname) && !empty($uname) && isset($password) && !empty($password))
        {
            $userData = DB::table(Config::get('tables.ACCOUNT_LOGIN_MST').' as lm')
                    ->join(Config::get('tables.ACCOUNT_MST').' as am', 'am.account_id', '=', 'lm.account_id')
                    ->select(DB::Raw('am.account_id,lm.account_type_id, concat(am.firstname," ",am.lastname) as full_name,lm.email,lm.mobile,lm.uname,am.is_deleted,lm.pass_key,lm.login_block'))
                    ->where('lm.account_type_id', Config::get('constants.ACCOUNT_TYPE.ADMIN'))
                    ->where(function($subquery) use($uname)
                    {
                        $subquery->where('lm.uname', $uname)
                        ->orWhere('lm.email', $uname)
                        ->orWhere('lm.mobile', $uname);
                    })
                    ->first();
            if (!empty($userData))
            {
                if ($userData->is_deleted == Config::get('constants.OFF'))
                {
                    if ($userData->pass_key == md5($password))
                    {
                        if ($userData->login_block == Config::get('constants.UNBLOCKED'))
                        {

                            unset($userData->is_deleted);
                            unset($userData->pass_key);
                            unset($userData->login_block);
                            Session::put('admin_data', $userData);

                            $login_log_id = DB::table(Config::get('tables.ACCOUNT_LOG'))
                                    ->insertGetId(array(
                                'account_id'=>$userData->account_id,
                                'user_login_ip'=>$ip,
                                'user_log_time'=>date('Y-m-d H:i:s')));
                            $userData->token = md5($login_log_id);
                            if (DB::table(Config::get('tables.ACCOUNT_LOGIN_MST'))
                                            ->where('account_id', $userData->account_id)
                                            ->update(array(
                                                'last_active'=>date('Y-m-d H:i:s'),
                                                'token'=>$userData->token)))
                            {
                                $op['response']['msg'] = 'Your are successfully logged in., Please Wait...';
                                $op['response']['url'] = URL::route('admin.dashboard');
                                $op['response']['user_details'] = $userData;
                                $op['statusCode'] = 200;
                            }
                        }
                        else
                        {
                            $op['response']['msg'] = 'Your Account Has Been Blocked.';
                            $op['statusCode'] = 403;
                        }
                    }
                    else
                    {
                        $op['response']['msg'] = 'Incorrect Password';
                        $op['statusCode'] = 406;
                    }
                }
                else
                {
                    $op['response']['msg'] = 'Your Account Not Available or deleted';
                    $op['statusCode'] = 403;
                }
            }
            else
            {
                $op['response']['msg'] = 'Invalid Username';
                $op['statusCode'] = 406;
            }
        }
        return (object) $op;
    }

    public function getIpCountry ()
    {
        try
        {
            if ($_SERVER['REMOTE_ADDR'] == '::1')
            {
                $ipInfo = json_decode(file_get_contents('http://freegeoip.net/json/45.123.3.250'));
            }
            else
            {
//$ipInfo = json_decode(file_get_contents('http://ipinfo.io/'.$_SERVER['REMOTE_ADDR'].'/json'));
                $ipInfo = json_decode(file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']));
            }
//return $ipInfo->country;
            return $ipInfo;
///$counrtyCode = Ip2Country::get();
        }
        catch (Exception $e)
        {
            return NULL;
        }
    }

    public function get_phone_code_by_cname ($country_name)
    {
        return DB::table(Config::get('tables.LOCATION_COUNTRY'))
                        ->where('name', $country_name)
                        ->pluck('phonecode');
    }

//
}
