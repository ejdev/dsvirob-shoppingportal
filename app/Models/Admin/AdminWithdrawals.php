<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Session;
use App\Models\Sendsms;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common;
use Config;
use URL;

class AdminWithdrawals extends Model
{

    public function __construct ()
    {
        parent::__construct();
        $this->commonObj = new Common;
    }

    public function withdrawals_list ($arr = array(), $count = false)
    {
        extract($arr);
        $query = DB::table(Config::get('tables.WITHDRAWAL').' as wid')
                ->join(Config::get('tables.ACCOUNT_LOGIN_MST').' as um', 'um.account_id', '=', 'wid.account_id')
                ->join(Config::get('tables.ACCOUNT_MST').' as ud', 'ud.account_id', '=', 'wid.account_id')
                ->join(Config::get('tables.PAYMENT_TYPES').' as pay_t', 'pay_t.payment_type_id', '=', 'wid.payment_type_id')
                ->join(Config::get('tables.CURRENCIES').' as c', 'wid.currency_id', '=', 'c.currency_id')
                ->join(Config::get('tables.WITHDRAWAL_STATUS').' as st', 'st.status_id', '=', 'wid.status_id');
        if (isset($from) && !empty($from))
        {
            $query->whereRaw("DATE(wid.created_on) >='".date('Y-m-d', strtotime($from))."'");
        }
        elseif (isset($to) && !empty($to))
        {
            $query->whereRaw("DATE(wid.created_on) <='".date('Y-m-d', strtotime($to))."'");
        }
        if (isset($w_id) && $w_id != '')
        {
            $query->where('wid.withdrawal_id', $w_id);
        }
        if (isset($currency) && !empty($currency))
        {
            $query->where('wid.currency_id', $currency);
        }
        if (isset($paytype) && !empty($paytype))
        {
            $query->where('wid.payment_type_id', $paytype);
        }
        if (isset($search_term) && !empty($search_term))
        {
            $query->where('um.uname', '=', $search_term);
        }
        if (isset($status_id) && !is_null($status_id))
        {
            $query->where('wid.status_id', $status_id);
        }
        if (isset($start) && isset($length))
        {
            $query->skip($start)->take($length);
        }
        if (isset($orderby))
        {
            $query->orderby($orderby, $order);
        }
        else
        {
            $query->orderBy('wid.created_on', 'DESC');
        }
        if ($count)
        {
            return $query->count();
        }
        else
        {
            $withdrawals = $query->select(DB::raw('wid.transaction_id,wid.status_id,wid.amount,wid.paidamt,wid.handleamt,wid.expected_on,wid.created_on,wid.cancelled_on,wid.confirmed_on,wid.updated_on,st.status,um.uname,c.currency,c.currency_symbol,concat(ud.firstname,"",ud.lastname) as full_name,um.mobile,pay_t.payment_type'))
                    ->get();
            array_walk($withdrawals, function(&$withdrawal)
            {
                $withdrawal->Famount = $withdrawal->currency_symbol.' '.number_format($withdrawal->amount, 2, '.', ',').' '.$withdrawal->currency;
                $withdrawal->Fpaidamt = $withdrawal->currency_symbol.' '.number_format($withdrawal->paidamt, 2, '.', ',').' '.$withdrawal->currency;
                $withdrawal->Fhandleamt = $withdrawal->currency_symbol.' '.number_format($withdrawal->handleamt, 2, '.', ',').' '.$withdrawal->currency;
                $withdrawal->Fexpected_on = date('d-M-Y', strtotime($withdrawal->expected_on));
                $withdrawal->Fcreated_on = date('d-M-Y H:i:s', strtotime($withdrawal->created_on));
                $withdrawal->Fupdated_on = date('d-M-Y H:i:s', strtotime($withdrawal->updated_on));
                $withdrawal->Fcancelled_on = !empty($withdrawal->cancelled_on) ? date('d-M-Y H:i:s', strtotime($withdrawal->cancelled_on)) : null;
                $withdrawal->Fconfirmed_on = !empty($withdrawal->confirmed_on) ? date('d-M-Y H:i:s', strtotime($withdrawal->confirmed_on)) : null;
                $withdrawal->actions = [];
                $withdrawal->actions['DETAILS'] = [
                    'title'=>'Details',
                    'class'=>'withdraw-details',
                    'data'=>[
                        'transaction_id'=>$withdrawal->transaction_id
                    ],
                    'url'=>URL::route('admin.withdraw.details')
                ];
                if (in_array($withdrawal->status_id, [ Config::get('constants.WITHDRAWAL_STATUS.PENDING')]))
                {
                    $withdrawal->actions['PROCESSED'] = [
                        'title'=>'Processing',
                        'data'=>[
                            'transaction_id'=>$withdrawal->transaction_id,
                            'status_id'=>Config::get('constants.WITHDRAWAL_STATUS.PROCESSED')
                        ],
                        'url'=>URL::route('admin.withdraw.update-status')
                    ];
                }
                if (in_array($withdrawal->status_id, [ Config::get('constants.WITHDRAWAL_STATUS.PENDING'), Config::get('constants.WITHDRAWAL_STATUS.PROCESSED')]))
                {
                    $withdrawal->actions['CONFIRMED'] = [
                        'title'=>'Confirm',
                        'data'=>[
                            'status'=>'confirmed',
                            'transaction_id'=>$withdrawal->transaction_id,
                            'status_id'=>Config::get('constants.WITHDRAWAL_STATUS.CONFIRMED')
                        ],
                        'class'=>'update-status',
                        'url'=>URL::route('admin.withdraw.update-status')
                    ];
                    $withdrawal->actions['CANCELLED'] = [
                        'title'=>'Cancel',
                        'data'=>[
                            'status'=>'cancelled',
                            'transaction_id'=>$withdrawal->transaction_id,
                            'status_id'=>Config::get('constants.WITHDRAWAL_STATUS.CANCELLED')
                        ],
                        'class'=>'update-status',
                        'url'=>URL::route('admin.withdraw.update-status')
                    ];
                }
            });
            return $withdrawals;
        }
    }

    public function updateWithdrawalStatus ($arr = array())
    {
        extract($arr);
        $udata = [];
        $udata['status_id'] = $status_id;
        DB::beginTransaction();
        $updateQuery = DB::table(Config::get('tables.WITHDRAWAL'))
                ->where('transaction_id', $transaction_id);
        switch ($status_id)
        {
            case Config::get('constants.WITHDRAWAL_STATUS.PROCESSED'):
                $updateQuery->where('status_id', Config::get('constants.WITHDRAWAL_STATUS.PENDING'));
                if ($updateQuery->update($udata))
                {
                    DB::commit();
                    return true;
                }
                break;
            case Config::get('constants.WITHDRAWAL_STATUS.CONFIRMED'):
                $updateQuery->whereIn('status_id', [Config::get('constants.WITHDRAWAL_STATUS.PENDING'), Config::get('constants.WITHDRAWAL_STATUS.PROCESSED')]);
                $withdrawal = DB::table(Config::get('tables.WITHDRAWAL'))
                        ->where('transaction_id', $transaction_id)
                        ->where('is_deleted', Config::get('constants.OFF'))
                        ->selectRaw('withdrawal_id,wallet_id,currency_id,paidamt')
                        ->first();
                if (!empty($withdrawal))
                {
                    if ($this->commonObj->update_account_transaction([
                                'from_wallet_id'=>$withdrawal->wallet_id,
                                'currency_id'=>$withdrawal->currency_id,
                                'amt'=>$withdrawal->paidamt,
                                'relation_id'=>$withdrawal->withdrawal_id,
                                'transaction_for'=>'WITHDRAWAL_PAYMENT'], true))
                    {
                        $udata['confirmed_on'] = date('Y-m-d H:i:s');
                        $udata['paid_proof_info'] = isset($paid_proof_info) && !empty($paid_proof_info) ? $paid_proof_info : null;
                        $udata['proof_file'] = isset($proof_file) && !empty($proof_file) ? $proof_file : null;
                        if ($updateQuery->update($udata))
                        {
                            DB::commit();
                            return true;
                        }
                    }
                }
                break;
            case Config::get('constants.WITHDRAWAL_STATUS.CANCELLED'):
                $withdrawal = DB::table(Config::get('tables.WITHDRAWAL'))
                        ->where('transaction_id', $transaction_id)
                        ->where('is_deleted', Config::get('constants.OFF'))
                        ->selectRaw('withdrawal_id,transaction_id,account_id,wallet_id,currency_id,amount,conversion_details')
                        ->first();
                if (!empty($withdrawal))
                {
                    $withdrawal->conversion_details = json_decode($withdrawal->conversion_details);
                    $this->commonObj->update_account_transaction([
                        'to_account_id'=>$withdrawal->account_id,
                        'from_wallet_id'=>$withdrawal->wallet_id,
                        'currency_id'=>$withdrawal->currency_id,
                        'amt'=>$withdrawal->amount,
                        'relation_id'=>$withdrawal->withdrawal_id,
                        'transaction_for'=>'WITHDRAW_CANCEL'
                    ]);
                    if (!empty($withdrawal->conversion_details))
                    {
                        array_walk($withdrawal->conversion_details, function($convert) use($withdrawal)
                        {
                            $this->commonObj->update_account_transaction([
                                'from_account_id'=>$withdrawal->account_id,
                                'from_wallet_id'=>$withdrawal->wallet_id,
                                'currency_id'=>$withdrawal->currency_id,
                                'amt'=>$convert->to_amount,
                                'to_account_id'=>$withdrawal->account_id,
                                'to_wallet_id'=>$convert->wallet_id,
                                'to_currency_id'=>$convert->currency_id,
                                'to_amt'=>$convert->from_amount,
                                'relation_id'=>$withdrawal->withdrawal_id,
                                'transaction_for'=>'CURRENCY_CONVERSION'
                            ]);
                        });
                    }
                }
                $updateQuery->whereIn('status_id', [Config::get('constants.WITHDRAWAL_STATUS.PENDING'), Config::get('constants.WITHDRAWAL_STATUS.PROCESSED')]);
                $udata['cancelled_on'] = date('Y-m-d H:i:s');
                $udata['reason'] = isset($reason) && !empty($reason) ? $reason : null;
                if ($updateQuery->update($udata))
                {
                    DB::commit();
                    return true;
                }
                break;
        }
        //DB::rollback();
        return false;
    }

    public function getWithdrawalDetails ($arr = array())
    {
        extract($arr);
        $query = DB::table(Config::get('tables.WITHDRAWAL').' as wid')
                ->join(Config::get('tables.PAYMENT_TYPES').' as pt', 'pt.payment_type_id', '=', 'wid.payment_type_id')
                ->join(Config::get('tables.TBL_WALLET').' as wt', 'wt.wallet_id', '=', 'wid.wallet_id')
                ->join(Config::get('tables.WITHDRAWAL_STATUS').' as st', 'st.status_id', '=', 'wid.status_id')
                ->join(Config::get('tables.CURRENCIES').' as ci', 'wid.currency_id', '=', 'ci.currency_id')
                ->where('wid.is_deleted', Config::get('constants.OFF'));
        if (isset($transaction_id))
        {
            $query->where('wid.transaction_id', $transaction_id);
        }
        $withdrawal = $query->select(DB::Raw('pt.payment_type,st.status,wid.transaction_id,wid.status_id,wid.amount,wid.paidamt,wid.handleamt,wid.created_on,wid.expected_on,wid.cancelled_on,wid.confirmed_on,ci.currency,ci.currency_symbol,wid.account_info,wid.conversion_details,wid.reason,wid.paid_proof_info'))
                ->first();
        if (!empty($withdrawal))
        {
            $withdrawal->amount = $withdrawal->currency_symbol.' '.number_format($withdrawal->amount, 2, '.', ',').' '.$withdrawal->currency;
            $withdrawal->paidamt = $withdrawal->currency_symbol.' '.number_format($withdrawal->paidamt, 2, '.', ',').' '.$withdrawal->currency;
            $withdrawal->handleamt = $withdrawal->currency_symbol.' '.number_format($withdrawal->handleamt, 2, '.', ',').' '.$withdrawal->currency;
            $withdrawal->expected_on = date('d-M-Y H:i:s', strtotime($withdrawal->expected_on));
            $withdrawal->created_on = date('d-M-Y H:i:s', strtotime($withdrawal->created_on));
            $withdrawal->confirmed_on = !empty($withdrawal->confirmed_on) ? date('d-M-Y H:i:s', strtotime($withdrawal->confirmed_on)) : null;
            $withdrawal->conversion_details = json_decode($withdrawal->conversion_details);
            $withdrawal->account_info = json_decode($withdrawal->account_info);
            if (!empty($withdrawal->conversion_details))
            {
                array_walk($withdrawal->conversion_details, function(&$convert) use($withdrawal)
                {
                    $convert->wallet = $this->commonObj->getWalletName($convert->wallet_id);
                    $currency = $this->commonObj->get_currency($convert->currency_id);
                    $convert->from_amount = $currency->currency.' '.number_format($convert->from_amount, 2, '.', ',').' '.$currency->currency_symbol;
                    $convert->to_amount = $withdrawal->currency.' '.number_format($convert->to_amount, 2, '.', ',').' '.$withdrawal->currency_symbol;
                    unset($convert->wallet_id);
                    unset($convert->currency_id);
                    unset($convert->rate);
                });
            }
            if ($withdrawal->status_id == Config::get('constants.WITHDRAWAL_STATUS.CONFIRMED'))
            {
                unset($withdrawal->reason);
            }
            else if ($withdrawal->status_id == Config::get('constants.WITHDRAWAL_STATUS.CANCELLED'))
            {
                unset($withdrawal->paid_proof_info);
            }
            $withdrawal->actions = [];
            if (in_array($withdrawal->status_id, [ Config::get('constants.WITHDRAWAL_STATUS.PENDING')]))
            {
                $withdrawal->actions['PROCESSED'] = [
                    'title'=>'Processing',
                    'data'=>[
                        'transaction_id'=>$withdrawal->transaction_id,
                        'status_id'=>Config::get('constants.WITHDRAWAL_STATUS.PROCESSED')
                    ],
                    'url'=>URL::to('admin/withdrawal/update-status')
                ];
            }
            if (in_array($withdrawal->status_id, [ Config::get('constants.WITHDRAWAL_STATUS.PENDING'), Config::get('constants.WITHDRAWAL_STATUS.PROCESSED')]))
            {
                $withdrawal->actions['CONFIRMED'] = [
                    'title'=>'Confirm',
                    'data'=>[
                        'transaction_id'=>$withdrawal->transaction_id,
                        'status_id'=>Config::get('constants.WITHDRAWAL_STATUS.CONFIRMED')
                    ],
                    'url'=>URL::to('admin/withdrawal/update-status')
                ];
                $withdrawal->actions['CANCELLED'] = [
                    'title'=>'Cancel',
                    'data'=>[
                        'transaction_id'=>$withdrawal->transaction_id,
                        'status_id'=>Config::get('constants.WITHDRAWAL_STATUS.CANCELLED')
                    ],
                    'url'=>URL::to('admin/withdrawal/update-status')
                ];
            }
            unset($withdrawal->status_id);
        }
        return $withdrawal;
    }

    public function withdrawal_status ()
    {
        return DB::table(Config::get('tables.WITHDRAWAL_STATUS'))
                        ->select('status_id', 'status')
                        ->get();
    }

    public function get_payout_types ()
    {
        $res = DB::table(Config::get('tables.PAYMENT_TYPES'))
                ->where('status', Config::get('constants.ON'))
                ->select('payment_type', 'payment_type_id')
                ->get();
        return (!empty($res) && count($res) > 0) ? $res : null;
    }

    public function get_withdraw_currency ()
    {
        $res = DB::table(Config::get('tables.WITHDRAWAL').' as bw')
                        ->join(Config::get('tables.CURRENCIES').' as cr', 'cr.currency_id', '=', 'bw.currency_id')
                        ->where('bw.is_deleted', 0)
                        ->select('bw.currency_id', 'cr.currency')
                        ->distinct('bw.currency_id')->get();
        return (!empty($res) && count($res) > 0) ? $res : null;
    }

}
