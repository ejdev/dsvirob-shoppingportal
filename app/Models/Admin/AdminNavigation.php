<?php

namespace App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use DB;
use Config;

class AdminNavigation extends Model
{
    public function get_menu ()
    {
        return DB::table(Config::get('tables.TBL_SITE_MENU'))->get();
    }
	
	public function menu_position_list ()
    {
        return DB::table(Config::get('tables.TBL_SITE_MENU_POSITION_LOOKUP'))
                        ->where('is_deleted', Config::get('constants.OFF'))
                        ->select('position_name', 'menu_postion_id')
                        ->get();
    }
	
	public function group_list ($data)
    {
        $res = DB::table(Config::get('tables.TBL_SITE_MENU_NAVIGATION').' as smn')
                ->leftJoin(Config::get('tables.TBL_SITE_MENU').' as sm', 'smn.menu_id', '=', 'sm.menu_id')
                ->where('sm.is_deleted', Config::get('constants.OFF'))
                ->where('sm.menu_id', $data['menu_id'])
                ->where('smn.type', 2)
                ->select('smn.navigation_name', 'smn.navigation_id')
                ->get();
        return $res;
    }
	
	public function get_link_details ($postdata)
    {
        return DB::table(Config::get('tables.TBL_SITE_MENU_NAVIGATION').' as smn')
                        ->leftJoin(Config::get('tables.TBL_SITE_MENU').' as sm', 'smn.menu_id', '=', 'sm.menu_id')
                        ->where('smn.is_deleted', Config::get('constants.OFF'))
                        ->where('smn.type', $postdata['type'])
                        ->where('smn.navigation_id', $postdata['navigation_id'])
                        ->select('smn.*')
                        ->first();
    }
	
	public function delete_nav ($postdata)
    {

        return DB::table(Config::get('tables.TBL_SITE_MENU_NAVIGATION'))
                        ->where('navigation_id', $postdata['navigation_id'])
                        ->update(array('is_deleted'=>1));
    }
	
	public function save_navigation ($sdata)
    {
        extract($sdata);
        $nav['created_on'] = date(Config::get('constants.DB_DATE_TIME_FORMAT'));
        $nav['menu_id'] = $sdata['menu_id'];
        $res = DB::table(Config::get('tables.TBL_SITE_MENU_NAVIGATION'))->insert($nav);
        if (!empty($res))
        {
            return $res;
        }
        return false;
    }
	
	public function check_menu ($menu_name)
    {
        $res = DB::table(Config::get('tables.TBL_SITE_MENU'))
                ->where('menu_name', $menu_name)
                ->where('is_deleted', Config::get('constants.OFF'))
                ->select('menu_id')
                ->first();
        if ($res)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }
	
	public function save_menu ($data)
    {
        $data['created_on'] = date(Config::get('constants.DB_DATE_TIME_FORMAT'));
        $res = DB::table(Config::get('tables.TBL_SITE_MENU'))
                ->insertGetId($data);
        if (!empty($res))
        {
            return $res;
        }
        return false;
    }
	
	public function top_menu_list ($data)
    {
        $res = DB::table(Config::get('tables.TBL_SITE_MENU_NAVIGATION').' as smn')
                ->leftJoin(Config::get('tables.TBL_SITE_MENU').' as sm', 'smn.menu_id', '=', 'sm.menu_id')
                ->where('sm.is_deleted', Config::get('constants.OFF'))
                ->where('sm.menu_id', $data['menu_id'])
                ->where('smn.type', 1)
                ->select('smn.navigation_name', 'smn.navigation_id', 'smn.no_of_columns')
                ->get();
        return $res;
    }	
	
	public function get_menus ()
    {
        return DB::table(Config::get('tables.TBL_SITE_MENU'))->get();
    }
	
	public function save_menu_settings ($data)
    {
        DB::table(Config::get('tables.TBL_SITE_MENU'))
                ->update(array('selected'=>Config::get('constants.OFF')));
        foreach ($data['nav'] as $key=> $val)
        {
            DB::table(Config::get('tables.TBL_SITE_MENU'))
                    ->where('menu_id', $val)
                    ->update(array('selected'=>Config::get('constants.ON')));
        }
        return true;
    }
	
	public function get_menu_list ($data)
    {
        $res = DB::table(Config::get('tables.TBL_SITE_MENU_NAVIGATION').' as smn')
                ->leftJoin(Config::get('tables.TBL_SITE_MENU').' as sm', 'smn.menu_id', '=', 'sm.menu_id')
                ->leftJoin(Config::get('tables.TBL_SITE_MENU_NAVIGATION').' as p', 'p.navigation_id', '=', 'smn.parent_navigation_id')
                ->where('sm.is_deleted', Config::get('constants.OFF'))
                ->where('smn.is_deleted', Config::get('constants.OFF'))
                ->selectRaw('smn.type,smn.navigation_id,smn.navigation_name,smn.created_on,sm.menu_name,p.navigation_name as top_menu');
        if (!empty($data['search_term']) && isset($data['search_term']))
        {
            $res->whereRaw('(smn.navigation_name like \'%'.$data['search_term'].'%\')');
        }
        if (!empty($data['top_menu_filter']) && isset($data['top_menu_filter']))
        {
            $res->where('p.navigation_id', $data['top_menu_filter']);
        }
        if (isset($data['start']) && isset($data['length']))
        {
            $res->skip($data['start'])->take($data['length']);
        }
        if (isset($data['orderby']))
        {
            $res->orderby('smn.created_on', $data['order']);
        }
        if (isset($data['menu_id']) && !empty($data['menu_id']))
        {

            $res->where('smn.menu_id', $data['menu_id']);
        }
        if (isset($data['count']) && !empty($data['count']))
        {
            return $res->count();
        }
        else
        {
            return $res->get();
        }
    }
}
