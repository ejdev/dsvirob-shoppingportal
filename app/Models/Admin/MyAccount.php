<?php

namespace App\Models\Admin;

use DB;
use config;
use Illuminate\Database\Eloquent\Model;
use TWMailer;

class MyAccount extends Model
{

    public function user_name_check ($arr = array())
    {
        if (!empty($arr))
        {

            extract($arr);
            $result = '';
            $result = DB::table(config('tables.ACCOUNT_LOGIN_MST'))
                            ->where('account_type_id', Config::get('constants.ACCOUNT_TYPE_USER'))->where('is_deleted', Config::get('constants.OFF'));
            if (isset($uname) && !empty($uname))
            {
                $result = $result->where('uname', $uname);
            }
            if (isset($email) && !empty($email))
            {
                $result = $result->where('email', $email);
            }
            else if (isset($mobile) && !empty($mobile))
            {
                $result = $result->where('mobile', $mobile);
            }
            $result = $result->first();
            if (!empty($result))
            {
                return $result;
            }
            else
            {
                return 'error';
            }
        }
    }

    public function save_user_details ($arr = array())
    {

        $currentdate = date('Y-m-d H:i:s');
        if (isset($arr) && !empty($arr))
        {
            $login['user_name'] = $arr['uname'];
            $login['email'] = $arr['primary_email'];
            $login['pass_key'] = md5($arr['password']);
            $login['account_id'] = $account_id;
            $login['account_type_id'] = config('constants.ACCOUNT_TYPE_COMPANY');
            $account_id = DB::table(config('constants.TW_USER_LOGIN_MST'))->insertGetId($login);
            if ($account_id)
            {
                $extras['salutation'] = $arr['User_salutation'];
                $extras['firstname'] = $arr['User_firstName'];
                $extras['lastname'] = $arr['User_lastName'];
                $extras['mobile'] = $arr['mobile_phone'];
                $extras['office_phone'] = $arr['office_phone'];
                $extras['account_id'] = $account_id;
                $extras['company_id'] = $arr['company_id'];
                $extras['profile_img'] = $arr['image_name'];
                $account_Details = DB::table(config('constants.TW_ACCOUNT_EXTRAS'))->insertGetId($extras);
                if (!empty($account_Details))
                {
                    $address['account_id'] = $account_id;
                    $address['street1'] = $arr['addr1'];
                    $address['street2'] = $arr['addr2'];
                    $address['city'] = $arr['city'];
                    $address['state'] = $arr['state'];
                    $address['postal_code'] = $arr['postal_code'];
                    $address['country'] = $arr['country_id'];
                    $address['post_type'] = 3;
                    $address['address_type_id'] = config('constants.PRIMARY_ADDRESS');
                    DB::table(config('constants.TW_ACCOUNT_ADDRESS'))->insertGetId($address);

                    if (!empty($arr['primary_email']))
                    {
                        $primary['user_account_id'] = $account_id;
                        $primary['email_type_id'] = config('constants.PRIMARY_EMAIL');
                        $primary['email'] = $arr['primary_email'];
                        $primary['create_on'] = $currentdate;
                        DB::table(config('constants.TW_USER_EMAILS'))->insertGetId($primary);
                    }
                    if (!empty($arr['secondary_email']))
                    {
                        $secondary['user_account_id'] = $account_id;
                        $secondary['email_type_id'] = config('constants.SECONDARY_EMAIL');
                        $secondary['email'] = $arr['secondary_email'];
                        $secondary['create_on'] = $currentdate;
                        DB::table(config('constants.TW_USER_EMAILS'))->insertGetId($secondary);
                    }
                }

                $mstatus = TWMailer::send(array(
                            'to'=>$arr['primary_email'],
                            'subject'=>'Congratulations, Your account has been created',
                            'view'=>'emails.employee_registration',
                            'data'=>$arr
                ));

                return $account_id;
            }
        }
        return false;
    }

    public function reset_pwd ($postdata)
    {
        if ($postdata)
        {
            $login['pass_key'] = $postdata['pass_key'];
            $status = DB::table(config('tables.ACCOUNT_LOGIN_MST'))
                    ->where('account_id', $postdata['account_id'])
                    //->where('is_deleted', config('constants.OFF'))
                    ->update($login);
            if ($status)
            {
                $ret['status'] = true;
                $ret['msg'] = 'Password Updated Successfully.';
            }
            else
            {
                $ret['status'] = false;
                $ret['msg'] = 'Password Updation Failed. Please try again.';
            }
            return $ret;
        }
    }

    public function account_details ($data)
    {
        $result = DB::table(config('tables.ACCOUNT_LOGIN_MST').' as alm')
                ->join(config('tables.ACCOUNT_MST').' as mst', 'mst.account_id', '=', 'alm.account_id')
                ->leftjoin(config('tables.GENDER_LOOKUP').' as g', 'g.gender_id', '=', 'mst.gender')
                ->select(DB::Raw("mst.firstname,mst.lastname,mst.gender as gender_id,alm.email,g.gender,alm.mobile"))
                ->where('alm.account_id', $data['account_id'])
                ->where('alm.account_type_id', config('tables.ACCOUNT_TYPE.USER'))
                ->where('mst.is_deleted', config('constants.OFF'));
        $res = $result->first();
        if (!empty($res))
        {
            return $res;
        }
        else
        {
            return NULL;
        }
    }

    public function update_personal_info ($sdata = array(), $data = array())
    {
        $res = DB::table(config('tables.ACCOUNT_MST'))
                ->where('account_id', $data['account_id'])
                ->where('is_deleted', config('constants.OFF'))
                ->update($sdata);
        if ($res)
        {
            return $res;
        }
        else
        {
            return NULL;
        }
    }

    public function old_password_check ($data)
    {
        $res = DB::table(config('tables.ACCOUNT_LOGIN_MST'))
                ->where('account_id', $data['account_id'])
                ->where('pass_key', md5($data['oldpassword']))
                ->get();
        if (!empty($res) && count($res) > 0)
        {
            return $res[0];
        }
        return false;
    }

    public function update_new_password ($data)
    {
        $pass['pass_key'] = md5($data['new_password']);
        $res = DB::table(config('tables.ACCOUNT_LOGIN_MST'))
                ->where('account_id', $data['account_id'])
                ->update($pass);
        if ($res)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function save_address ($adata)
    {
        //return $adata;
        $result = DB::table(config('tables.ACCOUNT_ADDRESS'))->insertGetId($adata);
        if (!empty($result))
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    public function get_address_list ($data = array())
    {
        $res = DB::table(config('tables.ACCOUNT_ADDRESS').' as addr')
                ->leftjoin(config('tables.LOCATION_COUNTRY').' as c', 'c.country_id', '=', 'addr.country_id')
                ->leftjoin(config('tables.LOCATION_STATE').' as s', 's.state_id', '=', 'addr.state_id')
                ->join(config('tables.ACCOUNT_MST').' as mst', 'mst.account_id', '=', 'addr.account_id')
                ->where('addr.account_id', $data['account_id'])
                ->where('addr.is_deleted', 0)
                ->select(DB::Raw("concat(mst.firstname,' ',mst.lastname) as full_name,addr.street1,addr.street2,addr.city,addr.postal_code,c.country,s.state,addr.address_id,addr.address_type_id"))
                ->get();
        if (!empty($res))
        {
            return $res;
        }
        return false;
    }

    public function address_type ($data = array())
    {
        $wdata = array();
        $wdata['address_type_id'] = 0;
        $res = DB::table(config('tables.ACCOUNT_ADDRESS'))
                ->where('account_id', $data['account_id'])
                ->where('is_deleted', 0)
                ->update($wdata);
        if ($res)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function default_address ($wdata = array())
    {
        $data = array();
        $data['address_type_id'] = 1;
        $res = DB::table(config('tables.ACCOUNT_ADDRESS'))
                ->where('address_id', $wdata['address_id'])
                ->update($data);
        if ($res)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function delete_address ($wdata = array())
    {
        $data = array();
        $data['is_deleted'] = 1;
        $res = DB::table(config('tables.ACCOUNT_ADDRESS'))
                ->where('address_id', $wdata['address_id'])
                ->update($data);
        if ($res)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function update_email ($adata = array(), $data = array())
    {
        $res = DB::table(config('tables.ACCOUNT_LOGIN_MST'))
                ->where('account_id', $data['account_id'])
                ->update($adata);
        if ($res)
        {
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function add_resetCode ($arr)
    {
        $res = DB::table(config('tables.ACCOUNT_RESETCODE_LOG'))
                ->InsertGetId($arr);
        if (!empty($res))
        {
            /* $mstatus = TWMailer::send(array(
              'to' => $mdata['mail'],
              'subject' => 'Drop Shipping - '.$arr['reset_code'].' is your verification code for secure access',
              'view' => 'emails.account.email_verification',
              'data' => $arr
              ));
              return $res; */
        }
        else
        {
            return false;
        }
    }

    public function check_reset_code ($arr)
    {
        $res = DB::table(config('tables.ACCOUNT_RESETCODE_LOG'))
                ->where('account_id', $arr['account_id'])
                ->where('reset_code', $arr['v_code'])
                ->orderBy('id', 'desc')
                ->first();
        if (!empty($res))
        {
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function update_mail ($arr)
    {
        $update['email'] = $arr['mail'];
        $res = DB::table(config('tables.ACCOUNT_LOGIN_MST'))
                ->where('account_id', $arr['account_id'])
                ->update($update);
        if (!empty($res))
        {
            return $res;
        }
        else
        {
            return false;
        }
    }

    public function save_deactivate ($data = array())
    {
        $adata = array();
        $chkpass = DB::table(config('tables.ACCOUNT_MST').' as tulm')
                ->join(config('tables.ACCOUNT_LOGIN_MST').' as mst', 'mst.account_id', '=', 'tulm.account_id')
                ->where('tulm.account_id', $data['account_id'])
                ->where('mst.account_type_id', 1)
                ->where('mst.pass_key', md5($data['password']))
                ->where('tulm.is_deleted', config('constants.OFF'))
                ->first();

        if (!empty($chkpass))
        {
            $adata = array();
            $adata['is_deactivated'] = config('constants.ON');
            $adata['deactivated_on'] = date('Y-m-d H:i:s');
            $result = DB::table(config('tables.ACCOUNT_MST'))
                    ->where('account_id', $data['account_id'])
                    ->where('is_deleted', config('constants.OFF'))
                    ->where('is_deactivated', config('constants.OFF'))
                    ->update($adata);
            if ($result)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }
        else
        {
            return 1;
        }
    }

}
