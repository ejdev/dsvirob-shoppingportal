<?php

namespace App\Models\Admin;
use DB;
use Config;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    public function getUserList ($arr = array(), $count = false)
    {	
		//return $arr;
        extract($arr);
        $Users = DB::table(Config::get('tables.ACCOUNT_MST').' as am')
                ->join(Config::get('tables.ACCOUNT_LOGIN_MST').' as alm', 'alm.account_id', '=', 'am.account_id')
                ->join(Config::get('tables.ACCOUNT_STATUS_LOOKUP').' as asl', 'asl.status_id', '=', 'am.status_id')
                ->where('am.is_deleted', Config::get('constants.OFF'))
                ->where('alm.account_type_id', Config::get('constants.ACCOUNT_TYPE_USER'));				
       
        if (isset($start) && isset($length))
        {
            $Users->skip($start)->take($length);
        }
        if (isset($orderby) && !empty($orderby))
        {
            $Users->orderby($orderby, $order);
        }
        else
        {
            $Users->orderby('am.created_on', 'DESC');
        }
        if ($count)
        {
            return $Users->count();
        }
        else
        {
            $Users = $Users->select(Db::raw('am.firstname, am.lastname, am.profile_img, asl.status_name, am.account_id, alm.user_code, alm.email, alm.mobile, am.created_on'))->get();
			
			array_walk($Users, function($use) 
            {
                $use->created_on = date('d-M-Y H:i:s', strtotime($use->created_on));
			});
			
			return $Users;
            
            
        }
    }
}
