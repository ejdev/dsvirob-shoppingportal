<?php

namespace App\Http\Middleware;
//use Route;
use Closure;
use Session;
use Response;
use Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

class AuthApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */    
	
	public function handle($request, Closure $next)
    {		
		
			$data = array();
			$op = array();
			$op['msg'] = '';	
			if (Session::has('user_data')) {  $data['user_data'] = Session::get('user_data');  } else { $data['user_data'] = 'empty1'; }
			$postdata = $request->all();				
			if (Session::has('apiUserData')) {  $data['api'] = Session::get('apiUserData');  } else { $data['api'] = 'empty'; }		
			$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			//$url = Route::getFacadeRoot()->current()->uri();
			
			if ((Request::segment(1) == 'api') && (Request::segment(2) == 'v1') && (strpos($url, 'token') > 0)) {	
				//return Response::json($url, 200);
				if (Session::has('apiUserData'))
				{						
					if ( (!empty($data['api'])) && (!empty($postdata['token'])) ) 
					{						
						if (isset($postdata['token'])) {
							if (($data['api']->token == $postdata['token']))
							{							
								return $next($request);
							} else {
								$op['msg'] = 'Token Mismatch... Unauthorized Request...';						
								return Response::json($op, 200);	
							}
						} else {
							$op['msg'] = 'Token Missing';						
							return Response::json($op, 200);	
						}
					}						
					return $next($request);	
				} else {
					if (strpos($url, 'api/v1/auth/login') !== false) {	
						return $next($request);
					} else {
						$op['msg'] = 'Token Mismatch... Unauthorized Request...';						
						return Response::json($op, 200);
					}	
				}				
				//return $next($request);
			} else {		
				
				//return Response::json($data, 200);
				return $next($request);
			}
			
			
		
       
    }
}
