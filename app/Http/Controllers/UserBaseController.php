<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use Response;
use Session;
use View;
use Config;
use DB;

class UserBaseController extends BaseController
{

    public function __construct ()
    {
        parent::__construct();        
        $this->theme = $this->sitesettings->user_site_theme;
        Config::set(['ThemeAsset'=>'themes/'.$this->theme.'/assets']);
        Config::set(['ThemeProviders'=>'themes/'.$this->theme.'/providers']);
        View::addLocation('themes/'.$this->theme.'/templates');
        $data = array();
        $menus = $this->HTTPApiObj->postRequest('get-menus', $data, 'POST');
		//echo '<pre>';print_r($menus);
        View::share('menu_data', $menus);
        $res = $this->HTTPVpiObj->postRequest('get-category', $data, 'POST');
        if (!empty($res->category))
        {
            $category = $res->category;
            foreach ($category as $cat)
            {
                $cat->url = $this->commonSettingsObj->generateBrowseURL(array('category'=>$cat));
            }
            View::share('category', $category);
        }
        //$payment = $this->commonSettingsObj->get_payment_settings();
        $payment = $this->HTTPApiObj->postRequest('get-payment-settings', $data, 'POST');
        if (!empty($payment))
        {
            View::share('payments', $payment);
        }
        else
        {
            echo '';
        }
        if (Session::has('user_data'))
        {
            $user_details = Session::get('user_data');
            if (!empty($user_details))
            {
                $this->account_id = $user_details->account_id;
                $this->uname = $user_details->uname;
                $this->email = $user_details->email;
                $this->full_name = $user_details->uname;
                $this->mobile = $user_details->mobile;
                $this->user_code = $user_details->user_code;
            }
            View::share('user_details', $user_details);
        }
    }

    public function get_gender_lookup ()
    {
        return DB::table(Config::get('tables.GENDER_LOOKUP'))
                        ->get();
    }

    public function get_countries ($arr = array())
    {
        $res = DB::table(config('tables.LOCATION_COUNTRY'));
        if (isset($arr['country_id']) && !empty($arr['country_id']))
        {
            $res->where('country_id', $arr['country_id']);
        }
        $result = $res->get();
        return $result;
    }

    public function get_states_list ($arr = array())
    {
        $query = DB::table(config('tables.LOCATION_STATE'))
                ->where('status', 1)
                ->orderby('state');
        if (!empty($arr['country_id']))
        {
            $query->where('country_id', $arr['country_id']);
        }
        if (!empty($arr['state_id']))
        {
            $query->where('state_id', $arr['state_id']);
        }
        return $query->get();
    }

    public function get_city_list ($arr = array())
    {
        $query = DB::table(config('tables.CITYS').' as cou')
                ->join(config('tables.LOCATION_STATE').' as ls', 'ls.state_id', '=', 'cou.state_id')
                ->where('cou.status', 1)
                ->select(DB::raw('cou.city_id,cou.city_name,cou.state_id,cou.district_id,cou.latitude,cou.longitude'));
        if (!empty($arr['state_id']))
        {
            $query->where('cou.state_id', $arr['state_id'])->get();
        }
        if (!empty($arr['country_id']))
        {
            $query->where('ls.country_id', $arr['country_id']);
        }
        if (!empty($arr['city_id']))
        {
            $query->where('cou.city_id', $arr['city_id']);
        }
        $result = $query->orderBy('cou.city_name', 'asc')
                ->get();
        if (!empty($result) && count($result) > 0)
        {
            return $result;
        }
    }

}
