<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserBaseController;
use Response;
use App\Models\User\User;
use App\Models\User\Cart;
use App\Http\Controllers\Api\HTTPApiController;
use App\Models\User\MyAccount;
use App\Models\Common;

class ApiOrderController extends AuthController
{
    public $headers = [];
    public $statusCode = 422;
    public $options = JSON_PRETTY_PRINT;

    public function __construct ()
    {
        parent::__construct();
        $this->userObj = new User();
        $this->cartObj = new Cart();
        $this->myaccObj = new MyAccount();
        $this->commonSettingsObj = new Common();
		/* api - used to call the url within current system */		
		$this->HTTPApiObj = new HTTPApiController('api'); 
		/* vpi - used to call the url  of Dropshipping website */
		$this->HTTPVpiObj = new HTTPApiController('vpi');
    }
	
	public function MyOrderList ()
    {
	  $data = $this->request->all();
	  if (isset($data['account_id'])) {
		$res = $this->HTTPVpiObj->postRequest('my-orders', $data, 'POST');	
	  } else {
		$res = array('status' => 'error', 'msg' => 'Parameter Missing....');
	  }
	  $this->statusCode = 200;
	  return Response::json($res, $this->statusCode, $this->headers, $this->options);	  
    }
	
	public function OrderDetails ()
    {
	  $data = $this->request->all();
	  //return Response::json($data, 200, $this->headers, $this->options);
	  if (isset($data['soc'])) {		
		$res = (array) $this->HTTPVpiObj->postRequest('my-orders-items', $data, 'POST');	
	  } else {
		$res = array('status' => 'error', 'msg' => 'Parameter Missing....');
	  }
	  $this->statusCode = 200;
	  return Response::json($res, $this->statusCode, $this->headers, $this->options);	  
    }
	
	
}
