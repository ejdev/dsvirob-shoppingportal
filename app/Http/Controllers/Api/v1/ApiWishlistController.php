<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserBaseController;
use Response;
use App\Models\User\User;
use App\Models\User\Cart;
use App\Http\Controllers\Api\HTTPApiController;
use App\Models\User\MyAccount;

class ApiWishlistController extends AuthController
{
    public $headers = [];
    public $statusCode = 422;    
    public $options = JSON_PRETTY_PRINT;
	
    public function __construct ()
    {
		parent::__construct();				
		$this->userObj = new User();
		$this->cartObj = new Cart();
		$this->myaccObj = new MyAccount();
	}
	
	public function remove_wish_list ()
    {
        $post = $this->request->all();
        $op['status'] = 'error';
        $op['msg'] = 'Invalid Parameter...';
        if (!empty($post['account_id']) && !empty($post['id']))
        {            
            $res = $this->cartObj->remove_wish_list($post);
            if ($res)
            {
                $op['status'] = 'ok';
                $op['msg'] = 'Product Successfully removed from  Wish List';
            }           
        } else {
			$op['status'] = 'error';
            $op['msg'] = 'Parameter Missing...';
		}
		$this->statusCode = 200;
		return Response::json($op, $this->statusCode, $this->headers, $this->options);
		 
    }
	
	public function get_wish_list ()
    {	
		$response 	= '';
		$postdata 	= $this->request->all();		
		if(!empty($postdata['account_id'])) {
			$response = $this->cartObj->my_wish_list($postdata);				
			if ($response['count'] > 0) {		
				$this->op = $response;		
			} else {
				$this->op['msg'] = 'No products found';
				$this->op['pid'] = NULL;
				$this->op['wid'] = NULL;
				$this->op['count'] = $response['count'];
			}
		} else {
			$this->op['status'] = 'error';
			$this->op['msg'] = 'Parameter Missing';
		}
		$this->statusCode = 200;
		return Response::json($this->op, $this->statusCode, $this->headers, $this->options);		
	}
	
	public function add_to_wish_list ()
    {	
		$data = [];			
		$data = $this->request->all();					
        if (!empty($data['account_id']))
        {	            			
			$res['account_id'] = $data['account_id'] = isset($this->account_id) ? $this->account_id : $data['account_id'];
			//$data['currency_id'] = isset($this->currency_id) ? $this->currency_id : $data['currency_id'];	
            if ($this->request->has('supplier_product_id'))
            {
                $res['product_id'] =  $data['supplier_product_id'] = $this->request->get('supplier_product_id');
                if ($this->request->has('spc_id'))
                {
            //        $res['spc_id'] = $data['spc_id'] = $this->request->get('spc_id');
                }                
				//$product_details = $this->HTTPVpiObj->postRequest('supplierProductDetails', $data, 'POST');								
                if ($data['supplier_product_id'])
                {		
					$wishlist = $this->cartObj->add_wish_list($res);						
                    if ($wishlist)
                    {
                        $this->statusCode = 200;
                        //$this->resp['msg'] = 'Product Successfully Added to Wish List';
						$this->resp['status'] = $wishlist['status'];	
                        $this->resp['msg'] = $wishlist['msg'];						
                    }
                    else
                    {
                        $this->resp['msg'] = 'Some thing went wrong...';
                    }
                }
                else
                {
                    $this->resp['msg'] = 'Parameter Missing...';
                }
            }
            else
            {
                $this->resp['msg'] = 'Parameter Missing...';
            }
        }
        else
        {
            $this->resp['msg'] = 'Please Login to Proceed...';
        }
        return Response::json($this->resp, 200, $this->headers, $this->options);	
	}
	
	
}
