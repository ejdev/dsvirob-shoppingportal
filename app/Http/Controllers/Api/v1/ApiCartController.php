<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Api\v1\AuthController;
use Response;
use App\Models\User\Wallet;
use Cart;
use Session;
use App\Http\Controllers\Api\HTTPApiController;
use App\Http\Controllers\User\CommonController;
use App\Models\Common;
use URL;
use View;
use Cookie;
use Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ApiCartController extends AuthController
{
    public $headers = [];
    public $statusCode = 422;    
    public $options = JSON_PRETTY_PRINT;	
	
    public function __construct ()
    {
		parent::__construct();						
		$this->walletObj = new Wallet();
		$this->HTTPApiObj = new HTTPApiController('api'); 
		$this->HTTPVpiObj = new HTTPApiController('vpi');
		$this->ObjComController = new CommonController();
		$this->commonSettingsObj = new Common();		
	}	
	
	
}
