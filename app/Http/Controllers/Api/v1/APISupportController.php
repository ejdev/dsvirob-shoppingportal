<?php
namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use App\Models\User\MyAccount;
use App\Models\User\User;
use App\Models\Sendsms;
use App\Models\Api\APISupport;
use App\Http\Controllers\UserBaseController;
use App\Http\Controllers\Api\HTTPApiController;
use URL;
use Response;
use View;
use Config;
use Redirect;

class APISupportController extends AuthController {

    public $myaccObj = '';
    public $userObj = '';
    public function __construct() {
        parent::__construct();
        $this->supportObj = new APISupport();
        $this->HTTPApiObj = new HTTPApiController('api');
        $this->HTTPVpiObj = new HTTPApiController('vpi');
    }
    public function helper_faq($category = '') {
        $data 				= $this->request->all();
        $this->statusCode   = 422;
        if(empty($category))
		{
			$data['qalist']   = $this->supportObj->faqs_list();
		}
		else
		{
			  $data['qalist'] = $this->supportObj->faqs_list(array('slug' => $category));
		} 
		$qaCatwise = array();
        if (!empty($data['qalist'])) {
            foreach ($data['qalist'] as $qaitem) {
                if (!isset($qaCatwise[$qaitem->faq_category_id])) {
                    $qaCatwise[$qaitem->faq_category_id]['category'] = array('faq_category_id' => $qaitem->faq_category_id, 'category_name' => $qaitem->category_name, 'category_slug' => $qaitem->category_slug);
                }
                $qaCatwise[$qaitem->faq_category_id]['qalist'][] = array('title' => $qaitem->title, 'description' => $qaitem->description,'id'=>$qaitem->id);
                if ($qaitem->related_faq_category_id != '') {
                    $rcatlist = $this->supportObj->faq_categories_list(array('ids' => $qaitem->related_faq_category_id));
                    if (!empty($rcatlist)) {
                        foreach ($rcatlist as $rcatitem) {
                            if (!isset($qaCatwise[$qaitem->faq_category_id]['related_catlist'][$rcatitem->faq_category_id])) {
                                $qaCatwise[$qaitem->faq_category_id]['related_catlist'][$rcatitem->faq_category_id] = array('title' => $rcatitem->category_name, 'category_slug' => $rcatitem->category_slug);
                            }
                        }
                    }
                }
            }
        }
        if (!empty($data)) {
            $this->statusCode	 = 200;
            $op	 = $qaCatwise;
        } else {
            $op['msg'] = 'Could not change';
        }
        return Response::json($op, $this->statusCode, $this->headers, $this->options);
    }
    public function contact_us() {
        $data = Input::all();
        $this->statusCode = 422;
        //$data['slug']=$category;
        
		$data['splist'] = $this->supportObj->support_contact_list();
		    //$data['related_cat_list'] = $this->supportObj->faq_related_categories_list();

        if (!empty($data['splist'])) {
            $cntct_array = array();
            $i = 0;

            foreach ($data['splist'] as $cntitem) {
                if (!isset($cntct_array[$cntitem->parent_issues_category_id])) {
                    $cntct_array[$cntitem->parent_issues_category_id]['categoryinfo'] = array('title' => $cntitem->parent_category, 'parent_id' => $cntitem->parent_issues_category_id);
                }
                if (!isset($cntct_array[$cntitem->parent_issues_category_id]['subcat'][$cntitem->issues_category_id])) {
                    $cntct_array[$cntitem->parent_issues_category_id]['subcat'][$cntitem->issues_category_id]['info'] = array('title' => $cntitem->category, 'child_id' => $cntitem->issues_category_id);
                }
                $cntct_array[$cntitem->parent_issues_category_id]['subcat'][$cntitem->issues_category_id]['quries'][] = array('title' => $cntitem->titles, 'question_id' => $cntitem->question_id, 'related_faq_id' => $cntitem->related_faq_id);
                // $cntct_array['type'] = $cntitem->issues_category_type;
                // $cntct_array[$cntitem->category][] =  $cntitem;
                /* if($cntitem->related_faq_id!=''){
                  $rfaqlist = $this->supportObj->related_faq_id(array('ids'=>$cntitem->related_faq_id));
                  $cntitem->related_faq_info = $rfaqlist;
                  } */
            }
        }


        if (!empty($data)) {
            $this->statusCode = 200;
            $this->response['msg'] = 'Your product has an Faq';
            //$this->response['related_cat_list'] = $data['related_cat_list'];
            $this->response['support_qacatgry'] = $cntct_array;
        } else {
            $this->response['msg'] = 'Could not change';
        }
        /* }} */



        return Response::json($this->response, $this->statusCode, $this->headers, $this->options);
    }
	public function getfeatures_faqs(){
		 $feature_faqs = $this->supportObj->getfeatures_faqs();
		 return Response::json($feature_faqs, 200, $this->headers, $this->options);
	}

}
