<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserBaseController;
use Response;
use App\Models\User\User;
use App\Models\User\Cart;
use App\Http\Controllers\Api\HTTPApiController;
use App\Models\User\MyAccount;
use App\Models\Common;
use App\Models\Api\ApiNavigation;
use URL;

class ApiMenuController extends AuthController
{

    public function __construct ()
    {
        parent::__construct();
        $this->navObj = new ApiNavigation();
    }	

    public function menus ()
    {
        $menu_id = $this->navObj->get_menu_ids();		
		
        if (!empty($menu_id))
        {
            if (!empty($menu_id[2]))
            {
                $menu = $this->navObj->get_menu_struct($menu_id[2]);
            }
            if (!empty($menu_id[5]))
            {
                $footer_catlog_menu = $this->navObj->get_menu_struct($menu_id[5]);
            }
            if (!empty($menu_id[4]))
            {
                $footer_secondary_menu = $this->navObj->get_menu_struct($menu_id[4]);
            }
            if (!empty($menu_id[3]))
            {
                $footer_primary_menu = $this->navObj->get_menu_struct($menu_id[3]);
            }
            if (!empty($menu_id[6]))
            {
                $account_menu = $this->navObj->get_menu_struct($menu_id[6]);
            }
            if (!empty($menu_id[7]))
            {
                $support_menu = $this->navObj->get_menu_struct($menu_id[7]);
            }
            if (!empty($menu_id[1]))
            {
                $catlog_menu = $this->navObj->get_menu_struct($menu_id[1]);
            }
        }
        $menudata = ['catlog_menu'=>[]];
        if (!empty($catlog_menu))
        {
            foreach ($catlog_menu as $data)
            {
                if ($data->parent_navigation_id == 0 && empty($data->group) && $data->type == 1)
                {
                    $menudata['catlog_menu'][] = ['title'=>$data->navigation_name, 'url'=>URL::asset($data->navigation_url), 'is_login_required'=>$data->is_login_required];
                }
                else if ($data->parent_navigation_id == 0 && !empty($data->group) && $data->type == 1)
                {
                    $menus_cols = array();
                    foreach ($data->group as $grp)
                    {
                        if (!empty($grp->normal))
                        {
                            $links = [];
                            foreach ($grp->normal as $link)
                            {
                                $links[] = ($link->type == 2) ? ['title'=>$link->navigation_name, 'is_login_required'=>$data->is_login_required] : ['title'=>$link->navigation_name, 'url'=>URL::asset($link->navigation_url), 'is_login_required'=>$data->is_login_required];
                            }
                            $menus_cols[$grp->column_no][] = ($grp->type == 2) ? ['title'=>$grp->navigation_name, 'url'=>URL::asset($grp->navigation_url), 'links'=>$links, 'is_login_required'=>$data->is_login_required] : ['title'=>$grp->navigation_name, 'url'=>URL::asset($grp->navigation_url), 'is_login_required'=>$data->is_login_required];
                        }
                    }
                    $menudata['catlog_menu'][] = ['title'=>$data->navigation_name, 'url'=>URL::asset($data->navigation_url), 'group'=>$menus_cols, 'is_login_required'=>$data->is_login_required];
                }
            }
        }

        if (!empty($menu))
        {
            foreach ($menu as $data)
            {
                if ($data->parent_navigation_id == 0 && empty($data->group) && $data->type == 1)
                {
                    $menudata['menu'][] = ['title'=>$data->navigation_name, 'url'=>URL::asset($data->navigation_url), 'is_login_required'=>$data->is_login_required];
                }
                else if ($data->parent_navigation_id == 0 && !empty($data->group) && $data->type == 1)
                {
                    $menus_cols = array();
                    foreach ($data->group as $grp)
                    {
                        if (!empty($grp->normal))
                        {
                            $links = [];
                            foreach ($grp->normal as $link)
                            {
                                $links[] = ($link->type == 2) ? ['title'=>$link->navigation_name, 'is_login_required'=>$data->is_login_required] : ['title'=>$link->navigation_name, 'url'=>URL::asset($link->navigation_url), 'is_login_required'=>$data->is_login_required];
                            }
                            $menus_cols[$grp->column_no][] = ($grp->type == 2) ? ['title'=>$grp->navigation_name, 'url'=>URL::asset($grp->navigation_url), 'links'=>$links, 'is_login_required'=>$data->is_login_required] : ['title'=>$grp->navigation_name, 'url'=>URL::asset($grp->navigation_url), 'is_login_required'=>$data->is_login_required];
                        }
                    }
                    $menudata['menu'][] = ['title'=>$data->navigation_name, 'url'=>URL::asset($data->navigation_url), 'group'=>$menus_cols, 'is_login_required'=>$data->is_login_required];
                }
            }
        }

        if (!empty($footer_secondary_menu))
        {
            foreach ($footer_secondary_menu as $f_sec_menu)
            {
                $menudata['footer_secondary_menu'][] = ['title'=>$f_sec_menu->navigation_name, 'url'=>$f_sec_menu->navigation_url, 'is_login_required'=>$data->is_login_required];
            }
        }

        if (!empty($footer_catlog_menu))
        {
            foreach ($footer_catlog_menu as $f_cat_menu)
            {
                if (!empty($f_cat_menu->normal))
                {
                    $normal = array();
                    foreach ($f_cat_menu->normal as $norm)
                    {
                        $normal[] = ['title'=>$norm->navigation_name, 'url'=>$norm->navigation_url, 'is_login_required'=>$data->is_login_required];
                    }
                }
                $menudata['footer_catlog_menu'][] = ['title'=>$f_cat_menu->navigation_name, 'url'=>$f_cat_menu->navigation_url, 'type'=>$f_cat_menu->type, 'normal'=>$normal, 'is_login_required'=>$data->is_login_required];
            }
        }

        if (!empty($footer_primary_menu))
        {
            $normal = array();
            foreach ($footer_primary_menu as $f_prim_menu)
            {
                if (!empty($f_prim_menu->normal))
                {
                    $normal = array();
                    foreach ($f_prim_menu->normal as $norm)
                    {
                        $normal[] = ['title'=>$norm->navigation_name, 'url'=>$norm->navigation_url, 'is_login_required'=>$data->is_login_required];
                    }
                }
                $menudata['footer_primary_menu'][] = ['title'=>$f_prim_menu->navigation_name, 'url'=>$f_prim_menu->navigation_url, 'type'=>$f_prim_menu->type, 'normal'=>$normal, 'is_login_required'=>$data->is_login_required];
            }
        }



        if (!empty($account_menu))
        {
            $normal = array();
            foreach ($account_menu as $acc_menu)
            {
                if (!empty($acc_menu->normal))
                {
                    $normal = array();
                    foreach ($acc_menu->normal as $norm)
                    {
                        $normal[] = ['title'=>$norm->navigation_name, 'url'=>$norm->navigation_url, 'is_login_required'=>$data->is_login_required];
                    }
                }
                $menudata['account_menu'][] = ['title'=>$acc_menu->navigation_name, 'url'=>$acc_menu->navigation_url, 'type'=>$acc_menu->type, 'normal'=>$normal, 'is_login_required'=>$data->is_login_required];
            }
        }

        if (!empty($support_menu))
        {
            $normal = array();
            foreach ($support_menu as $supp_menu)
            {
                if (!empty($supp_menu->normal))
                {
                    $normal = array();
                    foreach ($supp_menu->normal as $norm)
                    {
                        $normal[] = ['title'=>$norm->navigation_name, 'url'=>$norm->navigation_url, 'is_login_required'=>$data->is_login_required];
                    }
                }
                $menudata['support_menu'][] = ['title'=>$supp_menu->navigation_name, 'url'=>$supp_menu->navigation_url, 'type'=>$supp_menu->type, 'normal'=>$normal, 'is_login_required'=>$data->is_login_required];
            }
        }
        $this->statusCode = 200;
        return Response::json($menudata, $this->statusCode, $this->headers, $this->options);
    }

}
