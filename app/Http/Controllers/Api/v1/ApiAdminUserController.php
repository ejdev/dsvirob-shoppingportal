<?php

namespace App\Http\Controllers\Api\v1;
use Response;
use Illuminate\Http\Request;
use App\Models\Admin\Users;
use App\Http\Requests;
use App\Http\Controllers\AdminBaseController;

class ApiAdminUserController extends AdminBaseController
{
    public $myaccObj = '';
    public $adminObj = '';

    public function __construct ()
    {
        parent::__construct();
        //$this->myaccObj = new MyAccount();
        $this->UsersObj = new Users();
    }
	
	public function UserList ()
    {
        $data = $filter =  array();       
        $this->statusCode = 200;
        $postdata = $this->request->all();				        		
        $postdata['account_id'] = 123;        
		$this->op['recordsTotal'] = $this->UsersObj->getUserList($postdata, true);				
        $this->op['data'] = array();
        $this->op['recordsFiltered'] = $this->op['recordsFiltered'] = 0;		
        $filter['search_term'] = (isset($postdata['search_term']) && !empty($postdata['search_term'])) ? $postdata['search_term'] : null;		
        if ($this->op['recordsTotal'] > 0)
        {
            $this->op['recordsFiltered'] = $this->UsersObj->getUserList($data, true);			
            if ($this->op['recordsFiltered'] > 0)
            {
                $data['start'] = isset($postdata['start']) && !empty($postdata['start']) ? $postdata['start'] : 0;
                $data['length'] = isset($postdata['length']) && !empty($postdata['length']) ? $postdata['length'] : 10;
                $data['orderby'] = isset($postdata['orderby']) && !empty($postdata['orderby']) ? $postdata['orderby'] : null;
                $data['order'] = isset($postdata['order']) && !empty($postdata['order']) ? $postdata['order'] : null;
                $data = array_merge($data, $filter);
                $this->op['data'] = $this->UsersObj->getUserList($data);
            }
        }
		unset($this->op['msg']);
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }
	
}
