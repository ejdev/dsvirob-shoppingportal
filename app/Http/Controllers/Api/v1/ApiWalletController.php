<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserBaseController;
use Response;
use App\Models\User\Wallet;
use Cart;
use Session;
use App\Http\Controllers\Api\HTTPApiController;
use App\Http\Controllers\User\CommonController;
use App\Models\Common;


class ApiWalletController extends AuthController
{
    public $headers = [];
    public $statusCode = 422;    
    public $options = JSON_PRETTY_PRINT;
	
    public function __construct ()
    {
		parent::__construct();						
		$this->walletObj = new Wallet();
		$this->HTTPApiObj = new HTTPApiController('api'); 
		$this->HTTPVpiObj = new HTTPApiController('vpi');
		$this->ObjComController = new CommonController();
		$this->commonSettingsObj = new Common();
		
	}
	
	public function getProfitSharingInfo (Request $request)
    {
        $data = array();
        $data = $request->all();		
		$data['pop'] = $this->commonSettingsObj->get_discount_percentage();
		$data['currency_id'] = isset($this->currency_id) ? $this->currency_id : $data['currency_id'];				        
        $res =  $this->ObjComController->getProfitSharingInfo($data);		
		$res['wallet'] = $this->commonSettingsObj->getWalletInfo($data);       		
		$prod_pruchase_perc = $res['wallet']->prod_pruchase_perc;
		$bonus_perc = ($res['profit_sharing_perc'] / $prod_pruchase_perc ) * 100;		
		$result['bonus']['profit_sharing_perc'] = $res['profit_sharing_perc'];
		$result['bonus']['prod_pruchase_perc'] = $prod_pruchase_perc;
		$result['bonus']['selling_price'] = $res['selling_price'];
		$result['bonus']['bonus_perc'] = round($bonus_perc,2);
		$result['bonus']['bonus_wallet_amt'] = $res['selling_price'] * ($bonus_perc / 100);		
		return $result;		
    }
	
	public function getWallet ()
    {	
		$response 	= '';
		$shipping_charge = 0;
		$postdata 	= $this->request->all();		
		if (empty($postdata['products'])) {	
			$postdata['products'] = json_decode(Cart::instance('myCart')->content(), true); 	
		}
		if (empty($postdata['cart_subtotal'])) {
			$postdata['cart_subtotal'] = Cart::instance('myCart')->subtotal();
		}
		$products = $postdata['products'];
	 	foreach ($postdata['products'] as $po)
			{		
				$shipping_charge += $po['options']['shipping_info']['charge'] * $po['qty'];				
			}
		$postdata['shipping_charge'] = (float) $shipping_charge; 	
		
		if(!empty($postdata['account_id'])) {
			$this->op = $this->walletObj->getWallet($postdata);			
		} else {
			$this->op['msg'] = 'Parameter Missing';
		}
		$this->statusCode = 200;
		return Response::json($this->op, $this->statusCode, $this->headers, $this->options);		
	}
}
