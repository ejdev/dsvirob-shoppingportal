<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\v1\ApiBase;
use App\Models\User\User;
use URL;
use Response;

class AuthController extends ApiBase
{
	public function __construct ()
    {
        parent::__construct();        
		$this->UserObj = new User();
    } 

    public function loginCheck ()
    {
        $op = array();
        $op['status'] = 'fail';
        $op['msg'] = 'Invalid Username and Password';
        $res = '';
		$postdata = $this->request->all();       
        if (!empty($postdata))
        {
            if ($this->request->has('username') && $this->request->has('password'))
            {
                $postdata['request'] = 'api';				
                $res = (array) $this->UserObj->validateUser($postdata);
				//echo '<pre>';print_r($res);exit;
                $validate = $res['status_id'];
                if ($validate == 1)
                {
                    $op['status'] = '1';
                    $op['msg'] = $res['response']['msg'];
                    $op['url'] = URL::to('home');
                    $op['token'] = $res['token'];                
					$op['user_name'] = $res['data']->uname;	
					$op['email'] = $res['data']->email;	
					$op['mobile'] = $res['data']->mobile;	
					$op['account_id'] = $res['data']->account_id;						                    
                }
                else if ($validate == 2)
                {
                    $op['status'] = '2';
                    $op['msg'] = 'Your Account Not Available or deleted';
                }
                else if ($validate == 3)
                {
                    $op['status'] = '3';
                    $op['msg'] = 'Invalid Username and Password';
                }
                else if ($validate == 6)
                {
                    $op['status'] = '6';
                    $op['msg'] = 'Incorrect Password';
                }
                else
                {
                    $op['status'] = '5';
                    $op['msg'] = 'Your Account Has Been Blocked.';
                }
            }
            else
            {
                $op['status'] = 'error';
                $op['msg'] = 'Parameter Missing';
            }
            return Response::json($op, $status = 200, $headers = [], $options = JSON_PRETTY_PRINT);
        }
        return Response::json($op, $status = 401, $headers = [], $options = JSON_PRETTY_PRINT);
    }
}
