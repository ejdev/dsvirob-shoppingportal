<?php

namespace App\Http\Controllers\Api\v1;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApiBase extends Controller
{	
	public $data = array();
    public $apiUrl = '';
	public $response = '';
    public $request = '';
    public $redirect ='';
    public $session ='';
	public $test = 1;
	
	public $headers = [];
    public $statusCode = 422;
    public $op = ['msg'=>''];
    public $options = JSON_PRETTY_PRINT;
	
    public function __construct ()
    {
		$this->response  = response();
        $this->request  = request();
        $this->redirect = redirect();
        $this->session = session();		
	}
}
