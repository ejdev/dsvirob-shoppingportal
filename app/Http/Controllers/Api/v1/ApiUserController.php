<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserBaseController;
use Response;
use App\Models\User\User;
use App\Models\User\Cart;
use App\Http\Controllers\Api\HTTPApiController;
use App\Models\User\MyAccount;
use App\Models\Common;
use Session;

class ApiUserController extends AuthController
{

    public $headers = [];
    public $statusCode = 422;
    public $options = JSON_PRETTY_PRINT;

    public function __construct ()
    {
        parent::__construct();
        $this->userObj = new User();
        $this->cartObj = new Cart();
        $this->myaccObj = new MyAccount();
        $this->commonSettingsObj = new Common();
    }

    public function get_payment_settings ()
    {        
        $response = $this->commonSettingsObj->get_payment_settings();
        $this->op = $response;
        $this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }
	
	public function getDiscountPercentage ()
    {
        $postdata = $this->request->all();
        $response = $this->commonSettingsObj->get_discount_percentage();
        $this->op = $response;
        $this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function validateUser ()
    {
        $postdata = $this->request->all();
        $response = $this->userObj->validateUser($postdata);
        $this->op = $response;
        $this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function placeOrder ()
    {
        $postdata = $this->request->all();		
		//return Response::json($postdata, 200, $this->headers, $this->options);
        $response = $this->cartObj->placeOrder($postdata);
        $this->op = $response;
        $this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function updateOrdercode ()
    {
        $postdata = $this->request->all();
        $response = $this->cartObj->updateOrdercode($postdata);
        $this->op = $response;
        $this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function get_billing_address ()
    {
        $postdata = $this->request->all();
        $response = $this->cartObj->get_billing_address($postdata);
        if ($response)
        {
            $this->op = $response;
        }
        else
        {
            $this->op['msg'] = 'Address Not Found';
            $this->op['data'] = Null;
        }
        $this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function register_user ()
    {

        $postdata = $this->request->all();
        $this->op = '';

        $response = $this->userObj->user_register($postdata);
        if ($response)
        {

            //$op['status'] = 'ok';
            $this->op = 'Your Have Registered Successfully';
            $this->statusCode = 200;
        }
        else
        {
            $this->statusCode = 422;
            //$op['status'] = 'fail';
            $this->op = 'Something Went Wrong';
        }


        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function check_user_otp ()
    {
        $postdata = $this->request->all();

        $this->op = $this->userObj->check_user_otp($postdata);

        $this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function reset_pwd ()
    {
        $postdata = $this->request->all();
        $this->op = $this->userObj->reset_pwd($postdata);
        $this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function reset_activation_key ($arr = array())
    {
        $this->op = [];
        $postdata = $this->request->all();
        $postdata['reset_code'] = $postdata['code'];
        $postdata['forg_new_pass'] = rand(1111, 9999).time();
        $response = $this->userObj->reset_pwd($postdata);
        $this->op = $response;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function deny_activation_key ($arr = array())
    {
        $postdata = $this->request->all();
        $postdata['reset_code'] = $postdata['code'];
        $postdata['upd_reset_code'] = '';
        $response = $this->userObj->reset_pwd($postdata);
    }

    public function forgot_password ()
    {
        $this->statusCode = 422;
        $this->op = '';
        $postdata = $this->request->all();
        if (preg_match("/^-?[1-9][0-9]*$/D", $postdata['email_mob_verification']))
        {
            $arr['mobile'] = $postdata['email_mob_verification'];
        }
        else if (!empty(filter_var($postdata['email_mob_verification'], FILTER_VALIDATE_EMAIL)))
        {
            $arr['email'] = $postdata['email_mob_verification'];
        }
        else
        {
            $this->statusCode = 200; //$op['status'] = 'fail';
            $this->op = 'please enter a valid mobile format';
            return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
        }


        $userDetails = $this->myaccObj->user_name_check($arr);


        if ($userDetails == 'error')
        {
            $this->statusCode = 200;
            //  $op['status'] = 'error';
            $this->op = 'Enter a valid User Name';
        }
        else if ($userDetails)
        {
            $code = [];
            $code = rand(1111, 9999).time();
            $code = mb_substr($code, 0, 5);
            $wdata['otp_code'] = $code;
            $wdata['user_id'] = $userDetails->user_id;
            $this->code = $this->userObj->ins_reset_log($wdata);

            if (isset($arr['mobile']) && !empty($arr['mobile']))
            {
                //print_r($this->op);exit;
                /*
                  $this->sendsmsObj->otp_send_sms(array(
                  'otp_pwd' => $code,
                  'mobile' => $wdata['mobile'],
                  'sitename' => $arr['pagesettings']->site_name
                  )); */
                if (!empty($this->code))
                {
                    $this->op['code_id'] = $this->code;
                    $this->op['status'] = 'ok';
                    $this->op['msg'] = 'OTP has been send to your mobile';
                    $this->op['option'] = 'mobile';
                    $this->op['code'] = $code;
                    $this->op['status_val'] = Config('constants.ON');
                    $this->statusCode = 200;
                }

                return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
            }

            $this->statusCode = 200;
            $this->op = 'ok';
            $postdata['user_id'] = $userDetails->user_id;
            $postdata['uname'] = $userDetails->uname;
            $postdata['email'] = $userDetails->email;
            if (!empty($this->code))
            {
                $code = rand(1111, 9999).time();
                $update['reset_code'] = mb_substr($code, 0, 5).$res->id.$res->user_id;
                $res1 = DB::table(Config::get('tables.ACCOUNT_RESETCODE_LOG'))
                        ->where('id', $res->id)
                        ->update($update);
                $postdata['code'] = $update['reset_code'];
            }
            $postdata['resetlink'] = URL::to("/user/reset/activation/".$postdata['code']);
            $postdata['pwdreset_deny'] = URL::to("/ user/deny-activation/".$postdata['code']);

            $mstatus = 1;
            /* $mstatus = TWMailer::send(array(
              'to'=>$userDetails->email,
              'subject'=>'Password has been reseted successfully! Please check your e-mail',
              'view'=>'emails.user.reset_pwd',
              'data'=>$postdata
              )); */
            if ($mstatus)
            {
                $this->link = $postdata['act_link'];
                $this->statusCode = 200;
                $this->op['status'] = 'ok';
                $this->op['msg'] = 'Your account password has been send to your registered email id. Please check your Inbox';
            }
        }


        //print_r($mstatus);exit;



        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function get_personal_info ()
    {
        $postdata = $this->request->all();
		if (isset($postdata['account_id'])) {	
			$data = $this->myaccObj->account_details($postdata);				
			if (!empty($data))
			{            
				$this->op['status'] = 'ok';
				$this->op['data'] = $data;
				$this->op['msg'] = 'Data Found';
			}
			else
			{         
				$this->op['status'] = 'error';			
				$this->op['data'] = NULL;
				$this->op['msg'] = 'Data Not Found';
			}
		} else {
			$this->op['status'] = 'error';
			$this->op['msg'] = 'Parameter Missing...';
		}
		$this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function update_personal_info ()
    {
		$postdata = array();
		$op = array();
        $postdata = $this->request->all();                 
		if (!empty($postdata['account_id']))
		{			
			$data = $this->myaccObj->account_details($postdata);					
			if (!empty($data))	{
				$sdata['firstname'] = isset($postdata['firstname']) ? $postdata['firstname'] : $data->firstname;
				$sdata['lastname'] = isset($postdata['lastname']) ? $postdata['lastname'] : $data->lastname;
				$sdata['gender'] = isset($postdata['gender']) ? $postdata['gender'] : $data->gender_id;
				$account_id = $postdata['account_id'];
				$result = $this->myaccObj->update_personal_info($sdata, $account_id);			
				if (!empty($result))
				{
					$op['status'] = 'ok';
					$op['msg'] = 'Your changes have been saved successfully.';				
				} else {
					$op['status'] = 'error';
					$op['msg'] = 'No changes in the input';
				}
			} else {
				$op['status'] = 'error';
				$op['msg'] = 'Input Data Not Valid.';
			}
		} else {
			$op['status'] = 'error';
			$op['msg'] = 'Parameter Missing...';				
		}
		$this->statusCode = 200;
        return Response::json($op, $this->statusCode, $this->headers, $this->options);
    } 
	
	public function check_user_password ()
    {	
		$op['msg'] ='';
		$op['status'] ='';
        $postdata = $this->request->all();                 
		if (!empty($postdata['account_id']) && !empty($postdata['oldpassword']))
		{	
			$check_password = $this->myaccObj->old_password_check($postdata);
							
			if (!empty($check_password))	{
				$op['status'] = "ok";
				$op['msg'] = "Password Matched";				
			} else {
				$op['status'] = "error";
				$op['msg'] = "Old Password does't matched with the new password";				
			}
		} else {
			$op['status'] = 'error';
			$op['msg'] = 'Parameter Missing...';				
		}
		$this->statusCode = 200;
        return Response::json($op, $this->statusCode, $this->headers, $this->options);
    }

    public function save_password ()
    {
		$op['msg'] ='';
		$op['status'] ='';
        $postdata = $this->request->all();       
        $status = 422;
        
        if (!empty($postdata['account_id']) && !empty($postdata['new_password']) && !empty($postdata['confirm_password']) && !empty($postdata['oldpassword']))
        {      
			$data['confirm_password'] = $postdata['confirm_password'];
			$data['new_password'] = $postdata['new_password'];
			$data['oldpassword'] = $postdata['oldpassword'];
			if ($data['new_password'] == $data['confirm_password'])	{
				$result = $this->myaccObj->update_new_password($postdata);
				if ($result != '')
				{
					$op['status'] = "ok";
					$op['msg'] = "<div class='alert alert-warning'>New Password has been updated succesfully</div>";				
				}        
			} else {
				$op['status'] = "error";
				$op['msg'] = "<div class='alert alert-alert'>Cofirm Password not matched with the New Password.</div>";
			}
        }
        else
        {
            $op['status'] = "error";
            $op['msg'] = 'Parameter missing...';           
        }
		$status = 200;
        return Response::json($op, $status, $this->headers, $this->options);
    }
	
	public function save_address ()
    {
        $postdata = $this->request->all();        
        if (!empty($postdata) && !empty($postdata['account_id']))
        {
            $adata['street1'] = isset($postdata['street1']) ? $postdata['street1'] : '';
            $adata['street2'] = isset($postdata['street2']) ? $postdata['street2'] : '';
            $adata['city'] = isset($postdata['city']) ? $postdata['city'] : '';
            $adata['state_id'] = isset($postdata['state']) ? $postdata['state'] : '';
            $adata['country_id'] = isset($postdata['country']) ? $postdata['country'] : '';
            $adata['postal_code'] = isset($postdata['pin_code']) ? $postdata['pin_code'] : '';
            $adata['account_id'] = isset($postdata['account_id']) ? $postdata['account_id'] : '';
            $adata['create_on'] = date('Y-m-d H:i:s');
            $result = $this->myaccObj->save_address($adata);			
            if (!empty($result))
            {
                $op['status'] = 'ok';
                $op['msg'] = "New Address Saved Successfully";
            }
            else
            {                
                $op['status'] = 'err';
                $op['msg'] = "<div class='alert alert-danger'>you cant change this address</div>";
            }
        }
        else
        {           
            $op['status'] = 'error';
            $op['msg'] = 'Parameter Missing...';            
        }
		$this->statusCode = 200;
        return Response::json($op, $this->statusCode, $this->headers, $this->options);
    }   

    public function get_address_list ()
    {
        $data = $this->request->all();		
		if (!empty($data['account_id'])) {
			$result = $this->myaccObj->get_address_list($data);
			if (!empty($result)) {
				$this->op['status'] = 'ok';
				$this->op['data'] = $result;
				$this->op['msg'] = 'Data Found.';
			} else {
				$this->op['status'] = 'error';
				$this->op['data'] = NULL;
				$this->op['msg'] = 'Data Not Found.';
			}
		} else {
			$this->op['status'] = 'error';
			$this->op['msg'] = 'Parameter Missing...';
		}
       
		$this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function default_address ()
    {
        $postdata = $this->request->all();
        $wdata = array();        
		if (!empty($postdata['default_addr']) && !empty($postdata['account_id'])) {
			$wdata['address_id'] = $postdata['default_addr'];
			$data['account_id'] = $postdata['account_id'];
			$type = $this->myaccObj->address_type($data);
			$res = $this->myaccObj->default_address($wdata);
			if (!empty($res))
			{				
				$this->op['status'] = 'ok';
				$this->op['msg'] = 'Default Address Successfully Changed';
			} else {
				$this->op['status'] = 'error';
				$this->op['msg'] = 'Invalid Parameter...';
			}
		} else {
			$this->op['status'] = 'error';
			$this->op['msg'] = 'Parameter Missing....';
		}
		$this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function delete_address ()
    {
        $wdata = array();
        $op = 'ERR';
        $this->statusCode = 422;
        $postdata = $this->request->all();
		if (!empty($postdata['address_id'])) {
			$wdata['address_id'] = $postdata['address_id'];
			$res = $this->myaccObj->delete_address($wdata);
			if (!empty($res))
			{				
				$this->op['status'] = 'ok';
				$this->op['msg'] = 'Address Deleted Successfully';
			} else {
				$this->op['status'] = 'error';
				$this->op['msg'] = 'Invalid Parameter...';
			}
		} else {
			$this->op['status'] = 'error';
			$this->op['msg'] = 'Parameter Missing....';
		}
		$this->statusCode = 200;
        return Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }

    public function verify_email ()
    {
        $op = array();
        $wdata = array();
        $this->statusCode = 422;
		$verification_code = rand(1111, 9999);
        $postdata = $this->request->all();
		if (!empty($postdata['account_id']) && !empty($postdata['email_id'])) {
			$mdata['mail'] = $postdata['email_id'];
			$sdata['account_id'] = $postdata['account_id'];						
			$sdata['reset_code'] = $verification_code;
			$sdata['reset_code_timeout'] = date('H:i:s', strtotime('+30 minutes'));
			$add_code = $this->myaccObj->add_resetCode($sdata);
			if (!empty($add_code))
			{
                Session::set('email', $postdata['email_id']);
				$op['status'] = 'ok';
				$op['msg'] = 'Verification code has been send to Given Email address. Please check your inbox.';
				$op['code'] = $verification_code;	
                $op['notify'] = false;
            }
		} else {
			$op['status'] = 'error';
			$op['msg'] = 'Parameter Missing...';
		}
		$this->statusCode = 200;
        return Response::json($op, $this->statusCode, $this->headers, $this->options);
    }
	
	public function verify_mobile ()
    {
        $postdata = $this->request->all();
        $verification_code = rand(1111, 9999);
        $data['account_id'] = $sdata['account_id'] = $postdata['account_id'];		
        $sdata['reset_code'] = $verification_code;
        $sdata['reset_code_timeout'] = date('H:i:s', strtotime('+30 minutes'));
		if (!empty($postdata['account_id']) && !empty($postdata['mobile'])) {
			
			$data['mobile'] = $postdata['mobile'];
			$mobile_no_check = $this->myaccObj->check_mobile_no($data);			
			if ($mobile_no_check == false) {		
				$add_code = $this->myaccObj->add_resetCode($sdata);
				if (!empty($add_code))
				{
                    Session::set('mobile', $postdata['mobile']);
					$op['status'] = 'ok';
					$op['msg'] = 'Verification code has been send to Given Mobile Number. Please check your message box.';
                    $op['code'] = $verification_code;      
                    $op['notify'] = false;
				}
				else
				{
					$op['status'] = 'error';
					$op['msg'] = 'Invalid Parameters...';
				}
			} else {
				$op['status'] = 'error';
				$op['msg'] = 'Mobile Number Already Exists... Or Invalid Parameters...';
			}
		} else {
			$op['status'] = 'error';
			$op['msg'] = 'Parameter Missing....';
		}
		$this->status = 200;
        return Response::json($op, $this->status, $this->headers, $this->options);
    }

    public function checkverificationcode ()
    {
        $op = array();
        $wdata = array();
        $postdata = $this->request->all();
        
        $v_code = $postdata['v_code'];
        $check_code_expired = $this->myaccObj->check_reset_code($postdata);
        if (!empty($check_code_expired))
        {        
            $op['status'] = 'ok';            
            $op['msg'] = 'Successfully Verified';
            $op['notify'] = false;
        }
        else
        {
            $op['status'] = 'error';            
			$op['msg'] = 'Verification Code Expired or Invalid OTP';
            $op['notify'] = false;
        }
		$this->status = 200;
        return Response::json($op, $this->status, $this->headers, $this->options);
    }    

    public function update_email ()
    {
        $postdata = $this->request->all();
        $op['status'] = "error";
        $op['msg'] = "Something Went Wrong";
        $this->status = 422;
        $adata = array();
        $data = array();
        if (Session::get('email') != null) {
            $adata['email'] = Session::get('email');
        } else if (isset($postdata['email_show']) && !empty($postdata['email_show'])) {
            $adata['email'] = $postdata['email_show'];
        }
        if (Session::get('mobile') != null) {
            $adata['mobile'] = Session::get('mobile');
        } else if (isset($postdata['mob_show']) && !empty($postdata['mob_show'])) {
            $adata['mobile'] = $postdata['mob_show'];
        }/*
        if (isset($postdata['email_addr']) && !empty($postdata['email_addr']))
        {
            $adata['email'] = $postdata['email_addr'];
        }
        if (isset($postdata['email_show']) && !empty($postdata['email_show']))
        {
            $adata['email'] = $postdata['email_show'];
        }
        if (isset($postdata['mobile_no']) && !empty($postdata['mobile_no']))
        {
            $adata['mobile'] = $postdata['mobile_no'];
        }
        if (isset($postdata['mob_show']) && !empty($postdata['mob_show']))
        {
            $adata['mobile'] = $postdata['mob_show'];
        }*/
		/*
$data['post'] = $postdata;
$adata['email'] = "angel.ejugiter@gmail.com";*/
		if (!empty($postdata['account_id'])) {
			$data['account_id'] = $postdata['account_id'];
			$result = $this->myaccObj->update_email($adata, $data);		
			if ($result)
			{/*
$op['adata'] = $adata;
$op['data'] = $data;*/
				$op['status'] = "OK";
                $op['url'] = url('/logout');
                $op['msg'] = "Updated Successfully";
				/*if (!empty($adata['email'])) {
					$op['msg'] = "Email Updated Successfully";            
				} else {
					$op['msg'] = "Mobile Number Updated Successfully";
				}*/
			}
			else
			{/*
$op['adata'] = $adata;
$op['data'] = $data;*/
				$op['status'] = "error";            
				$op['msg'] = "Invalid Parameters...";
			}
		} else {
			$op['status'] = "error";            
			$op['msg'] = "Parameters Missing...";
		}
		$this->status = 200;
        return Response::json($op, $this->status, $this->headers, $this->options);
    }

    public function account_deactivate ()
    {
        $data = array();
        $data['account_deactivate'] = true;
        $data['account_id'] = $this->account_id;
        if ($this->response['account_details'] = $this->myAccountObj->account_details($data))
        {
            $this->statusCode = 200;
        }
        else
        {
            $this->response['url'] = URL::to('account/personalinformation');
            $this->statusCode = 308;
        }
        return Response::json($this->response, $this->statusCode, $this->headers, $this->options);
    }

    public function save_deactivate ()
    {
        $postdata = $this->request->all();		
        $op = array();
        $wdata = array();
        $op['status'] = 'ERR';
		if (!empty($postdata['account_id']) && !empty($postdata['password'])) {
			$valid = $this->myaccObj->save_deactivate($postdata);        
			if ($valid == 1)
			{
				$op['msg'] = 'Incorrect Password';
                $op['notify'] = false;
			}
			if ($valid == 2)
			{
				$op['status'] = 'ok';
				$op['msg'] = 'Account Has been Deactivated';
				//$op['url'] = URL::to('account/personalinformation');
			}
			if ($valid == 3)
			{
				$op['msg'] = 'Invalid Parameters...';           
			}
		} else {
			$op['msg'] = 'Parameter Missing...';
		}
		$this->statusCode = 200;
        return Response::json($op, $this->statusCode, $this->headers, $this->options);
    }

    private function validations ($frm, $section)
    {

        $validation_msg = ['ADD_PAYMENT'=>[
                'rules'=>[
                    'payment_list_id'=>'required',
                    'payment_gateway_select'=>'required'
                ],
                'msg'=>trans('add_payment.validation')
            ],
            'UPDATE_PERSONAL_INFO'=>[
                'rules'=>[
                    'firstname'=>'required',
                    'lastname'=>'required',
                    'gender'=>'required',
                ],
                'msg'=>trans('update_personal_info.validation')
            ],
            'CHANGE_PASSWORD'=>[
                'rules'=>[
                    'oldpassword'=>'required',
                    'new_password'=>'required',
                    'confirm_password'=>'required',
                ],
                'msg'=>trans('change_password.validation')
            ],
            'UPDATE_ADDRESS'=>[
                'rules'=>[
                    'street1'=>'required',
                    'city'=>'required',
                    'state'=>'required',
                    'country'=>'required',
                    'pin_code'=>'required',
                ],
                'msg'=>trans('update_address.validation')
        ]];

        return $validation_msg[$frm][$section];
    }

}
