<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Api\ApiCommon;
use App\Models\Common;
use Response;
use URL;
use GuzzleHttp\Client;
use Exception;

class HTTPApiController extends Controller
{

    //private $api_host = 'http://localhost/5dg-shopping-portal/';
    //private $api_version = 'api/v1/';
    //private $api_host = config('ecom_vpi.url');
    //private $api_version = getenv('API_VERSION');
    private $apiUrl = '';
    private $accessBaseURL = '';
    private $param = 0;
    private $url = '';

    public function __construct ($param = '')
    {
		$data = array();		
		$this->client = new Client();	
		$this->commonSettingsObj = new Common();       
		$this->site_settings = $this->commonSettingsObj->get_site_settings();
		//$this->ecom_url = $this->sitesettings->base_site;
		$this->vpiUrl 	= config('services.vpi.url');  /* one used to call the Dropshipping APIs */
		$this->apiUrl 	= config('services.api.url');      /* one used to call the own APIs */	
		$this->localApi = config('services.api.url');      /* one used to call the own APIs */		
		$this->appcode  = 'partner001';
		$this->appkey  = '1fc144d7aa3a9a43964428a979de6068';
		$this->param    = $param;
		$url = '';
		
	}
	
	public function postRequest ($url = '', $data, $method)
    { //print_r($data); exit();
    	//$data=[];
        $result = false;
        if ($this->param == 'api')
        {
            $accessBaseURL = $this->apiUrl;
        }
        else if ($this->param == 'vpi')
        {
            $accessBaseURL = $this->vpiUrl;
        }
        $data['appcode'] = $this->appcode;
		$data['token']  = $this->appkey;
		//echo $accessBaseURL.$url; exit();
		//echo '<pre>'; print_r($data); exit();
        $res = $this->client->$method(URL::to($accessBaseURL.$url), array('form_params'=>$data));



        if ($res->getStatusCode() == 200)
        {
            $result = json_decode($res->getBody());
        }
        //echo '<pre>'; print_r($result); exit();
        return $result;
    }
	
/* 	public function postRequest ($url='', $data, $method)
	{	
		$result = null;		
	    if($this->param == 'api') { $accessBaseURL = $this->apiUrl; } else if ($this->param == 'vpi') { $accessBaseURL  = $this->vpiUrl; }
		$data['appcode']  = $this->appcode;		
		$data['token']  = '28dd2c7955ce926456240b2ff0100bde';
		$res = $this->client->$method(URL::to($accessBaseURL.$url), array('form_params' => $data));
		if ($res->getStatusCode() == 200)
		{
			$result =  json_decode($res->getBody());	           		
		}
		return $result;
	}   */

   

}
