<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use App\Models\Api\APISupport;
use App\Http\Controllers\UserBaseController;
use App\Http\Controllers\Api\HTTPApiController;
use URL;
use Response;
use View;
use Config;
use Redirect;
class SupportController extends UserBaseController
{
    public function __construct ()
    {
        parent::__construct();
        $this->supportObj = new APISupport();
    }
    public function faq_s_data ()
    {
        $op['status'] 		= 'fail';
        $postdata 		    = $this->request->all();
        $op['search_term']  = $postdata;
        $res = $this->supportObj->search_faqs_datas($postdata);
        if ($res)
        {
            $op['status'] = 'ok';

            $op['contents'] = $res;
        }
        return View::make('frontend.faq.faq_category_list', $op);
    }
	
    public function faq_categories_list ($category_slug = '')
    {
	    $postdata  = $this->request->all();
        $data = array();
        $wdata = '';
	    if (!empty($category_slug))
        {
            if ($category_slug !== 'search-query')
            {
					$data['search_term'] = $postdata['search_term'];
					$res1  = $this->HTTPApiObj->postRequest('helper_faq',$postdata, 'POST');
                    $data['msg'] = $res1->msg;
                    $data['qaCatwise'] = $res1->qaCatwise;
            }
            else
            {
                $postdata 			 = $this->request->all();
                $data['search_term'] = $postdata['search_term'];
             	$res 			= $this->HTTPApiObj->postRequest('helper_faq/'.$data['search_term'], $postdata, 'POST');
				$ques = array();
			    if (!empty($res))
                {
                    foreach($res as $val){
						$ques = $val->qalist;
					}
					$data['content'] = $ques;
					$data['search_query_based'] =1;
				}
                else
                {
                    $data['msg'] = $postdata['search_term'].'Not Found';
                }
            }
        }
        if (empty($category_slug))
        {
         	$res 			    = $this->HTTPApiObj->postRequest('helper_faq', $postdata, 'POST');
	        $data['qaCatwise']  = $res;
        }
		$data['feature_faqs']	= $this->HTTPApiObj->postRequest('getfeatures_faqs', $postdata, 'POST');
		//echo '<pre>';print_R($data['feature_faqs']);exit;
        return View::make('faq.faq_category_list', $data);
    }
	
}
