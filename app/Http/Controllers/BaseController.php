<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Common;
use URL;
use Response;
use Session;
use View;
use Config;
use DB;

class BaseController extends Controller
{

    public $data = array();
    public $apiUrl = '';
    public $response = '';
    public $request = '';
    public $redirect = '';
    public $session = '';
    public $test = 1;
    public $headers = [];
    public $statusCode = 422;
    public $op = ['msg'=>''];
    public $options = JSON_PRETTY_PRINT;

    public function __construct ()
    {
        $this->HTTPApiObj = new Api\HTTPApiController('api');
        $this->HTTPVpiObj = new Api\HTTPApiController('vpi');
        $this->test = 5454;
        $this->response = response();
        $this->request = request();
        $this->redirect = redirect();
        $this->session = session();
        $this->ip = $this->request->getClientIp(true);
        $this->data = array();
        $this->commonSettingsObj = new Common();
        $this->sitesettings = $this->commonSettingsObj->get_site_settings();
        $this->sitesettings->lang = 'en';
        $this->currency_id = $this->sitesettings->site_currency_id;
        View::share('sitesettings', (object) $this->sitesettings);
		$this->country_id = 77;
        $this->state_id = 1203;
        $this->region_id = 1;
        $this->city_id = 1001;
        $this->geo_zone_id = 1;
    }

}
