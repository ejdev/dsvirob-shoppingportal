<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminBaseController;
use App\Models\Admin\Admin;
use URL;
use View;
use Config;
use Redirect;
use ShoppingPortal;
use App;

class AdminCategoryController extends AdminBaseController
{
    public $myaccObj = '';
    public $adminObj = '';

    public function __construct ()
    {
        parent::__construct();
       // $this->myaccObj = new MyAccount();
        $this->adminObj = new Admin();
    }


     public function categoryList ($status = '')
     {
        //echo 'eee'; exit();
        $data = $ajaxdata = [];
        $data['title'] = 'category';
        $postdata = $this->request->all();
        $this->statusCode = 200;
        $data['status'] = !empty($status) ? $status : (isset($postdata['status']) && !empty($postdata['status']) ? $postdata['status'] : '');
        $status_array = ['placed'=>'Placed Orders', 'confirmed'=>'Confirmed Orders', 'packed'=>'Packed Orders', 'dispatched'=>'Dispatched Orders', 'couriered'=>'Couriered', 'reached-hub'=>'Reached Hub', 'delivered'=>'Delivered', 'cancelled'=>'Cancelled', 'returned'=>'Return', 'refunded'=>'Refunded', 'replaced'=>'Replaced'];
        //return View::make('category.category_list', $data);
        //echo '000';
        if ($this->request->ajax())
        {
            //print_r($postdata);
            //echo 'EEE'; exit();
            $ajaxdata['recordsTotal'] = $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['draw'] = $postdata['draw'];
            $ajaxdata['data'] = array();
            //if (array_key_exists($postdata['status'], $status_array))
            //{  //echo 'WWW'; exit();
                $data['start'] = $postdata['start'];
                $data['length'] = $postdata['length'];
                $data['search_term'] = (isset($postdata['search_term']) && !empty($postdata['search_term'])) ? $postdata['search_term'] : null; 
                if (isset($postdata['order']))
                {
                    $data['orderby'] = $postdata['columns'][$postdata['order'][0]['column']]['name'];
                    $data['order'] = $postdata['order'][0]['dir'];
                }
                $orders = $this->HTTPVpiObj->postRequest('get-category', $data, 'POST'); 
                //echo '<pre>';
                //print_r($orders->category);               
                if ($orders)
                {

                    //$ajaxdata['recordsTotal'] = $orders->recordsTotal;
                    //$ajaxdata['recordsFiltered'] = $orders->recordsFiltered;

                    $ajaxdata['recordsTotal'] = count($orders);
                    $ajaxdata['recordsFiltered'] = count($orders);


/*                    array_walk($orders->data, function(&$row)
                    {                       
                        //$row->usercode = $this->adminObj->get_user_code($row->account_id);
                        array_walk($row->actions, function(&$action, $key) use($row)
                        {
                            switch ($key)
                            {
                                case 'PARTNER_HOLDING':
                                case 'PARTNER_CONFIRMED':
                                case 'PARTNER_CANCELLED':
                                case 'ADMIN_HOLDING':
                                case 'ADMIN_CONFIRMED':
                                case 'ADMIN_CANCELLED':
                                case 'SUPPLIER_HOLDING':
                                case 'SUPPLIER_CONFIRMED':
                                case 'SUPPLIER_CANCELLED':
                                    $action->url = URL::route('admin.order.update-approval-status');
                                    break;
                                case 'DETAILS':
                                    $action->target = '_blank';
                                    $action->url = URL::route('admin.order.details', ['order_code'=>$row->sub_order_code]);
                                    break;
                            }
                        });
                    });*/
                    $ajaxdata['data'] = $orders->category;
                }
            //}
            return $this->response->json($ajaxdata, $this->statusCode);
        }
        elseif (array_key_exists($status, $status_array))
        {  //echo '2';
            $data['title'] = $status_array[$status];
            return View::make('category.category_list', $data);
        }
        else
        {
            //return App::abort(404);
            return View::make('category.category_list', $data);
        }
    
     }
     
	
	
	
	
}
