<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Models\Admin\MyAccount;
use App\Models\Admin\Admin;
use App\Models\Sendsms;
use URL;
use View;
use Config;
use Redirect;
use ShoppingPortal;

class AdminOrderController extends AdminBaseController
{

    public $myaccObj = '';
    public $adminObj = '';

    public function __construct ()
    {
        parent::__construct();
        $this->myaccObj = new MyAccount();
        $this->adminObj = new Admin();
    }



    public function orderList ($status = '')
    {
        $data = $ajaxdata = [];
        $postdata = $this->request->all();
        $this->statusCode = 200;
        $data['status'] = !empty($status) ? $status : (isset($postdata['status']) && !empty($postdata['status']) ? $postdata['status'] : '');
        $status_array = ['placed'=>'Placed Orders', 'confirmed'=>'Confirmed Orders', 'packed'=>'Packed Orders', 'dispatched'=>'Dispatched Orders', 'couriered'=>'Couriered', 'reached-hub'=>'Reached Hub', 'delivered'=>'Delivered', 'cancelled'=>'Cancelled', 'returned'=>'Return', 'refunded'=>'Refunded', 'replaced'=>'Replaced'];
		
        if ($this->request->ajax())
        {
            $ajaxdata['recordsTotal'] = $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['draw'] = $postdata['draw'];
            $ajaxdata['data'] = array();
            if (array_key_exists($postdata['status'], $status_array))
            {
                $data['start'] = $postdata['start'];
                $data['length'] = $postdata['length'];
				$data['search_term'] = (isset($postdata['search_term']) && !empty($postdata['search_term'])) ? $postdata['search_term'] : null;	
                if (isset($postdata['order']))
                {
                    $data['orderby'] = $postdata['columns'][$postdata['order'][0]['column']]['name'];
                    $data['order'] = $postdata['order'][0]['dir'];
                }
                $orders = $this->HTTPVpiObj->postRequest('admin/orders', $data, 'POST');				
                if ($orders)
                {

                    $ajaxdata['recordsTotal'] = $orders->recordsTotal;
                    $ajaxdata['recordsFiltered'] = $orders->recordsFiltered;
                    array_walk($orders->data, function(&$row)
                    {						
						//$row->usercode = $this->adminObj->get_user_code($row->account_id);
                        array_walk($row->actions, function(&$action, $key) use($row)
                        {
                            switch ($key)
                            {
                                case 'PARTNER_HOLDING':
                                case 'PARTNER_CONFIRMED':
                                case 'PARTNER_CANCELLED':
                                case 'ADMIN_HOLDING':
                                case 'ADMIN_CONFIRMED':
                                case 'ADMIN_CANCELLED':
                                case 'SUPPLIER_HOLDING':
                                case 'SUPPLIER_CONFIRMED':
                                case 'SUPPLIER_CANCELLED':
                                    $action->url = URL::route('admin.order.update-approval-status');
                                    break;
                                case 'DETAILS':
                                    $action->target = '_blank';
                                    $action->url = URL::route('admin.order.details', ['order_code'=>$row->sub_order_code]);
                                    break;
                            }
                        });
                    });
                    $ajaxdata['data'] = $orders->data;
                }
            }
            return $this->response->json($ajaxdata, $this->statusCode);
        }
        elseif (array_key_exists($status, $status_array))
        {
            $data['title'] = $status_array[$status];
            return View::make('order.order_list', $data);
        }
        else
        {
            return App::abort(404);
        }
    }

    public function updateOrderApprovalStatus ()
    {
        $postdata = $this->request->all();		
        if ($this->request->ajax())
        {
            $orders = $this->HTTPVpiObj->postRequest('admin/orders/update-approval-status', $postdata, 'POST');
            if ($orders)
            {
				$account_type_id = 1;
				$this->account_id = 1;
				return ShoppingPortal::notify('ORDER_CANCALATION.PARTNER_ADMIN', $this->account_id, $account_type_id, $postdata, true, true, true);
                $this->statusCode = 200;
                $data['msg'] = 'Updated Successfully';
            }
        }
        return $this->response->json($data, $this->statusCode);
    }

    public function updateOrderStatus ()
    {
        $postdata = $this->request->all();
        if ($this->request->ajax())
        {
            $orders = $this->HTTPVpiObj->postRequest('admin/orders/update-status', $postdata, 'POST');
            if ($orders)
            {
                $this->statusCode = 200;
                $data['msg'] = 'Updated Successfully';
            }
        }
        return $this->response->json($data, $this->statusCode);
    }

    public function subOrderList ($status = '')
    {
        $data = $ajaxdata = [];
        $postdata = $this->request->all();
        $this->statusCode = 200;
        $data['status'] = !empty($status) ? $status : (isset($postdata['status']) && !empty($postdata['status']) ? $postdata['status'] : '');
        $status_array = ['placed'=>'Placed Orders', 'confirmed'=>'Confirmed Orders', 'packed'=>'Packed Orders', 'dispatched'=>'Dispatched Orders', 'couriered'=>'Couriered', 'reached-hub'=>'Reached Hub', 'delivered'=>'Delivered', 'cancelled'=>'Cancelled', 'returned'=>'Return', 'refunded'=>'Refunded', 'replaced'=>'Replaced'];
        if ($this->request->ajax())
        {
            $ajaxdata['recordsTotal'] = $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['draw'] = $postdata['draw'];
            if (isset($postdata['order_code']) && !empty($postdata['order_code']))
            {
                $data['order_code'] = $postdata['order_code'];
            }
            $ajaxdata['data'] = array();
            if (isset($postdata['start']) && isset($postdata['length']) && $postdata['length'] > 0)
            {
                $data['start'] = $postdata['start'];
                $data['length'] = $postdata['length'];
            }
            if (isset($postdata['order']))
            {
                $data['orderby'] = $postdata['columns'][$postdata['order'][0]['column']]['name'];
                $data['order'] = $postdata['order'][0]['dir'];
            }
            $orders = $this->HTTPVpiObj->postRequest('admin/sub-orders', $data, 'POST');
            if ($orders)
            {
                $ajaxdata['recordsTotal'] = $orders->recordsTotal;
                $ajaxdata['recordsFiltered'] = $orders->recordsFiltered;
                array_walk($orders->data, function(&$row)
                {
                    array_walk($row->actions, function(&$action, $key) use($row)
                    {
                        switch ($key)
                        {
                            case 'PARTNER_HOLDING':
                            case 'PARTNER_CONFIRMED':
                            case 'PARTNER_CANCELLED':
                            case 'ADMIN_HOLDING':
                            case 'ADMIN_CONFIRMED':
                            case 'ADMIN_CANCELLED':
                            case 'SUPPLIER_HOLDING':
                            case 'SUPPLIER_CONFIRMED':
                            case 'SUPPLIER_CANCELLED':
                                $action->url = URL::route('admin.sub-order.update-approval-status');
                                break;
                            case 'DETAILS':
                                $action->target = '_blank';
                                $action->url = URL::route('admin.sub-order.details', ['order_code'=>$row->sub_order_code]);
                                break;
                        }
                    });
                });
                $ajaxdata['data'] = $orders->data;
            }
            return $this->response->json($ajaxdata, $this->statusCode);
        }
        elseif (array_key_exists($status, $status_array))
        {
            $data['title'] = $status_array[$status];
            return View::make('order.sub_order_list', $data);
        }
    }

    public function orderItemsList ()
    {
        $data = $ajaxdata = [];
        $postdata = $this->request->all();
        $this->statusCode = 200;
        $data['status'] = !empty($status) ? $status : (isset($postdata['status']) && !empty($postdata['status']) ? $postdata['status'] : '');
        $status_array = ['placed'=>'Placed Orders', 'confirmed'=>'Confirmed Orders', 'packed'=>'Packed Orders', 'dispatched'=>'Dispatched Orders', 'couriered'=>'Couriered', 'reached-hub'=>'Reached Hub', 'delivered'=>'Delivered', 'cancelled'=>'Cancelled', 'returned'=>'Return', 'refunded'=>'Refunded', 'replaced'=>'Replaced'];
        if ($this->request->ajax())
        {
            $ajaxdata['recordsTotal'] = $ajaxdata['recordsFiltered'] = 0;
            if (isset($postdata['draw']))
            {
                $ajaxdata['draw'] = $postdata['draw'];
            }
            $ajaxdata['data'] = array();
            if (isset($postdata['start']) && isset($postdata['length']) && $postdata['length'] > 0)
            {
                $data['start'] = $postdata['start'];
                $data['length'] = $postdata['length'];
            }
            if (isset($postdata['order']))
            {
                $data['orderby'] = $postdata['columns'][$postdata['order'][0]['column']]['name'];
                $data['order'] = $postdata['order'][0]['dir'];
            }
            if (isset($postdata['order_code']) && !empty($postdata['order_code']))
            {
                $data['order_code'] = $postdata['order_code'];
            }
            if (isset($postdata['sub_order_code']) && !empty($postdata['sub_order_code']))
            {
                $data['sub_order_code'] = $postdata['sub_order_code'];
            }
            $orders = $this->HTTPVpiObj->postRequest('admin/order-items', $data, 'POST');
            if ($orders)
            {
                $ajaxdata['recordsTotal'] = $orders->recordsTotal;
                $ajaxdata['recordsFiltered'] = $orders->recordsFiltered;
                array_walk($orders->data, function(&$row)
                {
                    array_walk($row->actions, function(&$action, $key) use($row)
                    {
                        switch ($key)
                        {
                            case 'PARTNER_HOLDING':
                            case 'PARTNER_CONFIRMED':
                            case 'PARTNER_CANCELLED':
                            case 'ADMIN_HOLDING':
                            case 'ADMIN_CONFIRMED':
                            case 'ADMIN_CANCELLED':
                            case 'SUPPLIER_HOLDING':
                            case 'SUPPLIER_CONFIRMED':
                            case 'SUPPLIER_CANCELLED':
                                $action->url = URL::route('admin.sub-order.update-approval-status');
                                break;
                            case 'DETAILS':
                                $action->target = '_blank';
                                $action->url = URL::route('admin.sub-order.details', ['order_code'=>$row->sub_order_code]);
                                break;
                        }
                    });
                });
                $ajaxdata['data'] = $orders->data;
            }
            return $this->response->json($ajaxdata, $this->statusCode);
        }
        else
        {
            return View::make('order.order_items_list');
        }
    }

    public function updateSubOrderApprovalStatus ()
    {
        $data = [];
        $postdata = $this->request->all();
        if ($this->request->ajax())
        {
            $orders = $this->HTTPVpiObj->postRequest('admin/sub-orders/update-approval-status', $postdata, 'POST');
            if ($orders)
            {
                $this->statusCode = 200;
                $data['msg'] = 'Updated Successfully';
            }
        }
        return $this->response->json($data, $this->statusCode);
    }

    public function updateSubOrderStatus ()
    {
        $data = [];
        $postdata = $this->request->all();
        if ($this->request->ajax())
        {
            $orders = $this->HTTPVpiObj->postRequest('admin/sub-orders/update-status', $postdata, 'POST');
            if ($orders)
            {
                $this->statusCode = 200;
                $data['msg'] = 'Updated Successfully';
            }
        }
        return $this->response->json($data, $this->statusCode);
    }
	
	public function edit_emp_profile()
    {
        $data                = array();
        $postdata            = Input::all();
        $postdata['admin_id'] = $postdata['admin_id'];
        $data['admin_id']     = $postdata['admin_id'];
        $admin_id             = $data['admin_id'];
        if (isset($postdata['submit']) && !empty($postdata['submit']))
        {
            $response = $this->adminObj->edit_emp_profile($admin_id, $postdata);
            Session::flash($response['status'], $response['msg']);
            if ($response['status'] == 'success')
            {
           }
            return json_encode($response);
        }
        else
        {
            $data['countrylist']  = $this->admincommonObj->get_country_list();
            $data['states']       = $this->admincommonObj->get_states_list();
            $data['citys']        = $this->admincommonObj->get_city_list();
            $data['user_details'] = $this->adminObj->get_allemp_details($admin_id);
	        return View::make('admin.employee.employee_edit_profile', $data)->render();
        }
    }
	
	public function orderDetails ($order_code)
    {
        $data = [];
        $data['order_code'] = $order_code;		
        $result = $this->HTTPVpiObj->postRequest('admin/orders/details', $data, 'POST');
		//print_r($result->details);exit;
        if (isset($result->details) && !empty($result->details))
        {
            $data['details'] = $result->details;           
            $res =  View::make('order.order_details', $data)->render();
			return json_encode($res);
			
        }
        //return App::abort(404);
    }   

    public function subOrderDetails ($sub_order_code)
    {
        $data = [];
        $data['sub_order_code'] = $sub_order_code;
        $details = $this->HTTPVpiObj->postRequest('admin/sub-orders/details', $data, 'POST');
        if (isset($details->details) && !empty($details->details))
        {
            $data['details'] = $details->details;
            return View::make('order.sub_order_details', $data);
        }
        return App::abort(404);
    }

}
