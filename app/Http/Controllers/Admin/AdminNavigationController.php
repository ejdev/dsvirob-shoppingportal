<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminBaseController;
use URL;
use View;
use Config;
use Redirect;
use Response;
use App\Models\Admin\AdminNavigation;

class AdminNavigationController extends AdminBaseController
{
    public function __construct ()
    {
        parent::__construct();
        $this->NavigationObj = new AdminNavigation();
    }
	
	public function create_navigation ()
    {
        $data['menu'] = $this->NavigationObj->get_menu();
        //$data['parent_categories'] = $this->NavigationObj->get_category_childs();
		$category = array();
		$data['parent_category_id'] = 'empty';
        $res = $this->HTTPVpiObj->postRequest('get-category', $data, 'POST');		
        if (!empty($res->category))
        {
            $category = $res->category;
            foreach ($category as $cat)
            {
                $cat->url = $this->commonSettingsObj->generateBrowseURL(array('category'=>$cat));
            }            			
        }
		$data['parent_categories'] = $category;
		$data['menu_position_list'] = $this->NavigationObj->menu_position_list();
		//echo '<pre>';print_r($data);exit;
     /*    if (Request::ajax())
        {
            $postdata = Input::all();
            $data['child_categories'] = $this->admincat->getfull_child_cattree($postdata['category_id']);
            return Response::json($data);
        } */
        //return View::make('admin.navigation.create_navigations', $data);
		$data['title'] = 'Navigations';
		return View::make('navigations.create_navigations', $data);
    }
	
	public function save_navigation ()
    {
        $postdata = $this->request->all();		
        $op['msg'] = 'error';
        $op['status'] = 'fail';
        if (!empty($postdata))
        {
            if (isset($postdata['menu_id']))
            {
                $data['menu_id'] = $postdata['menu_id'];				
                $op['menu'] = $this->NavigationObj->save_navigation($postdata);
                if ($op['menu'])
                {
                    $op['msg'] = 'Created Successfully';
                    $op['status'] = 'ok';
                }
            }
            if (isset($postdata['menu_name']))
            {
                $data['menu_exists'] = $this->NavigationObj->check_menu($postdata['menu_name']);

                if (empty($postdata['menu_name']) && empty($postdata['menu_postion_id']))
                {
                    $op['msg'] = 'Mandatory fields should not be empty';
                    $op['status'] = 'fail';
                    return Response::json($op);
                }
                if (empty($postdata['menu_name']))
                {
                    $op['msg'] = 'Please Enter Menu Name';
                    $op['status'] = 'fail';
                    return Response::json($op);
                }
                if (empty($postdata['menu_postion_id']))
                {
                    $op['msg'] = 'Please Select Menu Position';
                    $op['status'] = 'fail';
                    return Response::json($op);
                }
                if (empty($data['menu_exists']->menu_id))
                {
                    $op['menu'] = $this->NavigationObj->save_menu($postdata);
                    if ($op['menu'])
                    {
                        $op['msg'] = 'Created Successfully';
                        $op['status'] = 'ok';
                        $op['menu_id'] = $op['menu'];
                    }
                }
                else
                {
                    $op['msg'] = 'Menu Aldready Exists';
                    $op['status'] = 'fail';
                }
            }
        }
        //menu structure
        return Response::json($op);
    }
	
	public function navigations_list ()
    {
        $data = $filter = array();
        $postdata = $this->request->all();
        if (!empty($postdata))
        {
            $filter['search_term'] = (isset($postdata['search_term']) && !empty($postdata['search_term'])) ? $postdata['search_term'] : '';
            $filter['top_menu_filter'] = (isset($postdata['top_menu_filter']) && !empty($postdata['top_menu_filter'])) ? $postdata['top_menu_filter'] : '';
            $filter['menu_id'] = (isset($postdata['menu_id']) && !empty($postdata['menu_id'])) ? $postdata['menu_id'] : '';
        }
         if ($this->request->ajax())
        {
            $data['count'] = true;
            $ajaxdata['recordsTotal'] = $ajaxdata['recordsFiltered'] = $this->NavigationObj->get_menu_list($data);
            $ajaxdata['data'] = array();
            $ajaxdata['draw'] = $postdata['draw'];
            if ($ajaxdata['recordsTotal'] > 0)
            {
                if (!empty(array_filter($filter)))
                {
                    $data = array_merge($data, $filter);
                    $ajaxdata['recordsFiltered'] = $this->NavigationObj->get_menu_list($data);
                }
                if (!empty($ajaxdata['recordsFiltered']))
                {
                    $data['start'] = $postdata['start'];
                    $data['length'] = $postdata['length'];
                    $data['orderby'] = $postdata['columns'][$postdata['order'][0]['column']]['name'];
                    $data['order'] = $postdata['order'][0]['dir'];
                    unset($data['count']);
                    $ajaxdata['data'] = $this->NavigationObj->get_menu_list($data);
                }
            }
            return Response::json($ajaxdata);
        }
    /*      else if (!empty($postdata) && !empty($data) && isset($postdata['submit']) && $postdata['submit'] == 'Export')
        {
            $data = array_merge($data, $filter);
            $res['orders'] = $this->NavigationObj->get_menu_list($data);
            $output = View::make('admin.navigation.navigations_list_excel', $res);
            $headers = array(
                'Pragma'=>'public',
                'Expires'=>'public',
                'Cache-Control'=>'must-revalidate, post-check=0, pre-check=0',
                'Cache-Control'=>'private',
                'Content-Disposition'=>'attachment; filename=Category_list'.date("Y-m-d").'.xls',
                'Content-Transfer-Encoding'=>'binary'
            );
            return Response::make($output, 200, $headers);
        }
        else if (!empty($postdata) && isset($postdata['submit']) && $postdata['submit'] == 'Print')
        {
            $data = array_merge($data, $filter);
            $res['orders'] = $this->NavigationObj->get_menu_list($data);
            return View::make('admin.navigation.navigations_list_print', $res);
        }
        else
        {
            return View::make('admin.navigation.navigations_list', $data);
        }  */
    }
	
	public function top_menu_list ()
    {
        $postdata = $this->request->all();
        $top_menu_list = $this->NavigationObj->top_menu_list($postdata);
        return Response::json(!empty($top_menu_list) ? $top_menu_list : array());
    }
	
	public function menu_position_list ()
    {
        $menu_position_list = $this->NavigationObj->menu_position_list();
        return Response::json(!empty($menu_position_list) ? $menu_position_list : array());
    }
	
	public function group_list ()
    {
        $postdata = $this->request->all();
        $group_list = $this->NavigationObj->group_list($postdata);
        return Response::json(!empty($group_list) ? $group_list : array());
    }
	
	public function get_link_details ()
    {
        $op['status'] = 'fail';
        $postdata = $this->request->all();
        if (!empty($postdata))
        {
            $data = $this->NavigationObj->get_link_details($postdata);
            if (!empty($data))
            {
                $op['data'] = $data;
                $op['status'] = 'ok';
                return Response::json($op);
            }
        }
    }
	
	public function delete_nav ()
    {
        $op['status'] = 'fail';
        $op['msg'] = 'Something went wrong';
        $postdata =  $this->request->all();
        if (!empty($postdata))
        {
            $res = $this->NavigationObj->delete_nav($postdata);
            if (!empty($res))
            {
                $op['msg'] = 'Deleted Successfully';
                $op['status'] = 'ok';
            }
        }
        return Response::json($op);
    }
	
	public function get_menu ()
    {
        $op['status'] = 'fail';
        $data = $this->NavigationObj->get_menus();
        if (!empty($data))
        {
            $op['data'] = $data;
            $op['status'] = 'ok';
        }
        return Response::json($op);
    }
	
	public function save_menu_settings ()
    {
        $postdata = $this->request->all();
        $op['status'] = 'error';
        $op['msg'] = 'failed';
        if (!empty($postdata))
        {
            $save_menu_settings = $this->NavigationObj->save_menu_settings($postdata);
            if (!empty($save_menu_settings))
            {
                $op['status'] = 'ok';
                $op['msg'] = 'saved successfully';
            }
        }
        return Response::json($op);
    }

	
}
