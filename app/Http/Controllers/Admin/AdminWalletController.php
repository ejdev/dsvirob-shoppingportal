<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Models\Admin\AdminWallet;
use URL;
use View;
use Config;
use Redirect;

class AdminWalletController extends AdminBaseController
{

    public function __construct ()
    {
        parent::__construct();
        $this->walletObj = new AdminWallet();
    }

    public function transferHistory ()
    {
        $post = $this->request->all();
        $submit = '';
        $data = $filter = array();
        $filter['search_term'] = isset($post['search_term']) && !empty($post['search_term']) ? $post['search_term'] : '';
        $filter['from'] = isset($post['from']) && !empty($post['from']) ? $post['from'] : '';
        $filter['to'] = isset($post['to']) && !empty($post['to']) ? $post['to'] : '';
        $filter['currency'] = isset($post['currency']) && !empty($post['currency']) ? $post['currency'] : '';
        if ($this->request->ajax())
        {
            $ajaxdata['draw'] = $post['draw'];
            $ajaxdata['recordsTotal'] = $this->walletObj->getTransferHistory($data, true);
            $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['data'] = [];
            if ($ajaxdata['recordsTotal'] > 0)
            {
                $data = array_merge($data, $filter);
                $ajaxdata['recordsFiltered'] = $this->walletObj->getTransferHistory($data, true);
                $data['start'] = $post['start'];
                $data['length'] = $post['length'];
                if (isset($post['order']))
                {
                    $data['orderby'] = $post['columns'][$post['order'][0]['column']]['name'];
                    $data['order'] = $post['order'][0]['dir'];
                }
                $ajaxdata['data'] = $this->walletObj->getTransferHistory($data);
            }
            return $this->response->json($ajaxdata);
        }
        else
        {
            return View::make('wallet.transfer_history', $data);
        }
    }

    public function transactionLog ()
    {
        $post = $this->request->all();
        $submit = '';
        $data = $filter = array();
        $filter['search_term'] = isset($post['search_term']) && !empty($post['search_term']) ? $post['search_term'] : '';
        $filter['from'] = isset($post['from']) && !empty($post['from']) ? $post['from'] : '';
        $filter['to'] = isset($post['to']) && !empty($post['to']) ? $post['to'] : '';
        $filter['currency'] = isset($post['currency']) && !empty($post['currency']) ? $post['currency'] : '';
        if ($this->request->ajax())
        {
            $ajaxdata['draw'] = $post['draw'];
            $ajaxdata['recordsTotal'] = $this->walletObj->getTransactionLog($data, true);
            $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['data'] = [];
            if ($ajaxdata['recordsTotal'] > 0)
            {
                $data = array_merge($data, $filter);
                $ajaxdata['recordsFiltered'] = $this->walletObj->getTransactionLog($data, true);
                $data['start'] = $post['start'];
                $data['length'] = $post['length'];
                if (isset($post['order']))
                {
                    $data['orderby'] = $post['columns'][$post['order'][0]['column']]['name'];
                    $data['order'] = $post['order'][0]['dir'];
                }
                $ajaxdata['data'] = $this->walletObj->getTransactionLog($data);
            }
            return $this->response->json($ajaxdata);
        }
        else
        {
            return View::make('wallet.transaction_log', $data);
        }
    }

}
