<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminBaseController;
use URL;
use View;
use Config;
use Redirect;
use App\Http\Controllers\Api\HTTPApiController;

class AdminUserController extends AdminBaseController
{
    public $myaccObj = '';
    public $adminObj = '';

    public function __construct ()
    {
        parent::__construct();        
		$this->HTTPApiObj = new HTTPApiController('api');
        $this->HTTPVpiObj = new HTTPApiController('vpi');
		$this->account_id = 111;
    }
	
	public function UserList1 ($arr = array())
    {	  	
      return $res = (array) $this->HTTPApiObj->postRequest('admin/user/list', $arr, 'POST');
    }
	
	public function UserList ()
    {
        $data = $ajaxdata = [];
        $postdata = $this->request->all();
        $this->statusCode = 200;	      		        		
        $data['account_id'] = $this->account_id;        								
        if ($this->request->ajax())
        {
            $ajaxdata['recordsTotal'] = $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['draw'] = $postdata['draw'];
            $ajaxdata['data'] = array();
            
                $data['start'] = $postdata['start'];
                $data['length'] = $postdata['length'];
				$data['search_term'] = (isset($postdata['search_term']) && !empty($postdata['search_term'])) ? $postdata['search_term'] : null;	
                if (isset($postdata['order']))
                {
                    $data['orderby'] = $postdata['columns'][$postdata['order'][0]['column']]['name'];
                    $data['order'] = $postdata['order'][0]['dir'];
                }
                $user = $this->HTTPApiObj->postRequest('admin/user/list', $data, 'POST');				
                if ($user)
                {

                    $ajaxdata['recordsTotal'] = $user->recordsTotal;
                    $ajaxdata['recordsFiltered'] = $user->recordsFiltered;                   
                    $ajaxdata['data'] = $user->data;
                }
            
            return $this->response->json($ajaxdata, $this->statusCode);
        }
        else
        {
            $data['title'] = 'User List';
            return View::make('user.user_list', $data);
        }        
    }
	
	public function UserOrderList ($account_id)
    {	        
		$data = $ajaxdata = [];
        $postdata = $this->request->all();
        $this->statusCode = 200;
       // $data['status'] = !empty($status) ? $status : (isset($postdata['status']) && !empty($postdata['status']) ? $postdata['status'] : '');
        $status_array = ['placed'=>'Placed Orders', 'confirmed'=>'Confirmed Orders', 'packed'=>'Packed Orders', 'dispatched'=>'Dispatched Orders', 'couriered'=>'Couriered', 'reached-hub'=>'Reached Hub', 'delivered'=>'Delivered', 'cancelled'=>'Cancelled', 'returned'=>'Return', 'refunded'=>'Refunded', 'replaced'=>'Replaced'];
		$data['account_id'] = $account_id;
        if ($this->request->ajax())
        {
            $ajaxdata['recordsTotal'] = $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['draw'] = $postdata['draw'];
            $ajaxdata['data'] = array();
            
                $data['start'] = $postdata['start'];
                $data['length'] = $postdata['length'];
				$data['search_term'] = (isset($postdata['search_term']) && !empty($postdata['search_term'])) ? $postdata['search_term'] : null;	
                if (isset($postdata['order']))
                {
                    $data['orderby'] = $postdata['columns'][$postdata['order'][0]['column']]['name'];
                    $data['order'] = $postdata['order'][0]['dir'];
                }
                $orders = $this->HTTPVpiObj->postRequest('admin/orders', $data, 'POST');				
                if ($orders)
                {

                    $ajaxdata['recordsTotal'] = $orders->recordsTotal;
                    $ajaxdata['recordsFiltered'] = $orders->recordsFiltered;
                    array_walk($orders->data, function(&$row)
                    {						
						//$row->usercode = $this->adminObj->get_user_code($row->account_id);
                        array_walk($row->actions, function(&$action, $key) use($row)
                        {
                            switch ($key)
                            {
                                case 'PARTNER_HOLDING':
                                case 'PARTNER_CONFIRMED':
                                case 'PARTNER_CANCELLED':
                                case 'ADMIN_HOLDING':
                                case 'ADMIN_CONFIRMED':
                                case 'ADMIN_CANCELLED':
                                case 'SUPPLIER_HOLDING':
                                case 'SUPPLIER_CONFIRMED':
                                case 'SUPPLIER_CANCELLED':
                                    $action->url = URL::route('admin.order.update-approval-status');
                                    break;
                                case 'DETAILS':
                                    $action->target = '_blank';
                                    $action->url = URL::route('admin.order.details', ['order_code'=>$row->sub_order_code]);
                                    break;
                            }
                        });
                    });
                    $ajaxdata['data'] = $orders->data;
                }
            
            return $this->response->json($ajaxdata, $this->statusCode);
        }
        else
        {
            $data['title'] = 'User Order List';
            return View::make('order.user_order_list', $data);
        }
       
		
		
    }
	
	
}
