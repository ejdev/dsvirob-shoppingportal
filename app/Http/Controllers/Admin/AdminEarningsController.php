<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\AdminBaseController;
use App\Models\Admin\Admin;
use URL;
use View;
use Config;
use Redirect;
use ShoppingPortal;
use App;

class AdminEarningsController extends AdminBaseController
{
    public $myaccObj = '';
    public $adminObj = '';

    public function __construct ()
    {
        parent::__construct();
       // $this->myaccObj = new MyAccount();
        $this->adminObj = new Admin();
    }
	
	public function EarningsList ($status = '')
    {
        $data = $ajaxdata = [];
        $postdata = $this->request->all();
        $this->statusCode = 200;
        $data['status'] = !empty($status) ? $status : (isset($postdata['status']) && !empty($postdata['status']) ? $postdata['status'] : '');
        $status_array = ['placed'=>'Placed Orders', 'confirmed'=>'Confirmed Orders', 'packed'=>'Packed Orders', 'dispatched'=>'Dispatched Orders', 'couriered'=>'Couriered', 'reached-hub'=>'Reached Hub', 'delivered'=>'Delivered', 'cancelled'=>'Cancelled', 'returned'=>'Return', 'refunded'=>'Refunded', 'replaced'=>'Replaced'];
		
        if ($this->request->ajax())
        {
            $ajaxdata['recordsTotal'] = $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['draw'] = $postdata['draw'];
            $ajaxdata['data'] = array();
            
                $data['start'] = $postdata['start'];
                $data['length'] = $postdata['length'];
				$data['search_term'] = (isset($postdata['search_term']) && !empty($postdata['search_term'])) ? $postdata['search_term'] : null;	
                if (isset($postdata['order']))
                {
                    $data['orderby'] = $postdata['columns'][$postdata['order'][0]['column']]['name'];
                    $data['order'] = $postdata['order'][0]['dir'];
                }
                $orders = $this->HTTPVpiObj->postRequest('admin/commision/earnings', $data, 'POST');				
                if ($orders)
                {
    				$ajaxdata['recordsTotal'] = $orders->recordsTotal;
                    $ajaxdata['recordsFiltered'] = $orders->recordsFiltered;                     
					array_walk($orders->data, function(&$row)
                    {						
						$row->usercode = $this->adminObj->get_user_code($row->account_id);      
						$row->currency = 'USD'; 
                    });
					$ajaxdata['data'] = $orders->data;
                }
            
            return $this->response->json($ajaxdata, $this->statusCode);
        }
        else
        {
            $data['title'] = 'Earnings List';
            return View::make('earnings.earnings_list', $data);
        }
        
    }
	
	
}
