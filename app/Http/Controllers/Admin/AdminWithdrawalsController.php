<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminBaseController;
use App\Models\Admin\AdminWithdrawals;
use URL;
use View;
use Config;
use Redirect;

class AdminWithdrawalsController extends AdminBaseController
{

    public function __construct ()
    {
        parent::__construct();
        $this->withdrawalsObj = new AdminWithdrawals();
    }

    public function withdrawal_list ($status = '')
    {
        $post = $this->request->all();
        $submit = '';
        $data = $filter = array();
        $status_array_id = ['pending'=>0, 'processing'=>2, 'transferred'=>1, 'cancelled'=>3];
        $data['status'] = !empty($status) ? $status : (isset($post['status']) && !empty($post['status']) ? $post['status'] : '');
        $data['status_id'] = !empty($data['status']) ? $status_array_id[$data['status']] : null;
        $status_array = ['pending'=>'Pending Withdrawals', 'processing'=>'Processing Withdrawals', 'transferred'=>'Transfer Withdrawals', 'cancelled'=>'Cancelled Withdrawals'];
        $filter['from'] = isset($post['from']) && !empty($post['from']) ? $post['from'] : '';
        $filter['to'] = isset($post['to']) && !empty($post['to']) ? $post['to'] : '';
        $filter['paytype'] = isset($post['paytype']) && !empty($post['paytype']) ? $post['paytype'] : '';
        $filter['currency'] = isset($post['currency']) && !empty($post['currency']) ? $post['currency'] : '';
        if ($this->request->ajax())
        {

            $ajaxdata['draw'] = $post['draw'];
            $ajaxdata['recordsTotal'] = $this->withdrawalsObj->withdrawals_list($data, true);
            $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['data'] = [];
            if ($ajaxdata['recordsTotal'] > 0)
            {
                $data = array_merge($data, $filter);
                $ajaxdata['recordsFiltered'] = $this->withdrawalsObj->withdrawals_list($data, true);
                $data['start'] = $post['start'];
                $data['length'] = $post['length'];
                if (isset($post['order']))
                {
                    $data['orderby'] = $post['columns'][$post['order'][0]['column']]['name'];
                    $data['order'] = $post['order'][0]['dir'];
                }
                $ajaxdata['data'] = $this->withdrawalsObj->withdrawals_list($data);
            }
            return $this->response->json($ajaxdata);
        }
        elseif (array_key_exists($status, $status_array))
        {
            $data['title'] = $status_array[$status];
            $data['withdrawal_status'] = $this->withdrawalsObj->withdrawal_status();
            $data['payout_types'] = $this->withdrawalsObj->get_payout_types();
            $data['withdrwa_currecies'] = $this->withdrawalsObj->get_withdraw_currency();
            return View::make('withdrawal.withdrawals_list', $data);
        }
    }

    public function withdrawalDetails ()
    {
        $postdata = $this->request->all();
        $status = 422;
        if (!empty($postdata))
        {
            $postdata['account_id'] = $this->account_id;
            $op['details'] = $this->withdrawalsObj->getWithdrawalDetails($postdata);
            if (!empty($op['details']))
            {
                $status = 200;
            }
        }
        return $this->response->json($op, $status);
    }

    public function updateWithdrawalStatus ()
    {
        $postdata = $this->request->all();
        $status = 422;
        $op['msg'] = 'Something Went Wrong';
        if (!empty($postdata))
        {
            $postdata['account_id'] = $this->account_id;
            if ($this->withdrawalsObj->updateWithdrawalStatus($postdata))
            {
                $status = 200;
                $op['msg'] = 'Updated Successfully';
            }
        }
        return $this->response->json($op, $status);
    }

}
