<?php

namespace App\Http\Controllers;

use App\Http\Controllers\UserBaseController;
use App\Http\Controllers\Api\HTTPApiController;
use App\Libraries\Mailer;

class FrontController extends UserBaseController
{
	public function __construct(){
		parent::__construct();

	}
	
	public function login ()
    {
        $data = array();
        $data['url'] = '';
				
		return View('login', $data);		
    }

    public function checkMail(){
    	
    	Mailer::send('gopintg@gmail.com', 'emails.user.testmail', 'Password has been reseted', []);
    }

}