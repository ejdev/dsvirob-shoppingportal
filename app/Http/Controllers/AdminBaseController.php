<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests;
use URL;
use Response;
use Session;
use View;
use Config;
use DB;

class AdminBaseController extends BaseController
{

    public function __construct ()
    {
        parent::__construct();
        $this->theme = $this->sitesettings->admin_site_theme;       
		View::addLocation('system/views');
        $data = array();
        if (Session::has('admin_data'))
        {
            $admin_details = Session::get('admin_data');
            if (!empty($admin_details))
            {
                $this->account_id = $admin_details->account_id;
                $this->uname = $admin_details->uname;
                $this->email = $admin_details->email;
                $this->full_name = $admin_details->uname;
                $this->mobile = $admin_details->mobile;
               // $this->user_code = $admin_details->user_code;
            }
            View::share('admin_details', $admin_details);
        }
    }

}
