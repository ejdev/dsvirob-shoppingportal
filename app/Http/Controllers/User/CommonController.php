<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\UserBaseController;
use App\Http\Requests;
use App\Http\Controllers\Api\HTTPApiController;
use URL;
use Response;
use Session;
use View;
use App\Models\Common;
use Illuminate\Support\Facades\Validator;
use Config;
use Cart;

class CommonController extends UserBaseController
{

    public function __construct ()
    {
        parent::__construct();
        $this->HTTPApiObj = new HTTPApiController('api');
        $this->HTTPVpiObj = new HTTPApiController('vpi');
        $this->commonSettingsObj = new Common();
    }	
	
	public function getWallet ()
    {
        $data['account_id'] = isset($this->account_id) ? $this->account_id : '0';    
		$data['cart_subtotal'] = Cart::instance('myCart')->subtotal();
		$data['products'] = json_decode(Cart::instance('myCart')->content(), true); 		
        return $res = (array) $this->HTTPApiObj->postRequest('get-wallet', $data, 'POST');
    }
	
	public function getDiscountPercentage ()
    {
		$data = [];        
        return $res = (array) $this->HTTPApiObj->postRequest('getDiscountPercentage', $data, 'GET');
    }
	
	public function getProfitSharingInfo ($arr = array())
    {
      return $res = (array) $this->HTTPVpiObj->postRequest('profit-sharing-info', $arr, 'POST');
    }

    public function getSliders ()
    {
        $data = array('page'=>'home');
        return $res = (array) $this->HTTPVpiObj->postRequest('get-sliders', $data, 'POST');
    }

    public function myCart ()
    {
        $data = array();
        return $res = (array) $this->HTTPVpiObj->postRequest('products/my-cart', $data, 'POST');
    }

    public function browse_products ()
    {
        return View('browse_products');
    }

    public function productsList ()
    {
        $data = array();
        $data = $this->request->all();		

        $res = (array) $this->HTTPVpiObj->postRequest('products/list', $data, 'POST');	
		return $res;
		// For Partner Discount Calculations
		array_walk($res['products'], function($product) use($res)
            {
				$partner_discount = $this->commonSettingsObj->get_discount_percentage();
                $product->partner_disc_price = $product->partner_price * ($partner_discount / 100); 
				$product->selling_price = $product->selling_price - $product->partner_disc_price; 
				$product->off_perc =  round(100 - (($product->selling_price / $product->mrp_price) * 100), 1);
			}); 
		return $res;
    }

    public function loadproductDetails (Request $request)
    {
        return View('single_page', ['product'=>true]);
    }

    public function getproductsDetails (Request $request)
    {
        $data = array();
        $data = $request->all();
        return $res = (array) $this->HTTPVpiObj->postRequest('products/details', $data, 'POST');
    }

    public function productSellers (Request $request)
    {
        return View('single_page', ['productSellers'=>true]);
    }

    public function home ()
    {
        return View('home');
    }

    public function getReviews (Request $request)
    {
        $data = array();
        $data = $request->all();
        return $res = (array) $this->HTTPVpiObj->postRequest('reviews', $data, 'POST');
    }

    public function likeReview (Request $request)
    {
        $data = array();
        $data = $request->all();
        return $res = (array) $this->HTTPVpiObj->postRequest('review/like', $data, 'POST');
    }

    public function unlikeReview (Request $request)
    {
        $data = array();
        $data = $request->all();
        return $res = (array) $this->HTTPVpiObj->postRequest('review/unlike', $data, 'POST');
    }

    public function report_abuse (Request $request)
    {
        $data = array();
        $data = $request->all();
        $data['account_id'] = isset($this->account_id) ? $this->account_id : '0';
        return $res = (array) $this->HTTPVpiObj->postRequest('review/abuse', $data, 'POST');
    }

    public function getAvaliablePaymentModes (Request $request)
    {
        $data = array();
        $data = $request->all();
		$data['account_id'] = isset($this->account_id) ? $this->account_id : '0';
        $res['payment_modes'] = $this->commonSettingsObj->getAvaliablePaymentModes($data);
        return $res;
    }

    public function rating ($post_type)
    {
        $data = array();
        $data = $this->request->all();
        $post_type = strtoupper($post_type);
        $this->statusCode = 422;
        $messages = ['title.required'=>'Enter Title', 'description.required'=>'Enter Description'];
        $rules = ['title'=>'required', 'description'=>'required'];
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails())
        {
            $this->op['error'] = $validator->messages();
        }
        else
        {
            if (key_exists($post_type, Config::get('constants.POST_TYPE')))
            {
                if (isset($data) && !empty($data))
                {
                    $data['post_type_id'] = Config::get('constants.POST_TYPE.'.$post_type);
                    $data['account_id'] = isset($this->account_id) ? $this->account_id : '0';
                    $data['created_on'] = date('Y-m-d H:i:s');
                    $data['review_code'] = $this->generate_review_code();
                    //$result = $this->productObj->rating($data);
                    return $result = (array) $this->HTTPVpiObj->postRequest('rating', $data, 'POST');
                    if (!empty($result))
                    {
                        $this->statusCode = 200;
                        $this->response['msg'] = 'Your product has been reviewed';
                    }
                    else
                    {
                        $this->response['msg'] = 'Could not change';
                    }
                }
            }
        }
        return Response::json($this->op, $this->statusCode);
    }

    public function generate_review_code ()
    {
        $function_ret = '';
        $iLoop = true;
        $disp = $this->rKeyGen(3, 1);
        $disp1 = "RE".$disp;
        return $disp1;
    }

    function rKeyGen ($digits, $datatype)
    {
        $key = '';
        $tem = '';
        $poss = array();
        $poss_ALP = array();
        $j = 0;
        if ($datatype == 1)
        {
            for ($i = 49; $i < 58; $i++)
            {
                $poss[$j] = chr($i);
                $poss_ALP[$j] = $poss[$j];
                $j = $j + 1;
            }
            for ($k = 1; $k <= $digits; $k++)
            {
                $key = $key.$poss[rand(1, 8)];
            }
            $key;
        }
        else
        {
            $key = $this->rKeyGen_ALPHA($digits, false);
        }
        return $key;
    }

    function rKeyGen_ALPHA ($digits, $lc)
    {
        $key = '';
        $tem = '';
        $poss = array();
        $j = 0;
        // Place numbers 0 to 10 in the array
        for ($i = 50; $i < 57; $i++)
        {
            $poss[$j] = chr($i);
            $j = $j + 1;
        }
        // Place A to Z in the array
        for ($i = 65; $i < 90; $i++)
        {
            $poss[$j] = chr($i);
            $j = $j + 1;
        }
        // Place a to z in the array
        for ($k = 97; $k < 122; $k++)
        {
            $poss[$j] = chr($k);
            $j = $j + 1;
        }
        $ub = 0;
        if ($lc == true)
            $ub = 61;
        else
            $ub = 35;
        for ($k = 1; $k <= 3; $k++)
        {
            $key = $key.$poss[rand(0, $ub)];
        }
        for ($k = 4; $k <= $digits; $k++)
        {
            $key = $key.$poss[rand(0, $ub)];
        }
        return $key;
    }
	
	

}
