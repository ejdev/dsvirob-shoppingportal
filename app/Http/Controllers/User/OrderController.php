<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserBaseController;
use App\Http\Controllers\Api\HTTPApiController;
use App\Models\User\User;
use View;
use Response;

class OrderController extends UserBaseController
{
	public $headers = [];
    public $statusCode = 422;   
    public $options = JSON_PRETTY_PRINT;
	
    public function __construct ()
    {
		parent::__construct();
		$this->HTTPApiObj = new HTTPApiController('api'); 
		$this->HTTPVpiObj = new HTTPApiController('vpi');
		$this->CommonControlObj = new CommonController();	
		$this->userObj = new User();
	}
	
    public function loadMyOrders()
    {			
		return View('account.my_orders', ['product'=>true]);			
    }
	
	public function getMyOrders()
    {			
		$data = array();
		$op = array();
		$data = $this->request->all();
		if (isset($this->account_id)) {
			$data['account_id'] = $this->account_id;					
			return $res = (array) $this->HTTPApiObj->postRequest('my-orders', $data, 'POST');					
		} else {
			$op['msg'] =  'Please login';
			return Response::json($op);
		}
    }
	
	public function getMyOrderDetails()
    {			
		$data = array();
		$data = $this->request->all();
		if ($this->account_id) {
			$data['account_id'] = $this->account_id;	
			return $res = (array) $this->HTTPApiObj->postRequest('order-details', $data, 'POST');						
		} else {
			$res['msg'] =  'Please login';
			return Response::json($res);
		}
    }
	
	public function orderCancel($soc)
    {			
		$data = array();
		$data = $this->request->all();
		$data['account_id'] = $this->account_id;					
		$data['full_name'] = $this->full_name;					
		$data['email'] = $this->email;					
		$data['soc'] = $soc;					
        return $res = (array) $this->HTTPVpiObj->postRequest('my-orders/cancel/'.$data['soc'], $data, 'POST');			
    }
	
	public function orderItemCancel($order_item_id)
    {			
		$data = array();
		$data = $this->request->all();
		$data['account_id'] = $this->account_id;					
		$data['full_name'] = $this->full_name;					
		$data['email'] = $this->email;					
		$data['order_item_id'] = $order_item_id;					
        return $res = (array) $this->HTTPVpiObj->postRequest('my-orders/item-cancel/'.$data['order_item_id'], $data, 'POST');			
    }
	
	public function get_Sub_Order_Details($so_id)
    {			
		$data = array();		
		$data['account_id'] = $this->account_id;			
		$data['soc'] = $so_id;							
        $res = (array) $this->HTTPVpiObj->postRequest('my-orders-items', $data, 'POST');			
		echo '<pre>';print_r($res);
    }
	
	public function myOrdersDetails ()
    {
        $ajaxdata = $data = [];
        $ajaxdata['data'] = [];
        $data = $this->request->all();		        
        $res = (array) $this->HTTPVpiObj->postRequest('my-orders-details', $data, 'POST');	
		return Response::json($res, 200, $this->headers, $this->options);
    }
	
	public function order_details($sub_order_code)
    {			
		$data = [];
        $data['sub_order_code'] = $sub_order_code;
		return View::make('account.my_order_details', $data);			
    }
	
	public function productSellers ()
    {
        $data = $ajaxdata = [];
		$data = $this->request->all();			
        if ($this->request->has('pid'))
        {			
            $data['product_code'] = $this->request->get('pid');			
            if ($this->request->has('cid'))
            {
                $data['product_cmb_code'] = $this->request->get('cid');
            }
            $data['currency_id'] = $this->currency_id;            		            
            return $product = (array) $this->HTTPVpiObj->postRequest('products-seller-details', $data, 'POST');				
        }        
    }
	

}
