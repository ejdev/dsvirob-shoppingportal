<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use App\Models\User\MyAccount;
use App\Models\User\User;
use App\Models\Sendsms;
use App\Http\Controllers\UserBaseController;
use App\Http\Controllers\Api\HTTPApiController;
use URL;
use Response;
use View;
use Config;
use Redirect;

class UserController extends UserBaseController {

    public $myaccObj = '';
    public $userObj = '';

    public function __construct() {
        parent::__construct();
        $this->myaccObj = new MyAccount();
        $this->userObj = new User();
        $this->HTTPApiObj = new HTTPApiController('api');
        $this->HTTPVpiObj = new HTTPApiController('vpi');
    }

    public function login(Request $request) {
        $data = array();
        $data['url'] = '';
        if (($request->has('redir'))) {
            $data['url'] = $request->get('redir');
        }
        if (!Session::has('user_data')) {
            return View('login', $data);
        } else {
            return back();
        }
    }

    public function login_check() {
        $op = array();
        $op['status'] = 'fail';
        $op['msg'] = 'Invalid Username and Password';


        if ($this->request->has('username')) {
            $postdata = $this->request->all();
            $validate = $this->userObj->validateUser($postdata);
            return $this->response->json($validate);
        }
    }

    public function logout() {
        $data = array();
        Session::forget('user_data');
        Session::flush();
        return Redirect::to('login');
        return false;
    }

    public function check_email_phone() {
        $postdata = $this->request->all();
        $usrExist = '';
        $usrExist['status'] = 422;
        $usrExist['msg'] = "error";
        $postdata['pagesettings'] = $this->sitesettings;

        if (filter_var($postdata['email_mobile'], FILTER_VALIDATE_EMAIL) !== false) {

            $postdata['email_id'] = $postdata['email_mobile'];
            $op['msg'] = 'We are already having an account with this email id';
            $op['status'] = 'fail';
        } else if (preg_match("/^[7-9]{1}[0-9]{9}$/i", $postdata['email_mobile'])) {
            $postdata['mobile'] = $postdata['email_mobile'];
            $op['msg'] = 'We are already having an account with this mobile number';
            $op['status'] = 'fail';
        } else {
            $op['notify'] = false;
            $op['msg'] = 'Invalid Format';
            $op['status'] = 'fail';
            return Response::json($op);
        }

        $usrExist = $this->userObj->check_email_phone_exists($postdata);
        if (empty($usrExist)) {

            $code = [];
            $code = rand(1111, 9999) . time();
            $code = mb_substr($code, 0, 5);
            if (!empty($postdata['mobile'])) {
                $result['email_mobile'] = $postdata['mobile'];
                $op['option'] = 'mobile';
                $result['code'] = $code;
                $op['code'] = $code;

                /*
                  $this->sendsmsObj->otp_send_sms(array(
                  'otp_pwd' => $code,
                  'mobile' => $postdata['mobile'],
                  'sitename' => $arr['pagesettings']->site_name
                  )); */
                $op['msg'] = ' OTP has been sent to your mobile';
                $op['status'] = 'ok';
            } else if ($postdata['email_id']) {
//                $code = [];
//                $code = rand(1111, 9999) . time();
//                $code = mb_substr($code, 0, 5);
                $result['email_mobile'] = $postdata['email_id'];
                $op['option'] = 'email';
//                $postdata['act_link'] = URL::to("/user/register/activation/" . $code);
//                $postdata['code'] = $code;
                $mstatus = 1;
                $op['msg'] = ' Verification link been Send to Your Email Id';
                $op['status'] = 'ok';

//                $mstatus = TWMailer::send(array(
//                            'to' => $userDetails->email,
//                            'subject' => 'Email Verification',
//                            'view' => 'emails.user.verify_user',
//                            'data' => $postdata
//                ));
            }
//          
//
            Session::set('register_code', $result);
        } else{
            $op['notify'] = false;
        }

        return Response::json($op);
    }

    public function check_register_otp() {
        $op['status'] = 'fail';
        $op['status'] = 'something went wrong';
        $user = Session::get('register_code');
        $postdata = $this->request->all();

        if ($user['code'] == $postdata['otp']) {

            unset($user['code']);
            $op['msg'] = '<div class="alert alert-danger"> OTP Verified successfully</div>';
            $op['status'] = 'ok';
        } else {
            $op['msg'] = '<div class="alert alert-danger">Enter Valid OTP code</div>';
            $op['status'] = 'fail';
        }
        return $this->response->json($op);
    }

    public function check_user_otp() { // used for forgot pwd.
        $op['status'] = 'fail';
        $op['status'] = 'something went wrong';
        $user = Session::get('user_postdata');
        $postdata     = $this->request->all();
        $validate     = $this->HTTPApiObj->postRequest('check/user/otp', $postdata, 'POST');
        return $this->response->json($validate);
    }

    public function user_register() {
        $validate = '';
        $postdata = $this->request->all();
        if (isset($postdata['email_id']) && !empty($postdata['email_id'])) {
            $op['msg'] = 'Your Email Id already exists';
            $op['status'] = 'fail';
            $wdata['email_id'] = $postdata['email_id'];
        } else {
            $op['msg'] = 'Your Mobile number already exists';
            $op['status'] = 'fail';
            $wdata['mobile'] = $postdata['mobile_no'];
        }
        $usrExist = $this->userObj->check_email_phone_exists($wdata);
        if (empty($usrExist)) {
            $op = '';
            $user = Session::get('register_code');
            if (isset($postdata['email_id'])) {
                $postdata['mobile_no'] = $user['email_mobile'];
                $postdata['mobile_verified'] = Config::get('constants.MOBILE_VERIFY');
            } else {
                $postdata['email_id'] = $user['email_mobile'];
                $postdata['email_verified'] = Config::get('constants.EMAIL_VERIFY');
            }
            if (!empty($postdata)) {


                $validate = $this->HTTPApiObj->postRequest('user/registers', $postdata, 'POST');
            }
        } else {
            return $this->response->json($op);
        }
        return $this->response->json($validate);
    }

    public function reset_password() {
        $data = array('status' => '', 'msg' => '');
        $mdata = array();
        $op = array();
        $postdata = $this->request->all();
        if ($postdata['forg_new_pass'] == $postdata['confirm_new_pass']) {
            $validate = $this->HTTPApiObj->postRequest('user/reset/pwd', $postdata, 'POST');
        }
        return $this->response->json($validate);
    }

    public function check_register_activation_key($code = '') {
        $postdata['code'] = $code;
        $user = Session::get('register_code');

        $op = '';
        $op = 'Your Email Verification failed';
        if ($postdata['code'] == $user['code']) {
            $sdata = '';
            $sdata['email_id'] = $user['email_mobile'];
            $sdata['email_verified'] = Config::get('constants.EMAIL_VERIFY');
            $res = $this->userObj->ins_user_settings($sdata);

            $op = 'Your Email Verified Successfully';
             return Redirect::to('login')->with('success', 'Your Email Verified Successfully');

        }
        return Redirect::to('login')->with('fail_msg', 'Your Email Verification failed');
        //Session::flash('msg', $op);
       
        //return Response::json($op);
    }

    public function reset_activation_key($code = '') {
        $postdata['code'] = $code;
        $validate = $this->HTTPApiObj->postRequest('user/activation/key', $postdata, 'POST');
        return Redirect::to('login')->with('msg', $validate['msg']);
    }
    
    public function deny_activation_key($code = '') {
        $postdata['code'] = $code;
        $validate = $this->HTTPApiObj->postRequest('user/deny-activation/key', $postdata, 'POST');
        return Redirect::to('login')->with('deny-msg',"We've recorded that you didn't ask to reset your password. You can log in to your account with your current password, and you don't need to do anything else.");
    }

    public function forgot_password() {
        $data = array('status' => '', 'msg' => '');
        $mdata = array();
        $op = array();
        $postdata = $this->request->all();
        $validate = $this->HTTPApiObj->postRequest('user/forgot/pwd', $postdata, 'POST');
        return $this->response->json($validate);
    }

    public function old_password_check() {
        $postdata = Input::all();
        $user_id = Session::get('userdata');
        $data['password_check'] = $this->myaccObj->old_password_check($user_id['user_id']);
        if ($data['password_check']->pass_key == md5($postdata['oldpassword'])) {
            $detail = "Correct Password";
            return "true";
        } else {
            return "false";
        }
    }

    public function change_password() {
        return View::make('member.myaccount.change_password');
    }
	
	public function contact_us() {
        return View::make('contact-us');
    }

}
