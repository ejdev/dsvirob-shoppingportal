<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserBaseController;
use App\Http\Controllers\Api\HTTPApiController;
use App\Http\Controllers\TWMailer;
use URL;
use Response;
use Session;
use View;
use Config;
use App\Models\User\Cart;
use App\Models\User\MyAccount;
use Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MyAccountController extends UserBaseController
{
	public $myaccObj = '';
	public $headers = [];
    public $statusCode = 422;   
    public $options = JSON_PRETTY_PRINT;
	
    public function __construct ()
    {
		parent::__construct();
		$this->HTTPApiObj = new HTTPApiController('api'); 
		$this->HTTPVpiObj = new HTTPApiController('vpi');
		$this->cartObj = new Cart();
		$this->myaccObj = new MyAccount();
	}
	
	public function addToWishList (Request $request) 
	{
		$data = [];
		$data = $request->all();
		//$data['currency_id'] = $this->currency_id;
		$data['account_id'] = $this->account_id;		
		return (array) $this->HTTPApiObj->postRequest('account/add-to-wishlist', $data, 'POST');	
	}	
	
	public function remove_wish_list (Request $request)
    {
        $post = $request->all();        
        if (!empty($post))
        {
            $data['id'] = $post['id'];
			$data['account_id'] = $this->account_id;	
			return $res = (array) $this->HTTPApiObj->postRequest('account/remove-wishlist', $data, 'POST');	           
        }
    }
	
	public function remove_wish_listbk (Request $request)
    {
        $post = $request->all();
        $op['status'] = 'error';
        $op['msg'] = 'failure';
        if (!empty($post))
        {
            $data['id'] = $post['id'];
            $res = $this->cartObj->remove_wish_list($data);
            if ($res)
            {
                $op['status'] = 'ok';
                $op['msg'] = 'Product Successfully removed from  Wish List';
            }
            return Response::json($op);
        }
    }

	public function MyWishList(Request $request) 
	{		
		$data = array();
        $data['account_id'] = $this->account_id;        
        $post = $request->all();			
        if ($post)
        {
            if (isset($post['search_term']))
            {
                $data['search_term'] = $post['search_term'];
            }
        }				
		$res = (array) $this->HTTPApiObj->postRequest('account/wishlist', $data, 'POST');	
		$data['spid'] = $res['pid'];		
        if (\Request::ajax())
        {			
            $data['count'] = true;			        
            //$ajaxdata['recordsTotal'] = $this->HTTPApiObj->postRequest('account/wishlist', $data, 'POST');		
			$ajaxdata['recordsTotal'] = $res['count'];
            $ajaxdata['draw'] = $post['draw'];
            $ajaxdata['url'] = URL::to('/user');
            $ajaxdata['recordsFiltered'] = 0;
            $ajaxdata['data'] = array();	
				
            if (!empty($ajaxdata['recordsTotal']) && $ajaxdata['recordsTotal'] > 0)
            {                
				$ajaxdata['recordsFiltered'] = (array) $this->HTTPVpiObj->postRequest('wishlist', $data, 'POST');					
                if (!empty($ajaxdata['recordsFiltered']) && $ajaxdata['recordsFiltered'] > 0)
                {
                    $data['start'] = $post['start'];
                    $data['length'] = $post['length'];
                    if (isset($post['order']))
                    {
                        $data['orderby'] = $post['columns'][$post['order'][0]['column']]['name'];
                        $data['order'] = $post['order'][0]['dir'];
                    }
                    unset($data['count']);
                    $ajaxdata['draw']  = $post['draw'];
                    $result = (array) $this->HTTPVpiObj->postRequest('wishlist', $data, 'POST');			
					
					if (!empty($result)) {	
						foreach ($res['wid'] as $key => $val) {						
							$result[$key]->wishlist_id = $val;
						}
					}
					$ajaxdata['data'] = $result;					
                }
            }
            return Response::json($ajaxdata);
        } else {
			return View('account.my_wish_list');
		}
	}
	
	public function edit_personal_info(){
		$data['account_id']  =  $this->account_id;
		$data['info'] 	     =  $this->HTTPApiObj->postRequest('get_personal_info', $data, 'POST');
		$data['gender']      = $this->get_gender_lookup();		
		return View('account.update_personal_info',$data);
	}
	
	public function update_personal_info ()
    {
        $postdata = $this->request->all();		
        $data = array();
        $sdata = array();
        $op['status'] = 'WARN';
        $status = 422;  	
        $validator = \Validator::make($postdata, $this->validations('UPDATE_PERSONAL_INFO', 'rules'), $this->validations('UPDATE_PERSONAL_INFO', 'msg'));				
        if (!$validator->fails())
        {
            $status = 200;
            if (!empty($postdata))
            {
                $sdata['firstname'] = $postdata['firstname'];
                $sdata['lastname'] = $postdata['lastname'];
                $sdata['gender'] = $postdata['gender'];                
				$postdata['account_id'] = $this->account_id;				
				$result = (array) $this->HTTPApiObj->postRequest('update-personal-info', $postdata, 'POST');					
                if (!empty($result))
                {
                    $op['status'] = 'ok';
                    $op['msg'] = 'Your changes have been saved successfully.';
                }
            }
        }
        else
        {
            $op['error'] = $validator->errors();
            $op['notify'] = false;
            $status = 422;
        }
		$this->statusCode = 200;
		return Response::json($op, $this->statusCode, $this->headers, $this->options);
    }
	
	public function savepassword ()
    {
        $postdata = $this->request->all();
        $data['confirm_password'] = $postdata['confirm_password'];
        $data['new_password'] = $postdata['new_password'];
        $data['oldpassword'] = $postdata['oldpassword'];		
        $status = 422;
		$op['msg'] ='';
		$op['status'] ='';
        $validator = \Validator::make($data, $this->validations('CHANGE_PASSWORD', 'rules'), $this->validations('CHANGE_PASSWORD', 'msg'));		
        if (!$validator->fails())
        {            
			$postdata['account_id'] = $this->account_id;		
			$result = (array) $this->HTTPApiObj->postRequest('check-user-existing-password', $postdata, 'POST');            
            if ($result['status'] == 'ok')
            {               
				$res = (array) $this->HTTPApiObj->postRequest('update-password', $postdata, 'POST');
                if ($res != '')
                {
                    $op['status'] = "ok";
                    $op['msg'] = "New Password has been updated succesfully";
                    $op['url'] = url('/logout');
                    $status = 200;
                }
            }
            else
            {
                $op['status'] = "WARN";
                $op['msg'] = "Old Password does't matched with the new password";
                $status = 200;
            }
        }
        else
        {
            $op['status'] = "param_err";
            $op['error'] = $validator->errors();
            $status = 200;
        }
        return Response::json($op, $status, $this->headers, $this->options);
    }
	
	public function saveaddress ()
    {
        $postdata = $this->request->all();
		$postdata['account_id'] = $this->account_id;       
        $validator = \Validator::make($postdata, $this->validations('UPDATE_ADDRESS', 'rules'), $this->validations('UPDATE_ADDRESS', 'msg'));		
        $op = '';
        $this->statusCode = 422;
        if (!$validator->fails())
        {
            $adata['street1'] = $postdata['street1'];
            $adata['street2'] = $postdata['street2'];
            $adata['city'] = $postdata['city'];
            $adata['state_id'] = $postdata['state'];
            $adata['country_id'] = $postdata['country'];
            $adata['postal_code'] = $postdata['pin_code'];
            $adata['account_id'] = $postdata['account_id'];
            $adata['create_on'] = date('Y-m-d H:i:s');
			$op = $this->HTTPApiObj->postRequest('save-address', $postdata, 'POST');				            
        }
        else
        {            
            $op['status'] = 'param_err';
            $op['error'] = $validator->errors();           
        }
		$this->statusCode = 200;
        return Response::json($op, $this->statusCode, $this->headers, $this->options);
    }
	
	private function validations ($frm, $section)
    {

        $validation_msg = ['ADD_PAYMENT'=>[
                'rules'=>[
                    'payment_list_id'=>'required',
                    'payment_gateway_select'=>'required'
                ],
                'msg'=>trans('add_payment.validation')
            ],
            'UPDATE_PERSONAL_INFO'=>[
                'rules'=>[
                    'firstname'=>'required',
                    'lastname'=>'required',
                    'gender'=>'required',
                ],
                'msg'=>trans('update_personal_info.validation')
            ],
            'CHANGE_PASSWORD'=>[
                'rules'=>[
                    'oldpassword'=>'required',
                    'new_password'=>'required',
                    'confirm_password'=>'required',
                ],
                'msg'=>trans('change_password.validation')
            ],
            'UPDATE_ADDRESS'=>[
                'rules'=>[
                    'street1'=>'required',
                    'city'=>'required',
                    'state'=>'required',
                    'country'=>'required',
                    'pin_code'=>'required',
                ],
                'msg'=>trans('update_address.validation')
        ]];

        return $validation_msg[$frm][$section];
    }	
	
	public function change_password(){
		$data = array();
		return View('account.change_password',$data);
	}
		
    public function address()
    {
		$data['countries'] = $this->get_countries();
		$data['default_country'] = config('tables.DEFAULT_COUNTRY');
        return View('account.account_address',$data);
    }
	
	public function get_state_phonecode(){
		$postdata  = $this->request->all();
		$data['states'] = $this->get_states_list($postdata);
		$op['status'] = 'ok';
		$op['state_list'] = $data['states'];
		return Response::json($op);
	}
	public function get_city(){
		$postdata  		  = $this->request->all();
		$citys   		  = $this->get_city_list($postdata);
		$op['status']     = 'ok';
		$op['city_list']  = $citys;
		return Response::json($op);
	}
	
	public function get_address_list(){
		$postdata['account_id']  = $this->account_id;
		$op['address_list']      = $this->HTTPApiObj->postRequest('get_address_list', $postdata, 'POST');
		return Response::json($op);
	}
	
	public function default_address(){
		$op['status']  		 	 = 'err';
		$op['msg'] 				 = 'something went wrong';
		$postdata 				 = $this->request->all();
		$postdata['account_id']  = $this->account_id;
		$res      = $this->HTTPApiObj->postRequest('default_address',$postdata,'POST');		
		return Response::json($res);
	}
	
	public function delete_address($id = ''){
		$op['status']  		 	 = 'err';
		$op['msg'] 				 = 'something went wrong';
		$postdata['address_id']  = $id;
		$postdata['account_id']  = $this->account_id;
		$res      = $this->HTTPApiObj->postRequest('delete-address', $postdata, 'POST');		
		return Response::json($res);
	}
	public function account_email_update ()
    {
		$data['email']  = $this->email;
		$data['mobile'] = $this->mobile;
        return View('account.account_mail_update',$data);
    }
	public function verify_email(){
		$postdata  				 = $this->request->all();
		$postdata['account_id']  = $this->account_id;
		$details = $this->HTTPApiObj->postRequest('verification-email', $postdata, 'POST');				
		return Response::json($details);
	}
	
	public function checkverificationcode(){
		$postdata  				 = $this->request->all();
		$postdata['account_id']  = $this->account_id;
		$details 	 = $this->HTTPApiObj->postRequest('check-verification-code', $postdata,'POST');		
		return Response::json($details);
	}
	
	public function updateemail(){
		$postdata = $this->request->all();
		$postdata['account_id'] = $this->account_id;
		$details 	 = $this->HTTPApiObj->postRequest('update-email', $postdata, 'POST');
		/* if(!empty($details) && $details->status == 'ok'){
			$op['status'] = "ok";
			$op['msg'] 	  = '<div class="text-success">Mail Updated Successfully.</div>';
		}else{
			$op['status'] = "WARN";
			$op['msg'] 	  = '<div class="text-danger">Verification Code Expired or not valid.</div>';
		} */
		return Response::json($details);
	}
	
	public function verify_mobile(){
		$postdata  				 = $this->request->all();
		$postdata['account_id']  = $this->account_id;
		$details 	 	= $this->HTTPApiObj->postRequest('verify-mobile', $postdata,'POST');		
		return Response::json($details);
	}
	public function account_deactivate ()
    {
        return View('account.account_deactivate');
    }
	public function save_deactivate ()
    {
		$postdata   = $this->request->all();
		$postdata['account_id'] = $this->account_id;
		$postdata['email'] = $this->email;
        $op 	 	= $this->HTTPApiObj->postRequest('account-deactivate', $postdata,'POST');
		return Response::json($op);
    }
	
}
