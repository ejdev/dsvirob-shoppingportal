<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\UserBaseController;
use URL;
use Response;
use Session;
use View;
use App\Http\Controllers\Api\HTTPApiController;
use App\Http\Controllers\User\CommonController;
use Cart;
use Cookie;
use Config;
use App\Models\User\User;
use App\Models\Common;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use ShoppingPortal;

class CartController extends UserBaseController
{
    public $headers = [];
    public $statusCode = 422;   
    public $options = JSON_PRETTY_PRINT;
	
    public function __construct ()
    {
		parent::__construct();
		$this->HTTPApiObj = new HTTPApiController('api'); 
		$this->HTTPVpiObj = new HTTPApiController('vpi');
		$this->CommonControlObj = new CommonController();	
		$this->userObj = new User();
		$this->commonSettingsObj = new Common();
	}
	
	public function addToCart(Request $request)
    {	
		if ($this->request->has('spid')) {
			$postData = array();
			$postData = $request->all();					
			// Stock Check Need to Do
			$is_stock = 10;
			if ($is_stock) {					
					$postData['currency_id'] = $this->currency_id;
					$postData['country_id'] = $this->country_id;
					$postData['user'] = ['country_id'=>$this->country_id, 'region_id'=>$this->region_id, 'city_id'=>$this->city_id, 'geo_zone_id'=>$this->geo_zone_id];
					$product_details = $this->HTTPVpiObj->postRequest('ProductInfo', $postData, 'POST');	
					if ($product_details)
					{
						$tax_amt = $product_details->selling_price * ($product_details->taxinfo[0]->tax_value / 100);
						$wallet = $this->HTTPApiObj->postRequest('get-profit-sharing-info', $postData, 'POST');	
						$cart_count = Cart::instance('myCart')->count();
						Cart::instance('myCart')->add(
											array(
												'id'=>$product_details->supplier_product_id,
												'name'=>$product_details->product_name,
												'qty'=>1,
												'price'=>$product_details->selling_price,														
												'options'=>array(			
													'supplier_prod_code'=>$product_details->supplier_product_code,
													'supplier_product_id'=>$product_details->supplier_product_id,					
													'supplier_id'=>$product_details->supplier_id,
													'image_path'=>$product_details->img_path,										
													'currency'=>$product_details->currency,
													'currency_symbol'=>$product_details->currency_symbol,						
													'currency_id'=>$product_details->currency_id,
													'bonus_wallet'=>$wallet->bonus->bonus_wallet_amt,												
													'stock_status'=>$product_details->stock_status,
													//'partner_discount_price'=>$product_details->partner_price,
													//'partner_discount_perc'=>$product_details->partner_discount,
													'off_perc'=>$product_details->off_per,     // Tax for single item
													'partner_info'=>['partner_price'=>$product_details->partner_price,
																	'partner_discount_perc'=>$product_details->partner_discount,
																	'partner_margin_price'=>$product_details->partner_actual_price,
																	'supplier_product_price'=>$product_details->price],
													'tax_info'=>['tax_perc'=>$product_details->taxinfo[0]->tax_value,'tax_value'=>$tax_amt], 
													'shipping_info'=>['charge'=>$product_details->shipping_charge,'delivery_days'=>$product_details->delivery_days],  
												)
											)
										);											
						if ($cart_count < Cart::instance('myCart')->count())
						{
							$this->statusCode = 200;
							//$this->response = array_merge($this->response, $this->myCart(true));
							$this->op['msg'] = 'Product Successfully added to cart';
						}
					}
					else
					{
						$this->op['msg'] = 'Not Avaliable...';
					}
					
			} 
			else
            {
                $this->op['msg'] = 'Not avaliable to purchase...';
            }
		}			
		return Response::json($this->op, $this->statusCode, $this->headers, $this->options);			
    }
	
	public function GetMyCart ($isDirect = false)
    {
        $this->op['cart_count'] = number_format(Cart::instance('myCart')->count(), 0, '.', ',');
        $mycartcontent = json_decode(Cart::instance('myCart')->content(), true);
		
        $currency_symbol = '';
		$currency_code = '';
		$this->op['cart_sub_total'] = $this->op['cart_tax'] = $this->op['cart_shipping_charge'] = $this->op['cart_total'] = $this->op['cart_tax_perc'] = 0.00;
		$data = array();
		$cart_tax = 0;
		$data['tax_class_id'] = 1;
		$data['geo_zone_id'] = 1;		
        array_walk($mycartcontent, function(&$item) use(&$currency, $data)
        {				
            $item = (object) $item;
            $currency_code = $item->options['currency'];
			$currency_symbol = $item->options['currency_symbol'];			
            $item->price = number_format($item->price, 2, '.', ',');   			
			$this->op['cart_sub_total'] += $item->subtotal;
            $item->qty = number_format($item->qty, 0, '.', ',');
			$this->op['cart_tax_perc'] = $item->options['tax_info']['tax_perc'];			
			$this->op['cart_tax'] = $this->op['cart_tax'] + ($item->qty * ($item->price * $item->options['tax_info']['tax_perc'] / 100));
			$this->op['cart_shipping_charge'] = $this->op['cart_shipping_charge'] + ($item->qty * $item->options['shipping_info']['charge']);		
        });        
		if (isset($this->account_id)) {	
			$data['account_id'] = $this->account_id;   
			$res['billing_address'] = $this->HTTPApiObj->postRequest('get_billing_address', $data, 'POST');
			$this->op['UserDetails'] = $res['billing_address']; 
		} 		
		$this->op['cart_tax'] = number_format($this->op['cart_tax'], 2, '.', ','); 		
		$sub_total = $this->op['cart_sub_total'];
		$cart_tax = $this->op['cart_tax'];
		$this->op['cart_sub_total'] = number_format(($sub_total - $cart_tax), 2, '.', ',');   
		$this->op['cart_sub_total_with_tax'] = number_format(($sub_total), 2, '.', ',');   
			
		if ($sub_total > 0) {
			$this->op['cart_shipping_charge'] = number_format($this->op['cart_shipping_charge'], 2, '.', ',');     
		} else {
			$this->op['cart_shipping_charge'] = number_format(0, 2, '.', ',');
		}
        $this->op['cart_total'] = number_format(($sub_total + $this->op['cart_shipping_charge']), 2, '.', ',');  		
        $this->op['cart_content'] = $mycartcontent;            
		return Response::json($this->op, 200, $this->headers, $this->options);	
    }
	
	public function GetCart(Request $request)
    {	
			//Cart::instance('myCart')->destroy();
			$content = Cart::instance('myCart')->content();			
			return Response::json($content, 200, [], JSON_PRETTY_PRINT);	
    }
	
	public function myCart ()
    {
        $data['chechoutUrl'] = Url::to('checkout');  		
		return View('account.my_cart', $data);		
    }
	
	public function checkout ()
    {
		$res = $data = array();
		$locations = $this->HTTPVpiObj->postRequest('get-locations', $data, 'POST');
		$res['country'] = $locations->country;
		$res['state'] = $locations->state;
		$res['city'] = $locations->city;
		//echo '<pre>';print_r($res);exit;
		if (isset($this->account_id)) {	
			$data['account_id'] = $this->account_id;   
			$res['billing_address'] = $this->HTTPApiObj->postRequest('get_billing_address', $data, 'POST');
		}	
		
        return View('account.checkout', $res);
    }
	
	public function saveMyCartAddressDetails (Request $request)
    {	
		$ops = [];
		$ops['error'] = '';
        if (isset($this->account_id))
        {           
            $postData = $this->request->all();				
			Cookie::queue('myCart', $postData, 24 * 60);
			$this->statusCode = 200;
			//$ops['msg'] = 'ok';
        }
        else
        {
			$this->statusCode = 200;
            $ops['msg'] = 'Please Login to Proceed...';
        }
		
		return Response::json($ops, $this->statusCode);
    }
	
	public function saveMyCartPaymentDetails (Request $request)
    {		
        $postData = $request->all();	
			
		if ($this->account_id)
		{
			if (!empty($postData['paymode_id']))
			{			
				switch ($postData['paymode_id'])
				{
					case Config::get('constants.PAYMENT_MODES.COD'):													
						$postData['payment_type_id'] = Config::get('constants.PAYMENT_TYPES.COD');							
						$order = $this->placeOrder($postData);	
						//echo '<pre>';print_r($order);exit;
						return Response::json($order, 200);
						if ($order['status'] == 'error') {
							return $order['msg'];								
						} else {						
								$this->resp['msg'] = 'Order Successfully Placed';
								$this->resp['url'] = $order['url'];
						}
						break;
					case Config::get('constants.PAYMENT_MODES.MYCASH_WALLET'):		
						$postData['payment_type_id'] = Config::get('constants.PAYMENT_TYPES.MYCASH_WALLET');	
						$order = $this->placeMycash_wallet_Order($postData);	
						//echo '<pre>';print_r($order);exit;
						return Response::json($order, 200);
						if ($order['status'] == 'error') {
							return $order['msg'];								
						} else {						
								$this->resp['msg'] = 'Order Successfully Placed';
								$this->resp['url'] = $order['url'];
						}
						break;
				} 		
			}
			else
			{
				$this->resp['msg'] = 'Please select payment mode..';
			}		 		
		} else {
			 $this->resp['msg'] = 'Please Login to Proceed...';			 
		}
		$this->statusCode = 200;
		return Response::json($this->resp, $this->statusCode);
    }
	
	public function placeOrder ($arr = array())
    {		
        $data = [];
        if (!empty($this->account_id))
        {
            if (Cookie::get('myCart'))
            {				
				if (isset($arr['wallets'])) { 
					 $data['bonus_wallet_amt_available'] = $this->commonSettingsObj->getBonusWalletBalance($this->account_id);					 
					 if ($data['bonus_wallet_amt_available'] > $arr['wallets']) {
						$data['bonus_wallet_amt'] = $arr['wallets'];
					 } else {
						$data['bonus_wallet_amt'] = $data['bonus_wallet_amt_available'];
					 }					 
				} else {
					$arr['wallets'] = 0;					
				}  
				
                $data['account_id'] = $this->account_id;
                $data['full_name'] = $this->full_name;
                $data['email'] = $this->email;
                $data['mobile'] = $this->mobile;
				$contact = Cookie::get('myCart');
                //$data['billing'] = $contact['address']['billing'];
                $data['shipping'] = $contact['address']['shipping'];
                $amount = Cart::instance('myCart')->subtotal();
                $data['amount'] = (double) str_replace( ',', '', $amount );
                $data['products'] = json_decode(Cart::instance('myCart')->content(), true);                
                $data['item_qty'] =  Cart::instance('myCart')->count();
				$data['product_qty'] =  Cart::instance('myCart')->content()->groupBy('id')->count();				              
                $data['tax'] = 0;
                $data['discount'] = 0;
                $data['currency_id'] = $this->currency_id;                
                $data['payment_type_id'] = $arr['payment_type_id'];                
                $data['net_pay'] = $data['amount'] + $data['tax'] - $data['discount'];
				//$data['shipping_charge'] = 0;
				//return $data;
				if (!empty($data['products'])) {					
					$result = $this->HTTPVpiObj->postRequest('place-order', $data, 'POST');	
					//print_r($result);exit;
					//return $result;										
					if ($result->order_code) {
						$data['ecom_order_code'] = $result->order_code;
						$data['ecom_sub_order_code'] = $result->sub_order_code;
						$res = $this->HTTPApiObj->postRequest('place-order', $data, 'POST');	
						//$res = 1;						
						if ($res)
						{			
							$supplier_ords = array();														
							$account_type_id = 2;
							foreach ($data['products'] as $product)
							{
								$supplier_ords[$product['options']['supplier_id']]['products'][] = $product;
								$supplier_ords[$product['options']['supplier_id']]['qty'] = isset($supplier_ords[$product['options']['supplier_id']]['qty']) ? $supplier_ords[$product['options']['supplier_id']]['qty'] + $product['qty'] : $product['qty'];
								$supplier_ords[$product['options']['supplier_id']]['sub_total'] = isset($supplier_ords[$product['options']['supplier_id']]['sub_total']) ? $supplier_ords[$product['options']['supplier_id']]['sub_total'] + $product['price'] : $product['price'];
								$supplier_ords[$product['options']['supplier_id']]['shipping_charge'] = isset($supplier_ords[$product['options']['supplier_id']]['shipping_charge']) ? $supplier_ords[$product['options']['supplier_id']]['shipping_charge'] + $product['options']['shipping_info']['charge'] : $product['options']['shipping_info']['charge'];
								$supplier_ords[$product['options']['supplier_id']]['net_pay'] = isset($supplier_ords[$product['options']['supplier_id']]['net_pay']) ? $supplier_ords[$product['options']['supplier_id']]['net_pay'] + ($product['price'] + ($product['options']['shipping_info']['charge'] * $product['qty'])) : ($product['price'] + ($product['options']['shipping_info']['charge'] * $product['qty']));
								$supplier_ords[$product['options']['supplier_id']]['sub_order_code'] = $data['ecom_sub_order_code']->$product['options']['supplier_id'];								
							}
							$data['supplier_ords'] = $supplier_ords;	
							unset($data['products']);
							ShoppingPortal::notify('ORDER_PLACED_TO_PLACED.CUSTOMER', $this->account_id, $account_type_id, $data, true, true, true);
							$this->statusCode = 200;
						    Cart::instance('myCart')->destroy();							
							//$this->resp['url'] = URL::to('orderresponse/'.$result->order_code);
							$this->resp['url'] = URL::to('account/my-orders');
							$this->resp['msg'] = 'Thank You for ordering...';								
							$this->resp['status'] = 'ok';
						}					
					}
				} else {
					$this->resp['status'] = 'error';
					$this->resp['msg'] = 'No items in the cart';
				}
            } 
			else
            {
				$this->resp['status'] = 'error';
                $this->resp['msg'] = 'Some thing went worng...';
            }
        }
        else
        {
			$this->resp['status'] = 'error';
            $this->resp['msg'] = 'Please Login to Proceed...';
        }
        return $this->resp;        
    }
	
	public function placeMycash_wallet_Order ($arr = array())
    {
		//return $arr;
        $data = [];
        if (!empty($this->account_id))
        {
            if (Cookie::get('myCart'))
            {		
				$amount = Cart::instance('myCart')->subtotal();
				$data['amount'] = str_replace(",", "", $amount);
				
				if (isset($arr['wallets'])) {					 
					 $data['bonus_wallet_amt_available'] = $this->commonSettingsObj->getBonusWalletBalance($this->account_id);					 
					 if ($data['bonus_wallet_amt_available'] > $arr['wallets']) {
						$data['bonus_wallet_amt'] = $arr['wallets'];
					 } else {
						$data['bonus_wallet_amt'] = $data['bonus_wallet_amt_available'];
					 }				
					 $data['mycash_amount'] = $data['amount'] - $data['bonus_wallet_amt'];	
				} else {
					$arr['wallets'] = 0;
					$data['mycash_amount'] = $data['amount'];
				}				
				$data['mycash_amt_available'] = $this->commonSettingsObj->getMycashWalletBalance($this->account_id);
                $data['account_id'] = $this->account_id;
                $data['email'] = $this->email;
                $data['mobile'] = $this->mobile;
                $contact = Cookie::get('myCart');
                $data['shipping'] = $contact['address']['shipping'];				
                $data['products'] = json_decode(Cart::instance('myCart')->content(), true);
                $data['item_qty'] = Cart::instance('myCart')->count();
                //$data['product_qty'] = Cart::count(false);
                $data['product_qty'] = Cart::instance('myCart')->content()->groupBy('id')->count();	
                $data['tax'] = 0;
                $data['discount'] = 0;
                $data['currency_id'] = $this->currency_id;
                $data['qty'] = Cart::instance('myCart')->count();
                $data['payment_type_id'] = $arr['payment_type_id'];                
                $data['net_pay'] = $data['amount'] + $data['tax'] - $data['discount'];				
				//return $data;
				if (!empty($data['products'])) {					
					$result = $this->HTTPVpiObj->postRequest('place-order', $data, 'POST');	
					//return $result;
					//print_r($result);exit;
					if ($result->order_code) {
						$data['ecom_order_code'] = $result->order_code;
						$data['ecom_sub_order_code'] = $result->sub_order_code;
						$res = $this->HTTPApiObj->postRequest('place-order', $data, 'POST');	
						//return $res;
						//print_r($res);exit;
						if ($res)
						{
							$this->statusCode = 200;
							Cart::instance('myCart')->destroy();							
							$this->resp['url'] = URL::to('orderresponse/'.$result->order_code);
							$this->resp['msg'] = 'Thank You for ordering...';								
							$this->resp['status'] = 'ok';
						}					
					}
				} else {
					$this->resp['status'] = 'error';
					$this->resp['msg'] = 'No items in the cart';
				}
            } 
			else
            {
				$this->resp['status'] = 'error';
                $this->resp['msg'] = 'Some thing went worng...';
            }
        }
        else
        {
			$this->resp['status'] = 'error';
            $this->resp['msg'] = 'Please Login to Proceed...';
        }
        return $this->resp;        
    }
	
	public function order_details ($ordercode) 
	{		
		$data['order_code'] = $ordercode;
		$data['order_details'] = $this->userObj->get_order_details($data);		
		//print_r($data);exit;
		return View('account.order_response', $data);
	}
	
	
	public function updateProductQty (Request $request)
    {        
		if ($request->has('rowid') && $request->has('qty') && $request->get('qty') >= 0)
        {
			$data = $request->all();			
            if ($product_details = Cart::instance('myCart')->get($data['rowid']))
            {
                $product_details = json_decode(json_encode($product_details));
                $data['supplier_product_id'] = $product_details->options->supplier_product_id;
                //if ($this->productObj->haveStock($data))
				if ($data)
                {
                    Cart::instance('myCart')->update($data['rowid'], $data['qty']);
                    $this->op = array_merge($this->op, $this->myCarts(true));           
					
                }
                else
                {
                    $this->op['msg'] = 'Not avaliable to purchase...';
                }
            }
            else
            {
                $this->op['msg'] = 'Invalid Product';
            }
        }
        return Response::json($this->op);
    }
	
	public function removeFromCart (Request $request)
    {
        if ($request->has('rowid'))
        {
            $cart_count = Cart::instance('myCart')->count();
            $data = $request->all();
            Cart::instance('myCart')->remove($data['rowid']);
            if ($cart_count > Cart::instance('myCart')->count())
            {               
                $this->op['msg'] = 'Product Removed Succesfully.';
                $this->op = array_merge($this->op, $this->myCarts(true));
            }
        }
        return Response::json($this->op);
    }
	
	public function myCarts ($isDirect = false)
    {
        $this->op['cart_count'] = number_format(Cart::instance('myCart')->count(), 0, '.', ',');
        $mycartcontent = json_decode(Cart::instance('myCart')->content(), true);
        $currency = '';
        array_walk($mycartcontent, function(&$item) use(&$currency)
        {
            $item = (object) $item;
            $currency = $item->options['currency'].' '.$item->options['currency_symbol'].' ';
            $item->price = $currency.number_format($item->price, 2, '.', ',');
            $item->subtotal = $currency.number_format($item->subtotal, 2, '.', ',');
            $item->qty = number_format($item->qty, 0, '.', ',');
        });
        //$this->response['cart_total'] = $currency.number_format(json_decode(Cart::instance('myCart')->total(), true), 2, '.', ',');
        $this->op['cart_total'] = $currency.Cart::instance('myCart')->subtotal();
        $this->op['cart_content'] = $mycartcontent;
        $this->statusCode = 200;
        return ($isDirect) ? $this->op : Response::json($this->op, $this->statusCode, $this->headers, $this->options);
    }
}
