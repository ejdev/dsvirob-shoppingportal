<?php

Route::post('init', function()
{
    if (Request::ajax())
    {
     return Response::json(['Constants'=>Config::get('constants'), 'UserDetails'=>(Session::has('user_data') ? Session::get('user_data') : null)]);
    }
});
Route::group(['prefix'=>'helper'], function()
{
    Route::get('faq', 'SupportController@faq_categories_list');
    Route::get('faq/{category}', 'SupportController@faq_categories_list');
});

Route::group(['middlewareGroups'=>['web']], function ()
{
    Route::group(['prefix'=>'admin', 'as'=>'admin.', 'namespace'=>'Admin'], function()
    {
        Route::get('login', ['as'=>'login', 'uses'=>'AdminController@login']);
        Route::post('login', ['as'=>'loginCheck', 'uses'=>'AdminController@loginCheck']);
        Route::get('logout', ['as'=>'logout', 'uses'=>'AdminController@logout']);
        Route::post('forgot-password', ['as'=>'forgotPassword', 'uses'=>'AdminController@forgotPassword']);
        Route::get('dashboard', ['as'=>'dashboard', 'uses'=>'AdminController@dashboard']);
        Route::group(['prefix'=>'orders', 'as'=>'order.'], function()
        {
            Route::get('{status}', ['as'=>'list', 'uses'=>'AdminOrderController@orderList']);
            Route::post('/', ['as'=>'list-data', 'uses'=>'AdminOrderController@orderList']);
            Route::post('update-approval-status', ['as'=>'update-approval-status', 'uses'=>'AdminOrderController@updateOrderApprovalStatus']);
            Route::post('update-status', ['as'=>'update-status', 'uses'=>'AdminOrderController@updateOrderStatus']);
            Route::get('details/{sub_order_code}', ['as'=>'details', 'uses'=>'AdminOrderController@orderDetails']);
            Route::post('details/{order_code}', ['as'=>'details', 'uses'=>'AdminOrderController@orderDetails']);
        });
        Route::group(['prefix'=>'sub-orders', 'as'=>'sub-order.'], function()
        {
            Route::get('{status}', ['as'=>'list', 'uses'=>'AdminOrderController@subOrderList']);
            Route::post('/', ['as'=>'list-data', 'uses'=>'AdminOrderController@subOrderList']);
            Route::post('update-approval-status', ['as'=>'update-approval-status', 'uses'=>'AdminOrderController@updateSubOrderApprovalStatus']);
            Route::post('update-status', ['as'=>'update-status', 'uses'=>'AdminOrderController@updateSubOrderStatus']);
            Route::get('details/{sub_order_code}', ['as'=>'details', 'uses'=>'AdminOrderController@subOrderDetails']);
        });
        Route::group(['prefix'=>'order-items', 'as'=>'order-items.'], function()
        {
            Route::get('/', ['as'=>'list', 'uses'=>'AdminOrderController@orderItemsList']);
            Route::post('/', ['as'=>'list-data', 'uses'=>'AdminOrderController@orderItemsList']);
            Route::post('update-approval-status', ['as'=>'update-approval-status', 'uses'=>'AdminOrderController@updateOrderItemApprovalStatus']);
            Route::post('update-status', ['as'=>'update-status', 'uses'=>'AdminOrderController@updateOrderItemStatus']);
            Route::get('details/{sub_order_code}', ['as'=>'details', 'uses'=>'AdminOrderController@orderItemDetails']);
        });

        //category start

         Route::group(['prefix'=>'product-category', 'as'=>'product-category.'], function()
        {
            Route::get('/', ['as'=>'list', 'uses'=>'AdminCategoryController@categoryList']);
            Route::post('/', ['as'=>'list-data', 'uses'=>'AdminCategoryController@categoryList']);
            //Route::post('/', ['as'=>'list-data', 'uses'=>'AdminOrderController@orderItemsList']);
            
        });


        //category end

        Route::group(['prefix'=>'widthdraw', 'as'=>'withdraw.'], function()
        {
            Route::get('list/{status}', ['as'=>'list', 'uses'=>'AdminWithdrawalsController@withdrawal_list']);
            Route::post('list', ['as'=>'list-data', 'uses'=>'AdminWithdrawalsController@withdrawal_list']);
            Route::post('details', ['as'=>'details', 'uses'=>'AdminWithdrawalsController@withdrawalDetails']);
            Route::post('update-status', ['as'=>'update-status', 'uses'=>'AdminWithdrawalsController@updateWithdrawalStatus']);
        });
        Route::group(['prefix'=>'wallet', 'as'=>'wallet.'], function()
        {
            Route::get('transfer-history', ['as'=>'transfer-history', 'uses'=>'AdminWalletController@transferHistory']);
            Route::post('transfer-history', ['as'=>'transfer-history-data', 'uses'=>'AdminWalletController@transferHistory']);
            Route::get('transaction-log', ['as'=>'transaction-log', 'uses'=>'AdminWalletController@transactionLog']);
            Route::post('transaction-log', ['as'=>'transaction-log-data', 'uses'=>'AdminWalletController@transactionLog']);
        });
		Route::group(['prefix'=>'user', 'as'=>'users.'], function()
        {
            Route::any('list', ['as'=>'list', 'uses'=>'AdminUserController@UserList']);
			Route::any('orders/{account_id}', ['as'=>'user-orders', 'uses'=>'AdminUserController@UserOrderList']);            
        });
		Route::group(['prefix'=>'menu', 'as'=>'menu.'], function()
        {
            Route::any('create', ['as'=>'create', 'uses'=>'AdminNavigationController@create_navigation']);
			Route::any('save', ['as'=>'save', 'uses'=>'AdminNavigationController@save_navigation']);
			Route::any('list', ['as'=>'list', 'uses'=>'AdminNavigationController@navigations_list']);
			Route::any('top-menu-list', ['as'=>'topmenu', 'uses'=>'AdminNavigationController@top_menu_list']);
			Route::post('position-list', 'AdminNavigationController@menu_position_list');
			Route::post('group-list', 'AdminNavigationController@group_list');
			Route::post('get_link_details', 'AdminNavigationController@get_link_details'); 
			Route::post('delete_nav', 'AdminNavigationController@delete_nav'); 
			Route::post('get_menu', 'AdminNavigationController@get_menu');
			Route::post('save_menu_settings', 'AdminNavigationController@save_menu_settings');			
        });
		Route::group(['prefix'=>'commision', 'as'=>'commision.'], function()
        {            
            Route::any('earnings', ['as'=>'earnings', 'uses'=>'AdminEarningsController@EarningsList']);
        });
    });

    Route::group(['namespace'=>'User'], function()
    {
        Route::get('login', 'UserController@login');
        Route::post('login', 'UserController@login_check');
        Route::get('logout', 'UserController@logout');
        Route::post('forgot/pwd', 'UserController@forgot_password');
        Route::get('contact-us', 'UserController@contact_us');
        Route::post('user/register', 'UserController@user_register');
        Route::get('/', 'CommonController@home');
        Route::get('home', 'CommonController@home');
        Route::any('get-sliders', 'CommonController@getSliders');    // POST
        Route::post('check_email_phone', 'UserController@check_email_phone');
        Route::post('user/reset/pwd', 'UserController@reset_password');
        Route::get('user/reset/activation/{activation_key}', 'UserController@reset_activation_key');
        Route::get('user/register/activation/{activation_key}', 'UserController@check_register_activation_key');
        Route::post('check/register/otp', 'UserController@check_register_otp'); //used for registration otp
        Route::post('check/user/otp', 'UserController@check_user_otp'); //used for forgot pwd otp.
			
        Route::get('my-cart', 'CartController@myCart');
        Route::get('checkout', 'CartController@checkout');
        Route::post('save-mycart-details/address', 'CartController@saveMyCartAddressDetails');
			Route::post('save-mycart-details/payment', 'CartController@saveMyCartPaymentDetails');
        Route::post('get-paymodes', 'CommonController@getAvaliablePaymentModes');
        Route::get('orderresponse/{ordercode}', 'CartController@order_details');

			Route::get('{slug}/br', 'CommonController@browse_products');
			Route::any('product/list', 'CommonController@productsList');  // POST
        Route::post('products/sellers', 'OrderController@productSellers');
        Route::get('product/sellers/{category}/{product_name}', 'CommonController@productSellers');
        Route::post('get-wallet-types', 'CommonController@getWallet');
        //Route::any('get-bonus-wallet-balance', 'CommonController@getBonusWalletBalance');
        Route::group(['prefix'=>'account'], function()
        {
            Route::get('personal-information', ['as'=>'account.personal_info', 'uses'=>'MyAccountController@edit_personal_info']); 
			Route::post('update-personal-info', 'MyAccountController@update_personal_info');  // POST
			Route::get('change_password', ['as'=>'account.change_password', 'uses'=>'MyAccountController@change_password']);
            Route::post('savepassword', ['as'=>'account.savepassword', 'uses'=>'MyAccountController@savepassword']);
			Route::get('address', ['as'=>'account.address', 'uses'=>'MyAccountController@address']);
			Route::post('address/default', 'MyAccountController@default_address');
			Route::post('address/delete/{id}', 'MyAccountController@delete_address');
			Route::post('get_address_list', 'MyAccountController@get_address_list');
			Route::post('saveaddress', 'MyAccountController@saveaddress');			
			Route::get('account_email_update', ['as'=>'account.email_update', 'uses'=>'MyAccountController@account_email_update']);
            Route::post('verify_email', 'MyAccountController@verify_email');
            Route::post('checkverificationcode', 'MyAccountController@checkverificationcode');
			Route::post('updateemail', 'MyAccountController@updateemail');
            Route::post('verify_mobile', 'MyAccountController@verify_mobile');
			
            Route::post('get_state_phonecode', 'MyAccountController@get_state_phonecode');
            Route::post('get_city', 'MyAccountController@get_city');
            
            Route::get('my-orders', 'OrderController@loadMyOrders');
            Route::post('my-orders', 'OrderController@getMyOrders');
            Route::post('my-orders-items', 'OrderController@getMyOrderDetails');
			
            Route::post('my-orders/cancel/{sub_order_id}', 'OrderController@orderCancel');
            Route::post('my-orders/item-cancel/{order_item_id}', 'OrderController@orderItemCancel');
            Route::get('my-orders/{order_code}', 'OrderController@order_details');
            Route::post('my-orders-details', 'OrderController@myOrdersDetails');
            Route::get('my-orders/{sub_ord_id}', 'OrderController@get_Sub_Order_Details');
            Route::get('account_deactivate', ['as'=>'account.account_deactivate', 'uses'=>'MyAccountController@account_deactivate']);
            Route::post('savedeactivate', 'MyAccountController@save_deactivate');
            Route::any('wishlist', 'MyAccountController@MyWishList');
            Route::post('remove-wishlist', 'MyAccountController@remove_wish_list');
        });
        Route::post('rating/{post_type}', 'CommonController@rating');
        Route::post('review/like', 'CommonController@likeReview');
        Route::post('review/unlike', 'CommonController@unlikeReview');
        Route::post('review/abuse', 'CommonController@report_abuse');
        Route::post('add-to-wish-list', 'MyAccountController@addToWishList');
        Route::group(['prefix'=>'product'], function()
        {
            Route::any('my-cart', 'CartController@GetMyCart');
            Route::get('{category}/{product_name}', 'CommonController@loadproductDetails');
            Route::any('details', 'CommonController@getproductsDetails');
            Route::any('add-to-cart', 'CartController@addToCart');
            Route::any('reviews', 'CommonController@getReviews');
            Route::any('getcart', 'CartController@GetCart');
            Route::post('change-cart-item-qty', 'CartController@updateProductQty');
            Route::post('remove-from-cart', 'CartController@removeFromCart');
        });
		Route::get('discount-percentage', 'CommonController@getDiscountPercentage');
    });
});

Route::get('checkMail', 'FrontController@checkMail');   //  Post

Route::group(['prefix'=>'api/v1', 'namespace'=>'Api\v1', 'middlewareGroups'=>'web'], function()
{
	
	Route::group(['prefix'=>'admin'], function()
    {
        Route::group(['prefix'=>'user'], function()
        {
            Route::any('list', 'ApiAdminUserController@UserList');            
        });       
    });
	
	
    Route::any('auth/login', 'AuthController@loginCheck');   //  Post
    Route::any('get-menus', 'ApiMenuController@menus');  //  Get
    Route::any('get-payment-settings', 'ApiUserController@get_payment_settings');  //Get
    Route::any('get_billing_address', 'ApiUserController@get_billing_address');  // Post
    Route::any('get-wallet', 'ApiWalletController@getWallet');  // Post
    Route::group(['middleware'=>'authapp'], function()
    {
        Route::any('account/wishlist', 'ApiWishlistController@get_wish_list');    //POST
    });
    Route::any('get-profit-sharing-info', 'ApiWalletController@getProfitSharingInfo');  // POST
    Route::any('validate/user', 'ApiUserController@validateUser');   // POST
    Route::any('place-order', 'ApiUserController@placeOrder');      // POST
    Route::any('account/add-to-wishlist', 'ApiWishlistController@add_to_wish_list');    //POST
    Route::any('account/remove-wishlist', 'ApiWishlistController@remove_wish_list');  // POST
    Route::any('my-orders', 'ApiOrderController@MyOrderList');  //POST
    Route::any('order-details', 'ApiOrderController@OrderDetails');  //POST
	Route::any('get_personal_info', 'ApiUserController@get_personal_info');   // POST
	Route::any('update-personal-info', 'ApiUserController@update_personal_info');   // POST	
	Route::any('check-user-existing-password', 'ApiUserController@check_user_password');  // POST
	Route::any('update-password', 'ApiUserController@save_password');      // POST	
	Route::any('get_address_list', 'ApiUserController@get_address_list');  // POST
	Route::any('default_address', 'ApiUserController@default_address');    // POST
	Route::any('delete-address', 'ApiUserController@delete_address');      // POST
	Route::any('save-address', 'ApiUserController@save_address');          // POST	
	Route::any('verification-email', 'ApiUserController@verify_email');    // POST
	Route::any('check-verification-code', 'ApiUserController@checkverificationcode');    // POST
	Route::any('update-email', 'ApiUserController@update_email');         // POST
	Route::any('verify-mobile', 'ApiUserController@verify_mobile');       // POST
	Route::any('account-deactivate', 'ApiUserController@save_deactivate');  // POST	
	Route::any('getDiscountPercentage', 'ApiUserController@getDiscountPercentage');  // GET	
	
	
    //Route::post('update-ordercode', 'ApiUserController@updateOrdercode');
    Route::post('user/forgot/pwd', 'ApiUserController@forgot_password');
    Route::post('user/registers', 'ApiUserController@register_user');
    Route::post('user/activation/key', 'ApiUserController@reset_activation_key'); 
    Route::post('check/user/otp', 'ApiUserController@check_user_otp');
    Route::post('user/reset/pwd', 'ApiUserController@reset_pwd');       
    Route::post('accountdeactivate', 'ApiUserController@account_deactivate');    
    Route::post('helper_faq', 'APISupportController@helper_faq');
    Route::get('helper_faq', 'APISupportController@helper_faq');
    Route::post('helper_faq/{category}', 'APISupportController@helper_faq');
    Route::post('helper_faq', 'APISupportController@helper_faq');
    Route::post('getfeatures_faqs', 'APISupportController@getfeatures_faqs');
});
