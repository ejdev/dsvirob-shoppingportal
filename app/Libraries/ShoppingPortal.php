<?php

class ShoppingPortal
{

    public static function recentProducts ($supplier_product_id)
    {
        $recentPros = [];
        if (Cookie::get('recentPros'))
        {
            $recentPros = Cookie::get('recentPros');
        }
        if (!in_array($supplier_product_id, $recentPros))
        {
            $recentPros[] = $supplier_product_id;
        }
        $cookie = Cookie::forever('recentPros', $recentPros);
        return (object) ['recentPros'=>$recentPros, 'cookie'=>$cookie];
    }

    /**
     * @param string $which which mail to be send
     * @param int|array $id notification to whom single id or array of ids
     * @param int $account_type_id user type id <i>(default 2-customer)</i>
     * @param array $data datas required to frame a content <i>(default empty)</i>
     * @param bool $email send email <i>(default true)</i>
     * @param bool $sms send sms <i>(default true)</i>
     * @param bool $notification send notification <i>(default true)</i>
     * @param bool $must_send it must be sent without any checking <i>(default false)</i>
     *
     * @return array|false sent user list or false
     */
    public static function notify ($which, $id, $account_type_id = 2, $data = [], $email = true, $sms = true, $notification = true, $must_send = false)
    {
        $id = is_array($id) ? array_filter($id) : [$id];
        if (!empty($id))
        {
            switch ($account_type_id)
            {
                /* case Config::get('constants.ACCOUNT_TYPE.SUPPLIER'):
                    $account_ids = DB::table(Config::get('tables.ACCOUNT_SUPPLIERS'))
                            ->whereIn('supplier_id', $id)
                            ->lists('account_id');
                    break; */
                case Config::get('constants.ACCOUNT_TYPE.ADMIN'):
                    $account_ids = DB::table(Config::get('tables.ADMIN_MST'))
                            ->where('status', Config::get('constants.ACTIVE'))
                            ->whereIn('admin_id', $id)
                            ->lists('account_id');
                    break;
                /* case Config::get('constants.ACCOUNT_TYPE.PARTNER'):
                    $account_ids = DB::table(Config::get('tables.PARTNER'))
                            ->where('status', Config::get('constants.ACTIVE'))
                            ->whereIn('partner_id', $id)
                            ->lists('account_id');
                    break; */
                default:
                    $account_ids = $id;
            }
            $account_ids = array_filter($account_ids);
            if (!empty($account_ids))
            {
                $query = DB::table(Config::get('tables.ACCOUNT_LOGIN_MST').' as alm')
                        ->join(Config::get('tables.ACCOUNT_MST').' as am', 'am.account_id', '=', 'alm.account_id')
                        ->join(Config::get('tables.ACCOUNT_PREFERENCE').' as ap', 'ap.account_id', '=', 'alm.account_id')
                        ->where('am.is_deleted', Config::get('constants.OFF'))
                        ->where('am.status_id', Config::get('constants.ACTIVE'))
                        ->whereIn('alm.account_id', $account_ids)
                        ->whereNotNull('alm.email')
                        ->whereNotNull('alm.mobile')
                        ->selectRaw('alm.account_id, concat(am.firstname," ",am.lastname) as name,alm.uname,alm.email,alm.mobile,ap.send_email,ap.send_sms,ap.send_notification,ap.is_mobile_verified,ap.is_email_verified');
                $users_list = $query->get();
                //$data['pagesettings'] = (object) Config::get('site_settings');
                $pagesettings = (object) Config::get('site_settings');                
                $settings = (object) Config::get('notify_settings.'.strtoupper($which));					
                if (!empty($settings))
                {
                    foreach ($users_list as $user)
                    {						
                        $data['name'] = $user->name;
                        $data['uname'] = $user->uname;
						$data['pagesettings'] = $pagesettings;						
                        if ($must_send || ($email && $user->send_email && $user->is_email_verified))
                        {                            
                            return Mailer::send($user->email, $settings->email['view'], Lang::get($settings->email['subject']), $data);
                        }						
                        if ($must_send || ($sms && $user->send_sms && $user->is_mobile_verified))
                        {							
                            // SMS::send($user->mobile, Lang::get($settings->sms['message']));
                        }
                       /* if ($must_send || ($notification && $user->send_notification))
                        {
                            PushNotification::send($user->account_id, Lang::get($settings->notification['title'], $data), Lang::get($settings->notification['body'], $data), $settings->notification['click_action']);
                        } */
                    }
                    //return $users_list;
                }
            }
        }
        return false;
    }

}
