<?php
namespace App\Libraries;

use DB;
use Mail;

class Mailer
{

    /**
     *
     * @param string $to whom to be mail send
     * @param string $view which mail view is to be used
     * @param string $subject subject of the mail
     * @param array $data datas which are used in view
     * @param string $email from email-id<i>(Optional)</i>
     * @param string $sender_name from user name<i>(Optional)</i>
     * @return array|bool response of mail sent or false
     */
    public static function send ($to, $view, $subject, $data, $email = '', $sender_name = '')
    {			
		$settings = DB::table(config('tables.EMAIL_SETTINGS'))
                ->select('driver_type', 'email', 'sender_name', 'settings')
                ->where('status', config('constants.ACTIVE'))
                ->where('is_deleted', config('constants.OFF'))
                ->first();
        $settings->email = !empty($email) ? $email : $settings->email;
        $settings->sender_name = !empty($sender_name) ? $sender_name : $settings->sender_name;
        if (!empty($settings))
        {
            //$settings = json_decode(stripslashes($settings->settings));
			$settings->settings = json_decode(stripslashes($settings->settings));
			
            if ($settings->driver_type == 1)//SMTP
            {				
				$mail = $settings->settings;
                config('mail', array(
                    'driver'=>'smtp',
                    'host'=>$mail->host,
                    'port'=>$mail->port,
                    'from'=>array('address'=>$settings->email, 'name'=>$settings->sender_name),
                    'encryption'=>$mail->encryption,
                    'username'=>$mail->username,
                    'password'=>$mail->password,
                    'sendmail'=>'/usr/sbin/sendmail -bs',
                    'pretend'=>false
                ));				
				//$vv =  View('emails.customer.orders')->render();print_r($data);exit();
				$vv = view($view, $data)->render();
				//print_r($view);die;
				
                 return Mail::send($view, $data, function($message) use ($to, $subject)
                        {
                            $message->to($to)->subject($subject);
                        });
            }
            else if ($settings->driver_type == 2) //SendGrid
            {
                $settings = config('services.sendgrid');
                $settings->api_user = !empty($settings->api_user) ? $settings->api_user : $settings['api_user'];
                $settings->api_key = !empty($settings->api_key) ? $settings->api_key : $settings['api_key'];
                $request = curl_init($settings['url']);
                curl_setopt($request, CURLOPT_POST, true);
                curl_setopt($request, CURLOPT_POSTFIELDS, array(
                    'api_user'=>$settings->api_user,
                    'api_key'=>$settings->api_key,
                    'to'=>$to,
                    'subject'=>$subject,
                    'text'=>'',
                    'html'=>view($view, $data)->render(),
                    'from'=>$settings->email,
                    'fromname'=>$settings->sender_name
                ));
                curl_setopt($request, CURLOPT_HEADER, false);
                curl_setopt($request, CURLOPT_SSLVERSION, false);
                curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($request);
                curl_close($request);
                return $response; 
            }
        }
        return false;
    }

}
