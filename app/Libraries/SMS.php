<?php

class SMS
{

    /**
     *
     * @param string $mobile whom SMS to be send
     * @param string $message who to be send
     *
     * @return array|false response of sent SMS or false
     */
    public static function send ($mobile, $message)
    {
        if (!empty($mobile) && !empty($message))
        {
            $settings = Config::get('services.sms');			
            $data = http_build_query(array(
                'user'=>$settings['user'],
                'key'=>$settings['key'],
                'senderid'=>$settings['senderid'],
                'mobile'=>$mobile,
                'message'=>$message,
                'accusage'=>1
            ));			
            $request = curl_init($settings['url'].$data);
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($request, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($request, CURLOPT_HEADER, 0);
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($request);
            $status = curl_getinfo($request, CURLINFO_HTTP_CODE);
            if ($response === FALSE)
            {
                die('Curl failed: '.curl_error($request));
            }
            curl_close($request);
            return $response;
        }
        return false;
    }

}
