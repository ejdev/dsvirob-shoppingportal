$(function () {
    var t = $('#withdraw-list'), f = $('#withdraw-list-form');
    t.DataTable({
        ordering: true,
        serverSide: true,
        processing: true,
        autoWidth: false,
        pagingType: 'input_page',
        sDom: "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
        oLanguage: {
            sLengthMenu: "_MENU_"
        },
        ajax: {
            url: f.attr('action'),
            type: 'POST',
            data: function (d) {
                $.extend(d, f.serializeObject());
            },
        },
        columns: [
            {
                data: 'Fcreated_on',
                name: 'created_on',
                class: 'text-center no-wrap',
                responsivePriority: 1
            },
            {
                data: 'full_name',
                name: 'full_name',
                responsivePriority: 2
            },
            {
                data: 'transaction_id',
                name: 'transaction_id',
                responsivePriority: 3
            },
            {
                data: 'Famount',
                name: 'amount',
                class: 'text-right no-wrap',
                responsivePriority: 4
            },
            {
                data: 'Fhandleamt',
                name: 'handleamt',
                class: 'text-right no-wrap',
                responsivePriority: 5
            },
            {
                data: 'Fpaidamt',
                name: 'paidamt',
                class: 'text-right no-wrap',
                responsivePriority: 10
            },
            {
                data: 'payment_type',
                name: 'payment_type',
                class: 'text-center no-wrap',
                responsivePriority: 11
            },
            {
                data: 'Fconfirmed_on',
                name: 'confirmed_on',
                class: 'text-center',
                render: function (data, type, row, meta) {
                    meta.settings.aoColumns[meta.col].bVisible = (row.status_id == 1 ? true : false);
                    return  row.Fconfirmed_on;
                }
            },
            {
                data: 'Fcancelled_on',
                name: 'cancelled_on',
                class: 'text-center',
                render: function (data, type, row, meta) {
                    meta.settings.aoColumns[meta.col].bVisible = (row.status_id == 3 ? true : false);
                    return  row.Fcancelled_on;
                }
            },
            {
                data: 'Fexpected_on',
                name: 'expected_on',
                class: 'text-center no-wrap',
                responsivePriority: 7,
                render: function (data, type, row, meta) {
                    meta.settings.aoColumns[meta.col].bVisible = (row.status_id != 1 && row.status_id != 3 ? true : false);
                    return  row.cancelled_on;
                }
            },
            {
                data: 'transaction_id',
                name: 'transaction_id',
                class: 'text-center',
                orderable: false,
                responsivePriority: 6,
                render: function (data, type, row, meta) {
                    return AddOptions(row);
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
    });
    function AddOptions(row) {
        var content = $('<div>').attr({class: 'pull-right', style: 'position: relative !important;'});
        content.append([
            $('<button>').attr({class: 'btn btn-xs btn-primary dropdown-toggle', 'data-toggle': 'dropdown'})
                    .append($('<span>').attr({class: 'caret'})),
            $('<ul>').attr({class: 'dropdown-menu dropdown-menu-right', role: 'menu'}).append(function () {
                var options = [];
                if (row.actions != undefined) {
                    $.each(row.actions, function (k, v) {
                        var data = {};
                        var data = {};
                        $.each(v.data, function (key, val) {
                            data['data-' + key] = val;
                        });
                        if (v.url != undefined) {
                            if (v.target != undefined) {
                                data['target'] = v.target;
                            }
                            else if (v.class != undefined) {
                                data['class'] = v.class;
                            }
                            else {
                                data['class'] = 'actions';
                            }
                        }
                        else {
                            data['class'] = 'show-modal';
                        }
                        options.push($('<li>').append($('<a>').attr($.extend({href: v.url}, data)).text(v.title)));
                    });
                }
                return options;
            })
        ]);
        return content[0].outerHTML;
    }
    $('#withdraw-list,#withdrawal_details').on('click', '.actions', function (e) {
        e.preventDefault();
        $('.alert').remove();
        var Ele = $(this), data = Ele.data();
        if (Ele.data('confirm') == undefined || (Ele.data('confirm') != null && Ele.data('confirm') != '' && confirm(Ele.data('confirm')))) {
            if (data.confirm != undefined) {
                delete data.confirm;
            }
            $.ajax({
                url: Ele.attr('href'),
                data: data,
                dataType: 'json',
                type: 'POST',
                success: function (data) {
                    t.dataTable().fnDraw();
                }
            });
        }
    });
    $('#withdraw-list').on('click', '.withdraw-details', function (e) {
        e.preventDefault();
        $('.alert').remove();
        var CurEle = $(this), data = CurEle.data();
        if (CurEle.data('confirm') == undefined || (CurEle.data('confirm') != null && CurEle.data('confirm') != '' && confirm(CurEle.data('confirm')))) {
            if (data.confirm != undefined) {
                delete data.confirm;
            }
            $.ajax({
                url: CurEle.attr('href'),
                data: data,
                dataType: 'JSON',
                type: 'POST',
                success: function (data) {
                    $('#withdraw_block').hide();
                    $('#withdrawal_details').show();
                    $('.panel-title', $('#withdrawal_details')).html(data.details.transaction_id);
                    $('.options', $('#withdrawal_details')).html(AddOptions(data));
                    $.each($('p.form-control-static', $('#withdrawal_details')), function (k, e) {
                        $('#' + $(e).attr('id')).html(data.details[$(e).attr('id')]);
                    });
                    if (data.details.conversion_details != undefined && data.details.conversion_details != null) {
                        $('#conversion_details').show();
                        $('tbody', $('#conversion_details')).html('');
                        $.each(data.details.conversion_details, function (k, e) {
                            console.log(e);
                            $('tbody', $('#conversion_details')).append([
                                $('<tr>').append([
                                    $('<td>').html(e.wallet),
                                    $('<td>', {class: 'text-right'}).html(e.from_amount),
                                    $('<td>', {class: 'text-right'}).html(e.to_amount),
                                    $('<td>', {class: 'text-center'}).html(e.debit_transaction_id),
                                    $('<td>', {class: 'text-center'}).html(e.credit_transaction_id)
                                ])
                            ])
                        });
                    }
                    else {
                        $('#conversion_details').hide();
                    }
                    if (data.details.account_info != undefined && data.details.account_info != null) {
                        $('#account_info').show();
                        $('tbody', $('#account_info')).html('');
                        $.each(data.details.account_info, function (k, e) {
                            $('tbody', $('#account_info')).append([
                                $('<tr>').append([
                                    $('<td>').html(k),
                                    $('<td>').html(e)
                                ])
                            ])
                        });
                    }
                    else {
                        $('#account_info').hide();
                    }
                }
            });
        }
    });
    $('.close-withdrawal_details').on('click', function (e) {
        e.preventDefault();
        $('#withdraw_block').show();
        $('#withdrawal_details').hide();
    });
    $(document.body).on('click', '#close_cancel', function (e)
    {
        $('#withdraw_block').show();
        $('#cancel_info').html('');
        if ($('#status option:selected').val() != '')
        {
            $('.panel-title').text($('#status option:selected').text() + ' Withdrawal');
        }
    });
    $('#search-btn').click(function (e) {
        t.dataTable().fnDraw();
    });
    $('#reset-btn').click(function (e) {
        f.resetForm();
        t.dataTable().fnDraw();
    });
});
