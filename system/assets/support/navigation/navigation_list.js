$(document).ready(function () {

    $('#search').click(function (e) {
        e.preventDefault();
        $("#menu_nav_list").dataTable({
            "bPaginate": true,
            "bProcessing": true,
            "bAutoWidth": false,
            "bDestroy": true,
            "bInfo": true,
            "bSort": true,
            "processing": true,
            "serverSide": true,
            "bFilter": true,
            "order": [[0, "desc"]],
            "sDom": "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
            "oLanguage": {
                "sLengthMenu": "_MENU_"
            },
            "ajax": {
                "type": "POST",
                "data": {
                    "search_term": $('#supplier_product_list_form #search_term').val(),
                    "category_id": $('#supplier_product_list_form #category_id').val(),
                }
            },
            "columns": [
                {
                    "data": "created_on",
                    "name": "created_on",
                    "class": "text-center",
                    "render": function (data, type, row, meta) {
                        return new String(row.created_on).dateFormat("dd-mmm-yyyy HH:MM:ss");
                    }
                },
                {
                    "data": "menu_name",
                    "name": "menu_name"
                },
                {
                    "data": "navigation_name",
                    "name": "navigation_name",
                },
                {
                    "name": "type",
                    "data": "type",
                },
                {
                    "name": "total_columns",
                    "data": "total_columns",
                },
                {
                    "name": "column",
                    "data": "column",
                },
                {
                    "class": "text-center",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var json = $.parseJSON(meta.settings.jqXHR.responseText);
                        var action_buttons = '<div class="btn-group">';
                        action_buttons += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>';
                        action_buttons += '<ul class="dropdown-menu pull-right" role="menu">';
                        action_buttons += '<li><a  data-navigation_id="' + row.navigation_id + '" class="edit_product" href="' + json.url + '/admin/preference/nav_mgt/edit">Configure</a></li>';
                        action_buttons += '</ul></div>';
                        return action_buttons;
                    }
                }
            ]
        });
    });
    $('#search').trigger('click');
    $('#new_menu_btn').click(function (e) {
        e.preventDefault();
        $('#list').hide();
        $('#menu').show();
    });
    $('#new_nav_btn').click(function (e) {
        e.preventDefault();
        $('#list').hide();
        $('#form').show();
    });
    $('#menu_form #cancel_btn').click(function (e) {
        e.preventDefault();
        $('#form').fadeOut('fast', function () {
            $('#list').fadeIn('fast');
        });
        $('#menu').fadeOut('fast', function () {
            $('#list').fadeIn('fast');
        });
    });
    $('#menu_form').on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            dataType: "json",
            data: $('#menu_form').serialize(),
            url: $('#menu_form').attr('action'),
            type: 'post',
            beforeSend: function () {
                
            },
            success: function (data) {
                
                $('#message').html(data.msg);
                if (data.status == 'OK' || data.status == 'WARN') {
                    $('#list').show();
                    $('#form').hide();
                    $('#search').trigger('click');
                }
            },
            error: function () {
                
                alert('something went wrong');
            }
        });

    });


});
