var modal_form_data = '';
var column_selected = '';
var nav_settings = '';
$(document).ready(function () {

	$('#sel_menu').on('change', function (event) {
        var menu_id = $('#sel_menu option:selected').attr('id');			
        $('#top_menu').loadSelect({
            firstOption: {key: '', value: 'Select'},
            firstOptionSelectable: true,
            data: {menu_id: menu_id},
            url: ajaxUrl + 'admin/menu/top-menu-list',
            key: 'navigation_id',
            value: 'navigation_name',
            cache: false,
        });
    });	
	
    $('#search').click(function (e) {
        e.preventDefault();
        $("#menu_nav_list").dataTable({
            "bPaginate": true,
            "bProcessing": true,
            "bAutoWidth": false,
            "bDestroy": true,
            "bInfo": true,
            "bSort": true,
            "processing": true,
            "serverSide": true,
            "bFilter": true,
            "order": [[0, "desc"]],
            "sDom": "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
            "oLanguage": {
                "sLengthMenu": "_MENU_"
            },
            "ajax": {
                "type": "POST",
                "url": ajaxUrl + 'admin/menu/list',
                "data": {
                    "search_term": $('#create_menu_nav #search_term').val(),
                    "menu_id": $("#sel_menu option:selected").attr('id'),
                    "top_menu_filter": $('#create_menu_nav #top_menu').val(),
                }
            },
            "columns": [
                {
                    "data": "created_on",
                    "name": "created_on",
                    "class": "text-center",
                    "render": function (data, type, row, meta) {
                        //return new String(row.created_on).dateFormat("dd-mmm-yyyy HH:MM:ss");
                        return row.created_on;
                    }
                },
                {
                    "data": "navigation_name",
                    "name": "navigation_name",
                },
                {
                    "data": "top_menu",
                    "name": "top_menu",
                },
                {
                    "name": "type",
                    "data": "type",
                    "render": function (data, type, row, meta) {
                        var type = {1: 'Top Menu', 2: 'Parent Link', 3: 'Link'};
                        return type[row.type];

                    }
                },
                {
                    "class": "text-center",
                    "orderable": false,
                    "render": function (data, type, row, meta) {
                        var json = $.parseJSON(meta.settings.jqXHR.responseText);
                        var action_buttons = '<div class="btn-group">';
                        action_buttons += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>';
                        action_buttons += '<ul class="dropdown-menu pull-right" role="menu">';
                        action_buttons += '<li><a data-link_type="' + row.type + '" data-navigation_id="' + row.navigation_id + '" class="edit_link" href="javascript:void(0)">Configure</a></li>';
                        action_buttons += '<li><a data-link_type="' + row.type + '" data-navigation_id="' + row.navigation_id + '" class="delete_link" href="javascript:void(0)">Delete</a></li>';
                        action_buttons += '</ul></div>';
                        return action_buttons;
                    }
                }
            ]
        });
    });
	
	


    $('#search').trigger('click');


    $('#create_navigation #link_type').on('change', function () {

        $('#edit_link').show();
        $('#edit_link').text($(this).val());
    });
    $('#menu_nav_list_wrapper #menu_nav_list').on('click', '.delete_link', function () {
        var navigation_id = $(this).data('navigation_id');
        var type = $(this).data('link_type');
        if (confirm("Are you sure you want to delete this?")) {
            $.ajax({
                url: ajaxUrl + 'admin/menu/delete_nav',
                type: "post",
                dataType: "JSON",
                data: {navigation_id: navigation_id, type: type},
                beforeSend: function () {

                },
                success: function (data) {

                    $('.errMsg').after('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                    if (data.status == 'ok')
                    {
                        $('#search').trigger('click');
                    }

                },
            });
        }
        else
        {
            return false;
        }
    });

    $('#menu_nav_list_wrapper #menu_nav_list').on('click', '.edit_link', function () {
        $('.panel_controls').hide();
        $('#create_navigation').show();
        $('#create_navigation #type').val($(this).data('link_type')).trigger('change');
        $('#list').hide();
        var navigation_id = $(this).data('navigation_id');
        var type = $(this).data('link_type');
        $.ajax({
            url: ajaxUrl + 'admin/menu/get_link_details',
            type: "post",
            dataType: "JSON",
            data: {navigation_id: navigation_id, type: type},
            beforeSend: function () {

            },
            success: function (res) {
				console.log(res);
				if ($("#sel_menu option:selected").attr('id')) {
					var mid = $("#sel_menu option:selected").attr('id');
				} else {
					var mid = res.data.menu_id;
				}
				
                $('.top_menu_id').loadSelect({
                    firstOption: {key: '', value: 'Select'},
                    firstOptionSelectable: true,
                    url: ajaxUrl + 'admin/menu/top-menu-list',
                    //data: {menu_id: $("#sel_menu option:selected").attr('id')},
                    data: {menu_id: mid},
                    key: 'navigation_id',
                    value: 'navigation_name',
                    optionData: [{key: 'no_of_columns', value: 'no_of_columns'}],
                    selected: res.data.parent_navigation_id,
                });
                $(".top_menu_id").find("option[value='" + res.data.parent_navigation_id + "']").attr('selected', true).trigger('change');
                $('#group_id').loadSelect({
                    firstOption: {key: '', value: 'Select'},
                    firstOptionSelectable: true,
                    url: ajaxUrl + 'admin/menu/group-list',
                    //data: {menu_id: $("#sel_menu option:selected").attr('id')},
                    data: {menu_id: mid},
                    key: 'navigation_id',
                    value: 'navigation_name',
                    selected: res.data.group_link,
                });
                $('#navigation_id').html('<input type=hidden name="navigation_id" value="' + navigation_id + '"/>')
                $('#no_of_columns').val(res.data.no_of_columns);
                $('#navigation_name').val(res.data.navigation_name);
                $('#navigation_url').val(res.data.navigation_url);
                $('#create_menu_nav').attr('id', 'edit_navigation');
                column_selected = res.data.column_no;
                nav_settings = $.parseJSON(res.data.nav_settings);
                if (res.data.is_login_required == 1)
                {
                    $('#is_login_required').prop('checked', true);
                }
                else
                {
                    $('#is_login_required').prop('checked', false);
                }
                $(".top_menu_id").find("option[value='" + res.data.parent_navigation_id + "']").attr('selected', true).trigger('change');
                $("#link_type").find("option[value='" + res.data.link_type + "']").attr('selected', true).trigger('change');
                $('#create_navigation .top_menu_id').on('change', function () {

                    var n = $("#create_navigation .top_menu_id option:selected").attr('data-no_of_columns');
                    var append_option = '';
                    $('#create_navigation #column').html('');
                    for (var i = 1; i <= n; i++)
                    {
                        if (column_selected != '' && column_selected == i)
                        {
                            append_option = append_option + '<option selected="selected"value="' + i + '">' + i + '</option>'
                        }
                        else
                        {
                            append_option = append_option + '<option value="' + i + '">' + i + '</option>';
                        }
                    }
                    $('#create_navigation #column').html(append_option);
                });
            },
        });
    });
    $('#create_navigation .top_menu_id').on('change', function () {

        var n = $("#create_navigation .top_menu_id option:selected").attr('data-no_of_columns');
        var append_option = '';
        $('#create_navigation #column').html('');
        for (var i = 1; i <= n; i++)
        {
            if (column_selected != '' && column_selected == i)
            {
                append_option = append_option + '<option selected="selected"value="' + i + '">' + i + '</option>'
            }
            else
            {
                append_option = append_option + '<option value="' + i + '">' + i + '</option>';
            }
        }
        $('#create_navigation #column').html(append_option);
    });
    $('#type').on('change', function () {
        switch ($("#type option:selected").attr('id'))
        {
            case "top_menu":
                $('#topmenu').show();
                $('#normal').hide();
                $('#group_select').hide();
                $('#no_of_columns').show();
                $('#choose_columns').hide();
                break;
            case "norm":
                $('#group_select').show();
                $('#topmenu').hide();
                $('#normal').show();
                $('#no_of_columns').hide();
                $('#choose_columns').show();
                $('.top_menu_id').loadSelect({
                    firstOption: {key: '', value: 'Select'},
                    firstOptionSelectable: true,
                    url: ajaxUrl + 'admin/menu/top-menu-list',
                    data: {menu_id: $("#sel_menu option:selected").attr('id')},
                    key: 'navigation_id',
                    value: 'navigation_name',
                    optionData: [{key: 'no_of_columns', value: 'no_of_columns'}]
                });
                $('#group_id').loadSelect({
                    firstOption: {key: '', value: 'Select'},
                    firstOptionSelectable: true,
                    url: ajaxUrl + 'admin/menu/group-list',
                    data: {menu_id: $("#sel_menu option:selected").attr('id')},
                    key: 'navigation_id',
                    value: 'navigation_name'
                });
                break;
            case "grp":
                $('#normal').show();
                $('#topmenu').hide();
                $('#group_select').hide();
                $('#no_of_columns').hide();
                $('#choose_columns').show();
                $('.top_menu_id').loadSelect({
                    firstOption: {key: '', value: 'Select'},
                    firstOptionSelectable: true,
                    url: ajaxUrl + 'admin/menu/top-menu-list',
                    data: {menu_id: $("#sel_menu option:selected").attr('id')},
                    key: 'navigation_id',
                    value: 'navigation_name',
                    optionData: [{key: 'no_of_columns', value: 'no_of_columns'}]
                });
                break;


        }
    });

    $('#edit_cat').on('click', function (event) {
        event.preventDefault();
        $('#link_model').modal();
        if ($('#create_navigation #link_type').val() == 2)
        {
            if (nav_settings != '')
            {
                $('#parent_category').val(nav_settings.parent_category).trigger('change');
                switch (nav_settings.filter_type) {
                    case'site_opt':
                        $("#filter_select").find("option[value=3]").attr('selected', true).trigger('change');
                        break;
                    case'brand_opt':
                        $("#filter_select").find("option[value=1]").attr('selected', true).trigger('change');
                        break;
                    case'property_opt':
                        $("#filter_select").find("option[value=2]").attr('selected', true).trigger('change');
                        break;
                }
            }
        }
    });
    $('.modal-body #filter_select').on('change', function (event) {
        event.preventDefault();
        var _this = $(this);
        if ($('.modal-body #child_category').val() != null)
        {
            var cat_id = $('.modal-body #child_category').val();
        }
        else
        {
            var cat_id = nav_settings.child_category;
        }
        $('.modal-body #filter #filter_type_hfld').html('<input type="hidden" name="filter_type" value="' + $("option:selected", this).attr('id') + '"/>');
        switch ($("option:selected", this).attr('id'))
        {
            case 'brand_opt':
                $.ajax({
                    url: ajaxUrl + 'admin/preference/get_brands',
                    type: "post",
                    dataType: "JSON",
                    data: {category_id: cat_id},
                    beforeSend: function () {

                    },
                    success: function (data) {
                        $('.modal-body #type_select').attr('name', 'brand_id');
                        $('.modal-body #filter #type_select').html('<option value="">Select Filter</option>');
                        $.each(data, function (key, val) {
                            $('.modal-body #filter #type_select').append('<option value="' + val.brand_id + '">' + val.brand_name + '</option>')
                        });
                        if (nav_settings != '')
                        {
                            $('#type_select').val(nav_settings.brand_id).trigger('change');
                        }
                    },
                });
                break;
            case 'property_opt':
                $.ajax({
                    url: ajaxUrl + 'admin/preference/get_filter',
                    type: "post",
                    dataType: "JSON",
                    data: {category_id: cat_id},
                    beforeSend: function () {

                    },
                    success: function (data) {

                        $('.modal-body #type_select').attr('name', 'value_id');
                        $('.modal-body #filter #type_select').html('<option value="">Select Filter</option>');
                        var append_data = '';
                        if (data != '')
                        {
                            $.each(data, function (key, val) {
                                if (val != null)
                                {
                                    append_data = append_data + '<optgroup label=' + val.property + '>';
                                    $.each(val.key_value, function (key, val) {
                                        if (val != null)
                                        {
                                            append_data = append_data + '<option value="' + val.id + '">' + val.key_value + '</option>';
                                        }
                                    });
                                    append_data = append_data + '</optgroup>';
                                }
                            });
                            $('.modal-body #filter #type_select').append(append_data);
                            if (nav_settings != '')
                            {
                                $('#type_select').val(nav_settings.value_id).trigger('change');
                            }
                        }

                    },
                });
                break;
            case 'site_opt':
                $.ajax({
                    url: ajaxUrl + 'admin/preference/get_filter',
                    type: "post",
                    dataType: "JSON",
                    data: {category_id: cat_id, is_general: 1},
                    beforeSend: function () {

                    },
                    success: function (data) {

                        $('.modal-body #type_select').attr('name', 'property_id');
                        $('.modal-body #filter #type_select').html('<option value="">Select Filter</option>');
                        $.each(data, function (key, val) {
                            $('.modal-body #filter #type_select').append('<option value="' + val.property_id + '">' + val.property + '</option>')
                        });
                        if (nav_settings != '')
                        {
                            $('#type_select').val(nav_settings.property_id).trigger('change');
                        }
                    },
                });
                break;

        }

    });
    $('#modal_form').on('submit', function (event) {
        event.preventDefault();
        modal_form_data = $('#modal_form').serializeObject();
        nav_settings = modal_form_data;
        if (modal_form_data != '')
        {
            if ($('.modal-body #type_select').attr('name') == 'value_id')
            {
                var data = {category_id: $('.modal-body #child_category').val(), value_id: $('.modal-body #type_select').val()}
            }
            if ($('.modal-body #type_select').attr('name') == 'brand_id')
            {
                var data = {category_id: $('.modal-body #child_category').val(), brand_id: $('.modal-body #type_select').val()}
            }
            if ($('.modal-body #type_select').attr('name') == 'property_id')
            {
                var data = {category_id: $('.modal-body #child_category').val(), property_id: $('.modal-body #type_select').val()}
            }

            $.ajax({
                url: ajaxUrl + 'admin/preference/generate_url',
                type: "post",
                dataType: "JSON",
                data: data,
                beforeSend: function () {

                },
                success: function (data) {

                    if (data.status == 'ok')
                    {
                        $('#navigation_url').val(data.url);
                    }
                },
            });
            $('#link_generate').before('<div class="alert alert-success">Saved Successfully</div>')
        }


    });
    $('.modal-body #parent_category').on('change', function (event) {
        event.preventDefault();
        $.ajax({
            url: ajaxUrl + 'admin/preference/nav_mgt',
            type: "get",
            dataType: "JSON",
            data: {category_id: $('.modal-body #parent_category').val()},
            beforeSend: function () {

            },
            success: function (data) {

                $('.modal-body #child_category').html('<option value="">Select Child Category</option>');
                $.each(data.child_categories, function (key, val) {
                    $('.modal-body #child_category').append('<option value="' + val.category_id + '">' + val.category + '</option>')
                });
                $("#child_category").val(nav_settings.child_category).trigger('change');
            },
        });
    });
    $('#select').on('click', function (event) {
        event.preventDefault();
        $('#menu_position').attr('disabled', 'disabled');
        $('#menu_name').attr('disabled', 'disabled');
        $('.panel_controls').show();
        $('#selected_menu_details').show();
        $('#menu_list').hide();
        $('#create_menu_heading').hide();
//        $('#menu_position').loadSelect({
//            firstOption: {key: '', value: 'Select'},
//            firstOptionSelectable: true,
//            url: ajaxUrl + 'menu_position-list',
//            key: 'menu_postion_id',
//            value: 'position_name',
//            selected: $("#sel_menu option:selected").attr('data-menu_postion_id')
//        });

        var menu_positions = {1: 'Header catalogue Menu', 2: 'Header Primary Menu', 3: 'Footer Primary Menu', 4: 'Footer Seconday Menu', 5: 'Footer Catlog Menu', 6: 'Footer Account Menu', 7: 'Footer Support Menu'};
        $('#selected_menu_details').html('<div >' + menu_positions[$("#sel_menu option:selected").attr('data-menu_postion_id')] + ' (' + $("#sel_menu option:selected").val() + ')</div> <div class="box-tools pull-right"><button id="createLinks" type="button" class=" createLinks btn btn-info btn-sm hidden">Create Links</button></div>'
                )
        $('#menu_list').hide();
        if ($("#sel_menu option:selected").val() == '')
        {
            $('#selected_menu_details').hide();
            $('#menu_list').show();
            $('#create_menu_heading').hide();
        }
        $('#list').show();
        $('#createLinks').show();
        $('#creatementBtn').addClass('hidden');
        $('#createLinks').removeClass('hidden');
        var selected_id = $("#sel_menu option:selected").attr('id');
        var selected = $("#sel_menu option:selected").val();
        $('#menu_id').val(selected_id);
        $('#menu_name').val(selected);
        $('#search').trigger('click');
        $('#create_navigation').hide();
    });

    $('#menu_management_btn').on('click', function (e) {
        $('#menu_management').show();
        $.ajax({
            url: ajaxUrl + 'admin/menu/get_menu',
            type: "post",
            dataType: "JSON",
            type: 'POST',
                    beforeSend: function () {

                    },
            success: function (data) {

                if (data.status == "ok") {
                    $('.alert').remove();
                    $('#top_catlog').empty();
                    $('#top_catlog').html('<option>None</option>');
                    $('#top_primary').empty();
                    $('#top_primary').html('<option>None</option>');
                    $('#footer_primary').empty();
                    $('#footer_primary').html('<option>None</option>');
                    $('#footer_secondary').empty();
                    $('#footer_secondary').html('<option>None</option>');
                    $('#footer_catlog').empty();
                    $('#footer_catlog').html('<option>None</option>');
                    $('#footer_support').empty();
                    $('#footer_support').html('<option>None</option>');
                    $('#footer_account').empty();
                    $('#footer_account').html('<option>None</option>');
                    if (data.data != '')
                    {
                        $.each(data.data, function (key, val)
                        {
                            console.log(val);
                            if (val.menu_postion_id != '') {
                                switch (val.menu_postion_id)
                                {
                                    case 1:
                                        if (val.selected == 1)
                                        {
                                            $('#top_catlog').append('<option value="' + val.menu_id + '"selected="slected">' + val.menu_name + '</option>');
                                        }
                                        else {
                                            $('#top_catlog').append('<option value="' + val.menu_id + '">' + val.menu_name + '</option>');
                                        }
                                        break;
                                    case 2:
                                        if (val.selected == 1)
                                        {
                                            $('#top_primary').append('<option value="' + val.menu_id + '"selected="selected">' + val.menu_name + '</option>');
                                        }
                                        else {
                                            $('#top_primary').append('<option value="' + val.menu_id + '">' + val.menu_name + '</option>');
                                        }
                                        break;
                                    case 3:
                                        if (val.selected == 1)
                                        {
                                            $('#footer_primary').append('<option value="' + val.menu_id + '"selected="selected">' + val.menu_name + '</option>');
                                        }
                                        else {
                                            $('#footer_primary').append('<option value="' + val.menu_id + '">' + val.menu_name + '</option>');
                                        }
                                        break;
                                    case 4:
                                        if (val.selected == 1)
                                        {
                                            $('#footer_secondary').append('<option value="' + val.menu_id + '"selected="selected">' + val.menu_name + '</option>');
                                        }
                                        else {
                                            $('#footer_secondary').append('<option value="' + val.menu_id + '">' + val.menu_name + '</option>');
                                        }
                                        break;
                                    case 5:
                                        if (val.selected == 1)
                                        {
                                            $('#footer_catlog').append('<option value="' + val.menu_id + '"selected="selected">' + val.menu_name + '</option>');
                                        }
                                        else {
                                            $('#footer_catlog').append('<option value="' + val.menu_id + '">' + val.menu_name + '</option>');
                                        }
                                        break;
                                    case 6:
                                        if (val.selected == 1)
                                        {
                                            $('#footer_account').append('<option value="' + val.menu_id + '"selected="selected">' + val.menu_name + '</option>');
                                        }
                                        else {
                                            $('#footer_account').append('<option value="' + val.menu_id + '">' + val.menu_name + '</option>');
                                        }
                                        break;
                                    case 7:
                                        if (val.selected == 1)
                                        {
                                            $('#footer_support').append('<option value="' + val.menu_id + '"selected="selected">' + val.menu_name + '</option>');
                                        }
                                        else {
                                            $('#footer_support').append('<option value="' + val.menu_id + '">' + val.menu_name + '</option>');
                                        }
                                        break;
                                }
                            }
                        }
                        );
                    }
                }
                else {
                    $('.alert').remove();
                    $('.errMsg').after('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                }
            },
        });
    });
    $('#menu_form').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: ajaxUrl + 'admin/menu/save_menu_settings',
            type: "post",
            dataType: "JSON",
            data: $('#menu_form').serialize(),
            type: 'POST',
                    beforeSend: function () {

                    },
            success: function (data) {

                if (data.status == "ok") {
                    $('.alert').remove();
                    $('#menu_form').before('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');

                }
                else {

                    $('.alert').remove();
                    $('#menu_form').before('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                }
            },
        });

    });
    $('#creatementBtn').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            url: ajaxUrl + 'admin/menu/save',
            type: "post",
            dataType: "JSON",
            data: {menu_name: $('#menu_name').val(), menu_postion_id: $('#menu_position').val()},
            type: 'POST',
			beforeSend: function () {

			},
            success: function (data) {
                if (data.status == "ok") {
                    $('.alert').remove();
                    $('.errMsg').after('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                    $('#sel_menu').append('<option id="' + data.menu_id + '" value="' + $('#menu_name').val() + '" data-menu_postion_id="' + $('#menu_position').val() + '" selected="selected">' + $('#menu_name').val() + '</option>');
                    $('#select').trigger('click');
                }
                else 
				{

                    $('.alert').remove();
                    $('.errMsg').after('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                }
            },
        });
    });

    if ($('#create #create_menu_nav').attr('id') == 'create_menu_nav')
    {
        $('#create #create_menu_nav').on('submit', function (e) {

            e.preventDefault();
            if ($('#menu_name').val() != null) {
                if ($('#create #create_menu_nav').attr('id') == 'create_menu_nav')
                {
                    if (modal_form_data != '')
                    {
                        var form_data = $('#create_menu_nav').find('input, select').not('[value=""], [value="0"], [value="DESC"]').serialize() + '&' + $.param({'nav[nav_settings]': $.stringify(modal_form_data)});
                    }
                    else
                    {
                        var form_data = $('#create_menu_nav').find('input, select').not('[value=""], [value="0"], [value="DESC"]').serialize();
                    }
                    var url = ajaxUrl + 'admin/menu/save'
                }
                else
                {
                    if (modal_form_data != '')
                    {
                        var form_data = $('#edit_navigation').find('input, select').not('[value=""], [value="0"], [value="DESC"]').serialize() + '&' + $.param({'nav[nav_settings]': $.stringify(modal_form_data)});
                    }
                    else
                    {
                        var form_data = $('#edit_navigation').find('input, select').not('[value=""], [value="0"], [value="DESC"]').serialize();
                    }
                    var url = ajaxUrl + 'admin/preference/update_navigation'
                }				
                $.ajax({
                    url: url,
                    type: "post",
                    dataType: "JSON",
                    data: form_data,
                    type: 'POST',
                            beforeSend: function () {

                            },
                    success: function (data) {

                        if (data.status == "ok") {
                            $('.alert').remove();
                            $('.errMsg').after('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                        }
                        else {

                            $('.alert').remove();
                            $('.errMsg').after('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                        }
                    },
                });
            }
        });
    }

    $('#create_menu_nav #selected_menu_details').on('click', '#createLinks', function () {
        $('.panel_controls').hide();		
        $('#create_navigation').show();		
        $('#list').hide();
        $('#edit_navigation').attr('id', 'create_menu_nav');
        // $('#create_menu_nav').resetForm();
    });
    $("#create_new").on('click', function () {
        $('#selected_menu_details').hide();
        $('#menu_list').hide();
        $('#create_menu_heading').show();
        $('#menu_position').attr('disabled', false);
        $('#menu_name').attr('disabled', false);
        $('#menu_position').loadSelect({
            firstOption: {key: '', value: 'Select'},
            firstOptionSelectable: true,
            url: ajaxUrl + 'admin/menu/position-list',
            key: 'menu_postion_id',
            value: 'position_name'
        });
        $('#sel_menu option').eq(0).attr('selected', 'selected');
        $('#createmenu,#creatementBtn').removeClass('hidden');
        $('#saveSubmit,#menusStr').addClass('hidden');
        $('#menu_name').val('')
        $('#menu_name').focus();
        $('#create_navigation').hide();
        $('#createLinks').hide();
        $('#list').hide();
        $('.panel_controls').hide();
    });

    $("#create_menu_nav_link").on('click', function () {
        $('#list').show();
        $('.panel_controls').show();
        $('#createLinks').hide();
        if ($("#sel_menu option:selected").val() == '')
        {
            $('#selected_menu_details').hide();
            $('#menu_list').show();
            $('#create_menu_heading').hide();
        }
        else
        {
            $('#selected_menu_details').show();
            $('#menu_list').hide();
            $('#create_menu_heading').hide();
        }

    });
    $("#delete_menu").on('click', function (e) {
        var ans = confirm('Do you want to delete this menu?');
        if (!ans)
            return false;
        var nextItem = 0;
        /*if($("#sel_menu option:selected").index()>0){
         if(($("#sel_menu option").length-1)>$("#sel_menu option:selected").index()){
         }
         nextItem = $("#sel_menu option:selected").index()+1;
         }
         else {
         $('#create_new').click();
         }*/
        var id = $("#sel_menu option:selected").attr('id');
        $.ajax({
            url: ajaxUrl + 'admin/system-settings/delete_menu',
            type: "post",
            data: {menu_id: id},
            type: 'POST',
                    beforeSend: function () {

                    },
            success: function (data) {

                if (data.status = "success")
                {
                    $("#sel_menu option:selected").remove();
                    $('.alert').remove();
                    $('.errMsg').after('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                    //$("#sel_menu option").eq(0).attr('selected',true);
                    //$('#select').click();
                    $('#create_new').click();
                }
                else
                {
                    $('.alert').remove();
                    $('.errMsg').after('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>' + data.msg + '</div>');
                }
            },
        });
    });
    /*$('.bootbox_confirm').click(function(e) {
     e.preventDefault();
     bootbox.confirm("Are you sure?", function(result) {
     //bootbox.alert("Confirm result: "+result);
     });
     });*/

    /*$('#toArrBtn').on('click', function(){ console.log($('#menus_structures').sortableListsToArray()); });
     $('#toHierBtn').on('click', function() { console.log($('#menus_structures').sortableListsToHierarchy()); });
     $('#toStrBtn').on('click', function() { console.log($('#menus_structures').sortableListsToString()); });*/
    $('#add_new').find('li .active a').trigger('click')
});
