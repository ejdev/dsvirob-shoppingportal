var notif, UserDetails = {}, PartnerDetails = {}, SupplierDetails = {}, CURFORM = null;
var Constants = {};
var ajaxUrl = $('base').attr('href');
var CurPage = {url: '', title: ''};
$.ajaxSetup({
    dataType: 'json',
    type: 'POST',
});
$(document).ajaxStart(function () {
    $('#loader-wrapper,#loader').fadeIn(100);
    if (notif != undefined) {
        notif({
            msg: 'Processing...',
            type: "info",
            position: "right"
        });
    }
});
$(document).ajaxComplete(function (event, xhr, settings) {
    var data = xhr.responseJSON;
    $('#loader-wrapper,#loader').fadeOut(500);
    if (xhr.status == 401) {
        window.location.href = data.url;
    }
    else if (xhr.status == 308) {
        if (data != undefined && data.url != undefined) {
            window.location.href = data.url;
        }
    }
    else if (xhr.status == 200) {
        if (data != undefined && data.msg != undefined && data.msg != '' && data.msg != null) {
            if (notif != undefined) {
                notif({
                    msg: data.msg,
                    type: "success",
                    position: "right"
                });
            }
        }
    }
    else if (xhr.status == 208) {
        if (data != undefined && data.msg != undefined && data.msg != '' && data.msg != null) {
            if (notif != undefined) {
                notif({
                    msg: data.msg,
                    type: "warning",
                    position: "right"
                });
            }
        }
    }
    else {
        if (xhr.status === 422) {
            if (CURFORM != null && data.error != undefined && data.error != null) {
                CURFORM.appendLaravelError(data.error);
                CURFORM = null;
            }
        }
        if (data != undefined && data.msg != undefined && data.msg != '' && data.msg != null) {
            if (notif != undefined) {
                notif({
                    msg: data.msg,
                    type: "error",
                    position: "right"
                });
            }
        }
        else {
            console.log(data);
        }
    }
});
$.extend({
    getURLParams: function (url) {
        url = (url != undefined) ? url : window.location.search;
        var params = {};
        url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (str, key, value) {
            params[decodeURI(key)] = decodeURI(value);
        });
        return params;
    },
    stringify: function stringify(obj) {
        var t = typeof (obj);
        if (t != "object" || obj === null) {
            if (t == "string")
                obj = '"' + obj + '"';
            return String(obj);
        } else {
            var n, v, json = [], arr = (obj && obj.constructor == Array);
            for (n in obj) {
                v = obj[n];
                t = typeof (v);
                if (obj.hasOwnProperty(n)) {
                    if (t == "string")
                        v = '"' + v + '"';
                    else if (t == "object" && v !== null)
                        v = jQuery.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
            }
            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    }
});
String.prototype.dotToArray = function () {
    var arr, s = '', str = this;
    arr = str.split('.');
    s = arr[0];
    for (var i in arr) {
        if (i != 0) {
            s += '[' + arr[i] + ']';
        }
    }
    return s;
}
$.fn.extend({
    resetForm: function () {
        var form = $(this);
        $.each($('input', form), function () {
            if (!$(this).hasClass('ignore-reset'))
            {
                switch ($(this).attr('type').toLowerCase())
                {
                    case "text":
                    case "password":
                    case "textarea":
                    case "hidden":
                    case "number":
                        $(this).val('');
                        break;
                    case "radio":
                    case "checkbox":
                        $(this).prop('checked', false);
                        break;
                }
            }
        });
        $.each($('textarea', form), function () {
            $(this).val('');
            if (!$(this).hasClass('ignore-reset'))
            {
                if (CKEDITOR != undefined) {
                    CKEDITOR.instances[$(this).attr('id')].setData('');
                    //CKEDITOR.instances[$(this).attr('id')].updateElement();
                }
            }
        });
        $.each($('select', form), function () {
            if (!$(this).hasClass('ignore-reset'))
            {
                $(this).val('');
            }
        });
    },
    appendLaravelError: function (error) {
        var form = this;
        if (error != undefined) {
            $.each(error, function (k, e) {
                k = k.dotToArray();
                $('[for="' + k + '"]', form).remove();
                if ($('[name="' + k + '"]', form).hasClass('noValidate') == false)
                {
                    if ($('[name="' + k + '"]', form).length == 1) {
                        if ($('[name="' + k + '"]', form).data('errmsg_to') != undefined) {
                            /* display errmsg on single container */
                            $('#' + $('[name="' + k + '"]', form).data('errmsg_to'), form).attr({for : k, class: 'text-danger'}).append(e);
                            $('[name="' + k + '"]', form).on('change', function () {
                                $('[for="' + $(this).attr('name') + '"]', form).remove();
                            });
                        }
                        else {
                            $('[name="' + k + '"]', form).after($('<span>').attr({for : k, class: 'text-danger'}).html(e)).on('change', function () {
                                $('[for="' + $(this).attr('name') + '"]', form).remove();
                            });
                        }
                    }
                    else if ($('[name="' + k + '"]', form).length > 1) { /* display errmsg for radio control */
                        $('#' + k + '_errmsg', form).attr({for : k, class: 'text-danger'}).html(e);
                        $('[name="' + k + '"]', form).on('change', function () {
                            $('[for="' + $(this).attr('name') + '"]', form).remove();
                        });
                    }
                }
            });
        }
    },
    serializeObject: function ()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }
});
$(document).ready(function () {
    $('#loader-wrapper,#loader').fadeOut(500);
    if ($.validator) {
        $.validator.addMethod("alphaSpace", function (value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
        }, 'please enter alphabets');
        $.validator.addMethod("phonecode", function (value, element) {
            return this.optional(element) || value == value.match(/^[+]{1}[0-9]+$/);
        }, 'please enter Phonecode');
        $.validator.addMethod("checkHost", function (value, element) {
            var pattern = new RegExp(/^[a-z0-9]+([\-\.]{1}[a-z0-9]+)+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/);
            return pattern.test(host);
        }, 'Please enter a valid Host Address.');
    }
});
function AddToUrl(title, url) {
    if (typeof (window.history.pushState) != undefined) {
        var href = window.location.href, c_url = (href.indexOf('?') > 1) ? href.substring(0, href.indexOf('?')) : href;
        var obj = {Page: title, Url: c_url + ((url != '') ? '?' + url : '')};
        CurPage.url = window.location.href;
        CurPage.title = document.title;
        window.history.pushState(obj, obj.Page, obj.Url);
    }
}
function ChangeUrl(title, url) {
    if (typeof (window.history.pushState) != undefined) {
        var obj = {Page: title, Url: url};
        CurPage.url = window.location.href;
        CurPage.title = document.title;
        window.history.pushState(obj, obj.Page, obj.Url);
    }
}
function GoToPrevious() {
    if (CurPage.url != null) {
        window.history.pushState(CurPage, CurPage.title, CurPage.url);
    }
}
function validateRegularExpression(event, expression) {
    var regex = new RegExp(expression);
    var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (regex.test(str)) {
        return true;
    }
    event.preventDefault();
    return false;
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 46)) {
        return false;
    }
    return true;
}
function isAlfa(evt) {
    // evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
        return false;
    }
    return true;
}
function alphaNumeric_withspace(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || code == 32 || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 9 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function alphaNumeric_specialchar(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || code == 32 || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 9 || code == 45 || code == 95 || code == 43 || code == 38 || code == 40 || code == 41 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function alphaNumeric_withoutspace_schar(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || code == 45 || code == 92)) {
        return true;
    }
    return false;
}
function alphaBets(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || code == 116 || code == 9 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function alphaBets_withspace(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 116 || code == 32 || code == 9 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function validateRegno(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 9 || code == 8 || code == 45 || code == 92 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function alphaNumeric_withoutspace(e) {
    var code = e.charCode ? e.charCode : e.keyCode;
    if (((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || (code >= 48 && code <= 57) || (code == 37 && e.charCode == 0) || (code == 39 && e.charCode == 0) || code == 9 || code == 8 || (code == 46 && e.charCode == 0))) {
        return true;
    }
    return false;
}
function isNumberKeydot(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode > 57 || charCode < 48 || charCode == 46)) {
        return false;
    }
    return true;
}
function RestrictSpace(evt) {
    if (event.keyCode == 32) {
        return false;
    }
}
function selectallchk(evt) {
    alert('fg');
    if (evt.checked) { // check select status
        $('.checkbox').each(function () { //loop through each checkbox
            this.checked = true; //select all checkboxes with class "checkbox1"
        });
    } else {
        $('.checkbox').each(function () { //loop through each checkbox
            this.checked = false; //deselect all checkboxes with class "checkbox1"
        });
    }
}
$(document).ready(function () {
    $('#selectall').on('ifChecked', function (event) {
        $('.check').iCheck('check');
    });
    $('#selectall').on('ifUnchecked', function (event) {
        $('.check').iCheck('uncheck');
    });
    $('#selectall').on('ifChanged', function (event) {
        if (!this.changed) {
            this.changed = true;
            $('#selectall').iCheck('check');
        } else {
            this.changed = false;
            $('#selectall').iCheck('uncheck');
        }
        $('#selectall').iCheck('update');
    });
});
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function stripHtmlTags(string) {
    return string.replace(/(<([^>]+)>)/ig, "");
}
function addSlashes(string) {
    return string.replace(/\\/g, '\\\\').
            replace(/\u0008/g, '\\b').
            replace(/\t/g, '\\t').
            replace(/\n/g, '\\n').
            replace(/\f/g, '\\f').
            replace(/\r/g, '\\r').
            replace(/'/g, '\\\'').
            replace(/"/g, '\\"');
}
function stripSlashes(string) {
    return string.replace(/\\/g, '');
}
$(window).on("popstate", function (e) {
    if (e.originalEvent.state !== null) {
        console.log(e.originalEvent.state);
    }
});
