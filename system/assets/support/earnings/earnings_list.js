$(function () {
    var t = $('#Earningslist'), f = $('#earnings-list-form');
    t.DataTable({
        ordering: false,
        serverSide: true,
        processing: true,
        autoWidth: false,
        pagingType: 'input_page',
        sDom: "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
        oLanguage: {
            sLengthMenu: "_MENU_"
        },
        ajax: {
            url: f.attr('action'),
            type: 'POST',
            data: function (d) {
                $.extend(d, f.serializeObject());
            },
        },
        columns: [
            {
                data: 'created_on',
                name: 'created_on',
                class: 'text-center no-wrap',
                responsivePriority: 1
            },
			 {
                data: 'order_code',
                name: 'order_code',
                responsivePriority: 3,
				render: function (data, type, row, meta) {						
                    return '<a href="#" class="order_details" data="'+row.order_code+'">'+row.order_code+'</a>';
                }
            },
            {
                data: 'usercode',
                name: 'usercode'                
            },                     			
            {
                data: 'order_item_id',
                name: 'order_item_id',
				class: 'text-center',
            },
			{
                data: 'currency',
                name: 'currency',
				class: 'text-center',
            },
            {
                data: 'mrp_price',
                name: 'mrp_price',
                class: 'text-right',
                responsivePriority: 9,
				render: function (data, type, row, meta) {						
                    return row.mrp_price.toFixed(2);
                }
            },
            {
                data: 'partner_sold_price',
                name: 'partner_sold_price',     
				class: 'text-right',
				render: function (data, type, row, meta) {						
                    return row.partner_sold_price.toFixed(2);
                }
            },
			{
                data: 'qty',
                name: 'qty',        
				class: 'text-center',
            },
            {
                data: 'shipping_fee',
                name: 'shipping_fee',    
				class: 'text-right',
				render: function (data, type, row, meta) {						
                    return row.shipping_fee.toFixed(2);
                }
            },
            {
                data: 'price_sub_total',
                name: 'price_sub_total',                
				class: 'text-right',
				render: function (data, type, row, meta) {						
                    return row.price_sub_total.toFixed(2);
                }
            },
			{
                data: 'price_sub_total',
                name: 'price_sub_total',                
				class: 'text-right',
				render: function (data, type, row, meta) {						
                    return (row.price_sub_total + row.shipping_fee).toFixed(2);
                }
            },
            {
                data: 'partner_total_profit',
                name: 'partner_total_profit',                
				class: 'text-right',
				render: function (data, type, row, meta) {						                    
                    return '<div title"'+row.partner_margin_sub_total+'">'+row.partner_margin_sub_total.toFixed(2)+'</div>' ;
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
    });	
	
		
	$('#Earningslist').on('click','.order_details',function(e){
		e.preventDefault();				
		$.post('admin/orders/details/'+$(this).attr('data'), {order_code: $(this).attr('data')}, function (data) {					
			$("#o_list").addClass('hidden');				
			$("#o_details").html(data);
			$("#o_details").removeClass('hidden');				
		})
	});	

    $('#search-btn').click(function (e) {
        t.dataTable().fnDraw();
    });
    $('#reset-btn').click(function (e) {
        f.resetForm();
        t.dataTable().fnDraw();
    });
});
