$(function () {
    var t = $('#order-items-list-table'), f = $('#order-items-list-form');
    t.DataTable({
        ordering: true,
        serverSide: true,
        paging: false,
        processing: true,
        autoWidth: false,
        pagingType: 'input_page',
        sDom: 't',
        oLanguage: {
            sLengthMenu: "_MENU_"
        },
        ajax: {
            url: f.attr('action'),
            type: 'POST',
            data: function (d) {
                $.extend(d, f.serializeObject());
            },
        },
        columns: [
            {
                data: 'Fqty',
                name: 'qty',
                class: 'text-right no-wrap',
                responsivePriority: 7
            },
            {
                data: 'Famount',
                name: 'amount',
                class: 'text-right no-wrap',
                responsivePriority: 8
            },
            {
                data: 'Ftax',
                name: 'tax',
                class: 'text-right no-wrap',
                responsivePriority: 13
            },
            {
                data: 'Fnet_pay',
                name: 'net_pay',
                class: 'text-right no-wrap',
                responsivePriority: 12
            },
            {
                data: 'payment_status',
                name: 'payment_status',
                class: 'text-center',
                responsivePriority: 11
            },
            {
                data: 'approval_status',
                name: 'approval_status',
                class: 'text-center',
                responsivePriority: 9
            },
            {
                data: 'order_item_status',
                name: 'order_item_status',
                class: 'text-center',
                responsivePriority: 10
            },
            {
                data: 'sub_order_code',
                name: 'sub_order_code',
                class: 'text-center',
                responsivePriority: 2,
                orderable: false,
                render: function (data, type, row, meta) {
                    var content = $('<div>').attr({class: 'pull-right', style: 'position: relative !important;'}).append($('<button>').attr({class: 'btn btn-xs btn-primary dropdown-toggle', 'data-toggle': 'dropdown'})
                            .append($('<span>').attr({class: 'caret'})),
                            $('<ul>').attr({class: 'dropdown-menu dropdown-menu-right', role: 'menu'}).append(function () {
                        var options = [];
                        $.each(row.actions, function (k, v) {
                            var data = {};
                            if (v.data != undefined) {
                                $.each(v.data, function (key, val) {
                                    data['data-' + key] = val;
                                });
                            }
                            if (v.target != undefined && v.target != null) {
                                data['target'] = v.target;
                            }
                            else {
                                data['class'] = 'actions';
                            }
                            options.push($('<li>').append($('<a>').attr($.extend({href: v.url}, data)).text(v.title)));
                        });
                        return options;
                    }));
                    return content[0].outerHTML;
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
    });
    t.on('click', '.actions', function (e) {
        e.preventDefault();
        $('.alert').remove();
        var Ele = $(this), data = Ele.data();
        if (Ele.data('confirm') == undefined || (Ele.data('confirm') != null && Ele.data('confirm') != '' && confirm(Ele.data('confirm')))) {
            if (data.confirm != undefined) {
                delete data.confirm;
            }
            $.ajax({
                url: Ele.attr('href'),
                data: data,
                dataType: 'json',
                type: 'POST',
                success: function (data) {
                    t.dataTable().fnDraw();
                }
            });
        }
    });
    $('#search-btn').click(function (e) {
        t.dataTable().fnDraw();
    });
    $('#reset-btn').click(function (e) {
        f.resetForm();
        t.dataTable().fnDraw();
    });
});
