$(function () {
    var t = $('#kyclist'), f = $('#orders-list-form');
    t.DataTable({
        ordering: false,
        serverSide: true,
        processing: true,
        autoWidth: false,
        pagingType: 'input_page',
        sDom: "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
        oLanguage: {
            sLengthMenu: "_MENU_"
        },
        ajax: {
            url: f.attr('action'),
            type: 'POST',
            data: function (d) {
                $.extend(d, f.serializeObject());
            },
        },
        columns: [
            {
                data: 'Fordered_on',
                name: 'ordered_on',
                class: 'text-center no-wrap',
                responsivePriority: 5
            },            
            {
                data: 'order_code',
                name: 'order_code',
                responsivePriority: 3
            },
            {
                data: 'sub_order_code',
                name: 'sub_order_code',
                responsivePriority: 1
            },
			{
                data: 'customer_name',
                name: 'customer_name',
                responsivePriority: 4
            },
            {
                data: 'supplier_code',
                name: 'supplier_code',
                responsivePriority: 6,
				render: function (data, type, row, meta) {					
                    return row.supplier_name +' ('+row.supplier_code+')';
                }
            },            
            {
                data: 'Fqty',
                name: 'qty',
                class: 'text-center ',
                responsivePriority: 7
            },
            {
                data: 'Fnet_pay',
                name: 'Fnet_pay',
                class: 'text-right no-wrap',
                responsivePriority: 8
            },
            
            {
                data: 'payment_type',
                name: 'payment_type',
                class: 'text-left no-wrap',
                responsivePriority: 12
            },
            {
                data: 'payment_status',
                name: 'payment_status',
                class: 'text-center',
                responsivePriority: 11,
				render: function (data, type, row, meta) {						
						return '<span class="'+row.payment_status_class+'">'+row.payment_status+'</span>';					
                }
            },
			{
                data: 'sub_order_status',
                name: 'sub_order_status',
                class: 'text-center',
                responsivePriority: 10,
				render: function (data, type, row, meta) {						
						return '<span class="'+row.sub_order_status_class+'">'+row.sub_order_status+'</span>';					
                }
            },
            {
                data: 'approval_status',
                name: 'approval_status',
                class: 'text-center',
                responsivePriority: 9,
				render: function (data, type, row, meta) {	
					if (row.approval_status == 'Partner Cancelled') {
						return '<span class="label label-danger">'+row.approval_status+'</span>';
					} else if (row.approval_status == 'Partner Holding') {
						return '<span class="label label-info">'+row.approval_status+'</span>';
					} else if (row.approval_status == 'Pending') {
						return '<span class="label label-warning">'+row.approval_status+'</span>';
					} else {
						//return '<span class="label label-success">'+row.approval_status+'</span>';
						return '';
					}
                }
            },            
            {
                data: 'sub_order_code',
                name: 'sub_order_code',
                class: 'text-center',
                responsivePriority: 2,
                orderable: false,
                render: function (data, type, row, meta) {
                    var content = $('<div>').attr({class: 'pull-right', style: 'position: relative !important;'}).append($('<button>').attr({class: 'btn btn-xs btn-primary dropdown-toggle', 'data-toggle': 'dropdown'})
                            .append($('<span>').attr({class: 'caret'})),
                            $('<ul>').attr({class: 'dropdown-menu dropdown-menu-right', role: 'menu'}).append(function () {
                        var options = [];
                        $.each(row.actions, function (k, v) {
                            var data = {};
                            if (v.data != undefined) {
                                $.each(v.data, function (key, val) {
                                    data['data-' + key] = val;
                                });
                            }
                            if (v.target != undefined && v.target != null) {
                                data['target'] = v.target;
                            }
                            else {
                                data['class'] = 'actions';
                            }
                            options.push($('<li>').append($('<a>').attr($.extend({href: v.url}, data)).text(v.title)));
                        });
                        return options;
                    }));
                    return content[0].outerHTML;
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
    });
    t.on('click', '.actions', function (e) {
        e.preventDefault();
        $('.alert').remove();
        var Ele = $(this), data = Ele.data();
        if (Ele.data('confirm') == undefined || (Ele.data('confirm') != null && Ele.data('confirm') != '' && confirm(Ele.data('confirm')))) {
            if (data.confirm != undefined) {
                delete data.confirm;
            }
            $.ajax({
                url: Ele.attr('href'),
                data: data,
                dataType: 'json',
                type: 'POST',
                success: function (data) {
                    t.dataTable().fnDraw();
                }
            });
        }
    });
    $('#search-btn').click(function (e) {
        t.dataTable().fnDraw();
    });
    $('#reset-btn').click(function (e) {
        f.resetForm();
        t.dataTable().fnDraw();
    });
});
