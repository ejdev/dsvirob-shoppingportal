$(function () {
    var t = $('#orderlist'), f = $('#orders-list-form');
    t.DataTable({
        ordering: false,
        serverSide: true,
        processing: true,
        autoWidth: false,
        pagingType: 'input_page',
        sDom: "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
        oLanguage: {
            sLengthMenu: "_MENU_"
        },
        ajax: {
            url: f.attr('action'),
            type: 'POST',
            data: function (d) {
                $.extend(d, f.serializeObject());
            },
        },
        columns: [
            {
                data: 'Fordered_on',
                name: 'ordered_on',
                class: 'text-center no-wrap',
                responsivePriority: 1
            },
			 {
                data: 'order_code',
                name: 'order_code',
                responsivePriority: 3,
				render: function (data, type, row, meta) {						
                    return '<a href="#" class="order_details" data="'+row.order_code+'">'+row.order_code+'</a>';
                }
            },
            {
                data: 'customer_name',
                name: 'customer_name',
                responsivePriority: 2,
				render: function (data, type, row, meta) {					
                    return row.customer_name +' ('+row.mobile_no+')';
                }
            },
           
            {
                data: 'qty',
                name: 'qty',
                class: 'text-center no-wrap',
                responsivePriority: 4
            },           			
            {
                data: 'Fnet_pay',
                name: 'net_pay',
                class: 'text-right no-wrap',
                responsivePriority: 11,
				render: function (data, type, row, meta) {
					var subtotal = 'Sub Total : '+row.currency_symbol + ' ' +row.amount+ ' '+ row.currency;
					var shipping = 'Shipping Charge : '+row.currency_symbol + ' ' +row.shipping_charges+ ' '+ row.currency;
					var tax = 'Tax : '+row.currency_symbol + ' ' +row.tax+ ' '+ row.currency;					
                    var content = '<div title="'+subtotal+ ' '+tax+ ' '+shipping+'">'+ row.net_pay.toFixed(2) + ' '+ row.currency+'</div>';
                    return content;
                }
            },
            {
                data: 'payment_type',
                name: 'payment_type',
                class: 'text-left',
                responsivePriority: 9
            },
            {
                data: 'payment_status',
                name: 'payment_status',
                class: 'text-center',
                responsivePriority: 7,				
				render: function (data, type, row, meta) {						
						return '<span class="'+row.payment_status_class+'">'+row.payment_status+'</span>';					
                }				
            },
			{
                data: 'order_status',
                name: 'order_status',
                class: 'text-center',
                responsivePriority: 8,
				render: function (data, type, row, meta) {						
						return '<span class="'+row.order_status_class+'">'+row.order_status+'</span>';					
                }
            },
            {
                data: 'approval_status',
                name: 'approval_status',
                class: 'text-center',
                responsivePriority: 8,
				render: function (data, type, row, meta) {	
					if (row.approval_status == 'Partner Cancelled') {
						return '<span class="label label-danger">'+row.approval_status+'</span>';
					} else if (row.approval_status == 'Partner Holding') {
						return '<span class="label label-info">'+row.approval_status+'</span>';
					} else if (row.approval_status == 'Pending') {
						return '<span class="label label-warning">'+row.approval_status+'</span>';
					} else {
						//return '<span class="label label-success">'+row.approval_status+'</span>';
						return '';
					}
                }
            },
            {
                data: 'order_code',
                name: 'order_code',
                class: 'text-center',
                orderable: false,
                responsivePriority: 6,
                render: function (data, type, row, meta) {
                    var content = $('<div>').attr({class: 'pull-right', style: 'position: relative !important;'}).append($('<button>').attr({class: 'btn btn-xs btn-primary dropdown-toggle', 'data-toggle': 'dropdown'})
                            .append($('<span>').attr({class: 'caret'})),
                            $('<ul>').attr({class: 'dropdown-menu dropdown-menu-right', role: 'menu'}).append(function () {
                        var options = [];
                        $.each(row.actions, function (k, v) {
                            if (k != 'details') {
                                var data = {};
                                $.each(v.data, function (key, val) {
                                    data['data-' + key] = val;
                                });
                                if (v.target != undefined && v.target != null) {
                                    data['target'] = v.target;
                                }
                                else {
                                    data['class'] = 'actions';
                                }
                                options.push($('<li>').append($('<a>').attr($.extend({href: v.url}, data)).text(v.title)));
                            }
                        });
                        return options;
                    }));
                    return content[0].outerHTML;
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
    });	
	
		
	$('#orderlist').on('click','.order_details',function(e){
		e.preventDefault();				
		$.post('admin/orders/details/'+$(this).attr('data'), {order_code: $(this).attr('data')}, function (data) {					
			$("#o_list").addClass('hidden');				
			$("#o_details").html(data);
			$("#o_details").removeClass('hidden');		
			//$("#view_order_details .modal-body").html(data);
			//$("#view_order_details .modal-title").html("View Order Details");
			//$("#view_order_details").modal();
		})
	});
	
	t.on('click', '.actions', function (e) {
        e.preventDefault();
        $('.alert').remove();
        var Ele = $(this), data = Ele.data();
        if (Ele.data('confirm') == undefined || (Ele.data('confirm') != null && Ele.data('confirm') != '' && confirm(Ele.data('confirm')))) {
            if (data.confirm != undefined) {
                delete data.confirm;
            }
            $.ajax({
                url: Ele.attr('href'),
                data: data,
                dataType: 'json',
                type: 'POST',
                success: function (data) {
                    t.dataTable().fnDraw();
                }
            });
        }
    });
    $('#search-btn').click(function (e) {
        t.dataTable().fnDraw();
    });
    $('#reset-btn').click(function (e) {
        f.resetForm();
        t.dataTable().fnDraw();
    });
});
