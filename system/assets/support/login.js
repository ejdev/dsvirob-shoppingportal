$(document).on('submit', '#loginfrm', function (e) {
    e.preventDefault();
    $('.alert,.help-block').remove();
    var frmObj = $(this);
    $.ajax({
        type: 'POST',
        url: frmObj.attr('action'),
        data: frmObj.serialize(),
        dataType: 'json',
        success: function (op) {
            setTimeout(function () {
                window.location.href = op.url;
            }, 1000);
        }
    });
});
$('#forgotfrm').submit(function (e) {
    e.preventDefault();
    var frmObj = $(this);
    $('.alert,.help-block').remove();
    $.ajax({
        type: 'POST',
        url: frmObj.attr('action'),
        data: frmObj.serialize(),
        dataType: 'json',
        success: function (op) {
            window.location.href = op.url;
        }
    });
});
$(document).on('click', '.btn-forgot', function (e) {
    e.preventDefault();
    $('.signin').fadeOut('fast', function () {
        $('.forgot').fadeIn('slow');
    });
});
$(document).on('click', '.backtoLogin', function (e) {
    e.preventDefault();
    $('.forgot').fadeOut('fast', function () {
        $('.signin').fadeIn('slow');
    });
});
