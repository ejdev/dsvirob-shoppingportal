$(function () {
    var t = $('#userlist'), f = $('#user-list-form');
    //alert(f.attr('action'));
    t.DataTable({
        ordering: false,
        serverSide: true,
        processing: true,
        autoWidth: false,
        pagingType: 'input_page',
        sDom: "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
        oLanguage: {
            sLengthMenu: "_MENU_"
        },
        ajax: {
            url: f.attr('action'),
            type: 'POST',
            data: function (d) {
                $.extend(d, f.serializeObject());
            },
        },
        columns: [
            {
                data: 'created_on',
                name: 'created_on',
                class: 'text-center no-wrap',
                responsivePriority: 1
            },
			{
                data: 'firstname',
                name: 'firstname',
                responsivePriority: 3,
				render: function (data, type, row, meta) {						
                    return row.firstname  + ' ' +  row.lastname;
                }
            },       
			{
                data: 'user_code',
                name: 'user_code',
                responsivePriority: 3,
				render: function (data, type, row, meta) {						
                    return row.user_code;
                }
            },
            {
                data: 'email',
                name: 'email',
                class: 'text-left no-wrap',
                responsivePriority: 11				
            },
            {
                data: 'mobile',
                name: 'mobile',
                class: 'text-left',
                responsivePriority: 9
            },
			{
                data: 'status_name',
                name: 'status_name',
                class: 'text-center',
                responsivePriority: 4,
				render: function (data, type, row, meta) {						
					if (row.status_name == 'Active') {
						return '<span class="label label-success">'+row.status_name+'</span>';	
					} else {
						return '<span class="label label-danger">'+row.status_name+'</span>';	
					}
                } 
            },			
            {
                data: 'order_code',
                name: 'order_code',
                class: 'text-center',
                orderable: false,
                responsivePriority: 6,
				render: function (data, type, row, meta) { 					
					var json = $.parseJSON(meta.settings.jqXHR.responseText);					
					var action_buttons = '';
					action_buttons = '<div class="btn-group"><button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-right" role="menu">';
						if (row.status_name == 'Active') {
							action_buttons = action_buttons + '<li><a class="change_status" data-account_id="' + row.account_id + '" >Block</a></li>';
						}
						else {
							action_buttons = action_buttons + '<li><a class="change_status" data-account_id="' + row.account_id + '" >Activate</a></li>';
						}
					action_buttons = action_buttons + '<li><a class="order_detail" href="admin/user/orders/'+row.account_id+'" data="' + row.account_id + '" >Order Details</a></li>';
					action_buttons = action_buttons + '</ul></div>';
					return action_buttons;
				}
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
    });		
	
    $('#search-btn').click(function (e) {
        t.dataTable().fnDraw();
    });
    $('#reset-btn').click(function (e) {
        f.resetForm();
        t.dataTable().fnDraw();
    });
});
