$(function () {
    var t = $('#transaction-list'), f = $('#transaction-form');
    t.DataTable({
        ordering: true,
        serverSide: true,
        processing: true,
        autoWidth: false,
        pagingType: 'input_page',
        sDom: "t" + "<'col-sm-6 bottom info align'li>r<'col-sm-6 info bottom text-right'p>",
        oLanguage: {
            sLengthMenu: "_MENU_"
        },
        ajax: {
            url: f.attr('action'),
            type: 'POST',
            data: function (d) {
                $.extend(d, f.serializeObject());
            },
        },
        columns: [
            {
                data: 'Fupdated_on',
                name: 'updated_on',
                class: 'text-center no-wrap',
                responsivePriority: 5
            },
            {
                data: 'full_name',
                name: 'full_name',
                responsivePriority: 4
            },
            {
                name: 'ftfull_name',
                data: 'ftfull_name',
                responsivePriority: 4
            },
            {
                data: 'description',
                name: 'description',
                responsivePriority: 5,
                render: function (data, type, row, meta) {
                    return  row.description + ((row.payment_type != '') ? ' through ' + row.payment_type : '');
                }

            },
            {
                data: 'wallet_name',
                name: 'wallet_name',
                responsivePriority: 5
            },
            {
                data: 'Famt',
                name: 'amt',
                class: 'text-right no-wrap',
                responsivePriority: 5,
                render: function (data, type, row, meta) {
                    return $('<span>', {class: row.transaction_type}).append(row.Famt)[0].outerHTML;
                }
            },
            {
                data: 'Fpaid_amt',
                name: 'paid_amt',
                class: 'text-right no-wrap',
                responsivePriority: 5,
                render: function (data, type, row, meta) {
                    return $('<span>', {class: row.transaction_type}).append(row.Fpaid_amt)[0].outerHTML;
                }
            },
            {
                data: 'Fhandle_amt',
                name: 'handle_amt',
                class: 'text-right no-wrap',
                responsivePriority: 5,
                render: function (data, type, row, meta) {
                    return $('<span>', {class: row.transaction_type}).append(row.Fhandle_amt)[0].outerHTML;
                }
            },
            {
                data: 'Fcurrent_balance',
                name: 'current_balance',
                class: 'text-right no-wrap',
                responsivePriority: 5,
                render: function (data, type, row, meta) {
                    return $('<span>', {class: row.transaction_type}).append(row.Fcurrent_balance)[0].outerHTML;
                }
            }
        ],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal({
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                    tableClass: 'table'
                })
            }
        },
    });
    $('#search-btn').click(function (e) {
        t.dataTable().fnDraw();
    });
    $('#reset-btn').click(function (e) {
        f.resetForm();
        t.dataTable().fnDraw();
    });
});
