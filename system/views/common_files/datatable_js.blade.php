<link rel="stylesheet" href="{{asset('system/assets/plugins/datatable-responsive/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('system/assets/plugins/datatable-responsive/css/responsive.bootstrap.min.css')}}">
<script src="{{asset('system/assets/plugins/datatable-responsive/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('system/assets/plugins/datatable-responsive/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('system/assets/plugins/datatable-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('system/assets/plugins/datatable-responsive/js/responsive.bootstrap.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('system/assets//plugins/datepicker/datepicker3.css')}}">
<script src="{{asset('system/assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script>
$(function () {
    if ($('#from').length && $('#to').length) {
        $('#from').datepicker({
            autoclose: true,
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            format: 'yyyy-mm-dd'
        }).on('changeDate', function (evt) {
            var pkDate = new Date(evt.date);
            pkDate.setDate(pkDate.getDate() + 1);
            var pday = ("0" + (pkDate.getDate())).slice(-2);
            pkDate.setMonth(pkDate.getMonth() + 1);
            var nDate = pkDate.getFullYear() + "-";
            var pMonth = ("0" + pkDate.getMonth()).slice(-2);
            nDate = nDate + pMonth + '-' + pday;
            nDate = nDate;
            toDate.val(nDate);
            toDate.datepicker('update');
            toDate.datepicker('setStartDate', pkDate);
        });
        var toDate = $('#to').datepicker({
            autoclose: true,
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            format: 'yyyy-mm-dd'
        });
    }
});
</script>
