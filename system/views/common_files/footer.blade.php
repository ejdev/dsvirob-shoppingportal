<footer class="main-footer">
    <strong>Copyright &copy; 2014-{{date('Y')}} <a href="{{url('/')}}">{{$sitesettings->site_name}}</a>.</strong> All rights reserved.
</footer>
