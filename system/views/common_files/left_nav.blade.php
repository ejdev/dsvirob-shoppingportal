<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-book"></i> <span>Dashboard</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Users</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.users.list')}}"><i class="fa fa-circle-o"></i>Users List</a></li>                    
                </ul>
            </li>                      
            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Orders</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.order.list',['status'=>'placed'])}}"><i class="fa fa-circle-o"></i>Placed</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'confirmed'])}}"><i class="fa fa-circle-o"></i>Confirmed</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'packed'])}}"><i class="fa fa-circle-o"></i>Packed</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'dispatched'])}}"><i class="fa fa-circle-o"></i>Dispatched</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'in-shipping'])}}"><i class="fa fa-circle-o"></i>In Shipping</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'reached-hub'])}}"><i class="fa fa-circle-o"></i>Reached Hub</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'delivered'])}}"><i class="fa fa-circle-o"></i>Delivered</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'cancelled'])}}"><i class="fa fa-circle-o"></i>Cancelled</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'returned'])}}"><i class="fa fa-circle-o"></i>Return</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'refunded'])}}"><i class="fa fa-circle-o"></i>Refunded</a></li>
                    <li><a href="{{route('admin.order.list',['status'=>'replaced'])}}"><i class="fa fa-circle-o"></i>Replaced</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Supplier Orders</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.sub-order.list',['status'=>'placed'])}}"><i class="fa fa-circle-o"></i>Placed</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'confirmed'])}}"><i class="fa fa-circle-o"></i>Confirmed</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'packed'])}}"><i class="fa fa-circle-o"></i>Packed</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'dispatched'])}}"><i class="fa fa-circle-o"></i>Dispatched</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'in-shipping'])}}"><i class="fa fa-circle-o"></i>In Shipping</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'reached-hub'])}}"><i class="fa fa-circle-o"></i>Reached Hub</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'delivered'])}}"><i class="fa fa-circle-o"></i>Delivered</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'cancelled'])}}"><i class="fa fa-circle-o"></i>Cancelled</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'returned'])}}"><i class="fa fa-circle-o"></i>Return</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'refunded'])}}"><i class="fa fa-circle-o"></i>Refunded</a></li>
                    <li><a href="{{route('admin.sub-order.list',['status'=>'replaced'])}}"><i class="fa fa-circle-o"></i>Replaced</a></li>
                </ul>
            </li>       
            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Package</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-circle-o"></i>Purchase Package</a></li>
                    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-circle-o"></i>My Packages</a></li>                    
                </ul>
            </li>
			<li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Settings</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-circle-o"></i>Change Password</a></li>                    
                    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-circle-o"></i>Change Email</a></li>                    
                </ul>
            </li> 
			 <!-- custom start -->
           

                <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Products</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.product-category.list')}}"><i class="fa fa-circle-o"></i>Category</a></li>                    
                    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-circle-o"></i>Products</a></li>                    
                </ul>
            </li> 



            <!-- custom end -->

            <li><a href="{{route('admin.order-items.list')}}"><i class="fa fa-circle-o"></i><span>Order Items</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Wallet</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.wallet.transfer-history')}}"><i class="fa fa-circle-o"></i>Transfer History</a></li>
                    <li><a href="{{route('admin.wallet.transaction-log')}}"><i class="fa fa-circle-o"></i>Wallet Transactions</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Withdrawals</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.withdraw.list',['status'=>'pending'])}}"><i class="fa fa-circle-o"></i>Pending</a></li>
                    <li><a href="{{route('admin.withdraw.list',['status'=>'processing'])}}"><i class="fa fa-circle-o"></i>Processing</a></li>
                    <li><a href="{{route('admin.withdraw.list',['status'=>'transferred'])}}"><i class="fa fa-circle-o"></i>Transferred</a></li>
                    <li><a href="{{route('admin.withdraw.list',['status'=>'cancelled'])}}"><i class="fa fa-circle-o"></i>Cancelled</a></li>
                </ul>
            </li>
			<li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Navigations</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.menu.create')}}"><i class="fa fa-circle-o"></i>Menu</a></li>
				</ul>
            </li>
			<li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Earnings</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.commision.earnings')}}"><i class="fa fa-circle-o"></i>Commision Earnings</a></li>
				</ul>
            </li>
        </ul>
    </section>
</aside>
