@extends('common_files.layout.baselayout')
@section('main-title')
@yield('title',$sitesettings->site_name)
@stop
@section('head-style')
<link rel="stylesheet" href="{{asset('system/assets/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/css/font-awesome.min.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/css/ionicons.min.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/dist/css/AdminLTE.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/dist/css/skins/_all-skins.min.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/plugins/iCheck/flat/blue.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/plugins/morris/morris.css')}}"/>
@stop
@section('head-script')
<script type="text/javascript" src="{{asset('system/assets/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('system/assets/plugins/jQueryUI/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('system/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('system/assets/plugins/iCheck/icheck.min.js')}}"></script>
<!--[if lt IE 9]>
  <script src="{{asset('system/assets/js/html5shiv/3.7.3/html5shiv.min.js')}}"></script>
  <script src="{{asset('system/assets/js/respond/1.4.2/respond.min.js')}}"></script>
  <![endif]-->
@stop
@section('body')
<body class="hold-transition skin-blue sidebar-mini">
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>
    <div class="wrapper">
        @include('common_files.header')
        @include('common_files.left_nav')
        <div class="content-wrapper">
            <section class="content-header">
                <h1><i class="@yield('title-class','fa fa-files-o')"></i>@yield('title')</h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
                    @yield('breadcrumb')
                    @if (trim($__env->yieldContent('breadcrumb')))
                    <li class="active">@yield('title')</li>
                    @endif
                </ol>
            </section>
            <section class="content">
                @yield('content')
            </section>
        </div>
        @include('common_files.footer')
    </div>
    <script type="text/javascript" src="{{asset('system/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/assets/plugins/fastclick/fastclick.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/assets/plugins/validation/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/assets/plugins/validation/additional-methods.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/assets/dist/js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/assets/dist/js/demo.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/assets/support/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('system/assets/plugins/Jquery-loadSelect.js')}}"></script>
    <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
    </script>
    @yield('scripts')
</body>
@stop
