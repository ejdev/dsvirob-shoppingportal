<!DOCTYPE html>
<html lang="{{$sitesettings->lang}}">
	<base href="{{URL::to('/')}}/">
    <head>
        <title>@yield('main-title',$sitesettings->site_name)</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="application-name" content="{{$sitesettings->site_meta_title}}" />
        <meta name="keywords" content="{{$sitesettings->site_meta_keyword}}"/>
        <meta name="description" content="{{$sitesettings->site_meta_description}}" />
        <meta name="Robots" Content="Index, follow">
        <link rel="alternate" href="{{ URL::to('/')}}" hreflang="en-gb"/>
        <link rel="canonical" href="{{ URL::to('/')}}/" />
        <link rel="manifest" href="{{URL::asset('manifest.json')}}"/>
        <meta name="author" content="{{$sitesettings->site_name}}">
        <meta name="csrf-token" content="{{csrf_token()}}" />
        <meta name="theme-color" content="#00ff00">
        @if (trim($__env->yieldContent('social')))
        @yield('social')
        @else
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="{{$sitesettings->site_url}}" />
        <meta name="twitter:creator" content="{{$sitesettings->site_name}}"/>
        <meta property="fb:app_id" content="444676755742829" />
        <meta property="og:site_name" content="{{$sitesettings->site_name}}"/>
        <meta property="og:url" content="{{URL::to('/')}}"/>
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="{{$sitesettings->site_meta_title}}"/>
        <meta property="og:description" content="{{$sitesettings->site_meta_description}}"/>
        <meta property="og:image" content="{{URL::asset($sitesettings->site_logo)}}"/>
        @endif
        <link rel="shortcut icon" type="image/x-icon"  href="{{isset($sitesettings->fav_icon)? URL::asset($sitesettings->fav_icon):''}}"/>
        @yield('head-style')
        @yield('head-script')
    </head>
    @yield('body')
</html>
