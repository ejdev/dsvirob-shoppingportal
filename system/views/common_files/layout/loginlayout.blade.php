@extends('common_files.layout.baselayout')
@section('main-title')
@yield('title',$sitesettings->site_name)
@stop
@section('head-style')
<link rel="stylesheet" href="{{asset('system/assets/css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/css/font-awesome.min.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/css/ionicons.min.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/dist/css/AdminLTE.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/dist/css/skins/_all-skins.min.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/plugins/iCheck/flat/blue.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/plugins/notifIt/notifIt.css')}}"/>
<link rel="stylesheet" href="{{asset('system/assets/plugins/morris/morris.css')}}"/>
@stop
@section('head-script')
<script type="text/javascript" src="{{asset('system/assets/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('system/assets/plugins/jQueryUI/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('system/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('system/assets/plugins/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{asset('system/assets/plugins/notifIt/notifIt.min.js')}}"></script>
<script type="text/javascript" src="{{asset('system/assets/support/app.js')}}"></script>
<!--[if lt IE 9]>
  <script src="{{asset('system/assets/js/html5shiv/3.7.3/html5shiv.min.js')}}"></script>
  <script src="{{asset('system/assets/js/respond/1.4.2/respond.min.js')}}"></script>
  <![endif]-->
@stop
@section('body')
<body class="hold-transition login-page">
    @yield('content')
    <script>
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
    </script>
    @yield('scripts')
</body>
@stop
