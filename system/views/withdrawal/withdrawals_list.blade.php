@extends('common_files.layout.layout')
@section('title',$title)
@section('breadcrumb')
<li>Withdrawals</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary" id='withdraw_block'>
            <div class="box-header with-border">
                <form class="form form-bordered" id="withdraw-list-form" action="{{route('admin.withdraw.list-data')}}">
                    <input type="hidden" class="ignore-reset" name="status" id="status" value="{{$status}}"/>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="search_term">Search Term</label>
                            <input type="text" class="form-control" name="search_term" id="search_term" placeholder="Search Term" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <div>
                                <button type="button" id="search-btn" class="btn btn-sm bg-olive"><i class="fa fa-search"></i> Search</button>
                                <button type="button" id="reset-btn" class="btn btn-sm bg-orange"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <table id="withdraw-list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Requested On</th>
                            <th>Requested By</th>
                            <th>Transaction ID</th>
                            <th>Requested Amount</th>
                            <th>Charges</th>
                            <th>Net Amount</th>
                            <th>Payment Mode</th>
                            @if ($status==Config::get('constants.WITHDRAWAL_STATUS.CONFIRMED'))
                            <th>{{Lang::get('general.transferred_on')}}</th>
                            @elseif ($status==Config::get('constants.WITHDRAWAL_STATUS.CANCELLED'))
                            <th>Cancelled On</th>
                            @else
                            <th>{{Lang::get('general.edoc')}}</th>
                            @endif
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box box-primary" id="withdrawal_details" style="display:none;">
            <div class="box-header with-border">
                <div class="pull-right">
                    <div class="options">

                    </div>
                    <a class="btn btn-xs btn-danger close-withdrawal_details"><i class="fa fa-times"></i></a>
                </div>

                <h4 class="box-title col-sm-6"></h4>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label class="form-label col-sm-4">Requested On</label>
                    <div class="col-sm-8">
                        <p class="form-control-static" id="created_on"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label col-sm-4">Payment Type</label>
                    <div class="col-sm-8">
                        <p class="form-control-static" id="payment_type"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label col-sm-4">Status</label>
                    <div class="col-sm-8">
                        <p class="form-control-static" id="status"></p>
                    </div>
                </div>
                <table class="table table-bordered table-stripped" id="conversion_details">
                    <thead>
                        <tr><th colspan="5">Related Transactions</th></tr>
                        <tr>
                            <th>Wallet</th>
                            <th>From Amount</th>
                            <th>To Amount</th>
                            <th>Debit Transaction ID</th>
                            <th>Credit Transaction ID</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

                <table class="table table-bordered table-stripped" id="account_info">
                    <thead><tr><th colspan="2">Account Information</th></tr></thead>
                    <tbody>

                    </tbody>
                </table>
                <div class="form-group">
                    <label class="form-label col-sm-4">Amount</label>
                    <div class="col-sm-8">
                        <p class="form-control-static" id="amount"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label col-sm-4">Charges</label>
                    <div class="col-sm-8">
                        <p class="form-control-static" id="handleamt"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label col-sm-4">Net Amount</label>
                    <div class="col-sm-8">
                        <p class="form-control-static" id="paidamt"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
@include('common_files.datatable_js')
<script src="{{asset('system/assets/support/withdraw/withdraw_list.js')}}"></script>
@stop
