@extends('common_files.layout.layout')
@section('title','Transfer History')
@section('breadcrumb')
<li>Wallet</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary" id='withdraw_block'>
            <div class="box-header with-border">
                <form class="form form-bordered" id="withdraw-list-form" action="{{route('admin.withdraw.list-data')}}">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="search_term">Search Term</label>
                            <input type="text" class="form-control" name="search_term" id="search_term" placeholder="Search Term" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <div>
                                <button type="button" id="search-btn" class="btn btn-sm bg-olive"><i class="fa fa-search"></i> Search</button>
                                <button type="button" id="reset-btn" class="btn btn-sm bg-orange"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <table id="withdraw-list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Requested On</th>
                            <th>Requested By</th>
                            <th>Transaction ID</th>
                            <th>Requested Amount</th>
                            <th>Charges</th>
                            <th>Net Amount</th>
                            <th>Payment Mode</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
@include('common_files.datatable_js')
<script src="{{asset('system/assets/support/wallet/transfer_history.js')}}"></script>
@stop
