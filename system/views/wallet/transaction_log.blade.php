@extends('common_files.layout.layout')
@section('title','Transaction Log')
@section('breadcrumb')
<li>Wallet</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <form class="form form-bordered" id="transaction-form" action="{{route('admin.wallet.transaction-log-data')}}">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="search_term">Search Term</label>
                            <input type="text" class="form-control" name="search_term" id="search_term" placeholder="Search Term" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <div>
                                <button type="button" id="search-btn" class="btn btn-sm bg-olive"><i class="fa fa-search"></i> Search</button>
                                <button type="button" id="reset-btn" class="btn btn-sm bg-orange"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <table id="transaction-list" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Created On</th>
                            <th>User</th>
                            <th>From/To User Name</th>
                            <th>Description</th>
                            <th>Wallet</th>
                            <th>Amount</th>
                            <th>Paid Amount</th>
                            <th>Charges</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
@include('common_files.datatable_js')
<script src="{{asset('system/assets/support/wallet/transaction_log.js')}}"></script>
@stop
