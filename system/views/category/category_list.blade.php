@extends('common_files.layout.layout')
@section('title','Category')
@section('breadcrumb')
<li>Category</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <form class="form form-bordered" id="category-items-list-form" action="{{route('admin.product-category.list-data')}}">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="search_term">Search Term</label>
                            <input type="text" class="form-control" name="search_term" id="search_term" placeholder="Search Term" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <div>
                                <button type="button" id="search-btn" class="btn btn-sm bg-olive"><i class="fa fa-search"></i> Search</button>
                                <button type="button" id="reset-btn" class="btn btn-sm bg-orange"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <table id="category-items-list-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Url str</th>
                            <th>category code</th>
                            <!-- <th>Suborder Code</th>
                            <th>Supplier</th>
                            <th>Supplier Name</th>
                            <th>Qty</th>
                            <th>Amount</th>
                            <th>Tax</th>
                            <th>Net Pay</th>
                            <th>Payment Type</th>
                            <th>Approval Status</th> 
                            <th>Status</th>
                            <th></th>-->
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
@include('common_files.datatable_js')
<script src="{{asset('system/assets/support/category/category_list.js')}}"></script>
@stop
