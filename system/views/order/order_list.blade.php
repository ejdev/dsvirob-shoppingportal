@extends('common_files.layout.layout')
@section('title',$title)
@section('breadcrumb')
<li>Orders</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary" id="o_list">
            <div class="box-header with-border">
                <form class="form form-bordered" id="orders-list-form" action="{{route('admin.order.list-data')}}">
                    <input type="hidden" class="ignore-reset" name="status" id="status" value="{{$status}}"/>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="search_term">Search Term</label>
                            <input type="text" class="form-control" name="search_term" id="search_term" placeholder="Search Term" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <div>
                                <button type="button" id="search-btn" class="btn btn-sm bg-olive"><i class="fa fa-search"></i> Search</button>
                                <button type="button" id="reset-btn" class="btn btn-sm bg-orange"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <table id="orderlist" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Ordered On</th>
							<th>Order Code</th>
                            <th>Buyer</th>                            
                            <th>Qty</th>                           							
                            <th>Net Pay</th>
                            <th>Pay Mode</th>
                            <th>Pay Status</th>
                            <th>Order Status</th>
							<th>Approval Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>							
            </div>
        </div>
		
		<div id="o_details" class="hidden"></div>
    </div>
</div>

<div class="modal fade" id="view_order_details" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog " style="width: 900px;">
		<div class="modal-content ">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Order Details</h4>
			</div>
			<div class="modal-body">
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>	


@stop
@section('scripts')
@include('common_files.datatable_js')
<script src="{{asset('system/assets/support/order/order_list.js')}}"></script>
@stop
