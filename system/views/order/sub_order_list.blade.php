@extends('common_files.layout.layout')
@section('title',$title)
@section('breadcrumb')
<li>Supplier Orders</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <form class="form form-bordered" id="orders-list-form" action="{{route('admin.sub-order.list-data')}}">
                    <input type="hidden" class="ignore-reset" name="status" id="status" value="{{$status}}"/>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label" for="search_term">Search Term</label>
                            <input type="text" class="form-control" name="search_term" id="search_term" placeholder="Search Term" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <div>
                                <button type="button" id="search-btn" class="btn btn-sm bg-olive"><i class="fa fa-search"></i> Search</button>
                                <button type="button" id="reset-btn" class="btn btn-sm bg-orange"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-body">
                <table id="kyclist" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Ordered On</th>                            
                            <th class="no-wrap">Order Code</th>
                            <th class="no-wrap">Sub Order Code</th>
							<th class="no-wrap">Buyer</th>
                            <th>Supplier</th>                            
                            <th>Qty</th>
                            <th>Net Pay</th>  
							<th>Pay Mode</th>                            
							<th>Pay Status</th>
							<th>Order Status</th>
                            <th>Approval Status</th>                            
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
@include('common_files.datatable_js')
<script src="{{asset('system/assets/support/order/sub_order_list.js')}}"></script>
@stop
