@extends('common_files.layout.layout')
@section('title',$sub_order_code)
@section('breadcrumb')
<li>Supplier Orders</li>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{$sub_order_code}}</h3>
            </div>
            <div class="box-body">
                @if (isset($details) && !empty($details))
                <table class="table table-borderless">
                    <tr>
                        <td>Customer</td><td>{{$details->customer_name}}</td>
                        <td>Supplier</td><td>{{$details->supplier_name}} ({{$details->supplier_code}})</td>
                    </tr>
                    <tr><td>Order Date</td><td>{{$details->Fordered_on}}</td><td>Qty</td><td>{{$details->Fqty}}</td></tr>
                    <tr><td>Amount</td><td>{{$details->Famount}}</td><td>Tax</td><td>{{$details->Ftax}}</td></tr>
                    <tr><td>Net Pay</td><td>{{$details->Fnet_pay}}</td><td>Payment Status</td><td>{{$details->payment_status}}</td></tr>
                    <tr><td>Status</td><td>{{$details->sub_order_status}}</td><td>Approval Status</td><td>{{$details->approval_status}}</td></tr>
                </table>
                <h2>Items</h2>
                @foreach($details->items as $item)
                <form class="form form-bordered" id="order-items-list-form" action="{{route('admin.order-items.list-data')}}">
                    <input type="hidden" name="order_code" id="order_code" value="{{$details->order_code}}"/>
                    <input type="hidden" name="sub_order_code" id="sub_order_code" value="{{$details->sub_order_code}}"/>
                </form>
                <table id="order-items-list-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Qty</th>
                            <th>Amount</th>
                            <th>Tax</th>
                            <th>Net Pay</th>
                            <th>Payment Type</th>
                            <th>Approval Status</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
@include('common_files.datatable_js')
<script src="{{asset('system/assets/support/order/sub_order_details.js')}}"></script>
@stop
