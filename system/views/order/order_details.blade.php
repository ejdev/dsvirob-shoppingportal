
		<div class="modal-content ">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">ORDER DETAILS</h4>
			</div>
			<section class="content invoice">                    
					<!-- title row -->
					<div class="row">
						<div class="col-xs-12">
							<h2 class="page-header">
								<i class="fa fa-globe"></i> Invoice #007612
								<small class="pull-right"><b>Date :</b> {{$details->Fordered_on}}</small>
							</h2>                            
						</div><!-- /.col -->
					</div>
					<!-- info row -->
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							
							<b>Order Code :</b> {{$details->order_code}}<br>
							<b>Customer Name :</b> {{$details->customer_name}}<br>
							<b>Payment Mode :</b> <span class="">{{$details->payment_type}}</span><br>
							<b>Payment Status :</b> <span class="{{$details->payment_status_class}}">{{$details->payment_status}}</span><br>
							<b>Order Status :</b> <span class="{{$details->order_status_class}}">{{$details->order_status}}</span><br>
							
						</div>
						<div class="col-sm-6 invoice-col">
							<strong>Shipping Address :</strong>
							<address>
								{{$details->Shipping[0]->full_name}}<br>
								{{$details->Shipping[0]->address1}}<br>
								{{isset($details->Shipping[0]->address2) ? $details->Shipping[0]->address2 : ''}}<br>
								{{$details->Shipping[0]->city}}, {{$details->Shipping[0]->state_id}}<br>
								
								{{$details->Shipping[0]->country_id}} - {{$details->Shipping[0]->postal_code}}<br>
								<b>Phone :</b> {{$details->Shipping[0]->mobile_no}}<br>
								<b>Email :</b> {{$details->Shipping[0]->email_id}}
							</address>
						</div>
						<!--div class="col-sm-4 invoice-col">
							To
							<address>
								<strong>John Doe</strong><br>
								795 Folsom Ave, Suite 600<br>
								San Francisco, CA 94107<br>
								Phone: (555) 539-1037<br>
								Email: john.doe@example.com
							</address>
						</div-->
						
					</div><!-- /.row -->

					<!-- Table row -->
					<div class="row">
						<div class="col-xs-12 table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>Image</th>
										<th>Product</th>
										<th><span class="pull-right">Unit Price</span></th>
										<th><span class="pull-right">Qty</span></th>
										<th><span class="pull-right">Sub Total</span></th>
									</tr>                                    
								</thead>
								<tbody>
									@if($details->product_particulars)
										@foreach ($details->product_particulars as $products)
											<tr>
												<td><img src="{{$products->imgs}}" width="45" height="45"></td>
												<td>{{$products->product_name}} <br><h6>(<b>Product Code :</b> {{$products->product_code}})</h6></td>
												<td><span class="pull-right">{{number_format($products->unit_price, 2, '.', ',')}} {{$details->currency}}</span></td>
												<td><span class="pull-right">{{$products->qty}}</span></td>
												<td><span class="pull-right">{{number_format($products->sub_total, 2, '.', ',')}} {{$details->currency}}</span></td>
											</tr>
										@endforeach
									@endif
									
								</tbody>
							</table>                            
						</div><!-- /.col -->
					</div><!-- /.row -->

					<div class="row">
						<!-- accepted payments column -->
						<div class="col-xs-6">
							
						</div><!-- /.col -->
						<div class="col-xs-6">				
							<div class="table-responsive">
								<table class="table">
									<tbody>
									<tr>
										<th style="width:50%">Subtotal :</th>
								<th><span class="pull-right"> {{number_format($details->amount, 2, '.', ',')}} {{$details->currency}}</span></th>
									</tr>
									<tr>
										<th>Tax Inclusive (14%) :</th>
								<th><span class="pull-right"> {{number_format($details->tax, 2, '.', ',')}} {{$details->currency}}</span></th>
									</tr>
									<tr>
										<th>Shipping Charge :</th>
								<th><span class="pull-right"> {{number_format($details->shipping_charges, 2, '.', ',')}} {{$details->currency}}</span></th>
									</tr>
									<tr>
										<th>Total :</th>
								<th><span class="pull-right"> {{number_format($details->net_pay, 2, '.', ',')}} {{$details->currency}}</span></th>
									</tr>
								</tbody></table>
							</div>
						</div><!-- /.col -->
					</div><!-- /.row -->

					<!-- this row will not appear when printing -->
					<div class="row no-print">
						<div class="col-xs-12">
							<button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>				
							<button class="btn btn-primary pull-right" onclick="window.print();" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
						</div>
					</div>
				</section>
		</div><!-- /.modal-content -->
<script>
$( document ).ready(function() {
    $( ".close" ).on( "click", function(e) {
        e.preventDefault();			
		$("#o_list").removeClass('hidden');				
		$("#o_details").html('');
		$("#o_details").addClass('hidden');				
    });
});
</script>	