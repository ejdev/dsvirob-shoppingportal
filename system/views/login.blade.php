@extends('common_files.layout.loginlayout')
@section('title',"Login")
@section('content')
<div class="login-box signin">
    <div class="login-logo">
        <a href="{{url('/')}}"><b>{{$sitesettings->site_name}}</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg"><b>Welcome!</b> Please Sign In.</p>
        <form id="loginfrm" method="POST" action="{{route('admin.loginCheck')}}">
            <div class="form-group has-feedback">
                <input type="email" name="uname" id="uname" class="form-control" placeholder="Enter Member ID/Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password"  class="form-control" placeholder="Enter Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" id="toploginBtn">Sign In</button>
                </div>
            </div>
        </form>
        <div class="login-box-footer">
            <a href="{{route('admin.forgotPassword')}}" class="btn-forgot">Forgot your password?</a>
        </div>
    </div>
</div>
<div class="login-box forgot"  style="display:none">
    <div class="login-logo">
        <a href="{{url('/')}}"><b>{{$sitesettings->site_name}}</b></a>
    </div>
    <div class="login-box-body">
        <h4 class="">Find your account</h4>
        <form id="forgotfrm"  method="POST" action="{{route('admin.forgotPassword')}}">
            {!! csrf_field() !!}
            <div class="form-group has-feedback">
                <input type="text" name="uname" id="uname"  class="form-control" placeholder="Enter your Member ID/Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4 col-xs-offset-8">
                    <button type="submit" class="btn btn-primary btn-block btn-flat"  name="topForgotBtn"  id="topForgotBtn">Search</button>
                </div>
            </div>
            <div class="login-box-footer">
                <a href="{{route('admin.forgotPassword')}}" class="backtoLogin">Go to Sign In</a>
            </div>
        </form>
    </div>
</div>
@stop
@section('scripts')
<script type="text/javascript" src="{{asset('system/assets/support/login.js')}}"></script>
@stop
