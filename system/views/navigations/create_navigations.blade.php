@extends('common_files.layout.layout')
@section('title',$title)
@section('breadcrumb')
<li>Orders</li>
@stop
@section('content')

<div class="row">
	<div class="col-md-12">
		<!-- Custom Tabs (Pulled to the right) -->
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs pull-right">
				<li><a id="menu_management_btn" href="#manage_menu" data-toggle="tab">Menu Locations</a></li>
				<li class="active"><a id="create_menu_nav_link" href="#create" data-toggle="tab">Create Menu</a></li>				
				<li class="pull-left header"><i class="fa fa-th"></i> Menu</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane" id="manage_menu">
					<form class="form-horizontal" id="menu_form" method="post" >
						<div class="box box-solid box-primary" id="menu_management">
							<div class="box-header">
								<h3 class="box-title">Menu Management</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
									<button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
								</div>
							</div>            
							
							<div class="box-body">
								<div class="form-group">
									<label class="control-label col-md-4" >Header Primary Menu:</label>
									<div class="col-md-4">
										<select name="nav[top_primary]" id="top_primary" class="form-control top_menu_id" class="form-control"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Header catalogue Menu:</label>
									<div class="col-md-4">
										<select name="nav[top_catlog]" id="top_catlog" class="form-control top_menu_id" class="form-control"></select>
									</div>
								</div>
							    <div class="form-group">
									<label class="control-label col-md-4">Footer Primary Menu:</label>
									<div class="col-md-4">
										<select name="nav[footer_primary]" id="footer_primary" class="form-control top_menu_id" class="form-control"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Footer catalogue Menu:</label>
									<div class="col-md-4">
										<select name="nav[footer_secondary]" id="footer_secondary" class="form-control top_menu_id" class="form-control"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Footer Catlog Menu:</label>
									<div class="col-md-4">
										<select name="nav[footer_catlog]" id="footer_catlog" class="form-control top_menu_id" class="form-control"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Footer Support Menu:</label>
									<div class="col-md-4">
										<select name="nav[footer_support]" id="footer_support" class="form-control top_menu_id" class="form-control"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-4" >Footer Account Menu:</label>
									<div class="col-md-4">
										<select name="nav[footer_account]" id="footer_account" class="form-control top_menu_id" class="form-control"></select>
									</div>
								</div>
							</div>
							<div class="panel-footer text-right">
								<button type="sumbit" class="btn btn-success">Save</button>
							</div>
						</div>
					</form>
				</div><!-- /.tab-pane -->
				
				<div class="tab-pane active" id="create">
					<form class="form-horizontal" id="create_menu_nav" method="post" >
						<div class="box box-solid box-primary">
							<div class="box-header">
								<h3 class="box-title">Create Menu</h3>
								<div class="box-tools pull-right">
									<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
									<button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
								</div>
							</div>            
							
							<div class="box-body">								
									<br>
									<div class="col-sm-3">
										<div class="form-group">											
											<select name="sel_menu" id="sel_menu" class="form-control ignore-reset">    
												<option value="">Select</option>                                                               
												@if (!empty($menu))                                
													@foreach ($menu as $men)
														<option value="{{$men->menu_name}}" id="{{$men->menu_id}}" data-menu_postion_id="{{$men->menu_postion_id}}"> {{$men->menu_name}} </option>
													@endforeach
												@endif
											</select>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">											
											<button id="select" type="submit" class="btn btn-primary ">Select</button>
										</div>
									</div>
									<div class="col-sm-1">
										<div class="form-group ">											
											<div style="margin-top:5px ! important;">
												<span >(or)</span>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">											
											<div style="margin-top:5px ! important;">
												<a id="create_new" class="create_new">Create new menu</a>
											</div>
										</div>
									</div>									
							</div>
						</div>
						<div class="errMsg"></div>
						
						<div class="box box-solid box-primary" >
							<div class="box-header">
								<h3 class="box-title" style="display:block;">
								
										<div id="create_menu_heading" style="display:none">
											<div class="form-group" id="site" style="margin:0">
												<div class="pull-right text-right">
													<button id="creatementBtn" type="submit" class="createMenu btn btn-default">Create Menu</button>
												</div>
												<label class="control-label col-sm-2">Menu Label<span class="error">*</span></label>
												<div class="col-sm-4">
													<input type="text" placeholder="" class="form-control" name="menu_name" id="menu_name" value=""/>
													<input type="hidden" placeholder="" class="form-control" name="menu_id" id="menu_id" value=""/>
												</div>
												<label class="control-label col-sm-2">Menu Position<span class="error">*</span></label>
												<div class="col-sm-2">
													<select name="" id="menu_position" class="form-control"></select>
												</div>
											</div>
										</div>
										<div id="selected_menu_details"></div>
									   
										<div id="menu_list">
											<h3 class="box-title">
												Menu List
											</h3>                                
										</div>
								
								</h3>
								<!--div class="box-tools pull-right">
									<button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
									<button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
								</div-->
							</div>       
							
							<div class="box-body">
								
								<div class="panel_controls">
									
									<div class="col-sm-3">
										<div class="form-group">
											<label class="control-label" for="search_term">Search Term</label>
											<input type="text" name="search_term" placeholder="Search terms" id="search_term" class="form-control">
										</div>
									</div>
									
									<div class="col-sm-3">
										<div class="form-group">
											<label class="control-label" for="search_term">Menu </label>
											<select name="" id="top_menu" class="form-control top_menu_id" class="form-control"></select>
										</div>
									</div>
									
									<div class="col-sm-3">
										<div class="form-group">
											<label class="control-label">&nbsp;</label>
											<div>                                
												<button id="search" type="button" class="btn btn-sm bg-olive">Search</button>
											</div>
										</div>
									</div>
									
								</div>
								
								<div id="list">
									<table id="menu_nav_list" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>Created On</th>
												<th>Navigation </th>
												<th>Top Menu</th>
												<th>Type</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
								
								<div id="create_navigation" style="display:none">
									<div id="navigation_id"></div>
									<div class="form-group">
										<label class="control-label col-md-2" for="category_id">Type:<span class="error">*</span></label>
										<div class="col-md-8">
											<select name="nav[type]" id="type" class="form-control">
												 <option  value="">Select</option>
												<option id="top_menu" value="1">Top Menu</option>
												<option id="grp" value="2">Parent Link</option>
												<option id="norm" value="3">Link</option>
											</select>
										</div>
									</div>
									<div id="normal" style="display:none">
										<input type="hidden" name="nav[nav_settings]" id="nav_settings" value=""/>
										<div class="form-group">
											<label class="control-label col-md-2" for="category_id">Choose Top Menu:<span class="error">*</span></label>
											<div class="col-md-8">
												<select name="nav[parent_navigation_id]" id="parent_navigation_id" class="form-control top_menu_id" class="form-control"></select>
											</div>
										</div>
										<div class="form-group" id="group_select">
											<label class="control-label col-md-2" for="category_id">Choose Group:<span class="error">*</span></label>
											<div class="col-md-8">
												<select name="nav[group_link]" id="group_id" class="form-control"></select>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-2" for="category_id">Link Type:<span class="error">*</span></label>
											<div class="col-md-8">
												<select name="nav[link_type]" id="link_type" class="form-control">
													<option value="">Select</option>
													<option value="1">Product</option>
													<option value="2">Category</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<div class="col-md-3"></div>
											<div class="col-md-8">
												<a href="javascript:void(0)" class="text-center" id="edit_cat" style="">Add filters</a>
											</div>
										</div>										
									</div>
									<div id="no_of_columns">
										<div class="form-group">
											<label class="control-label col-md-2" for="supplier_id">No Of Columns<span class="error">*</span></label>
											<div class="col-md-8">
												<input type="number"  name="nav[no_of_columns]" id="no_of_columns" class="form-control"/>
											</div>
										</div> 
									</div>
									<div id="choose_columns" style="display:none">
										<div class="form-group">
											<label class="control-label col-md-2" for="supplier_id">Choose the Column <span class="error">*</span></label>
											<div class="col-md-8">
												<select   name="nav[column_no]" id="column" class="form-control"/>
											</select>
											</div>
										</div> 
									</div> 
									<div class="form-group">
										<label class="control-label col-md-2" for="supplier_id">Login Required:<span class="error">*</span></label>
										<div class="col-md-8">
											
											<div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="nav[is_login_required]" value="1" id="is_login_required" class="form-control"/>
                                                </label>                                                
                                            </div>
										</div>
									</div> 
									 <div class="form-group">
										<label class="control-label col-md-2" for="supplier_id">Navigation:<span class="error">*</span></label>
										<div class="col-md-8">
											<input type="text"  name="nav[navigation_name]" id="navigation_name" class="form-control"/>
										</div>
									</div> 
									<div class="form-group">
										<label class="control-label col-md-2" for="supplier_id">Navigation URL:<span class="error">*</span></label>
										<div class="col-md-8">
											<input type="text"  name="nav[navigation_url]" id="navigation_url" class="form-control"/>
										</div>
									</div>
									 <div class="form-group">
									   <div class="col-md-4"></div>
									   <div class="col-md-8">
											<button type="sumbit" class="btn btn-sm btn-success"/>Save</button>
									   </div> 
									</div>  
								</div>								
							</div>
						</div>
					</form>
				</div><!-- /.tab-pane -->
			</div><!-- /.tab-content -->
		</div><!-- nav-tabs-custom -->
	</div><!-- /.col -->
</div>




@stop
@section('scripts')
@include('common_files.datatable_js')
<script src="{{asset('system/assets/support/navigation/create_navigation.js')}}"></script>
@stop
